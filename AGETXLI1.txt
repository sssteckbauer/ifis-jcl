//GETXLI1     JOB  (SYS000),'PROD CNTL- 0903',                          00001000
//            REGION=32M,                                               00001101
//            CLASS=0,                                                  00001200
//            MSGLEVEL=(1,1),                                           00001300
//            MSGCLASS=F,                                               00002000
//            NOTIFY=&SYSUID                                            00002101
//*======================                                               00002200
//* ZEKE EVENT # 3198                                                   00002200
//*======================                                               00002200
// JCLLIB ORDER=SYS3.PROCLIB                                            00002200
//*====================================================
//* CREATE NEW DATASET FOR LIBRARY CHECKWRITE PROCESS
//*====================================================
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//DD1       DD DSN=FISP.XLI1.APCHECKS.INPUT,
//             DISP=(,CATLG),
//             DCB=(RECFM=FB,DSORG=PS,LRECL=206,BLKSIZE=4120),
//             SPACE=(TRK,(45,10),RLSE),
//             UNIT=SYSDA,
//             VOL=SER=APC002
//*
//* ================================================================ */ 00002200
//* CPFILE:  COPY and convert and ASCII based file into an MVS       */
//*          dataset sent to sftp.                                   */
//*                                                                  */
//*   PARMS:                                                         */
//*          ID=   The mainframe userid of the sender.               */
//*                                                                  */
//*                NOTE: ID  must be in quotes and lower case        */
//*                                                                  */
//*          FILE= The source data file in /ftp/userid/in/ directory */
//*                The name has be agreed upon between you and user  */
//*                                                                  */
//*                NOTE: FILE must be in quotes (imbedded periods)   */
//*                                                                  */
//*          DSN=  The MVS target output dataset. This must be       */
//*                preallocated by another job or a previous step.   */
//*                                                                  */
//*                NOTE: DSN must be quotes (imbedded periods)       */
//*                                                                  */
//* ---------------------------------------------------------------- */ 00002200
//GET         EXEC CPFILE,ID='libzzz',
//            FILE='FISP.XLI1.APCHECKS.INPUT',
//            DSN='FISP.XLI1.APCHECKS.INPUT'
//*
//* ================================================================*/
//*                                                                 */
//* Delete files from /ftp/libzzz/in on USS                         */
//*                                                                 */
//* ----------------------------------------------------------------*/
//COMMAND     EXEC PGM=BPXBATCH,
//            REGION=0M,
//            TIME=NOLIMIT,
//* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
//* PARM='SH ls -al /ftp/libzzz/in'
//  PARM='SH rm /ftp/libzzz/in/FISP.XLI1.APCHECKS.INPUT'
//* ===============================================================  */
//* Route STDOUT and STDERR output to a /tmp file. BPXBATCH can't    */
//* print directly to JES2, so you drop them in a directory/file     */
//* Change the name from msgs.txt to something meaningful if you     */
//* prefer.                                                          */
//* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
//STDOUT      DD   PATH='/tmp/msgs.txt',
//            PATHDISP=(KEEP,KEEP),
//            PATHOPTS=(OWRONLY,OCREAT,OTRUNC),
//            PATHMODE=(SIRWXU,SIRWXG,SIRWXO)
//STDERR      DD   PATH='/tmp/msgs.txt',
//            PATHDISP=(KEEP,KEEP),
//            PATHOPTS=(OWRONLY,OCREAT,OTRUNC),
//            PATHMODE=(SIRWXU,SIRWXG,SIRWXO)
//SYSPRINT    DD   SYSOUT=*
//SYSOUT      DD   SYSOUT=*
//* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
//* Using IEBGENER, copy the run messages to JES2 so you can see if  */
//* the job ran clean...(or not).                                    */
//* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  */
//RUNMSGS     EXEC PGM=IEBGENER
//SYSPRINT    DD   SYSOUT=*
//SYSIN       DD   DUMMY
//SYSUT2      DD   SYSOUT=*,
//            DCB=(LRECL=255,BLKSIZE=4096,RECFM=VB)
//SYSUT1      DD   FILEDATA=TEXT,
//            PATHDISP=(DELETE,KEEP),
//            PATHOPTS=(ORDONLY),
//            PATHMODE=(SIRWXU,SIRWXG,SIROTH),
//            LRECL=255,BLKSIZE=4096,RECFM=VB,
//            PATH='/tmp/msgs.txt'
//*******END OF JCL  G E T O R M A X *********************************
