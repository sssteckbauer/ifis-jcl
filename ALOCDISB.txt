//ALOCHDSB JOB SYS000,'BEV-0903',MSGLEVEL=(1,1),                        00010000
// REGION=4096K,NOTIFY=APCDBM,CLASS=K,MSGCLASS=F                        00021003
//*++++++++++++++++++++++++++                                           00030000
//*    ZEKE EVENT # 567                                                 00040000
//*++++++++++++++++++++++++++                                           00050000
//*====================================================                 00051000
//* DELETE EXISTING DATASET                                             00052000
//*====================================================                 00053000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00054000
//DD1       DD DSN=FISP.JVDATA.HOSPDISB.D$BJDT,                         00054100
//             DISP=(MOD,DELETE,DELETE),                                00056000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00056100
//             SPACE=(TRK,(1,1),RLSE),                                  00058000
//             UNIT=SYSDA,                                              00059000
//             VOL=SER=APC002                                           00059100
//*====================================================                 00060000
//* CREATE NEW DATASET FOR HOSPDISB                                     00070005
//*====================================================                 00080000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00090000
//DD1       DD DSN=FISP.JVDATA.HOSPDISB.D$BJDT,                         00100000
//             DISP=(,CATLG),                                           00110000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00120000
//             SPACE=(TRK,(15,5),RLSE),                                 00130000
//             UNIT=SYSDA,                                              00140000
//             VOL=SER=APC002                                           00150000
//*====================================================                 00160000
//*============================================================         00170000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00180000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00190000
//*============================================================         00200000
//STOPZEKE EXEC STOPZEKE                                                00210000
//*                                                                     00220000
//*================ E N D  O F  J C L  ALOCDISB ========                00230000
