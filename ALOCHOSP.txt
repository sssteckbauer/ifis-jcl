//ALOCHOSP JOB SYS000,'BEV-0903',MSGLEVEL=(1,1),                        00010000
// REGION=4096K,NOTIFY=APCDBM,CLASS=K,MSGCLASS=F                        00021003
//*++++++++++++++++++++++++++                                           00030000
//*    ZEKE EVENT # 2960                                                00040000
//*++++++++++++++++++++++++++                                           00050000
//*====================================================                 00051000
//* DELETE EXISTING DATASET                                             00052000
//*====================================================                 00053000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00054000
//DD1       DD DSN=FISP.JVDATA.HOSPPRCD.D$BJDT,                         00054100
//             DISP=(MOD,DELETE,DELETE),                                00056000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00056100
//             SPACE=(TRK,(1,1),RLSE),                                  00058000
//             UNIT=SYSDA,                                              00059000
//             VOL=SER=APC002                                           00059100
//*====================================================                 00060000
//* CREATE NEW DATASET FOR HOSPITAL PROCARD                             00070000
//*====================================================                 00080000
//STEP2    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00090006
//DD1       DD DSN=FISP.JVDATA.HOSPPRCD.D$BJDT,                         00101000
//             DISP=(,CATLG),                                           00110000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00120000
//             SPACE=(TRK,(15,5),RLSE),                                 00130000
//             UNIT=SYSDA,                                              00140000
//             VOL=SER=APC002                                           00150000
//*============================================================         00170000
//* DELETE EXISTING .MSG DATASET                                        00171005
//*====================================================                 00172005
//STEP3    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00173005
//DD1       DD DSN=FISP.JVDATA.HOSPPRCD.D$BJDT.MSG,                     00174005
//             DISP=(MOD,DELETE,DELETE),                                00175005
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00176005
//             SPACE=(TRK,(1,1),RLSE),                                  00177005
//             UNIT=SYSDA,                                              00178005
//             VOL=SER=APC002                                           00179005
//*====================================================                 00179105
//* CREATE NEW DATASET .MSG FOR HOSPITAL PROCARD                        00179205
//*====================================================                 00179305
//STEP4    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00179405
//DD1       DD DSN=FISP.JVDATA.HOSPPRCD.D$BJDT.MSG,                     00179505
//             DISP=(,CATLG),                                           00179605
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00179705
//             SPACE=(TRK,(15,5),RLSE),                                 00179805
//             UNIT=SYSDA,                                              00179905
//             VOL=SER=APC002                                           00180005
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00181000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00190000
//*============================================================         00200000
//STOPZEKE EXEC STOPZEKE                                                00210000
//*                                                                     00220000
//*================ E N D  O F  J C L  ALOCHOSP ========                00230000
