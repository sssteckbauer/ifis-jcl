//ALOCHPV2 JOB SYS000,'PERLINA-0903',MSGLEVEL=(1,1),                    00010013
// REGION=4096K,NOTIFY=APCPSB,CLASS=K,MSGCLASS=F                        00021012
//*++++++++++++++++++++++++++                                           00030000
//*    ZEKE EVENT # 3178                                                00040014
//*++++++++++++++++++++++++++                                           00050008
//* THIS WILL CREATE THE ADDITIONAL DATASET NEEDED                      00050112
//* FOR HOSPREV (HOSPREV.02)LEDGER LOADS                                00050212
//*++++++++++++++++++++++++++++++++                                     00050405
//*====================================================                 00051000
//* DELETE EXISTING DATASETS                                            00052005
//*====================================================                 00053000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00054000
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.02.HOSPREV,                     00054212
//             DISP=(MOD,DELETE,DELETE),                                00056000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00056100
//             SPACE=(TRK,(1,1),RLSE),                                  00058000
//             UNIT=SYSDA,                                              00059000
//             VOL=SER=APC002                                           00059100
//*============================================================         00059215
//STEP2    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00059315
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.02.HOSPREV.MSG,                 00059415
//             DISP=(MOD,DELETE,DELETE),                                00059515
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00059615
//             SPACE=(TRK,(1,1),RLSE),                                  00059715
//             UNIT=SYSDA,                                              00059815
//             VOL=SER=APC002                                           00059915
//*============================================================         00060000
//* CREATE NEW DATASETS                                                 00070005
//*====================================================                 00080000
//STEP3    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00090015
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.02.HOSPREV,                     00100012
//             DISP=(,CATLG),                                           00110000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00120000
//             SPACE=(TRK,(15,5),RLSE),                                 00130000
//             UNIT=SYSDA,                                              00140000
//             VOL=SER=APC002                                           00150000
//*====================================================                 00151015
//STEP4    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00160015
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.02.HOSPREV.MSG,                 00161015
//             DISP=(,CATLG),                                           00162015
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00163015
//             SPACE=(TRK,(15,5),RLSE),                                 00164015
//             UNIT=SYSDA,                                              00165015
//             VOL=SER=APC002                                           00166015
//*============================================================         00170000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00180000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00190000
//*============================================================         00200000
//STOPZEKE EXEC STOPZEKE                                                00210000
//*                                                                     00220000
//*================ E N D  O F  J C L  ALOCHPV2 ========                00230013
