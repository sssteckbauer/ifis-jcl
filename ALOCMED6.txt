//ALOCMED6 JOB SYS000,'BOB-0903',MSGLEVEL=(1,1),                        00010002
// REGION=4096K,NOTIFY=APCDBM,CLASS=K,MSGCLASS=F                        00021004
//*++++++++++++++++++++++++++                                           00030000
//*    ZEKE EVENT # 721                                                 00040000
//*++++++++++++++++++++++++++                                           00050000
//*====================================================                 00051000
//* DELETE EXISTING DATASET                                             00052000
//*====================================================                 00053000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00054000
//DD1       DD DSN=FISP.MEDCTR.D$YYMM,                                  00054101
//             DISP=(MOD,DELETE,DELETE),                                00056000
//             DCB=(RECFM=FBA,DSORG=PS,LRECL=256,BLKSIZE=6144),         00056100
//             SPACE=(TRK,(1,1),RLSE),                                  00058000
//             UNIT=SYSDA,                                              00059000
//             VOL=SER=APC002                                           00059100
//*====================================================                 00060000
//* CREATE DATASET FOR MED CENTER                                       00070000
//* ORDER FILE: "FISP.MEDCTR.D$YYMM"                                    00070101
//* (AFTER MED CTR FTP'S FILES TO MAINFRAME, RUN MED060                 00071000
//*====================================================                 00080000
//STEP1    EXEC PGM=IEFBR14                                             00090000
//DD1       DD DSN=FISP.MEDCTR.D$YYMM,                                  00100001
//             DISP=(,CATLG),                                           00110000
//             DCB=(RECFM=FBA,DSORG=PS,LRECL=256,BLKSIZE=6144),         00120000
//             SPACE=(TRK,(1500,150),RLSE),                             00130000
//             UNIT=SYSALLDA                                            00140000
//*============================================================         00161000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00170000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00180000
//*============================================================         00190000
//STOPZEKE EXEC STOPZEKE                                                00200000
//*                                                                     00210000
//*================ E N D  O F  J C L  ALOCMED6 ========                00220000
