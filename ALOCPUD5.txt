//ALOCPUD5 JOB SYS000,'BEV-0903',MSGLEVEL=(1,1),                        00010000
// REGION=4096K,NOTIFY=APCDBM,CLASS=K,MSGCLASS=Z                        00021001
//*++++++++++++++++++++++++++                                           00030000
//*    ZEKE EVENT # 482                                                 00040000
//*                ** JOB BECAME INACTIVE AS OF 5/1/03 - PER KURT.      00041003
//*                   THEY WILL PRINT INTERNAL TO PPS  (BEV)            00042003
//*                                                                     00043003
//*++++++++++++++++++++++++++                                           00050000
//*====================================================                 00051000
//* DELETE EXISTING DATASET                                             00052000
//*====================================================                 00053000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00054000
//DD1       DD DSN=FISP.PUD050.PHYSICAL.PLANT.D$BDATE,                  00054100
//             DISP=(MOD,DELETE,DELETE),                                00056000
//             DCB=(RECFM=FBA,DSORG=PS,LRECL=133,BLKSIZE=1330),         00056100
//             SPACE=(TRK,(1,1),RLSE),                                  00058000
//             UNIT=SYSDA,                                              00059000
//             VOL=SER=APC002                                           00059100
//*====================================================                 00060000
//* CREATE DATASET FOR PHYSICAL PLANT SERVICES PURCHASE                 00070000
//* ORDER FILE: "FISP.PUD050.PHYSICAL.PLANT.D$BDATE"                    00070100
//* (AFTER PPS FTP'S FILES TO MAINFRAME, RUN PUD050                     00071000
//*====================================================                 00080000
//STEP1    EXEC PGM=IEFBR14                                             00090000
//DD1       DD DSN=FISP.PUD050.PHYSICAL.PLANT.D$BDATE,                  00100000
//             DISP=(,CATLG),                                           00110000
//             DCB=(RECFM=FBA,DSORG=PS,LRECL=133,BLKSIZE=1330),         00120000
//             SPACE=(TRK,(105,15),RLSE),                               00130000
//             UNIT=SYSDA,                                              00140000
//             VOL=SER=APC002                                           00150000
//*============================================================         00161000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00170000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00180000
//*============================================================         00190000
//STOPZEKE EXEC STOPZEKE                                                00200000
//*                                                                     00210000
//*================ E N D  O F  J C L  ALOCPUD5 ========                00220000
