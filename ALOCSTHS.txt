//ALOCSTHS JOB SYS000,'PRDCTL-0903',MSGLEVEL=(1,1),                     00010000
// REGION=4096K,NOTIFY=APCDBM,CLASS=K,MSGCLASS=F                        00021002
//*ROUTE PRINT RMT10                                                    00030000
//*++++++++++++++++++++++++++                                           00040000
//*    ZEKE EVENT # 182                                                 00050000
//*++++++++++++++++++++++++++                                           00060000
//*====================================================                 00061000
//* DELETE EXISTING DATASET                                             00062000
//*====================================================                 00063000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00064000
//DD1       DD DSN=FISP.STORHSAP.CHECKS.D$BJDT,                         00064100
//             DISP=(MOD,DELETE,DELETE),                                00066000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=206,BLKSIZE=4120),          00066100
//             SPACE=(TRK,(1,1),RLSE),                                  00068000
//             UNIT=SYSDA,                                              00069000
//             VOL=SER=APC002                                           00069100
//*====================================================                 00070000
//* CREATE NEW DATASET FOR STOREHOUSE AP CHECKWRITE                     00110000
//*=======================================                              00120000
//STEP1    EXEC PGM=IEFBR14                                             00130000
//DD1       DD DSN=FISP.STORHSAP.CHECKS.D$BJDT,                         00140000
//             MGMTCLAS=AS,DISP=(,CATLG),                               00150000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=206,BLKSIZE=4120),          00160000
//             SPACE=(TRK,(15,5),RLSE),                                 00170000
//             UNIT=SYSDA                                               00180000
//*====================================================                 00190000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00200000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00210000
//*============================================================         00220000
//STOPZEKE EXEC STOPZEKE                                                00230000
//*                                                                     00240000
//*================ E N D  O F  J C L  ALOCSTHS  =======                00250000
