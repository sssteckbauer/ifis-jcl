//ALOCXMCP JOB SYS000,'PRODCTR - 0903',                                 00010009
//    MSGLEVEL=(1,1),                                                   00020009
//    REGION=4096K,                                                     00021009
//    CLASS=K,                                                          00022009
//    MSGCLASS=F                                                        00023009
//*++++++++++++++++++++++++++                                           00030000
//*    ZEKE EVENT # 3678                                                00040010
//*++++++++++++++++++++++++++                                           00050006
//*==========================================================           00061004
//* CREATE NEW DATASET FOR CHECKWRITE PROCESS                           00070000
//*==========================================================           00081004
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00090000
//DD1       DD DSN=FISP.XMCP.APCHECKS.INPUT,                            00100009
//             DISP=(,CATLG),                                           00110000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=206,BLKSIZE=4120),          00120010
//             SPACE=(TRK,(15,5),RLSE),                                 00130000
//             UNIT=SYSDA,                                              00140000
//             VOL=SER=APC002                                           00150000
//*                                                                     00169202
//*==========================================================           00169304
//* CREATE NEW DATASET FOR CHECKWRITE PROCESS - COMMENT FILE            00169404
//*==========================================================           00169509
//STEP2    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00169604
//DD1       DD DSN=FISP.XMCP.APCHECKS.COMMENT,                          00169709
//             DISP=(,CATLG),                                           00169804
//             DCB=(RECFM=FB,DSORG=PS,LRECL=240,BLKSIZE=7200),          00169905
//             SPACE=(TRK,(15,5),RLSE),                                 00170004
//             UNIT=SYSDA,                                              00170104
//             VOL=SER=APC002                                           00170204
//*                                                                     00170304
//*============================================================         00171000
//STEP3    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00171109
//DD2       DD DSN=FISP.XMCP.APCHECKS.ADDRESS.INPUT,                    00171209
//             DISP=(,CATLG),                                           00171309
//             DCB=(RECFM=FB,DSORG=PS,LRECL=300,BLKSIZE=27900),         00171409
//             SPACE=(TRK,(15,5),RLSE),                                 00171509
//             UNIT=SYSDA,                                              00171609
//             VOL=SER=APC002                                           00171709
//*                                                                     00171809
//*==========================================================           00172009
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00180000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00190000
//*============================================================         00200000
//STOPZEKE EXEC STOPZEKE                                                00210000
//*                                                                     00220000
//*================ E N D  O F  J C L  ALOCXADC ========                00230001
