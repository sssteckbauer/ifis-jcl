//APBW190A JOB (SYS000),'DISBSMT-ZSUZSA',                               00010005
//             MSGCLASS=F,MSGLEVEL=(1,1),
//             CLASS=K,REGION=0M,
//*            RESTART=STEP05,
//*            RESTART=MAILIT,
//             NOTIFY=APCDBM                                            00020077
//*========================                                             00000700
//*     ZEKE EV # 1034          (CONTACT: ZSUZSA EX:40837)              00000700
//*========================                                             00000700
//* !! EVERY OTHER FRIDAY ** EXCEPT ** FIRST EXECUTION IN JANUARY
//*        THEN MUST RUN APBW190N !!
//*====================================================
//*  !!  VARIABLE $APBW190CDT IS SET BY SETDATFR (Z#771)
//*====================================================
//*============================================================
//*   INDEPENDENT CONTRACTORS REPORT
//*      FILE CREATED FOR DISBURSEMENTS TO EDIT
//*============================================================
//*
//OUT1 OUTPUT CLASS=G,FORMS=A002,DEST=LOCAL,GROUPID=DISBSMT
//OUT2 OUTPUT CLASS=X,FORMS=STD,DEST=U129,GROUPID=DISBSMT
//OUT3 OUTPUT CLASS=4,FORMS=A002,DEST=LOCAL,GROUPID=BACKUP1
//* (OUT3 CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION ONLY).
//*====================================================
//* OUT2 TURNED OFF PER BRANDI - 1/10/03 (DOUG)
//*====================================================                 00000700
//*        EXTRACT VENDOR IDS FROM FILE SENT TO EDD
//*====================================================                 00001100
//STEP01   EXEC PGM=SYNCSORT                                            00003600
//SYSOUT    DD SYSOUT=*                                                 00003700
//SORTIN    DD DSN=FISP.APBW190B.EDDEXT.FINAL.D$APBW190YDT,             00003900
//             DISP=SHR                                                 00004000
//SORTOUT   DD DSN=&&VNDRIDS,                                           00004100
//             DISP=(NEW,PASS,DELETE),                                  00004200
//             DCB=(RECFM=FB,BLKSIZE=27920,LRECL=80),                   00004300
//             SPACE=(TRK,(2,2)),                                       00004400
//             UNIT=SYSDA                                               00004500
//SYSPRINT  DD SYSOUT=*                                                 00003800
//SORTWK01  DD UNIT=SYSDA,SPACE=(TRK,20)                                00004100
//SYSIN     DD *                                                        00004600
           SORT FIELDS=COPY
           OMIT COND=(4,9,CH,EQ,C'956006144',OR,
                      4,7,CH,EQ,C'0000000')
           OUTFIL FILES=OUT,
                  OUTREC=(1:6X,4,9,65X)                                 00041100
/*                                                                      00004700
//*====================================================                 00000700
//*  COMBINE VENDOR IDS FROM LAST RUN TO YTD IDS AND SORT
//*  THIS SORTS ONLY &&VNDRIDS AND APPENDS TO END OF SORTOUT FILE
//*====================================================                 00001100
//SORT02   EXEC PGM=SORT
//SYSOUT    DD SYSOUT=*
//SORTIN    DD DSN=&&VNDRIDS,DISP=(OLD,DELETE)
//SORTOUT   DD DSN=FISP.VENDEXT.EDD.CY$APYEAR,
//             DISP=MOD
//SORTWK01  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SORTWK02  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SORTWK03  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SORTWK04  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SYSIN     DD *
   SORT FIELDS=(6,9,CH,A)
   SUM FIELDS=NONE
/*
//*
//*  DISPLAY VALUE $APYEAR (SHOULD EQUAL CURRENT YEAR VALUE)
//*====================================================                 00000700
//*  SORT VENDOR IDS
//*====================================================                 00001100
//SORT03   EXEC PGM=SORT
//SYSOUT    DD SYSOUT=*
//SORTIN    DD DSN=FISP.VENDEXT.EDD.CY$APYEAR,
//             DISP=SHR
//SORTOUT   DD DSN=FISP.VENDEXT.EDD.CY$APYEAR,
//             DISP=SHR
//SORTWK01  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SORTWK02  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SORTWK03  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SORTWK04  DD SPACE=(TRK,(300,150)),
//             UNIT=SYSDA
//SYSIN     DD *
   SORT FIELDS=(6,9,CH,A)
   SUM FIELDS=NONE
/*
//*====================================================                 00000300
//*  RUN IDCAMS PROGRAM         - DELETE OLD VSAM FILE OF IFIS        * 00001300
//*                               EXTERNAL REPORT CODES               * 00001400
//* VSAM KEY - FUND, ORG, START DATE, AND ACTI-CODE                   * 00001400
//*====================================================                 00001500
//IDCAM40  EXEC PGM=IDCAMS                                              00001600
//SYSPRINT  DD SYSOUT=*                                                 00001700
//SYSIN     DD *                                                        00001800
           DELETE -
                  FISP.EDD.VENDEXT.VSAM
           DEFINE -
           CLUSTER (NAME (FISP.EDD.VENDEXT.VSAM) -
                  FREESPACE (0 0) -
                  CISZ (4096) -
                  INDEXED -
                  REUSE -
                  KEYS (9,6) -
                  RECORDSIZE (80,80) -
                  VOLUME (APC002)) -
           DATA -
                  (NAME(FISP.EDD.VENDEXT.VSAM.DATA) -
                  TRACKS (10,2)) -
           INDEX -
                  (NAME(FISP.EDD.VENDEXT.VSAM.INDEX1))
/*                                                                      00001900
//*=====================================================================00002000
//*   REPOR THE SORTED EXTRACT FILE TO VSAM FILE                        00002100
//*=====================================================================00002200
/*                                                                      00002300
//REPRO50  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00002400
//SYSPRINT  DD SYSOUT=*                                                 00002500
//SYSIN     DD *                                                        00002600
     REPRO -
       INDATASET (FISP.VENDEXT.EDD.CY$APYEAR) -
       OUTDATASET (FISP.EDD.VENDEXT.VSAM) -
       REUSE;
/*                                                                      00002700
//*====================================================                 00002200
//*                                                                     00002800
//STEP04   EXEC PGM=UAPX190
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPX190) PLAN(UAPX190)
/*
//SYSOUT    DD SYSOUT=*                                                 00001800
//SYSLST    DD SYSOUT=*                                                 00001900
//SYSUDUMP  DD SYSOUT=D,                                                00002000
//             DEST=LOCAL                                               00002100
//SYSDBOUT  DD SYSOUT=*                                                 00002200
//CONTROLR  DD DSN=FISP.APBW190A.UAPX190.CR.D$APBW190CDT,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),
//             SPACE=(TRK,(2,2),RLSE),
//             UNIT=SYSDA
//VENDEXT   DD SYSOUT=*                                                 00002300
//ERREXT    DD DSN=FISP.APBW190A.UAPX190.ERREXT.D$APBW190CDT,           00002300
//            DISP=(NEW,CATLG,DELETE),                                  00002400
//            DCB=(RECFM=FB,LRECL=175),                                 00002500
//            SPACE=(TRK,(90,60),RLSE),                                 00002600
//            UNIT=SYSDA                                                00002700
//EDDEXT    DD DSN=FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT,           00002300
//            DISP=(NEW,CATLG,DELETE),                                  00002400
//            DCB=(RECFM=FB,LRECL=175),                                 00002500
//            SPACE=(TRK,(90,60),RLSE),                                 00002600
//            UNIT=SYSDA                                                00002700
//REPTVEN   DD DSN=FISP.EDD.VENDEXT.VSAM,
//             DISP=SHR,
//             AMP=('BUFNI=2')
//*************
//* COL 6      UNVRS-CODE
//* COL 12     CURRENT CALENDAR YEAR
//************
//DATEFILE  DD *                                                        00002900
     01    $APYEAR
/*                                                                      00003000
//*====================================================                 00002200
//*  PRINTS EDD INDEPENDENT CONTRACTOR REPORT                           00002800
//*====================================================                 00002200
//STEP05   EXEC PGM=UAPP190
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPP190) PLAN(UAPP190)
/*
//SYSOUT    DD SYSOUT=*                                                 00001800
//SYSLST    DD SYSOUT=*                                                 00001900
//SYSUDUMP  DD SYSOUT=D,                                                00002000
//             DEST=LOCAL                                               00002100
//SYSDBOUT  DD SYSOUT=*                                                 00002200
//REPTVEND  DD DSN=FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT,DISP=SHR   00002300
//PRTVEND   DD SYSOUT=(,),
//            OUTPUT=(*.OUT1,*.OUT2,*.OUT3)
/*                                                                      00003000
//*====================================================                 00002200
//*  PRINTS INDEPENDENT CONTRACTOR EDIT ERROR REPORT                    00002800
//*====================================================                 00002200
//STEP06   EXEC PGM=UAPP190
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPP190) PLAN(UAPP190)
/*
//SYSOUT    DD SYSOUT=*                                                 00001800
//SYSLST    DD SYSOUT=*                                                 00001900
//SYSUDUMP  DD SYSOUT=D,                                                00002000
//             DEST=LOCAL                                               00002100
//SYSDBOUT  DD SYSOUT=*                                                 00002200
//REPTVEND  DD DSN=FISP.APBW190A.UAPX190.ERREXT.D$APBW190CDT,DISP=SHR   00002300
//PRTVEND   DD SYSOUT=(,),
//            OUTPUT=(*.OUT1,*.OUT2,*.OUT3)
/*                                                                      00003000
//*====================================================
//STEP07   EXEC PGM=IEBGENER
//SYSPRINT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SYSUT1    DD DSN=FISP.APBW190A.UAPX190.CR.D$APBW190CDT,
//             DISP=OLD
//SYSUT2    DD SYSOUT=(,),
//            OUTPUT=(*.OUT1,*.OUT2,*.OUT3)
//SYSIN     DD DUMMY
/*
//*====================================================
//STEP08   EXEC PGM=IEBGENER
//SYSPRINT  DD SYSOUT=*
//SYSUT1    DD DSN=FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT,
//             DISP=SHR
//SYSUT2    DD DSN=FISP.APBW190A.EDDEXT.BACKUP.D$APBW190CDT,            00002300
//            DISP=(NEW,CATLG,DELETE),                                  00002400
//            DCB=(RECFM=FB,LRECL=175),                                 00002500
//            SPACE=(TRK,(90,60),RLSE),                                 00002600
//            UNIT=SYSDA                                                00002700
//SYSIN     DD DUMMY
/*
//*
//*==================================================================
//*   SEND EMAIL TO DISBURSEMENTS TO EDIT INDEP.CONTRACTOR'S FILE
//*==================================================================
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
EERMINO@UCSD.EDU
JLORANCA@UCSD.EDU
DCHOCK@UCSD.EDU
VHER@UCSD.EDU
NCOWELL@UCSD.EDU
TESWANK@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
REMINDER:

THE INDEPENDENT CONTRACTOR'S REPORT IS READY TO EDIT....

     FILE NAME:  'FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT'

/*
//* CONTROL REPORT FILENAME FOLLOWS:
//SYSTSIN   DD *
 %MAILRPT -
 'FISP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('ALERT! I.C. REPORT')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE...
//*---
//*
//*==================================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*==================================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  APBW190A =======                 00250007
