//APBW190N JOB (SYS000),'DISBSMT-POSNER',
//             MSGCLASS=F,MSGLEVEL=(1,1),
//*            RESTART=STEP05,
//*            RESTART=MAILIT,
//             CLASS=K,REGION=0M,NOTIFY=APCDBM
//*========================
//*     ZEKE EV # 734          (CONTACT: ZSUZSA EX:40837)
//*========================
//*====================================================                 00000700
//*      NEW YEAR JCL FOR EDD INDEPENDENT CONTRACTOR REPORT
//*
//*        THIS IS **ONLY** RUN ONCE IN JANUARY
//*
//*       (THIS IS TO REPLACE THE RUNNING OF APBW190A
//*            APBW190B & APBW190T ARE STILL TO BE RUN)
//*
//*             ( SEE NOTE BELOW )
//*====================================================                 00000700
//OUT1 OUTPUT CLASS=G,FORMS=A002,DEST=LOCAL,GROUPID=DISBSMT
//OUT2 OUTPUT CLASS=X,FORMS=A002,DEST=U129,GROUPID=DISBSMT
//OUT3 OUTPUT CLASS=4,FORMS=A002,DEST=LOCAL,GROUPID=BACKUP1
//*====================================================                 00000700
//* OUT2 TURNED OFF PER BRANDI - 1/10/03 (DOUG)
//*====================================================                 00001100
//*  !!  VARIABLE $APBW190CDT IS SET BY SETDATFR (Z#771)
//*====================================================                 00001100
//*====================================================                 00001100
//*                                                                     00002800
//STEP04   EXEC PGM=UAPX190
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPX190) PLAN(UAPX190)
/*
//SYSOUT    DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,                                                00002000
//             DEST=LOCAL                                               00002100
//SYSDBOUT  DD SYSOUT=*                                                 00002200
//CONTROLR  DD DSN=FISP.APBW190A.UAPX190.CR.D$APBW190CDT,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),
//             SPACE=(TRK,(2,2),RLSE),
//             UNIT=SYSDA
//ERREXT    DD DSN=FISP.APBW190A.UAPX190.ERREXT.D$APBW190CDT,           00002300
//            DISP=(NEW,CATLG,DELETE),                                  00002400
//            DCB=(RECFM=FB,LRECL=175),                                 00002500
//            SPACE=(TRK,(90,60),RLSE),                                 00002600
//            UNIT=SYSDA                                                00002700
//EDDEXT    DD DSN=FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT,           00002300
//            DISP=(NEW,CATLG,DELETE),                                  00002400
//            DCB=(RECFM=FB,LRECL=175),                                 00002500
//            SPACE=(TRK,(90,60),RLSE),                                 00002600
//            UNIT=SYSDA                                                00002700
//REPTVEN   DD DUMMY
//*************
//* COL 6      UNVRS-CODE
//* COL 12     CURRENT CALENDAR YEAR
//************
//DATEFILE  DD *                                                        00002900
     01    $APYEAR
/*                                                                      00003000
//*====================================================                 00002200
//*  PRINTS EDD INDEPENDENT CONTRACTOR REPORT                           00002800
//*====================================================                 00002200
//STEP05   EXEC PGM=UAPP190
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPP190) PLAN(UAPP190)
 /*
//SYSOUT    DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,                                                00002000
//             DEST=LOCAL                                               00002100
//SYSDBOUT  DD SYSOUT=*                                                 00002200
//REPTVEND  DD DSN=FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT,DISP=SHR   00002300
//PRTVEND   DD SYSOUT=(,),
//            OUTPUT=(*.OUT1,*.OUT2,*.OUT3)
/*                                                                      00003000
//*====================================================                 00002200
//*  PRINTS INDEPENDENT CONTRACTOR EDIT ERROR REPORT                    00002800
//*====================================================                 00002200
//STEP06   EXEC PGM=UAPP190
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPP190) PLAN(UAPP190)
/*
//SYSOUT    DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,                                                00002000
//             DEST=LOCAL                                               00002100
//SYSDBOUT  DD SYSOUT=*                                                 00002200
//REPTVEND  DD DSN=FISP.APBW190A.UAPX190.ERREXT.D$APBW190CDT,DISP=SHR   00002300
//PRTVEND   DD SYSOUT=(,),
//            OUTPUT=(*.OUT1,*.OUT2,*.OUT3)
/*                                                                      00003000
//*====================================================
//STEP07   EXEC PGM=IEBGENER
//SYSPRINT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SYSUT1    DD DSN=FISP.APBW190A.UAPX190.CR.D$APBW190CDT,
//             DISP=OLD
//SYSUT2    DD SYSOUT=(,),
//            OUTPUT=(*.OUT1,*.OUT2,*.OUT3)
//SYSIN     DD DUMMY
/*
//*====================================================
//STEP08   EXEC PGM=IEBGENER
//SYSPRINT  DD SYSOUT=*
//SYSUT1    DD DSN=FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT,
//             DISP=SHR
//SYSUT2    DD DSN=FISP.APBW190A.EDDEXT.BACKUP.D$APBW190CDT,            00002300
//            DISP=(NEW,CATLG,DELETE),                                  00002400
//            DCB=(RECFM=FB,LRECL=175),                                 00002500
//            SPACE=(TRK,(90,60),RLSE),                                 00002600
//            UNIT=SYSDA                                                00002700
//SYSIN     DD DUMMY
/*
//*====================================================
//*     CREATE  CURRENT YEAR VENDOR EXTRACT FILE
//*====================================================
//STEP09   EXEC PGM=IEFBR14
//VENDEXT   DD DSN=FISP.VENDEXT.EDD.CY$APYEAR,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(RECFM=FB,LRECL=80),
//            SPACE=(TRK,(10,2),RLSE),
//            UNIT=SYSDA
/*
//*
//*==================================================================
//*   SEND EMAIL TO DISBURSEMENTS TO EDIT INDEP.CONTRACTOR'S FILE
//*==================================================================
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
EERMINO@UCSD.EDU
DCHOCK@UCSD.EDU
TESWANK@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
REMINDER:

THE INDEPENDENT CONTRACTOR'S REPORT IS READY TO EDIT....

     FILE NAME:  'FISP.APBW190A.UAPX190.EDDEXT.D$APBW190CDT'

/*
//* CONTROL REPORT FILENAME FOLLOWS:
//SYSTSIN   DD *
 %MAILRPT -
 'FISP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('ALERT! I.C. REPORT')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE...
//*---
//*
//*==================================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*==================================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  APBW190A =======                 00250007
