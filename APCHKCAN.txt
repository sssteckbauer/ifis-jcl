//APCHKCAN JOB (SYS000),'PRDCTL',MSGLEVEL=(1,1),MSGCLASS=X,             00010005
//    NOTIFY=APCDBM,CLASS=S,REGION=0M                                   00020077
//*====================================================                 00000700
//*  THIS WILL CANCEL CHECKS WHICH HAVE ALREADY BEEN
//*  POSTED.
//*
//*  PLACE FILE CONTAINING CHECKS TO BE CANCELLED
//*  AT DD STATEMENT FAPUK05O                                           00014200
//*
//*====================================================                 00000700
//*  ZEKE EVENT #66
//*====================================================                 00000700
//STEP01   EXEC PGM=UAPU270                                             00012100
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPU270) PLAN(UAPU270)
/*
//CONTROLR  DD DSN=FISP.APCHKCAN.UAPU270.CR.D$APDAT..T$FTIME,           00013000
//             DISP=(NEW,CATLG,KEEP),                                   00013100
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00013200
//             SPACE=(TRK,(2,2),RLSE),                                  00013300
//             UNIT=SYSDA                                               00013400
//SYSOUT    DD SYSOUT=*                                                 00013500
//SYSLST    DD SYSOUT=*                                                 00013600
//SYSUDUMP  DD SYSOUT=D,                                                00013700
//             DEST=LOCAL                                               00013800
//SYSDBOUT  DD SYSOUT=*                                                 00013900
//RUNPARMS  DD DSN=FISP.FILE.CHECKS.RUNUPDT,                            00014000
//             DISP=SHR                                                 00014100
//FAPUK05O  DD DSN=FISP.FILE.UK05O.AR70.D092399,                        00014200
//             DISP=SHR                                                 00014300
//SYSIN     DD *
 000001 ISISDICT
//*                                                                     00014500
//*============================================================         00014500
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00014500
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00014500
//*============================================================         00014500
//STOPZEKE EXEC STOPZEKE                                                00014500
//*                                                                     00014500
//*================ E N D  O F  J C L  APCHKCAN ========                00250007
