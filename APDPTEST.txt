//APDPTEST JOB (SYS000),'PRDCTL',MSGCLASS=F,MSGLEVEL=(1,1),
//*====================================================
//        REGION=20M,
//        NOTIFY=APCDBM,CLASS=0
//*====================================================
//* STEP01 CONTAINS CASE-SENSITIVE CODE,
//*    DO NOT CHANGE ANY LOWERCASE TO UPPERCASE
//*====================================================
//*  YOU ** MUST ** SUBMIT THIS JOB THRU ZEKE !
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 1010       !! TEST JOB !!
//*++++++++++++++++++++++++++
//*====================================================
//*====================================================
//*       ACH EMAIL TRANSMIT
//*====================================================
//STEP01   EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSTSPRT DD SYSOUT=*
//SYSTSIN  DD *
transmit +
 UCSDMVSA.SMTP dataset('FISP.FILE.DK04C.EMAIL.TEST.D051801') +
      noepilog nolog noprolog
/*
//*============================================================
//* display dataset('FISP.FILE.DK04C.EMAIL.TEST.D051801')
//*============================================================
//RENAMER  EXEC PGM=IDCAMS
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
  ALTER      FISP.FILE.DK04C.EMAIL.TEST.D051801 -
             NEWNAME(FISP.FILE.DK04C.EMAIL.TEST.D051801.SENT$MDAY)
/*
//*DISPLAY   FISP.FILE.DK04C.EMAIL.TEST.D051801 -
//*DISPLAY   NEWNAME(FISP.FILE.DK04C.EMAIL.TEST.D051801.SENT$MDAY)
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  APDPTARE  ========
