//APDPXLI1 JOB (SYS000),'PRDCTL',MSGCLASS=F,MSGLEVEL=(1,1),
//*====================================================
//        REGION=20M,
//        NOTIFY=APCDBM,CLASS=0
//*====================================================
//* STEP01 CONTAINS CASE-SENSITIVE CODE,
//*    DO NOT CHANGE ANY LOWERCASE TO UPPERCASE
//*====================================================
//*  YOU ** MUST ** SUBMIT THIS JOB THRU ZEKE !
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 2990
//*++++++++++++++++++++++++++
//*====================================================
//*====================================================
//*       ACH EMAIL TRANSMIT
//*====================================================
//STEP01   EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSTSPRT DD SYSOUT=*
//SYSTSIN  DD *
transmit +
 UCSDMVSA.SMTP dataset('FISP.FILE.DK04C.EMAIL.XLI1.D$APDAT') +
      noepilog nolog noprolog
/*
//*============================================================
//* display dataset('FISP.FILE.DK04C.EMAIL.XLI1.D$APDAT')
//*============================================================
//RENAMER  EXEC PGM=IDCAMS
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
  ALTER      FISP.FILE.DK04C.EMAIL.XLI1.D$APDAT -
             NEWNAME(FISP.FILE.DK04C.EMAIL.XLI1.D$APDAT..SENT$MDAY)
/*
//*DISPLAY   FISP.FILE.DK04C.EMAIL.XLI1.D$APDAT -
//*DISPLAY   NEWNAME(FISP.FILE.DK04C.EMAIL.XLI1.D$APDAT..SENT$MDAY)
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  APDPXLI1  ========
