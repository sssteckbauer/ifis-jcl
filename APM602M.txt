//AP1042M JOB (SYS000),'PRDCTL',MSGCLASS=F,MSGLEVEL=(1,1),              00000100
//*       REGION=0M,                                                    00000500
//        NOTIFY=APCDBM,CLASS=K                                         00000600
//*++++++++++++++++++++++++++                                           00000600
//*    ZEKE EVENT # 2492      ** MONTHLY 1042S PROCESSING **            00000700
//*++++++++++++++++++++++++++                                           00000800
//*====================================================                 00001000
//APRPT1   OUTPUT DEST=LOCAL,CLASS=G,GROUPID=PAYROLL,FORMS=A002         00000900
//APRPT2   OUTPUT DEST=LOCAL,CLASS=4,GROUPID=BACKUP,FORMS=A002          00001000
//* (APRPT2 CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION PROJECT ONLY)
//*====================================================                 00001200
//* THIS JOB WILL PRODUCE THE 1042S REPORT  (602D)    *                 00001300
//*====================================================                 00001400
/*JOBPARM LINECT=0                                                      00001500
//*====================================================                 00001600
//REMOVE1  EXEC PGM=IEFBR14                                             00001700
//*            REGION=200K                                              00001800
//DD1       DD DSN=FISP.APM602M.APREPORT.TEN42RPT.D$BJDT,               00001900
//             DISP=(MOD,DELETE,DELETE),                                00002000
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00002100
//             SPACE=(TRK,(300,300),RLSE),                              00002200
//             UNIT=SYSDA                                               00002300
//*====================================================                 00002400
//STEP01   EXEC PGM=UAPXS61                                             00002500
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPXS61) PLAN(UAPXS61)
/*
//SYSOUT    DD SYSOUT=*                                                 00003500
//SYSLST    DD SYSOUT=*                                                 00003600
//PARMFILE  DD *
TAX-YEAR=$CYR
//*===============================================================
//* TAX-YEAR IS CURRENT TAX YEAR THAT IS CHANGED ON JANUARY 1ST.
//* EX:
//* TAX-YEAR=07
//*===============================================================
//SYSUDUMP  DD SYSOUT=D,                                                00003700
//            DEST=LOCAL                                                00003800
//CONTROLR  DD DSN=FISP.APM602M.UAPXS61.CONTROLR.D$BJDT,                00003900
//             DISP=(NEW,CATLG,DELETE),                                 00004000
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00004100
//             SPACE=(TRK,(1,1),RLSE),                                  00004200
//             UNIT=SYSDA                                               00004300
//EXTRACT   DD DSN=&&EXTRACT,                                           00004400
//             DISP=(NEW,PASS),                                         00004500
//             DCB=(RECFM=FB,LRECL=479,BLKSIZE=23471),                  00004600
//             SPACE=(TRK,(750,300),RLSE),                              00004700
//             UNIT=VIO                                                 00004800
//*====================================================                 00004900
//STEP02   EXEC PGM=SYNCSORT                                            00005000
//SYSLST    DD SYSOUT=*                                                 00005100
//SORTWK01  DD SPACE=(TRK,300),                                         00005200
//             UNIT=SYSDA                                               00005300
//SORTWK02  DD SPACE=(TRK,300),                                         00005400
//             UNIT=SYSDA                                               00005500
//SORTWK03  DD SPACE=(TRK,300),                                         00005600
//             UNIT=SYSDA                                               00005700
//SORTWK04  DD SPACE=(TRK,300),                                         00005800
//             UNIT=SYSDA                                               00005900
//SYSOUT    DD SYSOUT=*                                                 00006000
//SYSUDUMP  DD SYSOUT=D,                                                00006100
//            DEST=LOCAL                                                00006200
//SORTIN    DD DSN=&&EXTRACT,                                           00006300
//             DISP=(OLD,DELETE)                                        00006400
//SORTOUT   DD DSN=&&EXTRACTS,                                          00006500
//             DISP=(NEW,PASS),                                         00006600
//             DCB=(RECFM=FB,LRECL=479,BLKSIZE=23471),                  00006700
//             SPACE=(TRK,(750,300),RLSE),                              00006800
//             UNIT=VIO                                                 00006900
//SYSIN     DD *                                                        00007000
  SORT FIELDS=(72,35,CH,A,22,9,CH,A,36,2,CH,A)
//*====================================================                 00007100
//STEP03   EXEC PGM=UAPP602D                                            00007200
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*                                                 00008000
//SYSOUT    DD SYSOUT=*                                                 00008100
//SYSUDUMP  DD SYSOUT=D,                                                00008200
//             DEST=LOCAL                                               00008300
//CONTROLR  DD DSN=FISP.APM602M.UAPP602D.CONTROLR.D$BJDT,               00008400
//             DISP=(NEW,CATLG,DELETE),                                 00008500
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00008600
//             SPACE=(TRK,(1,1),RLSE),                                  00008700
//             UNIT=SYSDA                                               00008800
//EXTRACT   DD DSN=&&EXTRACTS,                                          00008900
//             DISP=(OLD,DELETE)                                        00009000
//PRINT     DD DSN=FISP.APM602M.APREPORT.TEN42RPT.D$BJDT,
//             MGMTCLAS=AS,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),
//             SPACE=(TRK,(750,300),RLSE),
//             UNIT=SYSDA
//APTRANS   DD DSN=FISP.APTRANS.S1042.D$BJDT,
//             DISP=(NEW,CATLG,DELETE),                                 00009200
//             DCB=(RECFM=FB,LRECL=282,BLKSIZE=282),                    00009300
//             SPACE=(TRK,(15,15),RLSE),                                00009400
//             UNIT=SYSDA                                               00009500
//*====================================================                 00009600
//STEP04   EXEC PGM=IEBGENER                                            00009700
//SYSPRINT  DD SYSOUT=*                                                 00009800
//SYSUDUMP  DD SYSOUT=D,                                                00009900
//             DEST=LOCAL                                               00010000
//SYSUT1    DD DSN=FISP.APM602M.UAPXS61.CONTROLR.D$BJDT,                00010100
//             DISP=(OLD)                                               00010200
//          DD DSN=FISP.APM602M.UAPP602D.CONTROLR.D$BJDT,               00010300
//             DISP=(OLD)                                               00010400
//SYSUT2    DD SYSOUT=(,),                                              00010500
//             OUTPUT=(*.APRPT1,*.APRPT2)                               00010600
//SYSIN     DD DUMMY                                                    00010700
//*====================================================                 00010800
//STEP05   EXEC PGM=IEBGENER                                            00010900
//SYSPRINT  DD SYSOUT=*                                                 00011000
//SYSUDUMP  DD SYSOUT=D,                                                00011100
//             DEST=LOCAL                                               00011200
//SYSUT1    DD DSN=FISP.APM602M.APREPORT.TEN42RPT.D$BJDT,               00011300
//             DISP=(OLD)                                               00011400
//SYSUT2    DD SYSOUT=(,),                                              00011500
//             OUTPUT=(*.APRPT1,*.APRPT2)                               00011600
//SYSIN     DD DUMMY                                                    00011700
//*============================================================         00011800
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00011900
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00012000
//*============================================================         00012100
//STOPZEKE EXEC STOPZEKE                                                00012200
//*                                                                     00012300
//*================ E N D  O F  J C L  AP1042M  =========               00012400
