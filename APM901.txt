//APMP901  JOB (SYS000),'APCDBM-PRDCTL',                                00010000
//            MSGLEVEL=(1,1),                                           00020000
//            MSGCLASS=F,                                               00030000
//            CLASS=K,                                                  00040000
//            NOTIFY=APCDBM,                                            00050000
//            REGION=20M                                                00060000
//*++++++++++++++++++++++++++                                           00070000
//*    ZEKE EVENT # XXX                                                 00080000
//*++++++++++++++++++++++++++                                           00090000
//OUT1 OUTPUT CLASS=F,FORMS=P011,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL   00100000
//*====================================================                 00110000
//*  RUN SYNCSORT PROGRAM - COPY VENDOR DATA FILE FROM DISK TO TAPE     00120000
//*====================================================                 00130000
//*====================================================                 00130100
//*  CHECK FOR INPUT DATASETS BEFORE EXECUTING JOB                      00130200
//*====================================================                 00130300
//CHKDSN1  EXEC PGM=IKJEFT01,DYNAMNBR=99                                00130400
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR                         00130500
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR                               00130600
//SYSTSPRT DD  TERM=TS,SYSOUT=*                                         00130700
//SYSTSIN  DD  *                                                        00130800
  %CHKDSN 'FISP.APM100.UAPX100D.VENDRFLE.CURRENT'                       00130900
/*                                                                      00131000
//CHKDSN2  EXEC PGM=IKJEFT01,DYNAMNBR=99,COND=(0,NE)                    00131100
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR                         00131200
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR                               00131300
//SYSTSPRT DD  TERM=TS,SYSOUT=*                                         00131400
//SYSTSIN  DD  *                                                        00131500
  %CHKDSN 'FISP.PARMLIB(SORTCOPY)'                                      00131600
/*                                                                      00131700
//CHKDSN3  EXEC PGM=IKJEFT01,DYNAMNBR=99,COND=(0,NE)                    00131800
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR                         00131900
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR                               00132000
//SYSTSPRT DD  TERM=TS,SYSOUT=*                                         00132100
//SYSTSIN  DD  *                                                        00132200
  %CHKDSN 'FISP.PARMLIB(APMADRS1)'                                      00132300
/*                                                                      00132400
//CHKDSN4  EXEC PGM=IKJEFT01,DYNAMNBR=99,COND=(0,NE)                    00132500
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR                         00132600
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR                               00132700
//SYSTSPRT DD  TERM=TS,SYSOUT=*                                         00132800
//SYSTSIN  DD  *                                                        00132900
  %CHKDSN 'FISP.PARMLIB(APMCMNT1)'                                      00133000
/*                                                                      00133100
//*====================================================                 00134000
//LODTU010 EXEC PGM=SYNCSORT,COND=(0,NE)                                00140000
//SORTIN    DD DSN=FISP.APM100.UAPX100D.VENDRFLE.CURRENT,               00150000
//             DISP=OLD                                                 00160000
//SORTOUT   DD DSN=FISP.APM100.UAPX100D.VENDTAPE.D91343,                00170000
//             DISP=(NEW,KEEP),                                         00180000
//             DCB=(DEN=3,LRECL=200,BLKSIZE=23400,RECFM=FB),            00190000
//             LABEL=(1,SL,RETPD=60),                                   00200000
//             UNIT=TAPE                                                00210000
//SYSOUT    DD DSN=&&R10CNT,                                            00220000
//             DISP=(NEW,PASS),                                         00230000
//             DCB=(BLKSIZE=132,RECFM=F,DSORG=DA),                      00240000
//             SPACE=(80,100),                                          00250000
//             UNIT=SYSDA                                               00260000
//SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),                              00270000
//             DISP=SHR                                                 00280000
/*                                                                      00290000
//*====================================================                 00300000
//*  RUN XMITTAL PROGRAM    - CREATE TAPE TRANSMITTAL                   00310000
//*====================================================                 00320000
//CRTTR015 EXEC PGM=XMITTAL                                             00330000
//TAPECARD  DD DSN=*.LODTU010.SORTOUT,                                  00340000
//             DISP=OLD,                                                00350000
//             DCB=*.LODTU010.SORTOUT,                                  00360000
//             VOL=REF=*.LODTU010.SORTOUT                               00370000
//RECCOUNT  DD DSN=&&R10CNT,                                            00380000
//             DISP=(OLD,DELETE)                                        00390000
//SYSOUT    DD SYSOUT=*                                                 00400000
//TRANSMIT  DD SYSOUT=C,                                                00410000
//             DEST=LOCAL                                               00420000
//MAILTO    DD DSN=FISP.PARMLIB(APMADRS1),                              00430000
//             DISP=SHR                                                 00440000
//COMMENTS  DD DSN=FISP.PARMLIB(APMCMNT1),                              00450000
//             DISP=SHR                                                 00460000
/*                                                                      00470000
//*============================================================         00480000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00490000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00500000
//*============================================================         00510000
//STOPZEKE EXEC STOPZEKE                                                00520000
//*                                                                     00530000
//*================ E N D  O F  J C L  APM901 ==========                00540000
