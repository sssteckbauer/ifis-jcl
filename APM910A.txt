//APM910A JOB (SYS000),'SONIA-DISBRSMNTS',MSGCLASS=F,MSGLEVEL=(1,1),    00000100
//*     RESTART=MAILIT,
//        NOTIFY=APCDBM,CLASS=K,REGION=0M                               00000200
//*====================================================                 00000300
//*  SELECT RECORDS FOR ACCOUNTS PAYABLE CHECK PURGING                  00000400
//*====================================================                 00000500
//*********************
//* AP CHECK PURGE SELECTION
//*********************
//*++++++++++++++++++++++++++                                           00000600
//*    ZEKE EVENT # 219         (or #111 for special run)               00000700
//*++++++++++++++++++++++++++                                           00000800
//OUT1 OUTPUT CLASS=F,FORMS=A011,JESDS=ALL,DEFAULT=YES,GROUPID=ACCTG    00000900
//OUT2 OUTPUT CLASS=G,FORMS=A002,GROUPID=DISBRSMT                       00001000
//OUT3 OUTPUT CLASS=4,FORMS=A002,GROUPID=BACKUP                         00001100
//* (OUT3 CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION PROJECT ONLY)
//*====================================================                 00001200
//SETVAR1  EXEC PGM=ZEKESET                                             00001300
//SYSPRINT  DD SYSOUT=(,),                                              00001400
//             OUTPUT=*.OUT1                                            00001500
//SYSIN     DD *                                                        00001600
  SET VAR $CHEXPG EQ $BDATE
  SET VAR $CHEXPGC EQ $GENPURG
/*                                                                      00001700
//*====================================================                 00001800
//SETVAR2  EXEC PGM=ZEKESET                                             00001900
//SYSPRINT  DD SYSOUT=(,),                                              00002000
//             OUTPUT=*.OUT1                                            00002100
//SYSIN     DD *                                                        00002200
  SET VAR $CHEXPURGE EQ ACTIVE
/*                                                                      00002300
//*====================================================                 00002400
//*       DELETE CATALOGED FILES                                        00002500
//*====================================================                 00002600
//STEP01   EXEC PGM=IEFBR14                                             00002700
//DD1       DD DSN=FISP.APM910A.UAPX910.EXTRAC01.R$BDATE,               00002800
//             DISP=(MOD,DELETE,DELETE),                                00002900
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00003000
//             SPACE=(TRK,(2010,210),RLSE),                             00003100
//             UNIT=SYSDA                                               00003200
//DD2       DD DSN=FISP.APM910A.UAPX910.EXTRAC01.SORTED.R$BDATE,        00003300
//             DISP=(MOD,DELETE,DELETE),                                00003400
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00003500
//             SPACE=(TRK,(1005,105),RLSE),                             00003600
//             UNIT=SYSDA                                               00003700
//*====================================================                 00003800
//*             EXTRACT FOR PURGE                                       00003900
//*====================================================                 00004000
//STEP02   EXEC PGM=UAPX910                                             00004100
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPX910) PLAN(UAPX910)
/*
//SYSOUT    DD SYSOUT=(,),                                              00004900
//             OUTPUT=*.OUT1                                            00005000
//SYSLST    DD SYSOUT=*                                                 00005100
//SYSUDUMP  DD SYSOUT=*                                                 00005200
//SYSDBOUT  DD SYSOUT=*                                                 00005300
//PARM01    DD *                                                        00005400
*CHECK DAYS 365
/*                                                                      00005500
//* --------------------------------                                    00005600
//* AS OF 7-1-93, THE CHECK DAYS (ABOVE) WILL REMAIN AT 365             00005700
//* --------------------------------                                    00005800
//EXTRAC01  DD DSN=FISP.APM910A.UAPX910.EXTRAC01.R$BDATE,               00005900
//             DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                       00006000
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00006100
//*            SPACE=(TRK,(2010,210),RLSE),                             00006200
//             SPACE=(TRK,(9010,910),RLSE),                             00006200
//             UNIT=SYSDA                                               00006300
//REPORT01  DD DSN=FISP.APM910A.UAPX910.CONTROLR.R$BDATE,               00006400
//             DISP=(NEW,CATLG,DELETE),                                 00006500
//             DCB=(RECFM=FB,LRECL=132,BLKSIZE=23364),                  00006600
//             SPACE=(TRK,(1,1),RLSE),                                  00006700
//             UNIT=SYSDA                                               00006800
//*====================================================                 00006900
//*       SORT EXTRACT FILE                                             00007000
//*====================================================                 00007100
//STEP03   EXEC PGM=SYNCSORT,COND=(0,NE)                                00007200
//SYSOUT    DD SYSOUT=(,),                                              00007300
//             OUTPUT=*.OUT1                                            00007400
//SORTIN    DD DSN=FISP.APM910A.UAPX910.EXTRAC01.R$BDATE,               00007500
//             DISP=SHR                                                 00007600
//SORTOUT   DD DSN=FISP.APM910A.UAPX910.EXTRAC01.SORTED.R$BDATE,        00007700
//             DISP=(NEW,CATLG,DELETE),                                 00007800
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00007900
//             SPACE=(TRK,(2010,210),RLSE),                             00008000
//             UNIT=SYSDA                                               00008100
//SORTWK01  DD SPACE=(TRK,150),                                         00008200
//             UNIT=SYSDA                                               00008300
//SORTWK02  DD SPACE=(TRK,150),                                         00008400
//             UNIT=SYSDA                                               00008500
//SORTWK03  DD SPACE=(TRK,150),                                         00008600
//             UNIT=SYSDA                                               00008700
//SORTWK04  DD SPACE=(TRK,150),                                         00008800
//             UNIT=SYSDA                                               00008900
//SYSIN     DD *                                                        00009000
   OMIT COND=(11,5,CH,NE,C'R4158',AND,11,5,CH,NE,C'R4157')
   SORT FIELDS=(1,20,CH,A)
/*                                                                      00009100
//*====================================================                 00009200
//*  THIS STEP PRINTS THE CHECK FILE                                    00009300
//*====================================================                 00009400
//STEP04   EXEC PGM=SYNCSORT                                            00009500
//SYSOUT    DD SYSOUT=(,),                                              00009600
//             OUTPUT=*.OUT1                                            00009700
//SORTIN    DD DSN=FISP.APM910A.UAPX910.EXTRAC01.SORTED.R$BDATE,        00009800
//             DISP=SHR                                                 00009900
//SORTOUT   DD DSN=FISP.APM910A.UAPX910.PURGE.REPORT.R$BDATE,           00010000
//             DISP=(NEW,CATLG,DELETE),                                 00010100
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00010200
//             SPACE=(TRK,(900,900),RLSE),                              00010300
//             UNIT=SYSDA                                               00010400
//*SORTOUT   DD SYSOUT=(A,,A002),                                       00010500
//*             OUTPUT=*.OUT1                                           00010600
//*SORTOUT  DD  SYSOUT=(,),OUTPUT=(*.OUT2,*.OUT3)                       00010700
//SORTWK01  DD SPACE=(TRK,150),                                         00010800
//             UNIT=SYSDA                                               00010900
//SORTWK02  DD SPACE=(TRK,150),                                         00011000
//             UNIT=SYSDA                                               00011100
//SORTWK03  DD SPACE=(TRK,150),                                         00011200
//             UNIT=SYSDA                                               00011300
//SORTWK04  DD SPACE=(TRK,150),                                         00011400
//             UNIT=SYSDA                                               00011500
//SYSIN     DD *                                                        00011600
               INCLUDE COND=(11,5,CH,EQ,C'R4158')
               SORT FIELDS=(80,8,CH,A)
               OUTFIL FILES=OUT,
                      OUTREC=(1:80,8,
                              12:88,5,PD,EDIT=(TTTT/TT/TT),
                              26:93,1,
                              32:106,1,
                              80:53X),
                       HEADER2=(1:'UAPX910',
                                21:'CHECK PURGE REPORT',
                                46:'DATE:',
                                51:&DATE,
                                61:'PAGE:',
                                66:&PAGE,//,
                                1:'CHECK #',
                                12:'CHECK DATE',
                                24:'TYPE',
                                30:'CNCL'),
                       TRAILER1=(1:'NUMBER OF RECORDS: ',COUNT)
                        END
/*                                                                      00011700
//*====================================================                 00011800
//*          PRINT CONTROL REPORTS                    =                 00011900
//*====================================================                 00012000
//CNTRL05  EXEC PGM=IEBGENER                                            00012100
//SYSPRINT  DD SYSOUT=*                                                 00012200
//SYSUT2    DD SYSOUT=(,),                                              00012300
//             OUTPUT=(*.OUT2,*.OUT3)                                   00012400
//SYSUT1    DD DSN=FISP.APM910A.UAPX910.CONTROLR.R$BDATE,               00012500
//             DISP=SHR                                                 00012600
//SYSIN     DD DUMMY                                                    00012700
//*====================================================                 00012800
//MAILIT   EXEC PGM=IKJEFT01,                                           00012900
//             DYNAMNBR=20                                              00013000
//SYSPROC   DD DSN=SYST.T.CLIST,                                        00013100
//             DISP=SHR                                                 00013200
//SYSPRINT  DD SYSOUT=*                                                 00013300
//SYSTSPRT  DD SYSOUT=*                                                 00013400
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:                    00013500
//DESTIDS   DD *                                                        00013600
DMESERVE@UCSD.EDU
/*                                                                      00013700
//* EMAIL TEXT FOLLOWS:                                                 00013800
//NOTES     DD *                                                        00013900
 The  A/P CHECKS  PURGE-SELECTION
 phase has been completed for the month of $MONTH.

 A control report from that selection process
 follows this mail text.

 Please review the control report, and notify
 me   A.S.A.P   if you detect any errors.

 The   PURGE   is scheduled to run this Saturday evening
 after this selection phase UNLESS errors are reported.

  Thank you,
  MGR, Production Control



/*                                                                      00014000
//* CONTROL REPORT FILENAME FOLLOWS:                                    00014100
//SYSTSIN   DD *                                                        00014200
 %MAILRPT -
 'FISP.APM910A.UAPX910.CONTROLR.R$BDATE' -
 SUBJECT('$MONTH A/P CHECKS PURGE SELECTION')
/*                                                                      00014300
//*                                                                     00014300
//*============================================================
//*   FTP TO BFS
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2009) REBUILD'
/*
//*============================================================         00014300
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00014300
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00014300
//*============================================================         00014300
//STOPZEKE EXEC STOPZEKE                                                00014300
//*                                                                     00014300
//*================ E N D  O F  J C L  APM910A  ========                00014600
