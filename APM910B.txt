//APM910B JOB (SYS000),'SUSAN-DISBRSMNTS',MSGCLASS=F,MSGLEVEL=(1,1),    00000100
//*       RESTART=MAILIT,
//        NOTIFY=APCDBM,CLASS=K,REGION=0M                               00000200
//*====================================================                 00000300
//*     PURGE ACCOUNTS PAYABLE CHECK RECORDS                            00000400
//*====================================================                 00000500
//*********************
//* AP CHECK PURGE PROCESS
//*********************
//*++++++++++++++++++++++++++                                           00000600
//*    ZEKE EVENT # 296                                                 00000700
//*++++++++++++++++++++++++++                                           00000800
//OUT1 OUTPUT CLASS=F,FORMS=A011,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL   00000900
//OUT2 OUTPUT CLASS=G,FORMS=A002,GROUPID=DISBRSMT                       00001000
//OUT3 OUTPUT CLASS=4,FORMS=A002,GROUPID=BACKUP                         00001100
//* (OUT3 CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION PROJECT ONLY).
//*====================================================                 00001200
//*====================================================                 00110000
//*  CHECK FOR INPUT DATASETS BEFORE EXECUTING JOB
//*====================================================                 00110000
//CHKDSN1  EXEC PGM=IKJEFT01,DYNAMNBR=99
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR
//SYSTSPRT DD  TERM=TS,SYSOUT=*
//SYSTSIN  DD  *
  %CHKDSN 'FISP.APM910A.UAPX910.EXTRAC01.SORTED.R$CHEXPG'
/*
//*====================================================                 00001200
//STEP01   EXEC PGM=UAPU910,COND=(0,NE)                                 00001300
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPU910) PLAN(UAPU910)
/*
//SYSOUT    DD SYSOUT=(,),                                              00002100
//             OUTPUT=*.OUT1                                            00002200
//SYSLST    DD SYSOUT=*                                                 00002300
//SYSUDUMP  DD SYSOUT=D                                                 00002400
//SYSDBOUT  DD SYSOUT=*                                                 00002500
//*EXTRAC01  DD DSN=FIST.APM910A.UAPX910.EXTRAC01.SORTED.R061896,       00002600
//EXTRAC01  DD DSN=FISP.APM910A.UAPX910.EXTRAC01.SORTED.R$CHEXPG,       00002600
//             DISP=SHR                                                 00002700
//REPORT01  DD DSN=FISP.APM910B.UAPU910.CONTROLR.R$BDATE,               00002900
//             DISP=(NEW,CATLG,DELETE),                                 00003000
//             DCB=(RECFM=FB,LRECL=132,BLKSIZE=23364),                  00003100
//             SPACE=(TRK,(15,15),RLSE),                                00003200
//             UNIT=SYSDA                                               00003300
//*====================================================                 00003400
//*          PRINT CONTROL REPORTS                    =                 00003500
//*====================================================                 00003600
//CNTRL05  EXEC PGM=IEBGENER                                            00003700
//SYSPRINT  DD SYSOUT=*                                                 00003800
//SYSUT2    DD SYSOUT=(,),                                              00003900
//             OUTPUT=(*.OUT2,*.OUT3)                                   00004000
//SYSUT1    DD DSN=FISP.APM910B.UAPU910.CONTROLR.R$BDATE,               00004100
//             DISP=SHR                                                 00004200
//SYSIN     DD DUMMY                                                    00004300
//*                                                                     00004400
//*====================================================                 00004500
//MAILIT   EXEC PGM=IKJEFT01,                                           00004600
//             DYNAMNBR=20                                              00004700
//SYSPROC   DD DSN=SYST.T.CLIST,                                        00004800
//             DISP=SHR                                                 00004900
//SYSPRINT  DD SYSOUT=*                                                 00005000
//SYSTSPRT  DD SYSOUT=*                                                 00005100
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:                    00005200
//DESTIDS   DD *                                                        00005300
AVIGIL@UCSD.EDU
DMESERVE@UCSD.EDU
/*                                                                      00005400
//* EMAIL TEXT FOLLOWS:                                                 00005500
//NOTES     DD *                                                        00005600
 The A/P CHECKS PURGE
 has been completed for the month of $MONTH.

 A control report from that purge process
 follows this mail text.

 Please review the control report, and notify
 me if you detect any errors or problems.



 Thank you,
 MGR, Production Control

/*                                                                      00005700
//* CONTROL REPORT FILENAME FOLLOWS:                                    00005800
//SYSTSIN   DD *                                                        00005900
 %MAILRPT -
 'FISP.APM910B.UAPU910.CONTROLR.R$BDATE' -
 SUBJECT('$MONTH A/P CHECKS PURGE PROCESSING')
/*                                                                      00006000
//*                                                                     00006000
//*============================================================         00006000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00006000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00006000
//*============================================================         00006000
//STOPZEKE EXEC STOPZEKE                                                00006000
//*                                                                     00006000
//*================ E N D  O F  J C L  APM910B  ========                00006300
