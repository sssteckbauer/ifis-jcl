//APM920A JOB (SYS000),'SONIA-DISBRSMNTS',MSGCLASS=F,                   00000100
//*    RESTART=MAILIT,
//        NOTIFY=APCDBM,CLASS=K,MSGLEVEL=(1,1),REGION=0M                00000200
//*====================================================                 00000300
//*       EXTRACT A/P INVOICE RECORDS FOR PURGING                       00000400
//*====================================================                 00000500
//*++++++++++++++++++++++++++                                           00000600
//*    ZEKE EVENT # 1480                                                00000700
//*++++++++++++++++++++++++++                                           00000800
//*********************
//* AP INVOICE PURGE SELECTION
//*********************
//OUT1 OUTPUT CLASS=F,FORMS=A011,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL   00000900
//OUT2 OUTPUT CLASS=X,FORMS=A002,GROUPID=DISBRSMT                       00001000
//OUT3 OUTPUT CLASS=G,FORMS=A002,GROUPID=BACKUP                         00001100
//*====================================================                 00001200
//* THE PRINTED REPORT HAS BEEN TURNED OFF AS OF 11-01-93               00001300
//*====================================================                 00001400
//SETVAR1  EXEC PGM=ZEKESET                                             00001500
//SYSPRINT  DD SYSOUT=(,),                                              00001600
//             OUTPUT=*.OUT1                                            00001700
//SYSIN     DD *                                                        00001800
  SET VAR $APINVC EQ $BDATE
  SET VAR $APINVCC EQ $GENPURG
/*                                                                      00001900
//*====================================================                 00002000
//SETVAR2  EXEC PGM=ZEKESET                                             00002100
//SYSPRINT  DD SYSOUT=(,),                                              00002200
//             OUTPUT=*.OUT1                                            00002300
//SYSIN     DD *                                                        00002400
  SET VAR $INVOICEPURGE EQ ACTIVE
/*                                                                      00002500
//*====================================================                 00002600
//*====================================================                 00002700
//*       DELETE CATALOGED FILES                                        00002800
//*====================================================                 00002900
//STEP01   EXEC PGM=IEFBR14                                             00003000
//DD1       DD DSN=FISP.APM920A.UAPX920.EXTRAC01.R$BDATE,               00003100
//             DISP=(MOD,DELETE,DELETE),                                00003200
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00003300
//             SPACE=(TRK,(2010,210),RLSE),                             00003400
//             UNIT=SYSDA                                               00003500
//DD2       DD DSN=FISP.APM920A.UAPX920.EXTRAC01.SORTED.R$BDATE,        00003600
//             DISP=(MOD,DELETE,DELETE),                                00003700
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00003800
//             SPACE=(TRK,(2010,105),RLSE),                             00003900
//             UNIT=SYSDA                                               00004000
//*====================================================                 00004100
//*             EXTRACT FOR PURGE                                       00004200
//*====================================================                 00004300
//STEP02   EXEC PGM=UAPX920                                             00004400
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPX920) PLAN(UAPX920)
/*
//SYSOUT    DD SYSOUT=(,),                                              00005200
//             OUTPUT=*.OUT1                                            00005300
//SYSLST    DD SYSOUT=*                                                 00005400
//SYSUDUMP  DD SYSOUT=D                                                 00005500
//SYSDBOUT  DD SYSOUT=*                                                 00005600
//PARM01    DD *                                                        00005700
*INVOICE DAYS 365
/*                                                                      00005800
//*------------------------------------------------------               00005900
//* AS OF 7-1-93, THE INVOICE DAYS (ABOVE) WILL REMAIN AT 365           00006000
//*------------------------------------------------------               00006100
//EXTRAC01  DD DSN=FISP.APM920A.UAPX920.EXTRAC01.R$BDATE,               00006200
//             DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                       00006300
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00006400
//*            SPACE=(TRK,(3010,310),RLSE),                             00006500
//             SPACE=(TRK,(9010,910),RLSE),                             00006500
//             UNIT=SYSDA                                               00006600
//REPORT01  DD DSN=FISP.APM920A.UAPX920.CONTROLR.R$BDATE,               00006700
//             DISP=(NEW,CATLG,DELETE),                                 00006800
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00006900
//             SPACE=(TRK,(1,1),RLSE),                                  00007000
//             UNIT=SYSDA                                               00007100
//*====================================================                 00007200
//*       SORT EXTRACT FILE                                             00007300
//*====================================================                 00007400
//STEP03   EXEC PGM=SYNCSORT,COND=(0,NE)                                00007500
//SYSOUT    DD SYSOUT=(,),                                              00007600
//             OUTPUT=*.OUT1                                            00007700
//SORTIN    DD DSN=FISP.APM920A.UAPX920.EXTRAC01.R$BDATE,               00007800
//             DISP=SHR                                                 00007900
//SORTOUT   DD DSN=FISP.APM920A.UAPX920.EXTRAC01.SORTED.R$BDATE,        00008000
//             DISP=(NEW,CATLG,DELETE),                                 00008100
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00008200
//             SPACE=(TRK,(2010,525),RLSE),                             00008300
//             UNIT=SYSDA                                               00008400
//SORTWK01  DD SPACE=(TRK,300),                                         00008500
//             UNIT=SYSDA                                               00008600
//SORTWK02  DD SPACE=(TRK,300),                                         00008700
//             UNIT=SYSDA                                               00008800
//SORTWK03  DD SPACE=(TRK,300),                                         00008900
//             UNIT=SYSDA                                               00009000
//SORTWK04  DD SPACE=(TRK,300),                                         00009100
//             UNIT=SYSDA                                               00009200
//SYSIN     DD *                                                        00009300
   OMIT COND=(11,5,CH,NE,C'R4150')
   SORT FIELDS=(1,20,CH,A)
/*                                                                      00009400
//*====================================================                 00009500
//*  THIS STEP PRINTS THE INVOICE FILE                                  00009600
//*====================================================                 00009700
//STEP04   EXEC PGM=SYNCSORT                                            00009800
//SYSOUT    DD SYSOUT=(,),                                              00009900
//             OUTPUT=*.OUT1                                            00010000
//SORTIN    DD DSN=FISP.APM920A.UAPX920.EXTRAC01.SORTED.R$BDATE,        00010100
//             DISP=SHR                                                 00010200
//SORTOUT   DD DSN=FISP.APM920A.UAPX920.PURGE.REPORT.R$BDATE,           00010300
//             DISP=(NEW,CATLG,DELETE),                                 00010400
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00010500
//             SPACE=(TRK,(750,750),RLSE),                              00010600
//             UNIT=SYSDA                                               00010700
//*SORTOUT   DD SYSOUT=(,),OUTPUT=(*.OUT2,*.OUT3)                       00010800
//*SORTOUT   DD SYSOUT=(A,,D002),                                       00010900
//*             OUTPUT=*.OUT1                                           00011000
//SORTWK01  DD SPACE=(TRK,150),                                         00011100
//             UNIT=SYSDA                                               00011200
//SORTWK02  DD SPACE=(TRK,150),                                         00011300
//             UNIT=SYSDA                                               00011400
//SORTWK03  DD SPACE=(TRK,150),                                         00011500
//             UNIT=SYSDA                                               00011600
//SORTWK04  DD SPACE=(TRK,150),                                         00011700
//             UNIT=SYSDA                                               00011800
//SYSIN     DD *                                                        00011900
               SORT FIELDS=(78,8,CH,A)
               OUTFIL FILES=OUT,
                      OUTREC=(1:78,8,
                              12:178,5,PD,EDIT=(TTTT/TT/TT),
                              24:133,8,
                              40:113,1,
                              50:95,1,
                              60:183,1,
                              70:94,1,
                              80:53X),
                       HEADER2=(1:'UAPX920',
                                21:'INVOICE PURGE REPORT',
                                46:'DATE:',
                                51:&DATE,
                                61:'PAGE:',
                                66:&PAGE,//,
                                1:'DOC #',
                                12:'TRANS DATE',
                                27:'PO #',
                                38:'O/P/H',
                                50:'CR',
                                59:'CNCL',
                                69:'1099'),
                       TRAILER1=(1:'NUMBER OF RECORDS: ',COUNT)
                        END
/*                                                                      00012000
//*====================================================                 00012100
//*          PRINT CONTROL REPORTS                    =                 00012200
//*====================================================                 00012300
//CNTRL05  EXEC PGM=IEBGENER                                            00012400
//SYSPRINT  DD SYSOUT=*                                                 00012500
//SYSUT2    DD SYSOUT=(,),                                              00012600
//             OUTPUT=(*.OUT2,*.OUT3)                                   00012700
//SYSUT1    DD DSN=FISP.APM920A.UAPX920.CONTROLR.R$BDATE,               00012800
//             DISP=SHR                                                 00012900
//SYSIN     DD DUMMY                                                    00013000
//*====================================================                 00013100
//MAILIT   EXEC PGM=IKJEFT01,                                           00013200
//             DYNAMNBR=20                                              00013300
//SYSPROC   DD DSN=SYST.T.CLIST,                                        00013400
//             DISP=SHR                                                 00013500
//SYSPRINT  DD SYSOUT=*                                                 00013600
//SYSTSPRT  DD SYSOUT=*                                                 00013700
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:                    00013800
//DESTIDS   DD *                                                        00013900
DMESERVE@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES     DD *
 The A/P INVOICES PURGE-SELECTION
 phase has been completed for the month of $MONTH.

 A control report from that selection process
 follows this mail text.

 Please review the control report, and notify
 me   A.S.A.P   if you detect any errors.

 The  PURGE  is scheduled to run TWO WORKING DAYS
 after this selection  UNLESS  errors are reported.

 Thank you,
 MGR, Production Control


/*                                                                      00014300
//* CONTROL REPORT FILENAME FOLLOWS:                                    00014400
//SYSTSIN   DD *                                                        00014500
 %MAILRPT -
 'FISP.APM920A.UAPX920.CONTROLR.R$BDATE' -
 SUBJECT('$MONTH A/P INVOICE PURGE SELECTION')
/*                                                                      00014600
//*                                                                     00014600
//*============================================================
//*   FTP TO BFS
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2010) REBUILD'
/*
//*============================================================         00014600
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00014600
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00014600
//*============================================================         00014600
//STOPZEKE EXEC STOPZEKE                                                00014600
//*                                                                     00014600
//*================ E N D  O F  J C L  APM920A  ========                00014900
