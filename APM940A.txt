//APMF940A JOB (SYS000),'ACCTG/PURCH/DSE',MSGCLASS=F,                   00000100
//        NOTIFY=APCDBM,CLASS=K,MSGLEVEL=(1,1),REGION=0M                00000200
//*++++++++++++++++++++++++++                                           00000300
//*    ZEKE EVENT # XXXX  (BROUGHT BACK FROM INACTIVE.NEEDS ZEKE PANEL) 00000400
//*++++++++++++++++++++++++++                                           00000500
//*********************
//* VENDOR PURGE SELECTION
//*********************
//OUT1 OUTPUT CLASS=F,FORMS=A011,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL   00000600
//OUT2 OUTPUT CLASS=P,FORMS=A002,GROUPID=ACOLE                          00000700
//OUT3 OUTPUT CLASS=P,FORMS=PU02,GROUPID=LCOLLINS                       00000800
//OUT4 OUTPUT CLASS=G,FORMS=A002,GROUPID=BACKUP                         00000900
//OUT5 OUTPUT CLASS=P,FORMS=A002,GROUPID=FMCCULLO                       00001000
//*OUT6 OUTPUT CLASS=P,FORMS=D002,GROUPID=RCRESSEY                      00001100
//*====================================================                 00001200
//*  VENDOR PURGE SELECTION                                             00001300
//*====================================================                 00001400
//*              CHANGE TARGET:  $BDATE                                 00001500
//*====================================================                 00001600
//SETVAR   EXEC PGM=ZEKESET,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00001700
//SYSPRINT  DD SYSOUT=*                                                 00001800
//SYSIN     DD *                                                        00001900
  SET VAR $VENDPRG EQ $BDATE
  SET VAR $VENDPRGC EQ $GENPURG
/*                                                                      00002000
//*====================================================                 00002100
//*       DELETE CATALOGED FILES                                        00002200
//*====================================================                 00002300
//STEP01   EXEC PGM=IEFBR14                                             00002400
//DD1       DD DSN=FISP.APM940A.UAPX940.EXTRAC01,                       00002500
//             DISP=(MOD,DELETE,DELETE),                                00002600
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00002700
//             SPACE=(TRK,(20,20),RLSE),                                00002800
//             UNIT=SYSDA                                               00002900
//DD2       DD DSN=FISP.APM940A.UAPX940.EXTRAC01.SORTED.D$BDATE,        00003000
//             DISP=(MOD,DELETE,DELETE),                                00003100
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00003200
//             SPACE=(TRK,(20,20),RLSE),                                00003300
//             UNIT=SYSDA                                               00003400
//DD3       DD DSN=FISP.APM940A.UAPX940.VENDRFLE,                       00003500
//             DISP=(MOD,DELETE,DELETE),                                00003600
//             DCB=(RECFM=FB,LRECL=200,BLKSIZE=23400),                  00003700
//             SPACE=(TRK,(20,20),RLSE),                                00003800
//             UNIT=SYSDA                                               00003900
//*====================================================                 00004000
//*             EXTRACT FOR PURGE                                       00004100
//*====================================================                 00004200
//STEP02   EXEC PGM=UAPX940                                             00004300
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPX940) PLAN(UAPX940)
/*
//SYSOUT    DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D                                                 00005300
//SYSDBOUT  DD SYSOUT=*                                                 00005400
//PARM01    DD *                                                        00005500
*VENDOR DAYS 090
/*                                                                      00005600
//EXTRAC01  DD DSN=FISP.APM940A.UAPX940.EXTRAC01,                       00005700
//             DISP=(NEW,CATLG,DELETE),                                 00005800
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00005900
//             SPACE=(TRK,(20,20),RLSE),                                00006000
//             UNIT=SYSDA                                               00006100
//VENDRFLE  DD DSN=FISP.APM940A.UAPX940.VENDRFLE,                       00006200
//             DISP=(NEW,CATLG,DELETE),                                 00006300
//             DCB=(RECFM=FB,LRECL=200,BLKSIZE=23400),                  00006400
//             SPACE=(TRK,(20,20),RLSE),                                00006500
//             UNIT=SYSDA                                               00006600
//REPORT01  DD DSN=FISP.APM940A.UAPX940.CONTROLR.D$BDATE,               00006700
//             DISP=(NEW,CATLG,DELETE),                                 00006800
//             DCB=(RECFM=FBA,LRECL=132,BLKSIZE=23364),                 00006900
//             SPACE=(TRK,(1,1),RLSE),                                  00007000
//             UNIT=SYSDA                                               00007100
//*                                                                     00007200
//*====================================================                 00007300
//*       SORT EXTRACT FILE                                             00007400
//*====================================================                 00007500
//STEP03   EXEC PGM=SORT,COND=(0,NE)                                    00007600
//SYSOUT    DD SYSOUT=*                                                 00007700
//SORTIN    DD DSN=FISP.APM940A.UAPX940.EXTRAC01,                       00007800
//             DISP=SHR                                                 00007900
//SORTOUT   DD DSN=FISP.APM940A.UAPX940.EXTRAC01.SORTED.D$BDATE,        00008000
//             DISP=(NEW,CATLG,DELETE),                                 00008100
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00008200
//             SPACE=(TRK,(20,20),RLSE),                                00008300
//             UNIT=SYSDA                                               00008400
//SORTWK01  DD SPACE=(TRK,150),                                         00008500
//             UNIT=SYSDA                                               00008600
//SORTWK02  DD SPACE=(TRK,150),                                         00008700
//             UNIT=SYSDA                                               00008800
//SORTWK03  DD SPACE=(TRK,150),                                         00008900
//             UNIT=SYSDA                                               00009000
//SORTWK04  DD SPACE=(TRK,150),                                         00009100
//             UNIT=SYSDA                                               00009200
//SYSIN     DD *                                                        00009300
   OMIT COND=(13,5,CH,NE,C'R4073')
   SORT FIELDS=(1,20,CH,A)
/*                                                                      00009400
//*====================================================                 00009500
//*  THIS STEP PRINTS THE VENDOR FILE                                   00009600
//*====================================================                 00009700
//STEP04   EXEC PGM=SYNCSORT                                            00009800
//SYSOUT    DD SYSOUT=*                                                 00009900
//SORTIN    DD DSN=FISP.APM940A.UAPX940.EXTRAC01.SORTED.D$BDATE,        00010000
//             DISP=SHR                                                 00010100
//SORTOUT   DD SYSOUT=(,),                                              00010200
//             OUTPUT=(*.OUT2,*.OUT3,*.OUT4,*.OUT5)                     00010300
//*            OUTPUT=(*.OUT2,*.OUT3,*.OUT4,*.OUT5,*.OUT6)              00010300
//SYSIN     DD *                                                        00010400
               SORT FIELDS=COPY
               OUTFIL FILES=OUT,
                      OUTREC=(1:3,10,
                              15:149,5,PD,EDIT=(TTTT/TT/TT),
                              80:1X),
                       HEADER2=(1:'UAPX940',
                                21:'VENDOR PURGE REPORT',
                                46:'DATE:',
                                51:&DATE,
                                61:'PAGE:',
                                66:&PAGE,//,
                                1:'VENDOR #',
                                15:'END   DATE'),
                       TRAILER1=(1:'NUMBER OF RECORDS: ',COUNT)
                        END
/*                                                                      00010500
//*====================================================                 00010600
//*  THIS STEP CREATES A VENDOR PURGE NAME REPORT                       00010700
//*====================================================                 00010800
//STEP04   EXEC PGM=SYNCSORT                                            00010900
//SYSOUT    DD SYSOUT=*                                                 00011000
//SORTIN    DD DSN=FISP.APM940A.UAPX940.EXTRAC01,                       00011100
//             DISP=SHR                                                 00011200
//SORTOUT   DD SYSOUT=(,),                                              00011300
//             OUTPUT=(*.OUT2,*.OUT3,*.OUT4,*.OUT5)                     00011400
//*            OUTPUT=(*.OUT2,*.OUT3,*.OUT4,*.OUT5,*.OUT6)              00011400
//SORTWK01  DD SPACE=(TRK,50),                                          00011500
//             UNIT=SYSDA                                               00011600
//SORTWK02  DD SPACE=(TRK,50),                                          00011700
//             UNIT=SYSDA                                               00011800
//SORTWK03  DD SPACE=(TRK,50),                                          00011900
//             UNIT=SYSDA                                               00012000
//SORTWK04  DD SPACE=(TRK,50),                                          00012100
//             UNIT=SYSDA                                               00012200
//SYSIN     DD *                                                        00012300
               INCLUDE COND=(13,5,CH,EQ,C'R6156',OR,                    00190003
                             13,5,CH,EQ,C'R6352')                       00200002
               SORT FIELDS=(1,12,CH,A)                                  00210003
               OUTFIL FILES=OUT,                                        00220000
                      OUTREC=(1:3,10,                                   00230000
                              15:78,35,                                 00240003
                              80:1X),                                   00250000
                       HEADER2=(1:'UAPX940',                            00260000
                                21:'VENDOR PURGE NAME XREF',            00270005
                                46:'DATE:',                             00280000
                                51:&DATE,                               00290000
                                61:'PAGE:',                             00300000
                                66:&PAGE,//,                            00310000
                                1:'VENDOR #',                           00320000
                                15:'VENDOR NAME')                       00330005
                        END                                             00340000
/*                                                                      00012400
//*====================================================                 00012500
//*          PRINT CONTROL REPORTS                    =                 00012600
//*====================================================                 00012700
//CNTRL050 EXEC PGM=IEBGENER                                            00012800
//SYSPRINT  DD SYSOUT=*                                                 00012900
//SYSUT2    DD SYSOUT=(,),                                              00013000
//             OUTPUT=(*.OUT2,*.OUT3,*.OUT4,*.OUT5)                     00013100
//*            OUTPUT=(*.OUT2,*.OUT3,*.OUT4,*.OUT5,*.OUT6)              00013100
//SYSUT1    DD DSN=FISP.APM940A.UAPX940.CONTROLR.D$BDATE,               00013200
//             DISP=SHR                                                 00013300
//SYSIN     DD DUMMY                                                    00013400
//*                                                                     00013500
//*============================================================         00013500
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00013500
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00013500
//*============================================================         00013500
//STOPZEKE EXEC STOPZEKE                                                00013500
//*                                                                     00013500
//*================ E N D  O F  J C L  APM940A  ========                00013700
