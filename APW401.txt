//INVAGING JOB (SYS000),'POSNER/DISB',MSGLEVEL=(1,1),                   00000100
//         MSGCLASS=F,REGION=0M,                                        00000200
//         NOTIFY=APCDBM,CLASS=K                                        00000300
/*JOBPARM LINES=1500                                                    00000400
//*++++++++++++++++++++++++++                                           00000500
//*    ZEKE EVENT # 812  ... OR #390 (FYE)                              00000600
//*++++++++++++++++++++++++++                                           00000700
//APRPT1   OUTPUT DEST=LOCAL,CLASS=G,GROUPID=DISBRSMT,FORMS=A002        00000800
//APRPT2   OUTPUT DEST=LOCAL,CLASS=4,GROUPID=BACKUP,FORMS=A002          00000900
//APRPT3   OUTPUT DEST=LOCAL,CLASS=G,GROUPID=DISBRSMT,FORMS=A002
//APRPT4   OUTPUT DEST=LOCAL,CLASS=4,GROUPID=BACKUP,FORMS=A002
//OUT1 OUTPUT CLASS=F,FORMS=A011,JESDS=ALL,DEFAULT=YES,GROUPID=DISBRSMT 00001000
//* (APRPT2 CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION PROJECT ONLY)
//* (APRPT4 CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION PROJECT ONLY)
//*====================================================                 00001100
//* THIS JOB WILL PRODUCE THE INVOICE AGING REPORT  (401D)       *      00001200
//*                           PAYAUTH AGING REPORT
//*                           MARKETPLACE AGING REPORT
//*                           TRANSITIONAL DEPTORD AGING REPORT (403D)
//*====================================================                 00001300
/*JOBPARM LINECT=0                                                      00001400
//*====================================================                 00001500
//REMOVE   EXEC PGM=IEFBR14                                             00001600
//*            REGION=200K                                              00001700
//DD1       DD DSN=FISP.APW401.APREPORT.INVAGING,                       00001800
//             DISP=(MOD,DELETE,DELETE),                                00001900
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00002000
//             SPACE=(TRK,(300,300),RLSE),                              00002100
//             UNIT=SYSDA                                               00002200
//DD2       DD DSN=FISP.APW401.APREPORT.PAYAGING,                       00001800
//             DISP=(MOD,DELETE,DELETE),                                00001900
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00002000
//             SPACE=(TRK,(300,300),RLSE),                              00002100
//             UNIT=SYSDA                                               00002200
//DD3       DD DSN=FISP.APW401.APREPORT.MKTPLACE,                       00001800
//             DISP=(MOD,DELETE,DELETE),                                00001900
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00002000
//             SPACE=(TRK,(300,300),RLSE),                              00002100
//             UNIT=SYSDA                                               00002200
//DD4       DD DSN=FISP.APW401.APREPORT.TDOAGING,                       00001800
//             DISP=(MOD,DELETE,DELETE),                                00001900
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00002000
//             SPACE=(TRK,(300,300),RLSE),                              00002100
//             UNIT=SYSDA                                               00002200
//*====================================================                 00002300
//STEP01   EXEC PGM=UAPXS40                                             00002400
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPXS40) PLAN(UAPXS40)
/*
//SYSOUT    DD SYSOUT=*                                                 00003400
//SYSLST    DD SYSOUT=*                                                 00003500
//SYSUDUMP  DD SYSOUT=D,                                                00003600
//             DEST=LOCAL                                               00003700
//EXTRACT   DD DSN=&&EXTRACT,                                           00003800
//             DISP=(NEW,PASS),                                         00003900
//             DCB=(RECFM=FB,LRECL=440,BLKSIZE=11000),                  00004000
//             SPACE=(TRK,(750,300),RLSE),                              00004100
//             UNIT=VIO                                                 00004200
//EXTRACT2  DD DSN=&&EXTRACT2,                                          00003800
//             DISP=(NEW,PASS),                                         00003900
//             DCB=(RECFM=FB,LRECL=440,BLKSIZE=11000),                  00004000
//             SPACE=(TRK,(750,300),RLSE),                              00004100
//             UNIT=VIO                                                 00004200
//CONTROLR  DD DSN=FISP.APW401.UAPXS40.CONTROLR.D$BJDT..T$FTIME,        00004300
//             DISP=(NEW,CATLG,DELETE),                                 00004400
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00004500
//             SPACE=(TRK,(1,1),RLSE),                                  00004600
//             UNIT=SYSDA                                               00004700
//*====================================================                 00004800
//STEP02   EXEC PGM=SYNCSORT                                            00004900
//SYSLST    DD SYSOUT=*                                                 00005000
//SORTWK01  DD SPACE=(TRK,300),                                         00005100
//             UNIT=SYSDA                                               00005200
//SORTWK02  DD SPACE=(TRK,300),                                         00005300
//             UNIT=SYSDA                                               00005400
//SORTWK03  DD SPACE=(TRK,300),                                         00005500
//             UNIT=SYSDA                                               00005600
//SORTWK04  DD SPACE=(TRK,300),                                         00005700
//             UNIT=SYSDA                                               00005800
//SYSOUT    DD SYSOUT=*                                                 00005900
//SYSUDUMP  DD SYSOUT=D,                                                00006000
//             DEST=LOCAL                                               00006100
//SORTIN    DD DSN=&&EXTRACT,                                           00006200
//             DISP=(OLD,DELETE)                                        00006300
//SORTOUT   DD DSN=&&EXTRACTS,                                          00006400
//             DISP=(NEW,PASS),                                         00006500
//             DCB=(RECFM=FB,LRECL=440,BLKSIZE=11000),                  00006600
//             SPACE=(TRK,(750,300),RLSE),                              00006700
//             UNIT=VIO                                                 00006800
//SYSIN     DD *                                                        00006900
  SORT FIELDS=(1,80,CH,A)
//*====================================================                 00004800
//STEP02A  EXEC PGM=SYNCSORT                                            00004900
//SYSLST    DD SYSOUT=*                                                 00005000
//SORTWK01  DD SPACE=(TRK,300),                                         00005100
//             UNIT=SYSDA                                               00005200
//SORTWK02  DD SPACE=(TRK,300),                                         00005300
//             UNIT=SYSDA                                               00005400
//SORTWK03  DD SPACE=(TRK,300),                                         00005500
//             UNIT=SYSDA                                               00005600
//SORTWK04  DD SPACE=(TRK,300),                                         00005700
//             UNIT=SYSDA                                               00005800
//SYSOUT    DD SYSOUT=*                                                 00005900
//SYSUDUMP  DD SYSOUT=D,                                                00006000
//             DEST=LOCAL                                               00006100
//SORTIN    DD DSN=&&EXTRACT2,                                          00006200
//             DISP=(OLD,DELETE)                                        00006300
//SORTOUT   DD DSN=&&EXTRACTT,                                          00006400
//             DISP=(NEW,PASS),                                         00006500
//             DCB=(RECFM=FB,LRECL=440,BLKSIZE=11000),                  00006600
//             SPACE=(TRK,(750,300),RLSE),                              00006700
//             UNIT=VIO                                                 00006800
//SYSIN     DD *                                                        00006900
  SORT FIELDS=(110,8,CH,A)
//*====================================================                 00007000
//STEP03   EXEC PGM=UAPP401D                                            00007100
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*                                                 00007900
//SYSOUT    DD SYSOUT=*                                                 00008000
//SYSUDUMP  DD SYSOUT=D,                                                00008100
//             DEST=LOCAL                                               00008200
//EXTRACT   DD DSN=&&EXTRACTS,                                          00008300
//             DISP=(OLD,DELETE)                                        00008400
//CONTROLR  DD DSN=FISP.APW401.UAPP401D.CONTROLR.D$BJDT..T$FTIME,       00008500
//             DISP=(NEW,CATLG,DELETE),                                 00008600
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00008700
//             SPACE=(TRK,(1,1),RLSE),                                  00008800
//             UNIT=SYSDA                                               00008900
//PRNTRT01  DD DSN=FISP.APW401.APREPORT.INVAGING,MGMTCLAS=AS,           00009000
//             DISP=(NEW,CATLG,DELETE),                                 00009100
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009200
//             SPACE=(TRK,(750,300),RLSE),                              00009300
//             UNIT=SYSDA                                               00009400
//PRNTRT02  DD DSN=FISP.APW401.APREPORT.PAYAGING,MGMTCLAS=AS,           00009000
//             DISP=(NEW,CATLG,DELETE),                                 00009100
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009200
//             SPACE=(TRK,(750,300),RLSE),                              00009300
//             UNIT=SYSDA                                               00009400
//PRNTRT03  DD DSN=FISP.APW401.APREPORT.MKTPLACE,MGMTCLAS=AS,           00009000
//             DISP=(NEW,CATLG,DELETE),                                 00009100
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009200
//             SPACE=(TRK,(750,300),RLSE),                              00009300
//             UNIT=SYSDA                                               00009400
//*====================================================                 00007000
//STEP03A  EXEC PGM=UAPP403D                                            00007100
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPP403D) PLAN(UAPP403D)
/*
//SYSLST    DD SYSOUT=*                                                 00007900
//SYSOUT    DD SYSOUT=*                                                 00008000
//SYSUDUMP  DD SYSOUT=D,                                                00008100
//             DEST=LOCAL                                               00008200
//EXTRACT   DD DSN=&&EXTRACTT,                                          00008300
//             DISP=(OLD,DELETE)                                        00008400
//CONTROLR  DD DSN=FISP.APW401.UAPP403D.CONTROLR.D$BJDT..T$FTIME,       00008500
//             DISP=(NEW,CATLG,DELETE),                                 00008600
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00008700
//             SPACE=(TRK,(1,1),RLSE),                                  00008800
//             UNIT=SYSDA                                               00008900
//PRNTRT01  DD DSN=FISP.APW401.APREPORT.TDOAGING,MGMTCLAS=AS,           00009000
//             DISP=(NEW,CATLG,DELETE),                                 00009100
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009200
//             SPACE=(TRK,(750,300),RLSE),                              00009300
//             UNIT=SYSDA                                               00009400
//*====================================================                 00009500
//STEP04   EXEC PGM=IEBGENER                                            00009600
//SYSPRINT  DD SYSOUT=*                                                 00009700
//SYSUDUMP  DD SYSOUT=D,                                                00009800
//             DEST=LOCAL                                               00009900
//SYSUT1    DD DSN=FISP.APW401.UAPXS40.CONTROLR.D$BJDT..T$FTIME,        00010000
//             DISP=(OLD)                                               00010100
//          DD DSN=FISP.APW401.UAPP401D.CONTROLR.D$BJDT..T$FTIME,       00010200
//             DISP=(OLD)                                               00010300
//          DD DSN=FISP.APW401.UAPP403D.CONTROLR.D$BJDT..T$FTIME,       00010200
//             DISP=(OLD)                                               00010300
//SYSUT2    DD SYSOUT=(,),                                              00010400
//             OUTPUT=(*.APRPT1,*.APRPT2)                               00010500
//SYSIN     DD DUMMY                                                    00010600
//*====================================================                 00010700
//STEP05   EXEC PGM=IEBGENER                                            00010800
//SYSPRINT  DD SYSOUT=*                                                 00010900
//SYSUDUMP  DD SYSOUT=D,                                                00011000
//             DEST=LOCAL                                               00011100
//SYSUT1    DD DSN=FISP.APW401.APREPORT.INVAGING,                       00011200
//             DISP=(OLD)                                               00011300
//SYSUT2    DD SYSOUT=(,),                                              00011400
//             OUTPUT=(*.APRPT1,*.APRPT2)                               00011500
//SYSIN     DD DUMMY                                                    00011600
//*============================================================         00011700
//STEP06   EXEC PGM=IEBGENER                                            00010800
//SYSPRINT  DD SYSOUT=*                                                 00010900
//SYSUDUMP  DD SYSOUT=D,                                                00011000
//             DEST=LOCAL                                               00011100
//SYSUT1    DD DSN=FISP.APW401.APREPORT.PAYAGING,                       00011200
//             DISP=(OLD)                                               00011300
//SYSUT2    DD SYSOUT=(,),                                              00011400
//             OUTPUT=(*.APRPT1,*.APRPT2)                               00011500
//SYSIN     DD DUMMY                                                    00011600
//*============================================================         00011700
//STEP07   EXEC PGM=IEBGENER                                            00010800
//SYSPRINT  DD SYSOUT=*                                                 00010900
//SYSUDUMP  DD SYSOUT=D,                                                00011000
//             DEST=LOCAL                                               00011100
//SYSUT1    DD DSN=FISP.APW401.APREPORT.MKTPLACE,                       00011200
//             DISP=(OLD)                                               00011300
//SYSUT2    DD SYSOUT=(,),                                              00011400
//             OUTPUT=(*.APRPT3,*.APRPT4)                               00011500
//SYSIN     DD DUMMY                                                    00011600
//*====================================================                 00010700
//STEP08   EXEC PGM=IEBGENER                                            00010800
//SYSPRINT  DD SYSOUT=*                                                 00010900
//SYSUDUMP  DD SYSOUT=D,                                                00011000
//             DEST=LOCAL                                               00011100
//SYSUT1    DD DSN=FISP.APW401.APREPORT.TDOAGING,                       00011200
//             DISP=(OLD)                                               00011300
//SYSUT2    DD SYSOUT=(,),                                              00011400
//             OUTPUT=(*.APRPT1,*.APRPT2)                               00011500
//SYSIN     DD DUMMY                                                    00011600
//*============================================================         00011700
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00011800
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00011900
//*============================================================         00012000
//STOPZEKE EXEC STOPZEKE                                                00012100
//*                                                                     00012200
//*================ E N D  O F  J C L  APW401  =========                00012300
