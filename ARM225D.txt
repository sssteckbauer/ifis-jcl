//ARMP225D JOB (SYS000),'ACCTG-GAO-ISIS',
//            MSGLEVEL=(1,1),
//            MSGCLASS=F,
//*           RESTART=(ARM225P.SORT040),
//            CLASS=K,
//            REGION=0M,
//            NOTIFY=APCDBM
//*++++++++++++++++++++++++++++++++++++
//***  ZEKE EVENT # 1080 DAILY
//*    ZEKE EVENT # 954  MONTHLY
//*++++++++++++++++++++++++++++++++++++
// JCLLIB ORDER=APCP.PROCLIB
//*
//OUT1 OUTPUT CLASS=X,FORMS=A011,GROUPID=ACCTG
//OUT2 OUTPUT CLASS=X,FORMS=A011,GROUPID=PRDCTL
//OUT3 OUTPUT CLASS=F,FORMS=A011,GROUPID=PRDCTL,JESDS=ALL,DEFAULT=YES
//OUT4 OUTPUT CLASS=G,FORMS=A011,GROUPID=BACKUP
//******************************************************
//* OUT1 - PRINT TURNED OFF PER BOB COLIO -12/12/2009. (WAS 'A')
//******************************************************
//*
//*
//JOBLIB  DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//        DD DSN=DB2P.DCA.LOAD,DISP=SHR
//        DD DSN=DB2F.DSNEXIT,DISP=SHR
//        DD DSN=DB2F.DSNLOAD,DISP=SHR
//*
//*====================================================                 00110000
//*                     AR GL LOAD TO IFIS
//*====================================================                 00110000
//ARM225P EXEC ARM225P,COND=(0,NE),
//             OUTU='(,),OUTPUT=(*.OUT1,*.OUT4)',        USER SYSOUT
//             OUTX='(,),OUTPUT=*.OUT2',                 SYSTEM SYSOUT
//             COPIES=1,                                 # OF COPIES
//             GROUPID='FISP',                           TEST OR PROD
//             GROUP1='SISP',                            TEST OR PROD
//             DB2REGN='DB2F',                           DB2 DATABASE
//             ACDATE='AC$BJDT',                         ZEKE DATE
//             SRT1CTL='FISP.PARMLIB(ARM225S2)',         SORT EXTRACT
//             SRT2CTL='FISP.PARMLIB(ARM225S3)',         SORT EXTRACT
//             SRT3CTL='FISP.PARMLIB(ARM225S4)'          SORT GL FEED
//**  THE FOLLOWING CARD WILL USE THE BUSINESS DATE TO OVERLAY
//**  THE HEADER RECORD CREATED IN SARXB01.  THIS WILL ALLOW FOR THE
//**  RUNNING OF AR'S GL FEED AFTER MIDNIGHT ON THE LAST DAY OF THE
//**  MONTH.
//EXTR030.FISPMEM  DD  *
UARXB05  $BDATE
/*
//*
//*============================================================
//SETDATE  EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*SYSPRINT DD SYSOUT=*
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT2)
//SYSIN    DD *
  SET VAR $ARM225 EQ $BJDT
//*
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*SYSPRINT DD SYSOUT=*
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT2)
//SYSIN     DD *
 SET SCOM 'ZADD EV (2023 2024 2025) REBUILD'
/*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE                                                00010000
//*                                                                     00170000
//*================ E N D  O F  J O B  ARM225D =========================
