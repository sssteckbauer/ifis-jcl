//ARMP6503 JOB (SYS000),'ACCTG-GAO-ISIS',
//            MSGLEVEL=(1,1),
//            MSGCLASS=F,
//            CLASS=K,
//            REGION=0M,
//            NOTIFY=APCDBM
//*++++++++++++++++++++++++++++
//*  ZEKE EV # 2401 (FISCAL-JULY-EOM)
//*++++++++++++++++++++++++++++
//*     THIS JOB WILL GIVE NORMAL CONDITION CODE OF 200 (CC = 200)
//*     IN COMP040 STEP.
//*++++++++++++++++++++++++++
//*
// JCLLIB ORDER=APCP.PROCLIB
//*
//JOBLIB  DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//        DD DSN=DB2P.DCA.LOAD,DISP=SHR
//        DD DSN=DB2F.DSNEXIT,DISP=SHR
//        DD DSN=DB2F.DSNLOAD,DISP=SHR
//*
//*====================================================                 00110000
//AR650P3 EXEC ARM650P3,COND=(0,NE),
//             GROUPID='SISP',                           TEST OR PROD
//             ACDATE='AC$ARM2DATE',                     ZEKE DATE
//             BDATE='D$ARM2DATE'                        ZEKE DATE
//*
//******************************************************************
//* COMMENT OUT THE REMAINING STEPS BECAUSE THE VARIANCE REPORT WILL
//* NO LONGER BE CREATED SINCE IFIS IS DEPRECATED.
//******************************************************************
//*
//*COMP040.FISPMEM2 DD  *
//*FISCAL YEAR $FISFSCLYRN
//*ACCOUNTING PERIOD 01
//*
//***DISPLAY***
//***FISCAL YEAR $FISFSCLYR
//***ACCOUNTING PERIOD 01
//*============================================================
//*  ACCOUNTING PERIOD IS HARD CODED VALUE OF '01' FOR MONTH
//*  OF JULY. THIS JOB WILL **ONLY** RUN AS THE LAST DAY OF
//*  JULY EACH YEAR.
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//*STOPZEKE EXEC STOPZEKE                                               00010000
//*                                                                     00170000
