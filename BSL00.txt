//BSL00C  JOB (SYS000),'BUDGET OFFICE-TPCS',NOTIFY=APCDBM,              00010025
//             CLASS=A,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M             00020008
//*ROUTE PRINT RMT10                                                    00030008
//OUTBSL00 OUTPUT DEFAULT=YES,JESDS=ALL,GROUPID=BUDGET,FORMS=BS11       00040015
//*====================================================                 00050008
//* JOBSTREAM: BSL00                                                    00060008
//*                                                                     00070008
//* ++ 'YA' OPTION USED IN ALL OTHER RUNS ( FOR ADJUSTMENTS )
//*    'YI' OPTION USED ONLY IN JULY ( FOR INCREASES )
//*           ('YI' **ONLY** WHEN BUDGET/STAFFING OFFICE REQUESTS
//*                 IT! THIS WILL ADD RECORDS INTO THE 'NEXT' FISCAL
//*                 YEAR)
//* ++ = OPTION IN EFFECT NOW
//*
//*            LASER PRINTER                                            00080008
//*====================================================                 00090008
//REMOVE1  EXEC PGM=IEFBR14                                             00100008
//*            REGION=200K                                              00110008
//DD1       DD DSN=FISP.BSL0001.SD.PR.PAYBJE,                           00120008
//             DISP=(MOD,DELETE,DELETE),                                00130008
//             DCB=(RECFM=FB,LRECL=115,BLKSIZE=23460),                  00140008
//             SPACE=(TRK,(15,1),RLSE),                                 00150008
//             UNIT=SYSDA                                               00160008
//*====================================================                 00170008
//EXTRACT  EXEC PGM=BSL000B                                             00180008
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(BSL000B) PLAN(BSL000B)
/*
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00290008
//             DISP=SHR                                                 00300008
//TABLEIN   DD DSN=FISP.BSL3100.SD.FILE,                                00310008
//             DISP=SHR                                                 00320008
//*====================================================                 00330008
//*                                                                     00340008
//*  BJEIN IS TO BE USED FOR PAYROLL ADJUSTMENT FILES                   00350008
//*                                                                     00360008
//*====================================================                 00370008
//*BJEIN      DD DSN=PPSP.PPSPA001.RX1610B.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.CX1807C.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.MA1807B.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.SX1806A.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA003.U91807A.MERIT.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.RX1906A.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.TX1906B.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.CX1907C.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA003.U91908A.MERIT.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.MA1910A.NPRVN.BUDGET,DISP=SHR        00390027
//*BJEIN      DD DSN=PPSP.PPSPA001.TX1907B.NPRVN.BUDGET,DISP=SHR        00390027
//BJEIN      DD DSN=PPSP.PPSPA001.RX1907C.NPRVN.BUDGET,DISP=SHR         00390027
//*
//* CONTROLS:   PYMERITS=YA OR YI                                       00430008
//*                                                                     00440008
//*  'YI' OPTION USED IN JULY ONLY (AND ONLY ON REQUEST FROM B/S)
//CCARDIN   DD *                                                        00450008
BSL000   YA                                                             00460014
//* COL 10 BUDGET JOURNAL ENTRY INPUT FLAG:  Y=YES                      00490008
//* COL 11 BUDGET JOURNAL ENTRY TRANS CLASS: A OR I                     00500008
//BUDOUT    DD DSN=FISP.BSL0001.SD.PR.PAYBJE,                           00510008
//             DISP=(NEW,CATLG,DELETE),                                 00520008
//             DCB=(LRECL=115,BLKSIZE=23460,RECFM=FB),                  00530008
//             SPACE=(TRK,(15,1),RLSE),                                 00540008
//             UNIT=SYSDA                                               00550008
//UUTC20V1  DD DSN=FISP.UUTC20V1,                                       00560008
//             DISP=SHR,                                                00570008
//             AMP=('BUFNI=2')                                          00580008
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00590008
//             DISP=SHR,                                                00600008
//             AMP=('BUFNI=2')                                          00610008
//REPORT    DD SYSOUT=*,                                                00620008
//             COPIES=1                                                 00630008
//SYSOUT    DD SYSOUT=*                                                 00640008
//SYSUDUMP  DD SYSOUT=D,                                                00650008
//             DEST=LOCAL                                               00660008
//SYSDBOUT  DD SYSOUT=*                                                 00670008
/*                                                                      00680008
//*============================================================         00690008
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00700008
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00710008
//*============================================================         00720008
//STOPZEKE EXEC STOPZEKE                                                00730008
//*                                                                     00740008
//*================ E N D  O F  J C L  BSL00  ==========                00750008
//
//* CONTROLS:  PAYTFF=YA    PYMERITS=YA OR YI                           00430008
//* COL  8 TRANSFER OF FUNDS INPUT FLAG:     Y=YES                      00470008
//* COL  9 TRANSFER OF FUNDS TRANS CLASS:    A OR I                     00480008
