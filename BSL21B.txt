//BSL21B JOB (ACCT),'BUDGET - 355 TPCS',NOTIFY=APCDBM,                  00010009
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M             00020006
//OUT1 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BUDGET,COPIES=2                00030010
//OUT2 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BACKUP,COPIES=2                00030110
//*OUT1 OUTPUT CLASS=P,FORMS=BS01,GROUPID=BUDGET,COPIES=1               00031005
//*OUT2 OUTPUT CLASS=G,FORMS=BS01,GROUPID=BACKUP,COPIES=1               00040005
//OUT3 OUTPUT CLASS=F,FORMS=BS11,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET2  00050003
//*====================================================                 00060003
//*    *N.T.E.*
//*                                                                     00061005
//* TURN OFF PRINTING AS OF 4/2009. HUGO
//*                                                                     00061005
//* NORMAL 'MONTHLY' = 2 COPY DUPLEX                                    00062010
//*                                                                     00063005
//* JOBSTREAM: BSL21B - LASER PRINTOUT REQUIRED.. SIMPLEX               00070003
//*                     2 COPIES PRODUCED AUTOMATICALLY                 00080010
//*
//*====================================================                 00100003
//*              REMOVE POTENTIAL OUTPUT FILES                          00001900
//*====================================================                 00002000
//CLEAN01  EXEC PGM=IEFBR14                                             00002100
//SYSPRINT  DD SYSOUT=*                                                 00002200
//DD1       DD DSN=FISP.BSL21B.BSL211B.REPORT,                          00490003
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//*====================================================                 00001800
//SORT     EXEC PGM=SYNCSORT                                            00110003
//SORTIN    DD DSN=FISP.BSL0200.SD.FILE,                                00120003
//             DISP=SHR                                                 00130003
//SORTOUT   DD DSN=&&BSL0200S,                                          00140003
//             DISP=(NEW,PASS,DELETE),                                  00150003
//             DCB=(LRECL=466,BLKSIZE=11650,RECFM=FB),                  00160003
//             SPACE=(TRK,(750,75),RLSE),                               00170003
//             UNIT=VIO                                                 00180003
//SYSIN     DD *                                                        00190003
   SORT FIELDS=(1,3,CH,A,                                               00200003
                5,1,CH,A,                                               00210003
                45,1,CH,A,                                              00220003
                55,3,CH,A,                                              00230003
                6,13,CH,A,                                              00240003
                4,1,CH,A,                                               00250003
                22,1,CH,A)                                              00260003
//*SORT FIELDS=(                                                        00270003
//SORTWK01  DD SPACE=(TRK,150),                                         00280003
//             UNIT=SYSDA                                               00290003
//SORTWK02  DD SPACE=(TRK,150),                                         00300003
//             UNIT=SYSDA                                               00310003
//SORTWK03  DD SPACE=(TRK,150),                                         00320003
//             UNIT=SYSDA                                               00330003
//SORTWK04  DD SPACE=(TRK,150),                                         00340003
//             UNIT=SYSDA                                               00350003
//SYSOUT    DD SYSOUT=*                                                 00360003
//*====================================================                 00370003
//REPORT   EXEC PGM=BSL211B                                             00380003
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*
//BUDMAS    DD DSN=&&BSL0200S,                                          00430003
//             DISP=(OLD,DELETE)                                        00440003
//SYSCTLI   DD DSN=FISP.BSL9000.SD.FILE,                                00450003
//             DISP=SHR                                                 00460003
//TABLES    DD DSN=FISP.BSL3100.SD.FILE,                                00470003
//             DISP=SHR                                                 00480003
//REPORT    DD DSN=FISP.BSL21B.BSL211B.REPORT,                          00490003
//             DISP=(NEW,CATLG,DELETE),                                 00500003
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 00510003
//             SPACE=(TRK,(150,150),RLSE),                              00520003
//             UNIT=SYSDA                                               00530003
//SYSOUT    DD SYSOUT=*                                                 00540003
//SYSUDUMP  DD SYSOUT=D,                                                00550003
//             DEST=LOCAL                                               00560003
//SYSDBOUT  DD SYSOUT=*                                                 00570003
//*====================================================                 00580003
//PRINTSTP EXEC PGM=IEBGENER                                            00590003
//SYSUT1    DD DSN=FISP.BSL21B.BSL211B.REPORT,                          00490003
//             DISP=SHR                                                 00610003
//SYSUT2    DD SYSOUT=(,),                                              00620003
//             OUTPUT=(*.OUT1,*.OUT2)                                   00630003
//SYSIN     DD DUMMY                                                    00640003
//SYSPRINT  DD SYSOUT=*                                                 00650003
/*                                                                      00660003
//*==================================================================== 00008500
//*       SEND MAIL NOTICES                                             00008600
//*==================================================================== 00008700
//*                                                                     00008800
//PDFMAIL1    EXEC PDFMAILT,
//            JOB=BSL21B,
//            IN=FISP.BSL21B.BSL211B.REPORT                             00490003
//*
//*============================================================         00670003
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00680003
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00690003
//*============================================================         00700003
//STOPZEKE EXEC STOPZEKE                                                00710003
//*                                                                     00720003
//*================ E N D  O F  J C L  BSL21B  =========                00730003
