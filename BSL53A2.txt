//BSL53A2 JOB (ACCT),'BUDGET - 355 TPCS',NOTIFY=APCDBM,                 00010060
//             CLASS=E,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M             00020012
//OUT1 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BUDGET,COPIES=1                00030017
//OUT2 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BACKUP,COPIES=1                00040017
//OUT3 OUTPUT CLASS=F,FORMS=BS11,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET2  00070011
//*====================================================
//* *N.T.E.*
//*
//* TURN OFF PRINTING AS OF 4/2009. HUGO
//*====================================================                 00080011
//* JOBSTREAM: BSL530 - JOB TO PRINT BSL5305                            00090011
//*       L#1=A1,A2,A3 ; L #38=1,2,3                                    00100011
//*====================================================                 00140011
//*              REMOVE POTENTIAL OUTPUT FILES                          00001900
//*====================================================                 00002000
//CLEAN01  EXEC PGM=IEFBR14                                             00002100
//SYSPRINT  DD SYSOUT=*                                                 00002200
//DD1       DD DSN=FISP.BSL53A.BSL500B.REPORT,                          00400011
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//DD2       DD DSN=FISP.BSL53A.BSL530B.REPORT,                          01050011
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//*====================================================                 00001800
//SELECT   EXEC PGM=BSL500B                                             00150011
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//*STEPLIB   DD DSN=FISQ.FIS.LOADLIB,DISP=SHR
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00200011
//             DISP=SHR                                                 00210011
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                00220011
//             DISP=SHR                                                 00230011
//MASTIN    DD DSN=FISP.BSL0200.SD.FILE,                                00240011
//             DISP=SHR                                                 00250011
//RPTCCIN   DD *                                                        00260011
BSL530 DET7   2                                                         00270060
//* SELECTION, TRANSLATION, AND SORTING OPTIONS                         00280011
//* SEE SECTION 4 OF THE OPERATIONS MANUAL                              00290011
//MASTOUT   DD DSN=&&BSL5000,                                           00300011
//             DISP=(NEW,PASS,DELETE),                                  00310011
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00320011
//             SPACE=(TRK,(2250,75),RLSE),                              00330011
//             UNIT=VIO                                                 00340011
//RPTCCOUT  DD DSN=&&RPTCCOUT,                                          00350011
//             DISP=(NEW,PASS,DELETE),                                  00360011
//             DCB=(LRECL=80,BLKSIZE=80,RECFM=F),                       00370011
//             SPACE=(TRK,1),                                           00380011
//             UNIT=VIO                                                 00390011
//REPORT    DD DSN=FISP.BSL53A.BSL500B.REPORT,                          00400011
//             DISP=(NEW,CATLG,DELETE),                                 00360011
//             DCB=(LRECL=133,BLKSIZE=27930,RECFM=FBA),                 00370011
//             SPACE=(TRK,(15,15),RLSE),                                00380011
//             UNIT=SYSDA                                               00390011
//SYSOUT    DD SYSOUT=*                                                 00410011
//SYSUDUMP  DD SYSOUT=D,                                                00420011
//             DEST=LOCAL                                               00430011
//SYSDBOUT  DD SYSOUT=*                                                 00440011
//*==================================================================== 00008500
//*       SEND MAIL NOTICES                                             00008600
//*==================================================================== 00008700
//*                                                                     00008800
//PDFMAIL1    EXEC PDFMAILT,
//            JOB=BSL53AA,
//            IN=FISP.BSL53A.BSL500B.REPORT                             00400011
//*
//*====================================================                 00450011
//SORT     EXEC PGM=SYNCSORT                                            00460011
//SORTIN    DD DSN=&&BSL5000,                                           00470011
//             DISP=(OLD,DELETE)                                        00480011
//SORTOUT   DD DSN=&&BSL5000S,                                          00490011
//             DISP=(NEW,PASS,DELETE),                                  00500011
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00510011
//             SPACE=(TRK,(2250,75),RLSE),                              00520011
//             UNIT=VIO                                                 00530011
//SYSIN     DD *                                                        00540011
*  SORT FIELDS=(467,45,CH,A)                                            00550011
      SORT FIELDS=(467,02,CH,A,   LOCATION-1                            00560011
                   469,01,CH,A,   DIST-CODE                             00570011
                   470,01,CH,A,   SAU                                   00580011
                   471,01,CH,A,   SUB-CAMPUS-ID                         00590011
                   192,06,CH,A,   IFIS ORG                              00600011
                   204,06,CH,A,   IFIS PROGRAM                          00610011
                   198,06,CH,A,   IFIS ACCOUNT                          00620011
                   186,06,CH,A,   IFIS FUND                             00630011
                   467,02,CH,A,   LOCATION-1                            00640011
                   469,01,CH,A,   DIST-CODE                             00650011
                   470,01,CH,A,   SAU                                   00660011
                   471,01,CH,A,   SUB-CAMPUS-ID                         00670011
                   472,01,CH,A,   FUND-TYPE                             00680011
                   473,02,CH,A,   FUNCTION-CODE-1                       00690011
                   475,02,CH,A,   COLLEGE-CD                            00700011
                   477,02,CH,A,   FUND-GROUP                            00710011
                   479,07,CH,A,   ACCOUNT-NO-1 OR FUND-NO-1             00720011
                   486,02,CH,A,   SUB-BUDGET-CD-1 OR FUNCTION-CD-2      00730011
                   488,07,CH,A,   ACCOUNT-NO-2 OR FUND-NO-2             00740011
                   495,02,CH,A,   SUB-BUDGET-CD-2                       00750011
                   497,01,CH,A,   RECORD-TYPE                           00760011
                   498,01,CH,A,   TRANS-CLASS                           00770011
                   499,05,CH,A,   TRANS-DOC-NO                          00780011
                   504,02,CH,A,   TRANS-DOC-SUF                         00790011
                   506,02,CH,A,   LOCATION-2                            00800011
                   508,04,CH,A)   FILLER                                00810011
//SORTWK01  DD SPACE=(TRK,150),                                         00820011
//             UNIT=SYSDA                                               00830011
//SORTWK02  DD SPACE=(TRK,150),                                         00840011
//             UNIT=SYSDA                                               00850011
//SORTWK03  DD SPACE=(TRK,150),                                         00860011
//             UNIT=SYSDA                                               00870011
//SORTWK04  DD SPACE=(TRK,150),                                         00880011
//             UNIT=SYSDA                                               00890011
//SYSOUT    DD SYSOUT=*                                                 00900011
//*====================================================                 00910011
//REPORT   EXEC PGM=BSL530B                                             00920011
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00970011
//             DISP=SHR                                                 00980011
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                00990011
//             DISP=SHR                                                 01000011
//MASTIN    DD DSN=&&BSL5000S,                                          01010011
//             DISP=(OLD,DELETE)                                        01020011
//RPTCCIN   DD DSN=&&RPTCCOUT,                                          01030011
//             DISP=(OLD,DELETE)                                        01040011
//REPORT    DD DSN=FISP.BSL53A.BSL530B.REPORT,                          01050011
//             DISP=(NEW,CATLG,DELETE),                                 01060011
//             DCB=(LRECL=133,BLKSIZE=27930,RECFM=FBA),                 00370011
//             SPACE=(TRK,(1650,150),RLSE),                             01080011
//             UNIT=SYSDA                                               01090011
//SYSOUT    DD SYSOUT=*                                                 01100011
//SYSUDUMP  DD SYSOUT=D,                                                01110011
//             DEST=LOCAL                                               01120011
//SYSDBOUT  DD SYSOUT=*                                                 01130011
//*====================================================                 01140011
//PRINTSTP EXEC PGM=IEBGENER                                            01150011
//SYSUT1    DD DSN=FISP.BSL53A.BSL530B.REPORT,                          01160011
//             DISP=SHR                                                 01170011
//SYSUT2    DD SYSOUT=(,),                                              01180011
//             OUTPUT=(*.OUT1,*.OUT2)                                   01190011
//SYSIN     DD DUMMY                                                    01200011
//SYSPRINT  DD SYSOUT=*                                                 01210011
/*                                                                      01220011
//*==================================================================== 00008500
//*       SEND MAIL NOTICES                                             00008600
//*==================================================================== 00008700
//*                                                                     00008800
//PDFMAIL2    EXEC PDFMAILT,
//            JOB=BSL53AB,
//            IN=FISP.BSL53A.BSL530B.REPORT                             00400011
//*
//*============================================================         01230011
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          01240011
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      01250011
//*============================================================         01260011
//STOPZEKE EXEC STOPZEKE                                                01270011
//*                                                                     01280011
//*================ E N D  O F  J C L  BSL53A  =========                01290011
