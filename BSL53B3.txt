//BSL53B3  JOB (SYS000),'BUDGET - 355 TPCS',NOTIFY=APCDBM,              00000100
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M             00000200
//*====================================================                 00140011
//* *N.T.E.*
//*====================================================
//*              REMOVE POTENTIAL OUTPUT FILES                          00001900
//*====================================================                 00002000
//CLEAN01  EXEC PGM=IEFBR14                                             00002100
//SYSPRINT  DD SYSOUT=*                                                 00002200
//DD1       DD DSN=FISP.BSL53B.BSLR53B3.REPORT,                         00400011
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//*====================================================                 00000600
//* JOBSTREAM: BSL53B                                                   00000700
//*====================================================                 00000800
//SELECT   EXEC PGM=BSL500B                                             00000900
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//*STEPLIB   DD DSN=FISQ.FIS.LOADLIB,DISP=SHR
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00001400
//             DISP=SHR                                                 00001500
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                00001600
//             DISP=SHR                                                 00001700
//MASTIN    DD DSN=FISP.BSL0200.SD.FILE,                                00001800
//             DISP=SHR                                                 00001900
//RPTCCIN   DD *                                                        00002000
BSL531 SUM7   3
//* SELECTION, TRANSLATION, AND SORTING OPTIONS                         00002100
//* SEE SECTION 4 OF THE OPERATIONS MANUAL                              00002200
//MASTOUT   DD DSN=&&BSL5000,                                           00002300
//             DISP=(NEW,PASS,DELETE),                                  00002400
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00002500
//             SPACE=(TRK,(750,75),RLSE),                               00002600
//             UNIT=VIO                                                 00002700
//RPTCCOUT  DD DSN=&&RPTCCOUT,                                          00002800
//             DISP=(NEW,PASS,DELETE),                                  00002900
//             DCB=(LRECL=80,BLKSIZE=80,RECFM=F),                       00003000
//             SPACE=(TRK,1),                                           00003100
//             UNIT=VIO                                                 00003200
//REPORT    DD SYSOUT=*                                                 00003300
//SYSOUT    DD SYSOUT=*                                                 00003400
//SYSUDUMP  DD SYSOUT=D,                                                00003500
//             DEST=LOCAL                                               00003600
//SYSDBOUT  DD SYSOUT=*                                                 00003700
//*====================================================                 00003800
//SORT     EXEC PGM=SYNCSORT                                            00003900
//SORTIN    DD DSN=&&BSL5000,                                           00004000
//             DISP=(OLD,DELETE)                                        00004100
//SORTOUT   DD DSN=&&BSL5000S,                                          00004200
//             DISP=(NEW,PASS,DELETE),                                  00004300
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00004400
//             SPACE=(TRK,(750,75),RLSE),                               00004500
//             UNIT=VIO                                                 00004600
//SYSIN     DD *                                                        00004700
   SORT FIELDS=(467,45,CH,A)
//*SORT FIELDS=(                                                        00004800
//SORTWK01  DD SPACE=(TRK,150),                                         00004900
//             UNIT=SYSDA                                               00005000
//SORTWK02  DD SPACE=(TRK,150),                                         00005100
//             UNIT=SYSDA                                               00005200
//SORTWK03  DD SPACE=(TRK,150),                                         00005300
//             UNIT=SYSDA                                               00005400
//SORTWK04  DD SPACE=(TRK,150),                                         00005500
//             UNIT=SYSDA                                               00005600
//SYSOUT    DD SYSOUT=*                                                 00005700
//SYSPRINT  DD SYSOUT=*                                                 00005800
//*====================================================                 00005900
//REPORT   EXEC PGM=BSL531B                                             00006000
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00006900
//             DISP=SHR                                                 00007000
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                00007100
//             DISP=SHR                                                 00007200
//MASTIN    DD DSN=&&BSL5000S,                                          00007300
//             DISP=(OLD,DELETE)                                        00007400
//RPTCCIN   DD DSN=&&RPTCCOUT,                                          00007500
//             DISP=(OLD,DELETE)                                        00007600
//REPORT    DD DSN=FISP.BSL53B.BSLR53B3.REPORT,                         00400011
//             DISP=(NEW,CATLG,DELETE),                                 01060011
//             DCB=(LRECL=133,BLKSIZE=27930,RECFM=FBA),                 00370011
//             SPACE=(TRK,(1650,150),RLSE),                             01080011
//             UNIT=SYSDA                                               01090011
//SYSOUT    DD SYSOUT=*                                                 00008200
//SYSUDUMP  DD SYSOUT=D,                                                00008300
//             DEST=LOCAL                                               00008400
//SYSDBOUT  DD SYSOUT=*                                                 00008500
//*====================================================                 00008600
//*----------------------------------------------------------------- */
//*  CONVERT THE REPORT FILE TO PDF AND EMAIL                        */
//*----------------------------------------------------------------- */
//PDFMAIL     EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.EXEC
//RPTFILE     DD   DSN=FISP.BSL53B.BSLR53B3.REPORT,DISP=SHR
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(CYL,(1,1)),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT    DD   SYSOUT=*
//SYSTSPRT    DD   SYSOUT=*
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(BSL53B3)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(BSL53B3)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(BSL53B3)
//*
//*============================================================         00009500
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00009600
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00009700
//*============================================================         00009800
//STOPZEKE EXEC STOPZEKE                                                00009900
//*                                                                     00010000
//*================ E N D  O F  J C L  BSL53B  =========                00010100
