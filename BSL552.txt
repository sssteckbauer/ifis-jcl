//BSL552 JOB (ACCT),'BUDGET - 355 TPCS',NOTIFY=APCDBM,                  00010054
//           CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M               00020042
//OUT1 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BUDGET,COPIES=1                00030053
//OUT2 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BACKUP,COPIES=1                00040053
//OUT5 OUTPUT CLASS=F,FORMS=BS11,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET2  00070006
//*====================================================                 00080006
//* *N.T.E.*
//*
//* TURN OFF PRINTING AS OF 4/2009. HUGO
//*====================================================                 00080006
//* JOBSTREAM: BSL550 - JOB TO PRINT REPORT BSL550                      00090006
//*                    NORMAL OUTPUT= ONE COPY DUPLEX                   00100012
//*     FOR SPECIAL REQUESTS, OUTPUT= ONE COPY SIMPLEX                  00120006
//*
//* L#1=551,552,553 ; L#41=1,2,3 (COL15)
//*====================================================                 00130006
//*              REMOVE POTENTIAL OUTPUT FILES                          00001900
//*====================================================                 00002000
//CLEAN01  EXEC PGM=IEFBR14                                             00002100
//SYSPRINT  DD SYSOUT=*                                                 00002200
//DD1       DD DSN=FISP.BSL55.BSL500B.REPORT,                           00390006
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//DD2       DD DSN=FISP.BSL55.BSL550B.REPORT,                           00790006
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//*====================================================                 00001800
//SELECT   EXEC PGM=BSL500B                                             00140006
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//*STEPLIB   DD DSN=FISQ.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00190006
//             DISP=SHR                                                 00200006
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                00210006
//             DISP=SHR                                                 00220006
//MASTIN    DD DSN=FISP.BSL0200.SD.FILE,                                00230006
//             DISP=SHR                                                 00240006
//RPTCCIN   DD *                                                        00250006
BSL550 1117   2                                                         00260054
//* SELECTION, TRANSLATION, AND SORTING OPTIONS                         00270006
//* SEE SECTION 4 OF THE OPERATIONS MANUAL                              00280006
//MASTOUT   DD DSN=&&BSL5000,                                           00290006
//             DISP=(NEW,PASS,DELETE),                                  00300006
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00310006
//             SPACE=(TRK,(750,75),RLSE),                               00320006
//             UNIT=VIO                                                 00330006
//RPTCCOUT  DD DSN=&&RPTCCOUT,                                          00340006
//             DISP=(NEW,PASS,DELETE),                                  00350006
//             DCB=(LRECL=80,BLKSIZE=80,RECFM=F),                       00360006
//             SPACE=(TRK,1),                                           00370006
//             UNIT=VIO                                                 00380006
//REPORT    DD DSN=FISP.BSL55.BSL500B.REPORT,                           00390006
//             DISP=(NEW,CATLG,DELETE),                                 00350006
//             DCB=(LRECL=133,BLKSIZE=27930,RECFM=FBA),                 00360006
//             SPACE=(TRK,(15,15),RLSE),                                00370006
//             UNIT=SYSDA                                               00380006
//SYSOUT    DD SYSOUT=*                                                 00400006
//SYSUDUMP  DD SYSOUT=D,                                                00410006
//             DEST=LOCAL                                               00420006
//SYSDBOUT  DD SYSOUT=*                                                 00430006
//*==================================================================== 00008500
//*       SEND MAIL NOTICES                                             00008600
//*==================================================================== 00008700
//*                                                                     00008800
//PDFMAIL1    EXEC PDFMAILT,
//            JOB=BSL55A,
//            IN=FISP.BSL55.BSL500B.REPORT                              00390006
//*
//*====================================================                 00440006
//SORT     EXEC PGM=SYNCSORT                                            00450006
//SORTIN    DD DSN=&&BSL5000,                                           00460006
//             DISP=(OLD,DELETE)                                        00470006
//SORTOUT   DD DSN=&&BSL5000S,                                          00480006
//             DISP=(NEW,PASS,DELETE),                                  00490006
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00500006
//             SPACE=(TRK,(750,75),RLSE),                               00510006
//             UNIT=VIO                                                 00520006
//SYSIN     DD *                                                        00530006
   SORT FIELDS=(467,45,CH,A)                                            00540006
//SORTWK01  DD SPACE=(TRK,150),                                         00550006
//             UNIT=SYSDA                                               00560006
//SORTWK02  DD SPACE=(TRK,150),                                         00570006
//             UNIT=SYSDA                                               00580006
//SORTWK03  DD SPACE=(TRK,150),                                         00590006
//             UNIT=SYSDA                                               00600006
//SORTWK04  DD SPACE=(TRK,150),                                         00610006
//             UNIT=SYSDA                                               00620006
//SYSOUT    DD SYSOUT=*                                                 00630006
//SYSPRINT  DD SYSOUT=*                                                 00640006
//*====================================================                 00650006
//REPORT   EXEC PGM=BSL550B                                             00660006
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*
//SYSCTLI   DD DSN=FISP.BSL9000.SD.FILE,                                00710006
//             DISP=SHR                                                 00720006
//TABLES    DD DSN=FISP.BSL3100.SD.FILE,                                00730006
//             DISP=SHR                                                 00740006
//BUDMAS    DD DSN=&&BSL5000S,                                          00750006
//             DISP=(OLD,DELETE)                                        00760006
//RPTCNTL   DD DSN=&&RPTCCOUT,                                          00770006
//             DISP=(OLD,DELETE)                                        00780006
//REPORT    DD DSN=FISP.BSL55.BSL550B.REPORT,                           00790006
//             DISP=(NEW,CATLG,DELETE),                                 00800006
//             DCB=(LRECL=133,BLKSIZE=27930,RECFM=FBA),                 00360006
//             SPACE=(TRK,(150,150),RLSE),                              00820006
//             UNIT=SYSDA                                               00830006
//SYSOUT    DD SYSOUT=*                                                 00840006
//SYSUDUMP  DD SYSOUT=D,                                                00850006
//             DEST=LOCAL                                               00860006
//SYSDBOUT  DD SYSOUT=*                                                 00870006
//*====================================================                 00880006
//PRINTSTP EXEC PGM=IEBGENER                                            00890006
//SYSUT1    DD DSN=FISP.BSL55.BSL550B.REPORT,                           00900006
//             DISP=SHR                                                 00910006
//SYSUT2    DD SYSOUT=(,),                                              00920006
//             OUTPUT=(*.OUT1,*.OUT2)                                   00930006
//SYSIN     DD DUMMY                                                    00940006
//SYSPRINT  DD SYSOUT=*                                                 00950006
/*                                                                      00960006
//*==================================================================== 00008500
//*       SEND MAIL NOTICES                                             00008600
//*==================================================================== 00008700
//*                                                                     00008800
//PDFMAIL2    EXEC PDFMAILT,
//            JOB=BSL55B,
//            IN=FISP.BSL55.BSL550B.REPORT                              00900006
//*
//*============================================================         00970006
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00980006
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00990006
//*============================================================         01000006
//STOPZEKE EXEC STOPZEKE                                                01010006
//*                                                                     01020006
//*================ E N D  O F  J C L  BSL55  ==========                01030006
