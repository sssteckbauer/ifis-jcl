//BSL56A   JOB (SYS000),'BUDGET - 355 TPCS',NOTIFY=APCDBM,              00010009
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M             00020007
//OUT1 OUTPUT CLASS=F,FORMS=BS11,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET   00030005
//*====================================================                 00040005
//* JOBSTREAM: BSL56A                                                   00050005
//*                                                                     00060005
//*  NOTE:  THIS JOB ALSO PRODUCES DATASETS FOR BUDGET OFFICE           00070005
//*        (TO FORWARD TO UCSD SCHOOL OF MEDICINE (SOM))                00080005
//*     ** N.T.E. **
//*====================================================                 00090005
//STEP1    EXEC PGM=BSL500B                                             00100005
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//*STEPLIB   DD DSN=FISQ.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00150005
//             DISP=SHR                                                 00160005
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                00170005
//             DISP=SHR                                                 00180005
//MASTIN    DD DSN=FISP.BSL0200.SD.FILE,                                00190005
//             DISP=SHR                                                 00200005
//RPTCCIN   DD *                                                        00210005
BSL530 DET7   1                                                         00220005
//* SELECTION, TRANSLATION, AND SORTING OPTIONS                         00230005
//* SEE SECTION 4 OF THE OPERATIONS MANUAL                              00240005
//MASTOUT   DD DSN=&&BSL5000,                                           00250005
//             DISP=(NEW,PASS,DELETE),                                  00260005
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00270005
//             SPACE=(TRK,(750,75),RLSE),                               00280005
//             UNIT=VIO                                                 00290005
//RPTCCOUT  DD DSN=&&RPTCCOUT,                                          00300005
//             DISP=(NEW,PASS,DELETE),                                  00310005
//             DCB=(LRECL=80,BLKSIZE=80,RECFM=F),                       00320005
//             SPACE=(TRK,(750,75),RLSE),                               00330005
//             UNIT=VIO                                                 00340005
//REPORT    DD SYSOUT=*                                                 00350005
//SYSOUT    DD SYSOUT=*                                                 00360005
//SYSUDUMP  DD SYSOUT=D,                                                00370005
//             DEST=LOCAL                                               00380005
//SYSDBOUT  DD SYSOUT=*                                                 00390005
//*====================================================                 00400005
//STEP6    EXEC PGM=SYNCSORT                                            00410005
//SORTIN    DD DSN=&&BSL5000,                                           00420005
//             DISP=(OLD,PASS)                                          00430005
//SORTOUT   DD DSN=&&BSL5000S,                                          00440005
//             DISP=(NEW,PASS,DELETE),                                  00450005
//             DCB=(LRECL=511,BLKSIZE=25550,RECFM=FB),                  00460005
//             SPACE=(TRK,(750,75),RLSE),                               00470005
//             UNIT=VIO                                                 00480005
//SYSIN     DD *                                                        00490005
      SORT FIELDS=(467,02,CH,A,   LOCATION-1                            00500005
                   469,01,CH,A,   DIST-CODE                             00510005
                   470,01,CH,A,   SAU                                   00520005
                   471,01,CH,A,   SUB-CAMPUS-ID                         00530005
                   192,06,CH,A,   IFIS ORG                              00540005
                   204,06,CH,A,   IFIS PROGRAM                          00550005
                   198,06,CH,A,   IFIS ACCOUNT                          00560005
                   186,06,CH,A,   IFIS FUND                             00570005
                   467,02,CH,A,   LOCATION-1                            00580005
                   469,01,CH,A,   DIST-CODE                             00590005
                   470,01,CH,A,   SAU                                   00600005
                   471,01,CH,A,   SUB-CAMPUS-ID                         00610005
                   472,01,CH,A,   FUND-TYPE                             00620005
                   473,02,CH,A,   FUNCTION-CODE-1                       00630005
                   475,02,CH,A,   COLLEGE-CD                            00640005
                   477,02,CH,A,   FUND-GROUP                            00650005
                   479,07,CH,A,   ACCOUNT-NO-1 OR FUND-NO-1             00660005
                   486,02,CH,A,   SUB-BUDGET-CD-1 OR FUNCTION-CD-2      00670005
                   488,07,CH,A,   ACCOUNT-NO-2 OR FUND-NO-2             00680005
                   495,02,CH,A,   SUB-BUDGET-CD-2                       00690005
                   497,01,CH,A,   RECORD-TYPE                           00700005
                   498,01,CH,A,   TRANS-CLASS                           00710005
                   499,05,CH,A,   TRANS-DOC-NO                          00720005
                   504,02,CH,A,   TRANS-DOC-SUF                         00730005
                   506,02,CH,A,   LOCATION-2                            00740005
                   508,04,CH,A)   FILLER                                00750005
           INCLUDE COND=(469,1,CH,EQ,C'L',OR,                           00760005
                         22,1,BI,EQ,X'00')                              00770005
//SORTWK01  DD SPACE=(TRK,150),                                         00780005
//             UNIT=SYSDA                                               00790005
//SORTWK02  DD SPACE=(TRK,150),                                         00800005
//             UNIT=SYSDA                                               00810005
//SORTWK03  DD SPACE=(TRK,150),                                         00820005
//             UNIT=SYSDA                                               00830005
//SORTWK04  DD SPACE=(TRK,150),                                         00840005
//             UNIT=SYSDA                                               00850005
//SYSOUT    DD SYSOUT=*                                                 00860005
//SYSPRINT  DD SYSOUT=*                                                 00870005
//*====================================================                 00880005
//STEP7    EXEC PGM=IEFBR14                                             00890005
//SOM5305   DD DSN=FISP.SOM.VC.FISP5305,                                00900005
//             DISP=(MOD,DELETE,DELETE),                                00910005
//             DCB=(RECFM=FBA,BLKSIZE=133,LRECL=133),                   00920005
//             SPACE=(TRK,(750,75),RLSE),                               00930005
//             UNIT=SYSDA                                               00940005
//SOM5000   DD DSN=FISP.SOM.VC.FISP5000,                                00950005
//             DISP=(MOD,DELETE,DELETE),                                00960005
//             DCB=(RECFM=FB,BLKSIZE=466,LRECL=466),                    00970005
//             SPACE=(TRK,(750,75),RLSE),                               00980005
//             UNIT=SYSDA                                               00990005
//*====================================================                 01000005
//STEP7A   EXEC PGM=BSL530B                                             01010005
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                01060005
//             DISP=SHR                                                 01070005
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                01080005
//             DISP=SHR                                                 01090005
//MASTIN    DD DSN=&&BSL5000S,                                          01100005
//             DISP=(OLD,PASS)                                          01110005
//RPTCCIN   DD DSN=&&RPTCCOUT,                                          01120005
//             DISP=(OLD,PASS)                                          01130005
//REPORT    DD DSN=FISP.SOM.VC.FISP5305,                                01140005
//             DISP=(NEW,CATLG,DELETE),                                 01150005
//             DCB=(RECFM=FBA,BLKSIZE=133,LRECL=133),                   01160005
//             SPACE=(TRK,(750,75),RLSE),                               01170005
//             UNIT=SYSALLDA,                                           01180005
//             VOL=SER=APC005                                           01190005
//SYSOUT    DD SYSOUT=*                                                 01200005
//SYSUDUMP  DD SYSOUT=D,                                                01210005
//             DEST=LOCAL                                               01220005
//SYSDBOUT  DD SYSOUT=*                                                 01230005
//*====================================================                 01240005
//STEP8    EXEC PGM=SYNCSORT                                            01250005
//SORTIN    DD DSN=&&BSL5000S,                                          01260005
//             DISP=(OLD,DELETE)                                        01270005
//SORTOUT   DD DSN=FISP.SOM.VC.FISP5000,                                01280005
//             DISP=(NEW,CATLG,DELETE),                                 01290005
//             DCB=(RECFM=FB,BLKSIZE=466,LRECL=466),                    01300005
//             SPACE=(TRK,(750,75),RLSE),                               01310005
//             UNIT=SYSALLDA,                                           01320005
//             VOL=SER=APC005                                           01330005
//SYSIN     DD *                                                        01340005
           SORT FIELDS=COPY                                             01350005
           INCLUDE COND=(469,1,CH,EQ,C'L')                              01360005
           OUTFIL OUTREC=(1:1,466)                                      01370005
//SYSOUT    DD SYSOUT=*                                                 01380005
//SYSPRINT  DD SYSOUT=*                                                 01390005
//*=====================================================
//STOPZEKE EXEC STOPZEKE                                                01400005
//*================ E N D  O F  J C L  BSL56A   ========                01410005
