//BSL70    JOB (SYS000),'BUDGET - 355 TPCS',
//*    RESTART=EXTR020,
//*    RESTART=EXTR010,
//       CLASS=K,MSGLEVEL=(1,1),MSGCLASS=F,
//       NOTIFY=APCDBM,REGION=0M
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 2998                DO NOT PRINT JCL (1/10/03)
//*  CURRENT VALUE OF BSL70C1=19        FOR BUDGET/STAFFING
//*++++++++++++++++++++++++++           NO LONGER NEEDED
//*
//* UPDATE PARM MEMBER BSL70C1 WITH FIRST RUN OF NEW FISCAL YEAR
//* ( TO REFLECT THE VALUE OF THE NEW FISCAL YEAR )
//* ( ** WHEN PROCESSING FIRST TIME IN NEW FISCAL YEAR - THIS
//*   JOB WILL ABEND UNLESS STAFFING JOBS ARE RUN FIRST ** )
//* SEND ALL OUTPUT FROM THIS JOB TO THE BUDGET OFFICE.
//*----------------------------------------------------                 00210041
//JOBLIB  DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//        DD DSN=DB2P.DCA.LOAD,DISP=SHR
//        DD DSN=DB2F.DSNEXIT,DISP=SHR
//        DD DSN=DB2F.DSNLOAD,DISP=SHR
//*                                                                     00120036
//OUT1 OUTPUT CLASS=Z,FORMS=BS02,GROUPID=BUDGET                         00160059
//OUT2 OUTPUT CLASS=Z,FORMS=BS02,GROUPID=BUDGET2                        00170059
//OUT3 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BACKUP
//OUT4 OUTPUT CLASS=F,FORMS=BS11,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET3
//*                                                                     00082055
//**********************************************************************
//**********************************************************************
//*               BSL/RSL EXTRACT TO DARWIN INTERFACE
//*
//*  PURPOSE: TO PRODUCE THE BSL/RSL EXTRACT FILES FOR FOR UPLOAD TO
//*           BAS SYBASE DATABASE ON DARWIN1.
//*
//**********************************************************************
//**********************************************************************
//*                                                                    *
//BSL70P  PROC GROUPID='FISP',                           TEST OR PROD?
//             OUTX='(,),OUTPUT=*.OUT2',                 SYSTEM SYSOUT  00310055
//             OUTU='(,),OUTPUT=(*.OUT1,*.OUT3)',        USER SYSOUT    00320055
//             DELCTL='FISP.PARMLIB(BSL70D1)',           IDCAMS DELETE
//             PRMCTL='FISP.PARMLIB(BSL70C1)'            BSL/SLPGB700
//*                                                                     00210041
//**********************************************************************00220041
//**********************************************************************00230041
//*  RUN IDCAMS UTILITY - CLEAN UP OLD INPUT/OUTPUT DATASETS            00240041
//*                                                                     00250041
//*  FILES:                                                             00260041
//*         SYSIN  :  PARAMETER FILE CONTAINING -                       00270041
//*                    EXTRACT FILES OF BUDGET SYSTEM LOCAL (BSL) AND   00280041
//*                    REPLACEMENT STAFFING LOCAL (RSL) DATA FROM       00280041
//*                    FROM PREVIOUS RUN                                00290041
//*                                                                     00300041
//**********************************************************************00310041
//**********************************************************************00320041
//*                                                                     00330041
//CLEAN000 EXEC PGM=IDCAMS                                              00340041
//SYSABEND  DD SYSOUT=&OUTX,HOLD=YES                                    00350041
//SYSPRINT  DD SYSOUT=&OUTX                                             00360041
//SYSIN     DD DSN=&DELCTL,                                             00370041
//             DISP=SHR                                                 00380041
//*
//STEP01   EXEC PGM=IEFBR14
//DD1       DD DSN=FISP.SLP.TITLECD.TCI,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=FB,LRECL=287,BLKSIZE=27839),
//             SPACE=(TRK,(75)),
//             UNIT=SYSDA
//*
//*====================================================
//*  STEP02: SORT TITLE CODE FILE BY TITLE CODE
//*====================================================
//STEP02   EXEC PGM=SYNCSORT,COND=(0,NE)
//SORTIN    DD DSN=PPSP.SLP.TITLECD.TCI,
//             DISP=SHR
//SORTOUT   DD DSN=FISP.SLP.TITLECD.TCI,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=287,BLKSIZE=27839),
//             SPACE=(TRK,(75)),
//             UNIT=VIO
//SYSIN     DD DSN=FISP.PARMLIB(BSL70S),DISP=SHR
//SYSOUT    DD SYSOUT=*
//SORTWK01  DD SPACE=(TRK,30),
//             UNIT=WORK
//SORTWK02  DD SPACE=(TRK,30),
//             UNIT=WORK
//SORTWK03  DD SPACE=(TRK,30),
//             UNIT=WORK
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//*
//**********************************************************************00310041
//**********************************************************************00320041
//*  RUN BSL700B PROGRAM - EXTRACT BSL DATA FOR DARWIN
//*
//*  FILES:                                                             00260041
//*         BUDCTLI :  DATA FILE OF BSL SBUDGET CONTROL DATA            00270041
//*         BUDTBLI :  DATA FILE OF BSL BUDGET TABLE DATA               00270041
//*         BUDMSTI :  DATA FILE OF BSL BUDGET MASTER DATA              00270041
//*         PRMCTLI :  PARAMETER FILE OF CURRENT FISCAL YEAR            00270041
//*         BUDCTLO :  EXTRACT FILE OF BUDGET CONTROL DATA              00270041
//*         BUDSCTO :  EXTRACT FILE OF BUDGET SUBCAMPUS DATA            00270041
//*         BUDSBTO :  EXTRACT FILE OF BUDGET SUBBUDGET DATA            00270041
//*         BUDVCTO :  EXTRACT FILE OF BUDGET VICE CHANCELLOR           00270041
//*                    DISTRIBUTION DATA                                00270041
//*         BUDMSTO :  EXTRACT FILE OF BUDGET MASTER DATA               00270041
//*         BUDXTNO :  EXTRACT FILE OF BUDGET TRANSACTION DATA          00270041
//*
//**********************************************************************00310041
//**********************************************************************00320041
//*
//EXTR010 EXEC PGM=BSL700B,COND=(0,NE)
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*
//BUDCTLI  DD DSN=&GROUPID..BSL9000.SD.FILE,
//            DISP=SHR
//BUDTBLI  DD DSN=&GROUPID..BSL3100.SD.FILE,
//            DISP=SHR
//BUDMSTI  DD DSN=&GROUPID..BSL0200.SD.FILE,
//            DISP=SHR
//BUDCTLO  DD DSN=&GROUPID..BSL7010.BUDCTL.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=34,RECFM=FB,BLKSIZE=23460),
//            SPACE=(TRK,(5,1),RLSE),UNIT=SYSDA
//BUDSCTO  DD DSN=&GROUPID..BSL7020.BUDSCT.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=47,RECFM=FB,BLKSIZE=23453),
//            SPACE=(TRK,(5,1),RLSE),UNIT=SYSDA
//BUDSBTO  DD DSN=&GROUPID..BSL7020.BUDSBT.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=48,RECFM=FB,BLKSIZE=23472),
//            SPACE=(TRK,(5,1),RLSE),UNIT=SYSDA
//BUDVCTO  DD DSN=&GROUPID..BSL7020.BUDVCT.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=47,RECFM=FB,BLKSIZE=23453),
//            SPACE=(TRK,(5,1),RLSE),UNIT=SYSDA
//BUDMSTO  DD DSN=&GROUPID..BSL7030.BUDMST.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=472,RECFM=FB,BLKSIZE=27848),
//            SPACE=(TRK,(75,15),RLSE),UNIT=SYSDA
//BUDXTNO  DD DSN=&GROUPID..BSL7035.BUDXTN.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=106,RECFM=FB,BLKSIZE=23426),
//            SPACE=(TRK,(75,15),RLSE),UNIT=SYSDA
//SYSOUT   DD SYSOUT=&OUTX
//SYSPRINT DD SYSOUT=&OUTU
//SYSDBOUT DD SYSOUT=&OUTX
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL
//PRMCTLI  DD DSN=&PRMCTL,                                              00370041
//            DISP=SHR                                                  00380041
//*
//**********************************************************************00310041
//**********************************************************************00320041
//*  RUN SLPGB700 PROGRAM - EXTRACT RSL DATA FOR DARWIN
//*
//*  FILES:                                                             00260041
//*         BUDTBLI  :  DATA FILE OF BSL BUDGET TABLE DATA              00270041
//*         TTLTBLI  :  DATA FILE OF PAYROLL TITLE TABLE DATA           00270041
//*         CONTROL  :  DATA FILE OF PAYROLL ACCESS DATA                00270041
//*         CONTROLR :  DATA FILE OF PAYROLL ACCESS DATA                00270041
//*         STFMSTI  :  DATA FILE OF RSL STAFFING MASTER DATA           00270041
//*         BUDMSTI  :  EXTRACT FILE OF BUDGET MASTER DATA              00270041
//*         BUDXTNI  :  EXTRACT FILE OF BUDGET TRANSACTION DATA         00270041
//*         PRMCTLI  :  PARAMETER FILE OF CURRENT FISCAL YEAR           00270041
//*         STFMSTO  :  EXTRACT FILE OF STAFFING MASTER DATA            00270041
//*         STFPDSO  :  EXTRACT FILE OF STAFFING PAYROLL DISTRIBUTION   00270041
//*                     DATA                                            00270041
//*         STFPXRO  :  EXTRACT FILE OF STAFFING PAYROLL CROSS          00270041
//*                     REFERENCE DATA                                  00270041
//*         STFPRVO  :  EXTRACT FILE OF STAFFING PROVISION DATA         00270041
//*         NOMPDSO  :  EXTRACT FILE OF STFPDSO MISMATCH DATA           00270041
//*         NOMPXRO  :  EXTRACT FILE OF STFPXRO MISMATCH DATA           00270041
//*         NOMPRVO  :  EXTRACT FILE OF STFPRVO MISMATCH DATA           00270041
//*
//**********************************************************************00310041
//**********************************************************************00320041
//*
//EXTR020 EXEC PGM=SLPGB700,COND=(0,NE)
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSLST    DD SYSOUT=*
//BUDTBLI  DD DSN=&GROUPID..BSL3100.SD.FILE,
//            DISP=SHR
//TTLTBLI  DD DSN=FISP.SLP.TITLECD.TCI,
//            DISP=SHR
//*CONTROL  DD DSN=PPSP.CTLV,
//*            DISP=SHR
//*CONTROLR DD DSN=PPSP.CTLV,
//*            DISP=SHR
//STFMSTI  DD DSN=&GROUPID..RSL.STFLST.MSTR,
//            DISP=SHR
//BUDMSTI  DD DSN=&GROUPID..BSL7030.BUDMST.FILE,
//            DISP=(OLD,KEEP,KEEP),
//            DCB=(LRECL=472,RECFM=FB,BLKSIZE=27848),
//            SPACE=(TRK,(75,15),RLSE),UNIT=SYSDA
//BUDXTNI  DD DSN=&GROUPID..BSL7035.BUDXTN.FILE,
//            DISP=(OLD,KEEP,KEEP),
//            DCB=(LRECL=106,RECFM=FB,BLKSIZE=23426),
//            SPACE=(TRK,(75,15),RLSE),UNIT=SYSDA
//STFMSTO  DD DSN=&GROUPID..SLEXT700.STFMST.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=69,RECFM=FB,BLKSIZE=23460),
//            SPACE=(TRK,(5,1),RLSE),UNIT=SYSDA
//STFPDSO  DD DSN=&GROUPID..SLEXT700.STFPDS.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=145,RECFM=FB,BLKSIZE=23345),
//            SPACE=(TRK,(75,15),RLSE),UNIT=SYSDA
//STFPXRO  DD DSN=&GROUPID..SLEXT700.STFPXR.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=158,RECFM=FB,BLKSIZE=23384),
//            SPACE=(TRK,(15,15),RLSE),UNIT=SYSDA
//STFPRVO  DD DSN=&GROUPID..SLEXT700.STFPRV.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=149,RECFM=FB,BLKSIZE=14900),
//            SPACE=(TRK,(15,15),RLSE),UNIT=SYSDA
//NOMPDSO  DD DSN=&GROUPID..SLEXT700.NMSTFPDS.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=137,RECFM=FB,BLKSIZE=13700),
//            SPACE=(TRK,(15,15),RLSE),UNIT=SYSDA
//NOMPXRO  DD DSN=&GROUPID..SLEXT700.NMSTFPXR.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=150,RECFM=FB,BLKSIZE=15000),
//            SPACE=(TRK,(15,15),RLSE),UNIT=SYSDA
//NOMPRVO  DD DSN=&GROUPID..SLEXT700.NMSTFPRV.FILE,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(LRECL=141,RECFM=FB,BLKSIZE=14100),
//            SPACE=(TRK,(15,15),RLSE),UNIT=SYSDA
//SYSOUT   DD SYSOUT=&OUTX
//SYSPRINT DD SYSOUT=&OUTU
//SYSDBOUT DD SYSOUT=&OUTX
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL
//PRMCTLI  DD DSN=&PRMCTL,                                              00370041
//            DISP=SHR                                                  00380041
//*                                                                     07210099
//**********************************************************************07220099
//**********************************************************************07230099
//*  RUN IEBGENER UTILITY - CONCATENATE ALL BUDGET & STAFFING FISCAL    07240099
//*                         YEAR DATASETS                               07240099
//*                                                                     07250099
//*  FILES:                                                             07260099
//*         SYSUT1    :  BUDGET & STAFFING FISCAL YEAR DATASETS         07270099
//*         SYSIN     :  DUMMY FILE                                     07280099
//*         SYSUT2    :  CONCATENATED FILE OF ALL B&S FSCYR DATASETS    07270099
//*                                                                     07300099
//*  NOTE:  THE DARWIN LOAD SCRIPTS HAVE BEEN CHANGED TO ACCEPT CURRENT 07300099
//*         YEAR FILES.  CONCATENATION OF PRIOR YEARS IS NO LONGER      07300099
//*         NECESSARY.                                                  07300099
//*                                                                     07300099
//**********************************************************************07310099
//**********************************************************************07320099
//*                                                                     07330099
//************************  E N D  O F  BSL70  *************************00310041
// PEND
//BSL70  EXEC BSL70P
//*============================================================
//*  SUBMIT BSL70 ARCHIVE JOB
//*============================================================
//SUBARCH  EXEC PGM=ZEKESET,COND=(0,NE)
//SYSPRINT  DD SYSOUT=*                                                 00000500
//SYSIN     DD *                                                        00000600
  SET SCOM 'ZADD EV 2500 REBUILD'                                       00006**2
/*                                                                      00000700
//*============================================================
//*  SUBMIT DATA WAREHOUSE DOWNLOAD TRIGGER
//*============================================================
//SUBDWZ22 EXEC PGM=ZEKESET,COND=(0,NE)
//SYSPRINT  DD SYSOUT=*                                                 00000500
//SYSIN     DD *                                                        00000600
  SET SCOM 'ZADD EV 2700 REBUILD'                                       00006**2
/*                                                                      00000700
//*                                                                     00000700
//*============================================================         00000700
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00000700
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00000700
//*============================================================         00000700
//STOPZEKE EXEC STOPZEKE                                                00000700
//*                                                                     00000700
//*================ E N D  O F  J C L  BSL70    ========                00250007
