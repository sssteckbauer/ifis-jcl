//CHDP300  JOB (SYS000),'UPDATE SPNOR CD',                              00000100
//            MSGLEVEL=(1,1),                                           00000200
//            MSGCLASS=F,                                               00000300
//            CLASS=K,                                                  00040002
//*           RESTART=UCHU305,                                          00040002
//            NOTIFY=ACLPLO,                                            00050002
//            REGION=20M                                                00060002
//************************                                              00001200
//*     ZEKE EV 1682                                                    00000800
//***************************************************************       00001200
//*                                                                     00000800
//*    THIS JOB IS TO UPDATE IFIS UCF TABLES FROM COES AWARD TABLE      00000800
//*    EXTRACT.  THIS JOB IS TO UPDATE AWARD TYPE, SPONSOR CODE         00000800
//*    SPONSOR CATAGORY AND CFDS.                                       00000800
//*                                                                     00000800
//*    FILE IS FTP'D FROM HOPPER UNDER DIRECTORY:                       00000800
//*
//*          /dw/coeus/extracts                                         00000800
//*
//*    FILE IS:            award_tier_mainframe.ext                     00000800
//*
//*    FTP'D TO M.F. AS:   'FISP.COEUS.AWARD.NONFMT'                    00000800
//* 'FISP.COEUS.SPNCD.NONFMT'                                           00000800
//*                                                                     00000800
//***************************************************************       00001200
//*              DELETE CATALOGED DATA SETS                             00001500
//*==================================================================== 00001600
//*                                                                     00001700
//DELEXT1  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00001800
//SYSPRINT  DD SYSOUT=*                                                 00001900
//SYSIN     DD *                                                        00002000
  DELETE    FISP.COEUS.AWARD
  DELETE    FISP.COEUS.AWARD.SORTED
  DELETE    FISP.COEUS.AWARD.DUP.OVERFL
  DELETE    FISP.COEUS.AWARD.NODUP.FUND
  DELETE    FISP.CHD300.UCHU305.CNTLRPT
  DELETE    FISP.COEUS.SPNCD
  DELETE    FISP.LOADARC.CNTLRPT
//*==================================================================== 00002100
//*   FORMATS  COUES UPLOADED FILES TO FLAT FILES                       00002200
//*==================================================================== 00002300
//*                                                                     00002400
//UUTXSTRN EXEC PGM=UNSTRNCO,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00002500
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 00003000
//INCOEUS   DD DSN=FISP.COEUS.AWARD.NONFMT,DISP=SHR                     00003100
//INSPN     DD DSN=FISP.COEUS.SPNCD.NONFMT,DISP=SHR
//OTCOEUS   DD DSN=FISP.COEUS.AWARD,                                    00003300
//             DISP=(NEW,CATLG,DELETE),                                 00003400
//             DCB=(LRECL=129,BLKSIZE=27993,RECFM=FB),                  00003500
//             SPACE=(TRK,(50,50),RLSE),                                00003600
//             UNIT=SYSDA                                               00003700
//OTSPN     DD DSN=FISP.COEUS.SPNCD,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(LRECL=200,BLKSIZE=27800,RECFM=FB),
//             SPACE=(TRK,(50,50),RLSE),
//             UNIT=SYSDA
//*
//*
//SORT010  EXEC PGM=SORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))             00014314
//SORTIN    DD DSN=FISP.COEUS.AWARD,DISP=OLD                            00003800
//SORTOUT   DD DSN=FISP.COEUS.AWARD.SORTED,                             00003800
//             DISP=(NEW,CATLG,DELETE),                                 00003400
//             DCB=(LRECL=129,BLKSIZE=27993,RECFM=FB),                  00003500
//             SPACE=(TRK,(50,50),RLSE),                                00003600
//             UNIT=SYSDA                                               00003700
//SYSIN     DD *                                                        00015215
  INCLUDE COND=(80,5,CH,GT,C'00000')
  SORT FIELDS=(80,6,CH,A,89,26,CH,D)                                    00041100
/*
//SORTWK01  DD SPACE=(CYL,(25,2),RLSE),                                 00015414
//             UNIT=SYSDA                                               00015514
//SORTWK02  DD SPACE=(CYL,(25,2),RLSE),                                 00015614
//             UNIT=SYSDA                                               00015714
//SORTWK03  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK04  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK05  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK06  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SYSOUT    DD SYSOUT=*                                                 00016014
//*
//*                                                                     00002400
//*====================================================
//*  UCHX304 READ THE SORTED FILE SELECT MOST CURRENT LEVEL5 FUND WITH *
//*  SUFFIX 'A' AND WHOSE LEVEL 4 IS ACTIVE
//*                                                                   *
//*====================================================
//UCHX304  EXEC PGM=UCHX304,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00002500
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UCHX304) PLAN(UCHX304)
/*
//SYSOUT    DD SYSOUT=*                                                 00003000
//INCOEUS2  DD DSN=FISP.COEUS.AWARD.SORTED,DISP=SHR                     00003100
//OTCOEUS2  DD DSN=FISP.COEUS.AWARD.DUP.OVERFL,                         00003300
//             DISP=(NEW,CATLG,DELETE),                                 00003400
//             DCB=(LRECL=81,BLKSIZE=27945,RECFM=FB),                   00003500
//             SPACE=(TRK,(50,50),RLSE),                                00003600
//             UNIT=SYSDA                                               00003700
/*
//*====================================================
//*  UCHX304 CAN NOT DEAL WITH OVER FLOW FUNDS, SO IT SORT BY LEVEL4   *
//*  AND UPDATE TIME STAMP.
//*                                                                   *
//*====================================================
//*
//SORT020  EXEC PGM=SORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))             00014314
//SORTIN    DD DSN=FISP.COEUS.AWARD.DUP.OVERFL,DISP=SHR                 00003800
//SORTOUT   DD DSN=FISP.COEUS.AWARD.NODUP.FUND,                         00003800
//             DISP=(NEW,CATLG,DELETE),                                 00003400
//             DCB=(LRECL=74,BLKSIZE=27972,RECFM=FB),                   00003500
//             SPACE=(TRK,(50,50),RLSE),                                00003600
//             UNIT=SYSDA                                               00003700
//SYSIN     DD *                                                        00015215
  SORT FIELDS=(1,6,CH,A,29,26,CH,D)                                     00041100
/*
//SORTWK01  DD SPACE=(CYL,(25,2),RLSE),                                 00015414
//             UNIT=SYSDA                                               00015514
//SORTWK02  DD SPACE=(CYL,(25,2),RLSE),                                 00015614
//             UNIT=SYSDA                                               00015714
//SORTWK03  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK04  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK05  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK06  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SYSOUT    DD SYSOUT=*                                                 00016014
//*
//*====================================================================
//*  UCHX304 LEVEL4 FILE AND UPDATE
//*  UCF TABLE FOR AWRD TYPE, SPONSOR CODE, SPN CATAGOR, CDF
//*  IT UPDATE FUND TABLE FOR STIP INDICATOR
//*
//*====================================================================
//UCHU305  EXEC PGM=UCHU305,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00220002
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UCHU305) PLAN(UCHU305)
/*
//SYSOUT    DD SYSOUT=*                                                 00320002
//SYSLST    DD SYSOUT=*                                                 00330002
//ERRRPT    DD SYSOUT=*                                                 00450002
//SYSUDUMP  DD SYSOUT=D,                                                00340002
//             DEST=LOCAL                                               00350002
//SYSIN     DD DUMMY                                                    00360002
//COEUSAWD  DD DSN=FISP.COEUS.AWARD.NODUP.FUND,DISP=SHR                 00470002
//CNTLRPT   DD DSN=FISP.CHD300.UCHU305.CNTLRPT,                         00320002
//             DISP=(NEW,CATLG,CATLG),                                  00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009800
//             SPACE=(TRK,(175,150),RLSE),                              00009900
//             UNIT=SYSDA                                               00010000
//**********************************************************************
//*
//*  LOAD SPN TABLE WITH NEW SPONSOR CODE
//*
//*
//**********************************************************************
//*
//LOADARC  EXEC PGM=LOADARC
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(LOADARC) PLAN(LOADARC)
/*
//UCOARC    DD DUMMY
//UCOUAS    DD DUMMY
//UCOSPN    DD DSN=FISP.COEUS.SPNCD,DISP=SHR
//CNTLRPT   DD DSN=FISP.LOADARC.CNTLRPT,
//             DISP=(NEW,CATLG,DELETE),                                 000
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 000
//             SPACE=(TRK,(50,10),RLSE),                                000
//             UNIT=SYSDA                                               000
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//*
//**********************************************************************
//* LOADDEPT - LOAD DEP_HOME_DT_UNIT TABLE FROM PPSP FILE
//*
//**********************************************************************
//LOADDEPT EXEC PGM=LOADDEPT
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(LOADDEPT) PLAN(LOADDEPT)
/*
//PPSPDEPT  DD DSN=PPSP.PXT.TABLE.DEPT,DISP=SHR
//CNTLRPT   DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//*
//****************************************************************
//* THIS PRODUCES THE PDFMAIL
//****************************************************************
//PDFOUT1     EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//STEPLIB     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.LOAD
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.LOAD
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//TXT1        DD   DISP=SHR,DSN=FISP.CHD300.UCHU305.CNTLRPT
//            DD   DISP=SHR,DSN=FISP.LOADARC.CNTLRPT
//PDF1        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//SYSPRINT    DD   SYSOUT=X
//SYSTSPRT    DD   SYSOUT=X
//SYSTSIN     DD   *
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT1                                                      +
     OUT DD:PDF1                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%XMITIP * +
  SUBJECT 'REPORT# - NIGHTLY UPDATE FROM COEUS DATA'                  +
  FROM ACT-PRODCONTROL@UCSD.EDU                                       -
  ADDRESSFILEDD ADDRLIST                                              -
  FILEDD (  PDF1 )                                                    -
  FILENAME (FILE1.PDF )                                               +
  MSGT 'REPORT RUN ON: &DAY &DATE &TIME, BY JOB: &JOB                 +
       \\\                                                            +
       \THIS REPORT IS VIEWABLE WITH ACROBAT READER.                  +
       \THE LATEST VERSION CAN BE DOWNLOADED FROM:                    +
       HTTP://WWW.ADOBE.COM/PRODINDEX/ACROBAT/READSTEP.HTML#READER +  +
       \\'
/*
//ADDRLIST    DD   *
TO  IFISHELPDESK@UCSD.EDU
CC  MMCGILL@UCSD.EDU
CC  DMESERVE@UCSD.EDU
CC  NCOWELL@UCSD.EDU
CC  DCHOCK@UCSD.EDU
/*
//*
//*==========  E N D  O F  J C L  -  L O A D U C A    ==========
//*================ E N D  O F  J C L  UNSTRNCO  ========               00010500
