//CHMP401  JOB (SYS000),'BEV - S003',                                   00000100
//            MSGLEVEL=(1,1),                                           00000200
//            MSGCLASS=F,                                               00000300
//            CLASS=K,                                                  00000400
//*====================================================                 00000500
//*    RESTART=DEVSM130,                                                00000600
//*====================================================                 00000700
//            NOTIFY=APCDBM,                                            00000800
//            REGION=20M                                                00000900
/*ROUTE PRINT RMT99                                                     00001000
//*++++++++++++++++++++++++++                                           00001100
//*    ZEKE EVENT # 433                                                 00001200
//*++++++++++++++++++++++++++                                           00001300
//*  IF THIS JOB FINISHES WITH ANY CONDITION CODE OF 12...THINK OF
//*  IT AS A DEADLOCK..RESTART IN LOGICAL STEP.
//*====================================================                 00001400
//*  PURPOSE:                                                         * 00001500
//*          CREATE THE INPUT FILES USED BY BLACKBOX PROGRAM 'UUTC200'* 00002900
//*          TO TRANSLATE IFIS DATA TO UCOP DATA.                     * 00003000
//*                                                                   * 00003100
//*          KEEP THE 'AMP/BUFNI' AND 'BUFFER'                        * 00003300
//*  PARAMETERS IN THE JCL TO SPEED PROCESSING. DO NOT ALTER THEM.)   * 00003400
//*                                                                   * 00003500
//*====================================================                 00002100
//JOBLIB    DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//*====================================================                 00002800
//*  ZEKE DATA-NAME $FISFSAB IS USED TO CONTROL THE FLOW OF THE JOB - * 00003600
//*  IN CASE IF UUTX200 PROGRAM ABORTED THE OLD VSAM FILE OF IFIS     * 00003700
//*  FOAPAL/RULE DATA NOT DELETED AND USED BY BLACKBOX PROGRAM.       * 00003800
//*  ZEKE DATA-NAME $FISCHD401STEP IS USED TO SAVE THE NAME OF THE    * 00003900
//*  LAST JOB STEP PROCESSED IN THE JOB STREAM, SO THAT WHEN ANY STEP * 00004000
//*  ABENDS, THE JOB WILL BE RESTARTED AT THE SAME STEP.              * 00004100
//*==================================================================== 00004200
//SETVAR1  EXEC PGM=ZEKESET,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00004300
//SYSPRINT  DD SYSOUT=*                                                 00004400
//SYSIN     DD *                                                        00004500
  SET VAR $CHM401 EQ INCOMPLETE
/*                                                                      00004600
//*====================================================                 00004700
//*  RUN DSDEL PROGRAM          - DELETE PRIOR CATALOGED SETS         * 00004800
//*====================================================                 00004900
//DSDEL010 EXEC DSDEL,                                                  00005000
//             PARM.D='NONVSAM,FISP.XTRN.EXTRACT'                       00005100
//*====================================================                 00005200
//DSDEL020 EXEC DSDEL,                                                  00005300
//             PARM.D='NONVSAM,FISP.FOAPAL.EXTRACT'                     00005400
//*====================================================                 00005500
//*  EXECUTE UCHX103D       - EXTRACT IFIS EXTERNAL REPORT CODES      * 00008400
//*                           FROM IFIS DATABASE INTO SEQUENTIAL FILE * 00008500
//*====================================================                 00008600
//EXSEQ030 EXEC PGM=UCHX103D,COND=(4095,LT)                             00008700
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UCHX103D) PLAN(UCHX103D)
/*
//EXTRACT   DD DSN=FISP.XTRN.EXTRACT,                                   00009000
//             MGMTCLAS=AS,DISP=(,CATLG,DELETE),                        00009100
//             DCB=(RECFM=FB,LRECL=31,BLKSIZE=23467),                   00009200
//             SPACE=(TRK,(90,255),RLSE),                               00009300
//             UNIT=SYSDA                                               00009400
//SYSLST    DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//SYSDBOUT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//*                                                                     00009500
//*====================================================                 00009600
//*  RUN ZEKESET PROGRAM - INITIALIZE ZEKE DATA-NAME                  * 00009700
//*====================================================                 00009800
//INITZ040 EXEC PGM=ZEKESET                                             00009900
//SYSPRINT  DD SYSOUT=*                                                 00010000
//SYSIN     DD *                                                        00010100
  SET VAR $FISFSAB EQ NO
/*                                                                      00010200
//*====================================================                 00010300
//*  RUN 'UUTX200'              - EXTRACT IFIS FOAPAL/RULE DATA FROM  * 00010400
//*                               IFIS DATABASE INTO SEQUENTIAL FILE  * 00010500
//*====================================================                 00010600
//FOSEQ050 EXEC PGM=UUTX200                                             00010700
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UUTX200) PLAN(UUTX200)
/*
//SYSOUT    DD SYSOUT=*                                                 00011000
//PARMFILE  DD *                                                        00011100
               DATE 073191/*
//CNTLRPT1  DD SYSOUT=*,                                                00011200
//             DCB=(LRECL=133,BLKSIZE=23408,RECFM=FB)                   00011300
//OUTPUT    DD DSN=FISP.FOAPAL.EXTRACT,                                 00011400
//             MGMTCLAS=AS,DISP=(,CATLG,DELETE),                        00011500
//             DCB=(RECFM=FB,LRECL=70,BLKSIZE=23450,BUFNO=8),           00011600
//             SPACE=(TRK,(75,210),RLSE),                               00011700
//             UNIT=SYSDA                                               00011800
//SYSUDUMP  DD SYSOUT=D,                                                00011900
//             DEST=LOCAL                                               00012000
//*====================================================                 00012100
//*                                                                   * 00012200
//*  RUN IDCAMS PROGRAM         - DELETE OLD VSAM FILE OF IFIS        * 00012300
//*                               EXTERNAL REPORT CODES               * 00012400
//*====================================================                 00012500
//DEVSM060 EXEC PGM=IDCAMS                                              00012600
//SYSPRINT  DD SYSOUT=*                                                 00012700
//SYSIN     DD *                                                        00012800
           DELETE -
                  FISP.UUTC20V3 -
                  PURGE -
                  ERASE
           DEFINE -
           CLUSTER (NAME (FISP.UUTC20V3) -
                  FREESPACE (20 30) -
                  CISZ (4096) -
                  INDEXED -
                  REUSE -
                  IMBED -
                  NOREPLICATE -
                  KEYS (21,0) -
                  RECORDSIZE (31,31) -
                  VOLUME (APC003)) -
           DATA -
                  (NAME(FISP.UUTC20V3.DATA) -
                  TRACKS (10,7)) -
           INDEX -
                  (NAME(FISP.UUTC20V3.INDEX1))
/*                                                                      00012900
//*====================================================                 00013000
//*  RUN SYNCSORT PROGRAM       - CREATE VSAM FILE OF IFIS EXTERNAL   * 00013100
//*                               REPORT CODES                        * 00013200
//*====================================================                 00013300
//EXVSM070 EXEC PGM=SYNCSORT                                            00013400
//SYSOUT    DD SYSOUT=*                                                 00013500
//SYSPRINT  DD SYSOUT=*                                                 00013600
//SORTIN    DD DSN=FISP.XTRN.EXTRACT,                                   00013700
//             DISP=SHR,                                                00013800
//             DCB=(BUFNO=8)                                            00013900
//SORTOUT   DD DSN=FISP.UUTC20V3,                                       00014000
//             DISP=SHR,                                                00014100
//             AMP=('BUFNI=2')                                          00014200
//SORTWK01  DD SPACE=(TRK,20),                                          00014300
//             UNIT=SYSDA                                               00014400
//SORTWK02  DD SPACE=(TRK,20),                                          00014500
//             UNIT=SYSDA                                               00014600
//SYSIN     DD *                                                        00014700
           SORT  FIELDS=(1,21,CH,A)
/*                                                                      00014800
//*====================================================                 00014900
//*  RUN ZEKESET PROGRAM - SET CONDITION CODE DEPENDING ON VALUE      * 00015000
//*                        ZEKE DATA-NAME                             * 00015100
//*====================================================                 00015200
//CHKCD080 EXEC PGM=ZEKESET                                             00015300
//SYSPRINT  DD SYSOUT=*                                                 00015400
//SYSIN     DD *                                                        00015500
     SET CONDCODE 01 IF $FISFSAB EQ YES
/*                                                                      00015600
//*====================================================                 00015700
//*  RUN IDCAMS PROGRAM         - DELETE OLD VSAM FILE OF IFIS        * 00015800
//*                               FOAPAL/RULE DATA                    * 00015900
//*====================================================                 00016000
//DEVSM090 EXEC PGM=IDCAMS,COND=(0,NE,CHKCD080)                         00016100
//SYSPRINT  DD SYSOUT=*                                                 00016200
//SYSIN     DD *                                                        00016300
           DELETE -
                  FISP.UUTC20V4 -
                  PURGE -
                  ERASE
           DEFINE -
           CLUSTER (NAME (FISP.UUTC20V4) -
                  FREESPACE (20 30) -
                  CISZ (4096) -
                  INDEXED -
                  REUSE -
                  IMBED -
                  NOREPLICATE -
                  KEYS (15,0) -
                  RECORDSIZE (70,70) -
                  VOLUME (APC003)) -
           DATA -
                  (NAME(FISP.UUTC20V4.DATA) -
                  TRACKS (10,7)) -
           INDEX -
                  (NAME(FISP.UUTC20V4.INDEX1))
/*                                                                      00016400
//*====================================================                 00016500
//*  RUN SYNCSORT PROGRAM       - CREATE VSAM FILE OF IFIS            * 00016600
//*                               FOAPAL/RULE DATA                    * 00016700
//*====================================================                 00016800
//FOVSM100 EXEC PGM=SYNCSORT,COND=(0,NE,CHKCD080)                       00016900
//SYSOUT    DD SYSOUT=*                                                 00017000
//SYSPRINT  DD SYSOUT=*                                                 00017100
//SORTIN    DD DSN=FISP.FOAPAL.EXTRACT,                                 00017200
//             DISP=SHR,                                                00017300
//             DCB=(BUFNO=8)                                            00017400
//SORTOUT   DD DSN=FISP.UUTC20V4,                                       00017500
//             DISP=SHR,                                                00017600
//             AMP=('BUFNI=2')                                          00017700
//SORTWK01  DD SPACE=(TRK,20),                                          00017800
//             UNIT=SYSDA                                               00017900
//SORTWK02  DD SPACE=(TRK,20),                                          00018000
//             UNIT=SYSDA                                               00018100
//SYSIN     DD *                                                        00018200
           SORT  FIELDS=(1,21,CH,A)
/*                                                                      00018300
//*====================================================                 00018400
//NOEOF110 EXEC PGM=ZEKESET                                             00018500
//SYSPRINT  DD SYSOUT=*                                                 00018600
//SYSIN     DD *                                                        00018700
  SET VAR $FISCHD401STEP EQ *
/*                                                                      00018800
//*====================================================                 00018900
//ABEND120 EXEC PGM=ZEKESET,COND=ONLY                                   00019000
//SYSPRINT  DD SYSOUT=*                                                 00019100
//SYSIN     DD *                                                        00019200
  SET VAR $FISCHD401STEP EQ LASTSTEP
/*                                                                      00019300
//*====================================================                 00019400
//*  RUN IDCAMS PROGRAM         - DELETE OLD VSAM FILE OF IFIS        * 00001300
//*                               EXTERNAL REPORT CODES               * 00001400
//*====================================================                 00001500
//DEVSM130 EXEC PGM=IDCAMS                                              00001600
//SYSPRINT  DD SYSOUT=*                                                 00001700
//SYSIN     DD *                                                        00001800
           DELETE -
                  FISP.UUTC20V1 -
                  PURGE -
                  ERASE
           DEFINE -
           CLUSTER (NAME (FISP.UUTC20V1) -
                  FREESPACE (20 30) -
                  CISZ (4096) -
                  INDEXED -
                  REUSE -
                  IMBED -
                  NOREPLICATE -
                  KEYS (21,0) -
                  RECORDSIZE (31,31) -
                  VOLUME (APC002)) -
           DATA -
                  (NAME(FISP.UUTC20V1.DATA) -
                  TRACKS (10,7)) -
           INDEX -
                  (NAME(FISP.UUTC20V1.INDEX1))
/*                                                                      00001900
//*====================================================                 00002000
//*   VSAM COPY   - FISP.UUTC20V3 TO FISP.UUTC20V1                      00002100
//*====================================================                 00002200
/*                                                                      00002300
//COPV3140 EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00002400
//SYSPRINT  DD SYSOUT=*                                                 00002500
//SYSIN     DD *                                                        00002600
     REPRO -
       INDATASET (FISP.UUTC20V3)      -
       OUTDATASET (FISP.UUTC20V1)     -
       REPLACE;
/*                                                                      00002700
//*                                                                     00002800
//*====================================================                 00002900
//*  RUN IDCAMS PROGRAM         - DELETE OLD VSAM FILE OF IFIS        * 00003000
//*                               FOAPAL/RULE DATA                    * 00003100
//*====================================================                 00003200
//DEVSM150 EXEC PGM=IDCAMS                                              00003300
//SYSPRINT  DD SYSOUT=*                                                 00003400
//SYSIN     DD *                                                        00003500
           DELETE -
                  FISP.UUTC20V2 -
                  PURGE -
                  ERASE
           DEFINE -
           CLUSTER (NAME (FISP.UUTC20V2) -
                  FREESPACE (20 30) -
                  CISZ (4096) -
                  INDEXED -
                  REUSE -
                  IMBED -
                  NOREPLICATE -
                  KEYS (15,0) -
                  RECORDSIZE (70,70) -
                  VOLUME (APC002)) -
           DATA -
                  (NAME(FISP.UUTC20V2.DATA) -
                  TRACKS (10,7)) -
           INDEX -
                  (NAME(FISP.UUTC20V2.INDEX1))
/*                                                                      00003600
//*====================================================                 00003700
//*   VSAM COPY   - FISP.UUTC20V4 TO FISP.UUTC20V2                      00003800
//*====================================================                 00003900
/*                                                                      00004000
//COPV4160 EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00004100
//SYSPRINT  DD SYSOUT=*                                                 00004200
//SYSIN     DD *                                                        00004300
     REPRO -
       INDATASET (FISP.UUTC20V4)      -
       OUTDATASET (FISP.UUTC20V2)     -
       REPLACE;
/*                                                                      00004400
//*====================================================                 00004500
//SETVAR1  EXEC PGM=ZEKESET,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00081400
//SYSPRINT  DD SYSOUT=*                                                 00081500
//SYSIN     DD *                                                        00081600
  SET VAR $CHM401 EQ DONE
/*                                                                      00081700
//*============================================================         00081800
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00081900
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00082000
//*============================================================         00082100
//STOPZEK2 EXEC STOPZEK2                                                00082200
//*                                                                     00082300
//*================ E N D  O F  J C L  CHM401   ========                00082400
