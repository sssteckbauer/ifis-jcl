//CREGPAY2 JOB (SYS000),'REGISTER-PAYROLL',MSGLEVEL=(1,1),              00010005
//            MSGCLASS=F,REGION=20M,                                    00010005
//*======================
//*   RESTART=STEP03,
//*======================
//*   ZEKE EVENT # 1430    - PRINT PR02 CHECK REGISTER TO
//*======================    PAYROLL'S PRINTER (PAUL R./LINDA)
//    NOTIFY=APCDBM,CLASS=K                                             00020077
//*====================================================
//OUT1 OUTPUT CLASS=Z,FORMS=STD,GROUPID=LINDA
//OUT2 OUTPUT CLASS=G,FORMS=STD,GROUPID=PRDCTL
//OUT3 OUTPUT CLASS=4,FORMS=STD,GROUPID=OGS
//* (OUT1 & OUT3 ARE THE SAME..JUST DUPLICATED FOR PRINT REDUCTION)
//*====================================================
//* SORT FOR PAYROLL
//*====================================================
//STEP01   EXEC PGM=IEFBR14,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//FAPUK07S  DD DSN=FISP.FILE.UK07S6,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=FB,LRECL=285,BLKSIZE=5985),
//             SPACE=(TRK,(15,15),RLSE),
//             UNIT=SYSDA
/*
//*====================================================
//*       SORT PAYROLL - CHECK REGISTER DATA
//*====================================================
//STEP02   EXEC PGM=SYNCSORT,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSLST    DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SORTIN    DD DSN=FISP.FILE.UK07O,
//             DISP=SHR
//SORTOUT   DD DSN=FISP.FILE.UK07S6,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=285,BLKSIZE=5985),
//             SPACE=(TRK,(45,45),RLSE),
//             UNIT=SYSDA
//SORTWK01  DD SPACE=(TRK,30),
//             UNIT=SYSDA
//SORTWK02  DD SPACE=(TRK,30),
//             UNIT=SYSDA
//SORTWK03  DD SPACE=(TRK,30),
//             UNIT=SYSDA
//SYSIN     DD *
  INCLUDE COND=(43,1,CH,EQ,C'A',OR,21,4,CH,EQ,C'PR02')
  SORT FIELDS=(1,43,CH,A)
/*
//*====================================================
//*          CHECK REGISTER PRINT DRIVER #1
//*====================================================
//STEP03   EXEC PGM=UAPUK07,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPUK07) PLAN(UAPUK07)
/*
//CONTROLR  DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SYSDBOUT  DD SYSOUT=*
//UAPUK07O  DD DSN=FISP.FILE.UK07S6,
//             DISP=SHR
//PARM01    DD *
*COMMIT COUNTER 0100
*FINAL
/*
//REGISTER  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1,*.OUT2,*.OUT3)
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  CREGPAY2 ========
