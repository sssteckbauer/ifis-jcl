//APCNALEL  JOB (SYS000),'PRODUCTION CONTROL',                          00000100
//          CLASS=K,MSGCLASS=X,MSGLEVEL=(1,1),NOTIFY=APCDBM             00000200
//LEDGER  OUTPUT GROUPID=EL760173,FORMS=L010                            00000300
//*====================================================                 00000400
//*             CREATE THE INPUT PARAMETER FILE                         00000500
//*====================================================                 00000600
//STEP001  EXEC PGM=SORT                                                00000700
//SYSOUT    DD SYSOUT=*                                                 00000800
//SORTIN    DD *                                                        00000900
*UNIVERSITY CODE 01                                                     00100008
*COA CODE A                                                             00110008
*FISCAL YEAR 92                                                         00120008
*ACCOUNTING PERIOD 03                                                   00130008
*ORGANIZATION CODE 760173                                               00140008
/*                                                                      00001000
//SORTOUT   DD DSN=&&PARMS,                                             00001100
//             DISP=(NEW,PASS,DELETE),                                  00001200
//             DCB=(RECFM=FB,LRECL=80,BLKSIZE=3120),                    00001300
//             SPACE=(TRK,(1,1),RLSE),                                  00001400
//             UNIT=VIO                                                 00001500
//SYSIN     DD *                                                        00001600
       SORT FIELDS=COPY                                                 00210008
/*                                                                      00001700
//*====================================================                 00001800
//*       EXTRACT FOR ENCMBRANCE LEDGER                                 00001900
//*====================================================                 00002000
//UGAX105  EXEC PGM=UGAX105,                                            00002100
//             REGION=4M                                                00002200
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX105) PLAN(UGAX105)
/*
//SYSOUT    DD SYSOUT=*                                                 00003100
//SYSLST    DD SYSOUT=*                                                 00003200
//SYSUDUMP  DD SYSOUT=D,                                                00003300
//             DEST=LOCAL                                               00003400
//SYSDBOUT  DD SYSOUT=*                                                 00003500
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00003600
//             DISP=SHR,                                                00003700
//             AMP=('BUFNI=2')                                          00003800
//UGAX105   DD DSN=FISP.GAM040.UGAX115,                                 00003900
//             DISP=SHR,                                                00004000
//             AMP=('BUFNI=2')                                          00004100
//EXTRAC01  DD DSN=&&UGAX105,                                           00004200
//             DISP=(NEW,PASS,DELETE),                                  00004300
//             DCB=(RECFM=FB,LRECL=165,BLKSIZE=23430),                  00004400
//             SPACE=(TRK,(15,150),RLSE),                               00004500
//             UNIT=VIO                                                 00004600
//REPORT01  DD SYSOUT=*                                                 00004700
//PARM01    DD DSN=&&PARMS,                                             00004800
//             DISP=(SHR,PASS)                                          00004900
//*                                                                     00005000
//*====================================================                 00005100
//*       SORT ENCMBRANCE EXTRACT FILE                                  00005200
//*====================================================                 00005300
//SORTXTRT EXEC PGM=SORT,                                               00005400
//             PARM='DYNALLOC=(SYSDA,5)'                                00005500
//SYSLST    DD SYSOUT=*                                                 00005600
//SYSOUT    DD SYSOUT=*                                                 00005700
//SYSUDUMP  DD SYSOUT=D,                                                00005800
//             DEST=LOCAL                                               00005900
//SORTIN    DD DSN=&&UGAX105,                                           00006000
//             DISP=(OLD,DELETE,DELETE)                                 00006100
//SORTOUT   DD DSN=&&UGAS105,                                           00006200
//             DISP=(NEW,PASS,DELETE),                                  00006300
//             DCB=(RECFM=FB,LRECL=165,BLKSIZE=23430),                  00006400
//             SPACE=(TRK,(15,150),RLSE),                               00006500
//             UNIT=VIO                                                 00006600
//SYSIN     DD DSN=FISP.PARMLIB(UGAX105),                               00006700
//             DISP=SHR                                                 00006800
//SORTWK01  DD SPACE=(TRK,(150,150)),                                   00006900
//             UNIT=SYSDA                                               00007000
//SORTWK02  DD SPACE=(TRK,(150,150)),                                   00007100
//             UNIT=SYSDA                                               00007200
//SORTWK03  DD SPACE=(TRK,(150,150)),                                   00007300
//             UNIT=SYSDA                                               00007400
//*                                                                     00007500
//*====================================================                 00007600
//*             ENCMBRANCE LEDGER REPORT                                00007700
//*====================================================                 00007800
//UGAP105  EXEC PGM=UGAP105,                                            00007900
//             REGION=4M                                                00008000
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAP105) PLAN(UGAP105)
/*
//SYSOUT    DD SYSOUT=*                                                 00008900
//SYSLST    DD SYSOUT=*                                                 00009000
//SYSUDUMP  DD SYSOUT=D,                                                00009100
//             DEST=LOCAL                                               00009200
//SYSDBOUT  DD SYSOUT=*                                                 00009300
//PARM01    DD DSN=&&PARMS,                                             00009400
//             DISP=(OLD,DELETE,DELETE)                                 00009500
//EXTRAC01  DD DSN=&&UGAS105,                                           00009600
//             DISP=(OLD,DELETE,DELETE)                                 00009700
//REPORT01  DD SYSOUT=*                                                 00009800
//REPORT02  DD SYSOUT=P,                                                00009900
//             OUTPUT=*.LEDGER                                          00010000
/*                                                                      00010100
//*================ E N D  O F  J C L  ELPRINTX  =======                00010200
