//EXW300B JOB (SYS000),'FAYE-DISBRSMNTS',MSGLEVEL=(1,1),                00010001
//        NOTIFY=APCDBM,CLASS=K,MSGCLASS=F,REGION=20M                   00020001
//*++++++++++++++++++++++++++                                           00030001
//*    ZEKE EVENT # XXXX          1932                                  00040001
//*++++++++++++++++++++++++++                                           00050001
//OUT1 OUTPUT CLASS=F,FORMS=A011,JESDS=ALL,DEFAULT=YES,GROUPID=ACCTG    00060001
//OUT2 OUTPUT CLASS=P,FORMS=L010,GROUPID=DISBSMT1                       00070001
//OUT3 OUTPUT CLASS=G,FORMS=L010,GROUPID=BACKUP1                        00080001
//OUT4 OUTPUT CLASS=P,FORMS=A002,GROUPID=DISBSMT2                       00090001
//OUT5 OUTPUT CLASS=G,FORMS=A002,GROUPID=BACKUP2                        00100001
//*====================================================                 00110001
//*     ** RE-PRINT **   EXPRESS STATEMENTS                             00120001
//*                                                                     00130001
//* CHANGE TARGET: -BDATE                                               00140001
//* CHANGE TARGET: *STATEMENT NUMBER (RANGE)                            00150001
//*====================================================                 00160001
//*       EXPRESS ORDERS STATEMENTS REPRINT                             00170001
//*====================================================                 00180001
//STEP01   EXEC PGM=UEXP300                                             00190001
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UEXP300) PLAN(UEXP300)
/*
//SYSOUT    DD SYSOUT=*                                                 00270001
//SYSLST    DD SYSOUT=*                                                 00280001
//SYSUDUMP  DD SYSOUT=D,                                                00290001
//             DEST=LOCAL                                               00300001
//SYSDBOUT  DD SYSOUT=*                                                 00310001
//REPORT02  DD DSN=FISP.EXW300B.UEXP300.REPORT02.D$MDATE,               00320001
//             DISP=(NEW,CATLG,DELETE),MGMTCLAS=MY,                     00330001
//             DCB=(RECFM=FB,BLKSIZE=18100,LRECL=181),                  00340001
//             SPACE=(TRK,(20,20),RLSE),                                00350001
//             UNIT=SYSDA                                               00360001
//CONTROLR  DD DSN=FISP.EXW300B.UEXP300.CR.D$MDATE..T$FTIME,            00370001
//             DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                       00380001
//             DCB=(RECFM=FB,BLKSIZE=23408,LRECL=133),                  00390001
//             SPACE=(TRK,(20,20),RLSE),                                00400001
//             UNIT=SYSDA                                               00410001
//PARM01    DD *                                                        00420001
*VENDOR CODE 222451761                                                  00430001
*UNIVERSITY CODE B1                                                     00440001
*STATEMENT NUMBER 00000066 00000099                                     00450001
//*                                                                     00460001
//*====================================================                 00470001
//*       PRINT STATEMENTS                                              00480001
//*====================================================                 00490001
//STEP04   EXEC PGM=IEBGENER                                            00500001
//SYSPRINT  DD SYSOUT=*                                                 00510001
//SYSUT2    DD SYSOUT=(,),                                              00520001
//             OUTPUT=(*.OUT2,*.OUT3)                                   00530001
//SYSUT1    DD DSN=FISP.EXW300B.UEXP300.REPORT02.D$MDATE,               00540001
//             DISP=SHR                                                 00550001
//SYSIN     DD DUMMY                                                    00560001
//*                                                                     00570001
//*====================================================                 00580001
//*       PRINT CONTROL REPORT                                          00590001
//*====================================================                 00600001
//STEP05   EXEC PGM=IEBGENER                                            00610001
//SYSPRINT  DD SYSOUT=*                                                 00620001
//SYSUT2    DD SYSOUT=(,),                                              00630001
//             OUTPUT=(*.OUT2,*.OUT3)                                   00640001
//SYSUT1    DD DSN=FISP.EXW300B.UEXP300.CR.D$MDATE..T$FTIME,            00650001
//             DISP=SHR                                                 00660001
//SYSIN     DD DUMMY                                                    00670001
//*                                                                     00680001
//*============================================================         00680001
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00680001
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00680001
//*============================================================         00680001
//STOPZEKE EXEC STOPZEKE                                                00680001
//*                                                                     00680001
//*================ E N D  O F  J C L  EXW300B  ========                00700001
