//GAAP010F JOB (SYS000),'GEN-ACCTG',                                    00010004
//         CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M,                00020004
//*        RESTART=IDCM020,                                             00030006
//*        RESTART=SORT120,                                             00030006
//         NOTIFY=APCDBM                                                00040004
//*++++++++++++++++++++++++++                                           00050004
//*--> ZEKE EVENT # 1453  "F2"  FINAL LEDGER -UPDATE FLAG- BUDGET ROLL  00060004
//*   (ZEKE EVENT # 1451  "F1"  FINAL LEDGER -REPORTS ONLY- BUDGET ROLL)00060004
//*++++++++++++++++++++++++++                                           00070004
//*              DELETE CATALOGED DATA SETS                             00080004
//* (RENAME 'FISP.GAA010F.ACCTINDX' IF RUNNING FOR SECOND
//*  TIME OF FISCAL CLOSING)(AFTER JOB HAS RUN-BEFORE FINAL)
//*
//*  NOTE:
//*  "*REPORTS ONLY"  STATEMENT TURNED **OFF** WITHIN JCL
//*====================================================                 00090004
//CLEAN000 EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00100004
//DD1       DD DSN=FISP.GAA010F.UGAXC03.RPTDATA.P$FISYYMM,              00110004
//             DISP=(MOD,DELETE,DELETE),                                00120004
//             DCB=(RECFM=FB,LRECL=210,BLKSIZE=23310),                  00130004
//             SPACE=(TRK,(15,15),RLSE),                                00140004
//             UNIT=SYSDA                                               00150004
//DD2       DD DSN=FISP.GAA010F.UGAXC03.BDGTROLL.P$FISYYMM,             00160004
//             DISP=(MOD,DELETE,DELETE),                                00170004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00180004
//             SPACE=(TRK,(75,75),RLSE),                                00190004
//             UNIT=SYSDA                                               00200004
//DD3       DD DSN=FISP.GAA010F.UGAXC03.JVFILE01.P$FISYYMM,             00160004
//             DISP=(MOD,DELETE,DELETE),                                00170004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00180004
//             SPACE=(TRK,(75,75),RLSE),                                00190004
//             UNIT=SYSDA                                               00200004
//DD4       DD DSN=FISP.GAA010F.UGAXC03.JVFILE65.P$FISYYMM,             00160004
//             DISP=(MOD,DELETE,DELETE),                                00170004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00180004
//             SPACE=(TRK,(75,75),RLSE),                                00190004
//             UNIT=SYSDA                                               00200004
//DD5       DD DSN=FISP.GAA010F.UGAXC03.SORTOUT.P$FISYYMM,              00160004
//             DISP=(MOD,DELETE,DELETE),                                00170004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00180004
//             SPACE=(TRK,(75,75),RLSE),                                00190004
//             UNIT=SYSDA                                               00200004
//DD6       DD DSN=FISP.GAA010F.UGAXC03.NBINDEX,                        00160004
//             DISP=(MOD,DELETE,DELETE),                                00170004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00180004
//             SPACE=(TRK,(75,75),RLSE),                                00190004
//             UNIT=SYSDA                                               00200004
//DD7       DD DSN=FISP.GAA010F.UGAXC03.BINDEX,                         00160004
//             DISP=(MOD,DELETE,DELETE),                                00170004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00180004
//             SPACE=(TRK,(75,75),RLSE),                                00190004
//             UNIT=SYSDA                                               00200004
//*====================================================                 00210004
//*   RUN IDCAMS PROGRAM TO DEFINE AND CATALOG A VSAM FILE              00220004
//*   USING ACCESS METHOD SERVICES COMMANDS.                            00230004
//*====================================================                 00240004
//IDCM010  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00250004
//SYSPRINT  DD SYSOUT=*                                                 00260004
//SYSIN     DD *                                                        00270004
            DELETE -                                                    00280004
                FISP.GAA010F.UGAXC03 -                                  00290004
                PURGE -                                                 00300004
                ERASE                                                   00310004
/*                                                                      00320004
//*====================================================                 00330004
//*       IDCAMS - DEFINE VSAM FILE                                     00340004
//*====================================================                 00350004
//IDCM020  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00360004
//SYSPRINT  DD SYSOUT=*                                                 00370004
//SYSIN     DD *                                                        00380004
            DEFINE CLUSTER -                                            00390004
                (NAME (FISP.GAA010F.ACCTINDX) -                         00400004
                 FREESPACE (20 30) -                                    00410004
                 CISZ (4096) -                                          00420004
                 INDEXED -                                              00430004
                 REUSE -                                                00440004
                 IMBED -                                                00450004
                 NOREPLICATE -                                          00460004
                 KEYS (18,0) -                                          00470004
                 RECORDSIZE (28 28) -                                   00480004
                 VOLUME (APC001)) -                                     00490004
            DATA -                                                      00500004
                (NAME(FISP.GAA010F.ACCTINDX.DATA) -                     00510004
                 CYLINDERS (2,2)) -                                     00520004
            INDEX -                                                     00530004
                (NAME(FISP.GAA010F.ACCTINDX.INDEX))                     00540004
/*                                                                      00550004
//*====================================================                 00560004
//*       SORT INDEX FILE FOR VSAM INPUT                                00570004
//*       (BFS TO PROVIDE FILE -  'GAOP.BDGTROLL.ACCTINDX')
//*====================================================                 00580004
//SORT030  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00590004
//SYSLST    DD SYSOUT=*                                                 00600004
//SYSOUT    DD SYSOUT=*                                                 00610004
//SYSUDUMP  DD SYSOUT=D                                                 00620004
//SORTIN    DD DSN=GAOP.BDGTROLL.ACCTINDX,                              00630004
//             DISP=SHR                                                 00640004
//SORTWK01  DD SPACE=(TRK,75),                                          00650004
//             UNIT=SYSDA                                               00660004
//SORTWK02  DD SPACE=(TRK,75),                                          00670004
//             UNIT=SYSDA                                               00680004
//SORTWK03  DD SPACE=(TRK,75),                                          00690004
//             UNIT=SYSDA                                               00700004
//SORTOUT   DD DSN=&&ACCTINDX,                                          00710004
//             DISP=(NEW,PASS,DELETE),                                  00720004
//             DCB=(RECFM=FB,LRECL=28,BLKSIZE=23464),                   00730004
//             SPACE=(TRK,(75,15),RLSE),                                00740004
//             UNIT=VIO                                                 00750004
//SYSIN     DD *                                                        00760004
            OUTREC FIELDS=(1:1,28)                                      00770004
            SORT FIELDS=(1,18,CH,A)                                     00780004
/*                                                                      00790004
//*====================================================                 00800004
//*     COPY INDEX FILE INTO VSAM FILE                                  00810004
//*====================================================                 00820004
//IDCM040  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00830004
//INPUT     DD DSN=&&ACCTINDX,                                          00840004
//             DISP=(OLD,DELETE)                                        00850004
//OUTPUT    DD DSN=FISP.GAA010F.ACCTINDX,                               00860004
//             DISP=SHR                                                 00870004
//SYSPRINT  DD SYSOUT=*                                                 00880004
//SYSIN     DD *                                                        00890004
            REPRO -                                                     00900004
                INFILE(INPUT) -                                         00910004
                OUTFILE(OUTPUT) -                                       00920004
                REUSE                                                   00930004
/*                                                                      00940004
//*====================================================                 00950004
//*       BUDGET ROLL                                                   00960004
//*====================================================                 00970004
//EXTR050  EXEC PGM=UGAXC03,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00980004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAXC03) PLAN(UGAXC03)
/*
//SYSOUT    DD SYSOUT=*                                                 01090004
//SYSLST    DD SYSOUT=*                                                 01100004
//SYSUDUMP  DD SYSOUT=D                                                 01110004
//SYSDBOUT  DD SYSOUT=*                                                 01120004
//JVFILE01  DD DSN=FISP.GAA010F.UGAXC03.JVFILE01.P$FISYYMM,             01130004
//             DISP=(NEW,CATLG,CATLG),                                  01140004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  01150004
//             SPACE=(TRK,(960,750),RLSE),                              01160004
//             UNIT=SYSDA                                               01170004
//EXTRAC01  DD DSN=FISP.GAA010F.UGAXC03.RPTDATA.P$FISYYMM,              01180004
//             DISP=(NEW,CATLG,DELETE),                                 01190004
//             DCB=(RECFM=FB,LRECL=210,BLKSIZE=23310),                  01200004
//             SPACE=(TRK,(750,225),RLSE),                              01210004
//             UNIT=SYSDA                                               01220004
//REPORT01  DD SYSOUT=(P,,L010)                                         01230004
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       01240004
//             DISP=SHR,                                                01250004
//             AMP=('BUFNI=2')                                          01260004
//VSAM01    DD DSN=FISP.GAA010F.ACCTINDX,                               01270004
//             DISP=SHR,                                                01280004
//             AMP=('BUFNI=2')                                          01290004
//PARM02    DD DUMMY                                                    01300004
//PARM01    DD *                                                        01310004
*UNIVERSITY CODE $FISUNVRSCODE                                          01320004
*COA CODE $FISCOACODE                                                   01330004
*FISCAL YEAR $FISFSCLYR                                                 01340004
*SUBSYSTEM-ID BUDROLL                                                   01350004
/*                                                                      01360004
//*
//*REPORTS ONLY (CANNOT RUN WITH 'REPORTS ONLY' DURING
//*              FINAL CLOSING !! - WILL NOT SET
//*              ACCURAL FLAGS CORRECTLY SO THAT GAAP015F
//*              WILL CLOSE ACCOUTING PERIODS "14" AND "00")
//* (IF ABENDS WITH "REPORTS ONLY", TRY "TESTING")
//*====================================================                 01370004
//*       SORT EXTRACT FROM UGAXC03                                     01380004
//*====================================================                 01390004
//SORT060  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         01400004
//SYSLST    DD SYSOUT=*                                                 01410004
//SYSOUT    DD SYSOUT=*                                                 01420004
//SYSUDUMP  DD SYSOUT=D                                                 01430004
//SORTIN    DD DSN=FISP.GAA010F.UGAXC03.RPTDATA.P$FISYYMM,              01440004
//             DISP=SHR                                                 01450004
//SORTOUT   DD DSN=FISP.GAA010F.UGAXC03.RPTDATA.P$FISYYMM,              01460004
//             DISP=SHR                                                 01470004
//SORTWK01  DD SPACE=(TRK,450),                                         01480004
//             UNIT=SYSDA                                               01490004
//SORTWK02  DD SPACE=(TRK,450),                                         01500004
//             UNIT=SYSDA                                               01510004
//SORTWK03  DD SPACE=(TRK,450),                                         01520004
//             UNIT=SYSDA                                               01530004
//SORTWK04  DD SPACE=(TRK,450),                                         01540004
//             UNIT=SYSDA                                               01550004
//SORTWK05  DD SPACE=(TRK,450),                                         01560004
//             UNIT=SYSDA                                               01570004
//SORTWK06  DD SPACE=(TRK,450),                                         01580004
//             UNIT=SYSDA                                               01590004
//SORTWK07  DD SPACE=(TRK,450),                                         01600004
//             UNIT=SYSDA                                               01610004
//SORTWK08  DD SPACE=(TRK,450),                                         01620004
//             UNIT=SYSDA                                               01630004
//SORTWK09  DD SPACE=(TRK,450),                                         01640004
//             UNIT=SYSDA                                               01650004
//SORTWK10  DD SPACE=(TRK,450),                                         01660004
//             UNIT=SYSDA                                               01670004
//SYSIN     DD DSN=FISP.PARMLIB(UGAPC03A),                              01680004
//             DISP=SHR                                                 01690004
//*                                                                     01700004
//*====================================================
//*       SORT EXTRACT FROM UGAXC03
//*====================================================
//SORT062  EXEC  PGM=SYNCSORT
//SYSOUT   DD  SYSOUT=*
//SYSPRINT DD  SYSOUT=*
//SORTIN    DD DSN=FISP.GAA010F.UGAXC03.JVFILE01.P$FISYYMM,
//             DISP=SHR
//SORTOUT   DD DSN=FISP.GAA010F.UGAXC03.SORTOUT.P$FISYYMM,              01140005
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=27750),
//             SPACE=(TRK,(1005,150),RLSE),
//             UNIT=SYSDA                                               00090000
//SORTWK01  DD SPACE=(TRK,900),
//             UNIT=SYSDA
//SORTWK02  DD SPACE=(TRK,900),
//             UNIT=SYSDA
//SORTWK03  DD SPACE=(TRK,900),
//             UNIT=SYSDA
//SORTWK04  DD SPACE=(TRK,900),
//             UNIT=SYSDA
//SYSIN    DD  *
           SORT FIELDS=(11,8,CH,A,19,1,CH,A,77,6,CH,A,83,6,CH,A,
                        95,6,CH,A,89,6,CH,A,113,10,CH,A,75,1,CH,A)
           SUM FIELDS=(28,12,ZD)
/*                                                                      01430005
//*====================================================                 01740004
//*==         ROLLUP LIKE ACCOUNTS  ===================                 01740004
//*====================================================                 01740004
//RLUP065  EXEC PGM=UGAXC05,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          01020004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 01110004
//SYSLST    DD SYSOUT=*                                                 01120004
//SYSUDUMP  DD SYSOUT=D                                                 01130004
//SYSDBOUT  DD SYSOUT=*                                                 01140004
//UGAUA00I  DD DSN=FISP.GAA010F.UGAXC03.SORTOUT.P$FISYYMM,DISP=SHR      01150004
//ROLLUP    DD DSN=FISP.GAA010F.UGAXC03.JVFILE65.P$FISYYMM,             01150004
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(TRK,(1005,150),RLSE),                             00001900
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=27750),
//             UNIT=SYSDA                                               00002000
//*============================================================         00047000
//SORT066  EXEC  PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))        00048000
//SYSOUT   DD  SYSOUT=*                                                 00049000
//SYSPRINT DD  SYSOUT=*                                                 00050000
//SORTIN   DD  DSN=FISP.GAA010F.UGAXC03.JVFILE65.P$FISYYMM,DISP=SHR     00060000
//SORTOUT  DD  DSN=FISP.GAA010F.UGAXC03.BDGTROLL.P$FISYYMM,             00070000
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=27750),
//             SPACE=(TRK,(1005,150),RLSE),
//             UNIT=SYSDA                                               00090000
//SORTWK01  DD SPACE=(TRK,900),                                         00092000
//             UNIT=SYSDA                                               00093000
//SORTWK02  DD SPACE=(TRK,900),                                         00094000
//             UNIT=SYSDA                                               00095000
//SORTWK03  DD SPACE=(TRK,900),                                         00096000
//             UNIT=SYSDA                                               00097000
//SORTWK04  DD SPACE=(TRK,900),                                         00098000
//             UNIT=SYSDA                                               00099000
//SYSIN    DD  *                                                        00100000
           SORT FIELDS=(1,29,CH,A)                                      00110000
/*                                                                      00110500
//*====================================================                 01710004
//*  BUDGET ROLL REPORT                                                 01720004
//*====================================================                 01730004
//PRNT070  EXEC PGM=UGAPC03A,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         01740004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAPC03A) PLAN(UGAPC03A)
/*
//SYSOUT    DD SYSOUT=*                                                 01830004
//SYSLST    DD SYSOUT=*                                                 01840004
//SYSUDUMP  DD SYSOUT=D                                                 01850004
//SYSDBOUT  DD SYSOUT=*                                                 01860004
//REPORT01  DD SYSOUT=(P,,L010)                                         01870004
//REPORT02  DD SYSOUT=(P,,L010)                                         01880004
//EXTRAC01  DD DSN=FISP.GAA010F.UGAXC03.RPTDATA.P$FISYYMM,              01890004
//             DISP=SHR                                                 01900004
//PARM01    DD *                                                        01910004
*UNIVERSITY CODE $FISUNVRSCODE                                          01920004
*COA CODE $FISCOACODE                                                   01930004
*FISCAL YEAR $FISFSCLYR                                                 01940004
/*                                                                      01950004
//*====================================================                 01960004
//*       SORT EXTRACT FROM UGAXC03                                     01970004
//*====================================================                 01980004
//SORT080  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         01990004
//SYSLST    DD SYSOUT=*                                                 02000004
//SYSOUT    DD SYSOUT=*                                                 02010004
//SYSUDUMP  DD SYSOUT=D                                                 02020004
//SORTIN    DD DSN=FISP.GAA010F.UGAXC03.RPTDATA.P$FISYYMM,              02030004
//             DISP=SHR                                                 02040004
//SORTWK01  DD SPACE=(TRK,900),                                         02050004
//             UNIT=SYSDA                                               02060004
//SORTWK02  DD SPACE=(TRK,900),                                         02070004
//             UNIT=SYSDA                                               02080004
//SORTWK03  DD SPACE=(TRK,900),                                         02090004
//             UNIT=SYSDA                                               02100004
//SORTWK04  DD SPACE=(TRK,900),                                         02110004
//             UNIT=SYSDA                                               02120004
//SORTWK05  DD SPACE=(TRK,900),                                         02130004
//             UNIT=SYSDA                                               02140004
//SORTWK06  DD SPACE=(TRK,900),                                         02150004
//             UNIT=SYSDA                                               02160004
//SORTWK07  DD SPACE=(TRK,900),                                         02170004
//             UNIT=SYSDA                                               02180004
//SORTWK08  DD SPACE=(TRK,900),                                         02190004
//             UNIT=SYSDA                                               02200004
//SORTWK09  DD SPACE=(TRK,900),                                         02210004
//             UNIT=SYSDA                                               02220004
//SORTWK10  DD SPACE=(TRK,900),                                         02230004
//             UNIT=SYSDA                                               02240004
//SORTOUT   DD DSN=&&UGAP03B,                                           02250004
//             DISP=(NEW,PASS,DELETE),                                  02260004
//             DCB=(RECFM=FB,LRECL=210,BLKSIZE=23310),                  02270004
//             SPACE=(TRK,(1005,150),RLSE),                             02280004
//             UNIT=VIO                                                 02290004
//SYSIN     DD DSN=FISP.PARMLIB(UGAPC03B),                              02300004
//             DISP=SHR                                                 02310004
//*====================================================                 02320004
//*  BUDGET ROLL REPORT                                                 02330004
//*====================================================                 02340004
//PRNT090  EXEC PGM=UGAPC03B,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02350004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAPC03B) PLAN(UGAPC03B)
/*
//SYSOUT    DD SYSOUT=*                                                 02440004
//SYSLST    DD SYSOUT=*                                                 02450004
//SYSUDUMP  DD SYSOUT=D                                                 02460004
//SYSDBOUT  DD SYSOUT=*                                                 02470004
//REPORT01  DD SYSOUT=(P,,L010)                                         02480004
//REPORT02  DD SYSOUT=(P,,L010)                                         02490004
//EXTRAC01  DD DSN=&&UGAP03B,                                           02500004
//             DISP=(OLD,DELETE)                                        02510004
//PARM01    DD *                                                        02520004
*UNIVERSITY CODE $FISUNVRSCODE                                          02530004
*COA CODE $FISCOACODE                                                   02540004
*FISCAL YEAR $FISFSCLYR                                                 02550004
/*                                                                      02560004
//*====================================================                 02570004
//STEP100  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02580004
//SYSOUT    DD SYSOUT=*                                                 02590004
//SYSPRINT  DD SYSOUT=*                                                 02600004
//SORTIN    DD DSN=FISP.GAA010F.UGAXC03.BDGTROLL.P$FISYYMM,             02610004
//             DISP=SHR                                                 02620004
//SORTOUT   DD DSN=&&UGAUA00I,                                          02630004
//             DISP=(NEW,PASS,DELETE),                                  02640004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  02650004
//             SPACE=(TRK,(1005,150),RLSE),                             02660004
//             UNIT=VIO                                                 02670004
//SYSIN     DD *                                                        02680004
           SORT FIELDS=(1,19,CH,A,20,4,ZD,A)                            02690004
/*                                                                      02700004
//SORTWK01  DD SPACE=(TRK,900),                                         02710004
//             UNIT=SYSDA                                               02720004
//SORTWK02  DD SPACE=(TRK,900),                                         02730004
//             UNIT=SYSDA                                               02740004
//*====================================================                 02750004
//PRNT110  EXEC PGM=FGAP020D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02760004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 02830004
//SYSUDUMP  DD SYSOUT=D                                                 02840004
//UGAUA00I  DD DSN=&&UGAUA00I,                                          02850004
//             DISP=(OLD,DELETE)                                        02860004
//GA019R1   DD SYSOUT=(P,,L010)
//GA019R2   DD SYSOUT=(P,,L010)
//*
//* {GA019R2 IS ALWAYS PRINTED - ON REPORTS ONLY OPTION OR LIVE PROD}
//*******************************************************************
//*  SORT THE FILE TO  SELECT BLANK INDEX ONLY
//*******************************************************************
//*
//SORT120  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02580004
//SYSOUT    DD SYSOUT=*                                                 02590004
//SYSPRINT  DD SYSOUT=*                                                 02600004
//SORTIN    DD DSN=FISP.GAA010F.UGAXC03.BDGTROLL.P$FISYYMM,             02610004
//             DISP=SHR                                                 02620004
//SORTOUT   DD DSN=FISP.GAA010F.UGAXC03.BINDEX,                         02630004
//             DISP=(NEW,CATLG,DELETE),                                 02640004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  02650004
//             SPACE=(TRK,(1005,150),RLSE),                             02660004
//             UNIT=SYSDA                                               02670004
//SYSIN     DD *                                                        02680004
  INCLUDE COND=(19,1,CH,EQ,C'2',AND,113,10,CH,EQ,C'          '),        02690004
  FORMAT=CH
  SORT FIELDS=(113,10,CH,A,77,36,CH,A)                                  02690004
/*                                                                      02700004
//SORTWK01  DD SPACE=(TRK,900),                                         02710004
//             UNIT=SYSDA                                               02720004
//SORTWK02  DD SPACE=(TRK,900),                                         02730004
//             UNIT=SYSDA                                               02740004
//*                                                                     02740004
//*  SORT THE FILE TO  SELECT NON BLANK INDEX ONLY
//*                                                                     02740004
//SORT130  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02580004
//SYSOUT    DD SYSOUT=*                                                 02590004
//SYSPRINT  DD SYSOUT=*                                                 02600004
//SORTIN    DD DSN=FISP.GAA010F.UGAXC03.BDGTROLL.P$FISYYMM,             02610004
//             DISP=SHR                                                 02620004
//SORTOUT   DD DSN=FISP.GAA010F.UGAXC03.NBINDEX,                        02630004
//             DISP=(NEW,CATLG,DELETE),                                 02640004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  02650004
//             SPACE=(TRK,(1005,150),RLSE),                             02660004
//             UNIT=SYSDA                                               02670004
//SYSIN     DD *                                                        02680004
  INCLUDE COND=(19,1,CH,EQ,C'2',AND,113,10,CH,GT,C'          '),        02690004
  FORMAT=CH
  SORT FIELDS=(113,10,CH,A)                                             02690004
  SUM FIELDS=NONE
/*                                                                      02700004
//*                                                                     02740004
//SORT140  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02580004
//SYSOUT    DD SYSOUT=*                                                 02590004
//SYSPRINT  DD SYSOUT=*                                                 02600004
//SORTIN    DD DSN=FISP.GAA010F.UGAXC03.NBINDEX,                        02610004
//             DISP=SHR                                                 02620004
//SORTOUT   DD DSN=FISP.GAA010F.UGAXC03.NBINDEX,                        02630004
//             DISP=SHR                                                 02620004
//SYSIN     DD *                                                        02680004
  SORT FIELDS=(113,10,CH,A,77,36,CH,A)                                  02690004
/*                                                                      02700004
//SORTWK01  DD SPACE=(TRK,900),                                         02710004
//             UNIT=SYSDA                                               02720004
//SORTWK02  DD SPACE=(TRK,900),                                         02730004
//             UNIT=SYSDA                                               02740004
//*                                                                     02750004
//PRNT150  EXEC PGM=UGAXP03,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          02760004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR                            00004800
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR                               00005000
//          DD DSN=DB2F.DSNEXIT,DISP=SHR                                00005200
//          DD DSN=DB2F.DSNLOAD,DISP=SHR                                00005301
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAXP03) PLAN(UGAXP03)
//SYSOUT    DD SYSOUT=*                                                 02830004
//SYSUDUMP  DD SYSOUT=*                                                 02840004
//UGAUA00I  DD DSN=FISP.GAA010F.UGAXC03.BINDEX,DISP=SHR                 02850004
//          DD DSN=FISP.GAA010F.UGAXC03.NBINDEX,DISP=SHR                02860004
//CONTROLR  DD SYSOUT=*                                                 02870004
//INDXERR   DD SYSOUT=(1,,L010)                                         02880004
/*                                                                      02890004
//*============================================================
//* ALWAYS LEAVE ** GA019R2 ** TURNED ON TO PRINT (EVEN
//* DURING INTERIM, AFTER PRELIM AND BEFORE FINAL).
//*============================================================         02900004
//*      RENAMER OF EXTERNAL FILE
//*============================================================
//RENAMER  EXEC PGM=IDCAMS
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   ALTER      GAOP.BDGTROLL.ACCTINDX -
              NEWNAME(GAOP.BDGTROLL.ACCTINDX.P$FISYYMM)
/*
//*============================================================         02900004
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          02910004
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      02920004
//*============================================================         02930004
//STOPZEKE EXEC STOPZEKE                                                02940004
//*                                                                     02950004
//*================ E N D  O F  J C L  GAA010F  ========                02960004
