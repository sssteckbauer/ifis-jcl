//GAAP010P JOB (SYS000),'ACCTG-COLIO',MSGCLASS=X,MSGLEVEL=(1,1),        00000100
//        NOTIFY=APCDBM,CLASS=K,REGION=20M                              00000200
//*++++++++++++++++++++++++++                                           00000300
//*    ZEKE EVENT # 1348                                                00000400
//*++++++++++++++++++++++++++                                           00000500
//*====================================================                 00000600
//*              DELETE CATALOGED DATA SETS                             00000700
//*====================================================                 00000800
//CLEAN000 EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00000900
//DD1       DD DSN=FISP.GAA010P.UGAX610.OLDYEAR.P$FISYYMM,              00001000
//             DISP=(MOD,DELETE,DELETE),                                00001100
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=2500),                   00001200
//             SPACE=(TRK,(1,1),RLSE),                                  00001300
//             UNIT=SYSDA                                               00001400
//DD2       DD DSN=FISP.GAA010P.UGAX610.NEWYEAR.P$FISYYMM,              00001500
//             DISP=(MOD,DELETE,DELETE),                                00001600
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=2500),                   00001700
//             SPACE=(TRK,(1,1),RLSE),                                  00001800
//             UNIT=SYSDA                                               00001900
/*                                                                      00002000
//*====================================================                 00002100
//*              LIEN REVERSAL PROCESS                                  00002200
//*====================================================                 00002300
//EXTR010  EXEC PGM=UGAX610,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00002400
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX610) PLAN(UGAX610)
/*
//SYSOUT    DD SYSOUT=*                                                 00003300
//SYSLST    DD SYSOUT=*                                                 00003400
//SYSUDUMP  DD SYSOUT=D                                                 00003500
//SYSDBOUT  DD SYSOUT=*                                                 00003600
//REPORT01  DD SYSOUT=*,                                                00003700
//             DCB=BLKSIZE=133                                          00003800
//JVFILE01  DD DSN=FISP.GAA010P.UGAX610.OLDYEAR.P$FISYYMM,              00003900
//             DISP=(NEW,CATLG,DELETE),                                 00004000
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00004100
//             SPACE=(TRK,(75,75),RLSE),                                00004200
//             UNIT=SYSDA                                               00004300
//JVFILE02  DD DSN=FISP.GAA010P.UGAX610.NEWYEAR.P$FISYYMM,              00004400
//             DISP=(NEW,CATLG,DELETE),                                 00004500
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00004600
//             SPACE=(TRK,(1125,1125),RLSE),                            00004700
//             UNIT=SYSDA                                               00004800
//PARM01    DD *                                                        00004900
*UNIVERSITY CODE $FISUNVRSCODE
*COA CODE $FISCOACODE
*FISCAL YEAR $FISFSCLYR
*SUBSYSTEM-ID CLOSING
*OLD YEAR DOCUMENT CXENC695/BLANKET-TRIP LIEN REVERSAL
*NEW YEAR DOCUMENT CZENC695/BLANKET-TRIP LIEN REVERSAL
/*                                                                      00005000
//*============================================================         00005100
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00005200
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00005300
//*============================================================         00005400
//STOPZEKE EXEC STOPZEKE                                                00005500
//*                                                                     00005600
//*================ E N D  O F  J C L  GAA010P  ========                00005700
