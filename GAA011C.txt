//GAAP011C JOB (SYS000),                                                00000100
//        'STE.MARIE-ACCTG',                                            00000200
//        CLASS=K,                                                      00000300
//        MSGCLASS=P,                                                   00000400
//        MSGLEVEL=(1,1),                                               00000500
//        NOTIFY=APCDBM,                                                00000600
//        REGION=20M                                                    00000700
//*++++++++++++++++++++++++++                                           00000800
//*    ZEKE EVENT # 1404                                                00000900
//*      ( EXTRACT JOB WAS GAA010W FOR THIS LOAD JOB )
//*++++++++++++++++++++++++++                                           00001000
// JCLLIB ORDER=APCP.PROCLIB
//*
//CNTL1    OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=1,FORMS=A011             00001100
//CNTL2    OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=Z,FORMS=A011             00001200
//CNTL3    OUTPUT COPIES=1,GROUPID=PRDCTL,CLASS=G,FORMS=A011            00001300
//PRNT021  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=1,FORMS=A011             00001400
//PRNT022  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=Z,FORMS=A011             00001500
//PRNT023  OUTPUT COPIES=1,GROUPID=PRDCTL,CLASS=G,FORMS=A011            00001600
//PRNT031  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=Z,FORMS=A011             00001700
//PRNT032  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=Z,FORMS=A011             00001800
//PRNT033  OUTPUT COPIES=1,GROUPID=PRDCTL,CLASS=G,FORMS=A011            00001900
//PRNT041  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=Z,FORMS=A011             00002000
//PRNT042  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=Z,FORMS=A011             00002100
//PRNT043  OUTPUT COPIES=1,GROUPID=PRDCTL,CLASS=G,FORMS=A011            00002200
//OUT3 OUTPUT CLASS=F,FORMS=A011,GROUPID=PRDCTL,JESDS=ALL,DEFAULT=YES   00002300
//*====================================================                 00002300
//*                     FISPLD                                          00002400
//*====================================================                 00002836
//GAD001P  EXEC GAD001P,                                                00002925
//  CNTLOUT='*,OUTPUT=(*.PRNT021,*.PRNT022,*.PRNT023)',    USER SYOUT   00003125
//  DCMNTOUT='*,OUTPUT=(*.PRNT031,*.PRNT032,*.PRNT033)',   USER SYOUT   00003125
//  TRANSOUT='*,OUTPUT=(*.PRNT041,*.PRNT042,*.PRNT043)',   USER SYOUT   00003125
//  OUTX='*',                                             SYSTEM SYSOUT 00003025
//  COPIES=1,                                             # OF COPIES   00003200
//  GROUPID='FISP',                                       TEST OR PROD  00003300
//  JOBID='GAAP011C',                                     JOB NAME
//  JVOUCHER='FISP.GAA010W.UGAX615.OLDYEAR.P$FISYYMM',    INPUT DS
//  DB2REGN='DB2F'                                        DB2 DATABASE
//*
//*============================================================         00006900
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00007000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00007100
//*============================================================         00007200
//STOPZEKE EXEC STOPZEKE                                                00007300
//*                                                                     00007400
//*================ E N D  O F  J C L  GAA011C  ========                00007500
