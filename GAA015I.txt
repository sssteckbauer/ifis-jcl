//GAAP015I JOB (SYS000),'BOB COLIO-ACCTG',                              00000100
//         MSGCLASS=F,                                                  00000200
//         MSGLEVEL=(1,1),                                              00000200
//         CLASS=K,                                                     00000200
//         REGION=20M,                                                  00000200
//*        RESTART=SORT040,                                             00000300
//         NOTIFY=APCDBM                                                00000400
//*++++++++++++++++++++++++++                                           00000500
//*    ZEKE EVENT # 1375                                                00000600
//*++++++++++++++++++++++++++                                           00000700
//*====================================================                 00000800
//*        INTERIM - PLANT CLOSING - FOR REPORTS ONLY                   00000900
//*====================================================                 00001000
//*              DELETE CATALOGED DATA SETS                             00001100
//*====================================================                 00001200
//CLEAN000 EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00001300
//DD1       DD DSN=FISP.GAA015I.UGAX615.RPTDATA.P$FISYYMM,              00001400
//             DISP=(MOD,DELETE,DELETE),                                00001500
//             DCB=(RECFM=FB,LRECL=168,BLKSIZE=27888),                  00001600
//             SPACE=(TRK,(75,75),RLSE),                                00001700
//             UNIT=SYSDA                                               00001800
//DD2       DD DSN=FISP.GAA015I.UGAX615.OLDYEAR.P$FISYYMM,              00001900
//             DISP=(MOD,DELETE,DELETE),                                00002000
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=2500),                   00002100
//             SPACE=(TRK,(75,75),RLSE),                                00002200
//             UNIT=SYSDA                                               00002300
//DD3       DD DSN=FISP.GAA015I.UGAX615.NEWYEAR.P$FISYYMM,              00002400
//             DISP=(MOD,DELETE,DELETE),                                00002500
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=2500),                   00002600
//             SPACE=(TRK,(75,75),RLSE),                                00002700
//             UNIT=SYSDA                                               00002800
/*                                                                      00002900
//*====================================================                 00003000
//*  EXTRACT FOR PLANT CLOSING                                          00003100
//*====================================================                 00003200
//EXTR010  EXEC PGM=UGAX615,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00003300
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX615) PLAN(UGAX615)
/*
//SYSOUT    DD SYSOUT=*                                                 00004200
//SYSLST    DD SYSOUT=*                                                 00004300
//SYSUDUMP  DD SYSOUT=D                                                 00004400
//SYSDBOUT  DD SYSOUT=*                                                 00004500
//REPORT01  DD SYSOUT=(P,,L010)                                         00004600
//REPORT02  DD DSN=FISP.GAA015I.UGAX615.RPTDATA.P$FISYYMM,              00004700
//             DISP=(NEW,CATLG,DELETE),                                 00004800
//             DCB=(RECFM=FB,LRECL=168,BLKSIZE=27888),                  00001600
//             SPACE=(TRK,(75,75),RLSE),                                00005000
//             UNIT=SYSDA                                               00005100
//JVFILE01  DD DSN=FISP.GAA015I.UGAX615.OLDYEAR.P$FISYYMM,              00005200
//             DISP=(NEW,CATLG,DELETE),                                 00005300
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=2500),                   00005400
//             SPACE=(TRK,(75,75),RLSE),                                00005500
//             UNIT=SYSDA                                               00005600
//JVFILE02  DD DSN=FISP.GAA015I.UGAX615.NEWYEAR.P$FISYYMM,              00005700
//             DISP=(NEW,CATLG,DELETE),                                 00005800
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=2500),                   00005900
//             SPACE=(TRK,(75,75),RLSE),                                00006000
//             UNIT=SYSDA                                               00006100
//PARM01    DD *                                                        00006200
*UNIVERSITY CODE $FISUNVRSCODE
*COA CODE $FISCOACODE
*FISCAL YEAR $FISFSCLYR
*SUBSYSTEM-ID CLOSING
*CAPITALIZATION DOCUMENT CACAP695/PLANT CAPITALIZATION
*WRITE-OFF DOCUMENT CAPLT695/PLANT WRITEOFF
*OLD YEAR CIP DOCUMENT CXCIP695/CONSTRUCTION IN PROGRESS
*NEW YEAR CIP DOCUMENT CZCIP695/CONSTRUCTION IN PROGRESS
/*                                                                      00006300
//*                                                                     00006400
//*====================================================                 00006500
//*       SORT EXTRACT                                                  00006600
//*====================================================                 00006700
//SORT020  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00006800
//SYSLST    DD SYSOUT=*                                                 00006900
//SYSOUT    DD SYSOUT=*                                                 00007000
//SYSUDUMP  DD SYSOUT=D                                                 00007100
//SORTIN    DD DSN=FISP.GAA015I.UGAX615.RPTDATA.P$FISYYMM,              00007200
//             DISP=SHR                                                 00007300
//SORTWK01  DD SPACE=(TRK,75),                                          00007400
//             UNIT=SYSDA                                               00007500
//SORTWK02  DD SPACE=(TRK,75),                                          00007600
//             UNIT=SYSDA                                               00007700
//SORTWK03  DD SPACE=(TRK,75),                                          00007800
//             UNIT=SYSDA                                               00007900
//SORTOUT   DD DSN=&&REPORT,                                            00008000
//             DISP=(NEW,PASS,DELETE),                                  00008100
//             DCB=(RECFM=FB,LRECL=168,BLKSIZE=27888),                  00001600
//             SPACE=(TRK,(75,75),RLSE),                                00008300
//             UNIT=VIO                                                 00008400
//SYSIN     DD *                                                        00008500
  SORT FIELDS=(1,49,CH,A)                                               00600041
//*                                                                     00008600
//*====================================================                 00008700
//*  PLANT CLOSING REPORT                                               00008800
//*====================================================                 00008900
//PRNT030  EXEC PGM=UGAP615,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00009000
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 00009900
//SYSLST    DD SYSOUT=*                                                 00010000
//SYSUDUMP  DD SYSOUT=D                                                 00010100
//SYSDBOUT  DD SYSOUT=*                                                 00010200
//REPORT01  DD SYSOUT=(P,,L010)                                         00010300
//REPORT02  DD SYSOUT=(P,,L010)                                         00010400
//EXTRAC01  DD DSN=&&REPORT,                                            00010500
//             DISP=(OLD,DELETE)                                        00010600
//*====================================================                 00010700
//SORT040  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00010800
//SYSOUT    DD SYSOUT=*                                                 00010900
//SYSPRINT  DD SYSOUT=*                                                 00011000
//SORTIN    DD DSN=FISP.GAA015I.UGAX615.OLDYEAR.P$FISYYMM,              00011100
//             DISP=(OLD,DELETE)                                        00011200
//          DD DSN=FISP.GAA015I.UGAX615.NEWYEAR.P$FISYYMM,              00011300
//             DISP=(OLD,DELETE)                                        00011400
//SORTOUT   DD DSN=&&UGAUA00I,                                          00011500
//             DISP=(NEW,PASS,DELETE),                                  00011600
//             DCB=(RECFM=FB,BLKSIZE=23250,LRECL=250),                  00011700
//             SPACE=(TRK,(75,75),RLSE),                                00011800
//             UNIT=VIO                                                 00011900
//SYSIN     DD *                                                        00012000
            SORT FIELDS=(1,19,CH,A,20,4,ZD,A)                           00041100
/*                                                                      00012100
//SORTWK01  DD SPACE=(TRK,300),                                         00012200
//             UNIT=SYSDA                                               00012300
//SORTWK02  DD SPACE=(TRK,300),                                         00012400
//             UNIT=SYSDA                                               00012500
//*====================================================                 00012600
//PRINT50  EXEC PGM=FGAP020D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00012700
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 00013400
//SYSUDUMP  DD SYSOUT=D                                                 00013500
//UGAUA00I  DD DSN=&&UGAUA00I,                                          00013600
//             DISP=(OLD,DELETE)                                        00013700
//GA019R1   DD SYSOUT=(P,,L010)                                         00013800
//GA019R2   DD SYSOUT=(P,,L010)                                         00013900
/*                                                                      00014000
//*============================================================         00014100
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00014200
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00014300
//*============================================================         00014400
//STOPZEKE EXEC STOPZEKE                                                00014500
//*                                                                     00014600
//*================ E N D  O F  J C L  GAA015I  ========                00014700
