//GAAP015X JOB (SYS000),'R.COLIO-ACCTG',                                00000100
//         CLASS=K,MSGCLASS=X,MSGLEVEL=(1,1),                           00000200
//*        RESTART=UGAX630B,
//         NOTIFY=APCDBM,REGION=20M                                     00000400
//*++++++++++++++++++++++++++                                           00000500
//*   ZEKE EV # 1625  (*N.T.E.* -ALL ZEKE VAR.7/2008)                   00000600
//*     *N.T.E*
//*++++++++++++++++++++++++++                                           00000700
//********************************************************************
//* PREREQUISIT FOR THIS JOB
//*   RUN GAA015W - UGAX620
//*   LOAD AND POST UGAX620'S JVFILES-- ESPECIALLY FOR OLD YEARS.
//********************************************************************
//*
//*====================================================                 00012100
//*  RUN IDCAMS PROGRAM         - DELETE OLD VSAM FILE OF IFIS        * 00012300
//*                               EXTERNAL REPORT CODES               * 00012400
//*====================================================                 00012500
//DEVSM010 EXEC PGM=IDCAMS                                              00012600
//SYSPRINT  DD SYSOUT=*                                                 00012700
//SYSIN     DD *                                                        00012800
           DELETE   FISP.GAA015X.UGAX630A.RPTDATA.P$FISYYMM
           DELETE   FISP.GAA015X.UGAX630B.RPTFMTED.P$FISYYMM
           DELETE   FISP.GAA015X.UGAX630B.OLDYEAR.P$FISYYMM
           DELETE   FISP.GAA015X.UGAX630B.NEWYEAR.P$FISYYMM
//*********************************************************************
//* CALCULATE REVENUE, EXPENDITURE, ALLOCATION AMOUNTS FROM OPERATING
//* LEDGER AND GENERAL LEDGER FOR THE RESTRICTED FUND
//*********************************************************************
//*
//UGAX630A EXEC PGM=UGAX630A,DYNAMNBR=20,
//         COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR                            00004800
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR                               00005000
//          DD DSN=DB2F.DSNEXIT,DISP=SHR                                00005200
//          DD DSN=DB2F.DSNLOAD,DISP=SHR                                00005301
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX630A) PLAN(UGAX630A)
/*
//SYSOUT    DD SYSOUT=*                                                 00003700
//SYSLST    DD SYSOUT=*                                                 00003800
//SYSUDUMP  DD SYSOUT=*                                                 00003900
//SYSDBOUT  DD SYSOUT=*                                                 00004000
//REPORT01  DD SYSOUT=*                                                 00004100
//EXTRACT1  DD DSN=FISP.GAA015X.UGAX630A.RPTDATA.P$FISYYMM,             00005300
//             DISP=(NEW,CATLG,DELETE),                                 00005400
//             DCB=(RECFM=FB,LRECL=171,BLKSIZE=27873),                  00005500
//             SPACE=(TRK,(75,75),RLSE),                                00005600
//             UNIT=SYSDA                                               00005700
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00005800
//             DISP=SHR,                                                00005900
//             AMP=('BUFNI=2')                                          00006000
//PARM01    DD *                                                        00006200
*UNIVERSITY CODE $FISUNVRSCODE
*COA CODE $FISCOACODE
*FISCAL YEAR $FISFSCLYR
/*                                                                      00006300
//********************************************************************* 00012800
//* SORT BY LEVEL FUND NUMBER(PSEUDO), LEVEL NUMBER, FUND NUMBER        00012800
//********************************************************************* 00012800
//SORT030  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00012900
//SYSOUT    DD SYSOUT=*                                                 00013000
//SYSPRINT  DD SYSOUT=*                                                 00013100
//SORTIN    DD DSN=FISP.GAA015X.UGAX630A.RPTDATA.P$FISYYMM,             00013200
//             DISP=SHR                                                 00013300
//SORTOUT   DD DSN=FISP.GAA015X.UGAX630A.RPTDATA.P$FISYYMM,             00013400
//             DISP=OLD                                                 00013500
//SYSIN     DD *                                                        00013900
            SORT FIELDS=(15,6,CH,A,22,1,CH,A,1,6,CH,A)                  00041100
/*                                                                      00014000
//SORTWK01  DD SPACE=(TRK,300),                                         00014100
//             UNIT=SYSDA                                               00014200
//SORTWK02  DD SPACE=(TRK,300),                                         00014300
//             UNIT=SYSDA                                               00014400
//*
//********************************************************************* 00012800
//* CALCULATE REVENUE DEFER OR OVER EXPENDITURES.                       00012800
//* BASED ON OVER OR DEFER, CREATES JV FILES.                           00012800
//********************************************************************* 00012800
//UGAX630B EXEC PGM=UGAX630B,DYNAMNBR=20,
//         COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR                            00004800
//          DD DSN=CISP.ONLINE.LOADLIB,DISP=SHR                         00004800
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR                               00005000
//          DD DSN=DB2F.DSNEXIT,DISP=SHR                                00005200
//          DD DSN=DB2F.DSNLOAD,DISP=SHR                                00005301
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX630B) PLAN(UGAX630B)
/*
//SYSOUT    DD SYSOUT=*                                                 00003700
//SYSLST    DD SYSOUT=*                                                 00003800
//SYSUDUMP  DD SYSOUT=*                                                 00003900
//SYSDBOUT  DD SYSOUT=*                                                 00004000
//EXTRACTI  DD DSN=FISP.GAA015X.UGAX630A.RPTDATA.P$FISYYMM,             00005300
//             DISP=OLD                                                 00005400
//EXTRACTO  DD DSN=FISP.GAA015X.UGAX630B.RPTFMTED.P$FISYYMM,            01130004
//             DISP=(NEW,CATLG,DELETE),                                 01140004
//             DCB=(RECFM=FB,LRECL=186,BLKSIZE=27900),                  01150004
//             SPACE=(TRK,(960,750),RLSE),                              01160004
//             UNIT=SYSDA                                               01170004
//REPORT01  DD SYSOUT=*                                                 00004100
//*REPORT02  DD SYSOUT=(P,,L010)                                        00004200
//REPORT02  DD SYSOUT=(1,,L010)                                         00004200
//JVFILE01  DD DSN=FISP.GAA015X.UGAX630B.OLDYEAR.P$FISYYMM,             01130004
//             DISP=(NEW,CATLG,CATLG),                                  01140004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  01150004
//             SPACE=(TRK,(960,750),RLSE),                              01160004
//             UNIT=SYSDA                                               01170004
//JVFILE02  DD DSN=FISP.GAA015X.UGAX630B.NEWYEAR.P$FISYYMM,             01130004
//             DISP=(NEW,CATLG,CATLG),                                  01140004
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  01150004
//             SPACE=(TRK,(960,750),RLSE),                              01160004
//             UNIT=SYSDA                                               01170004
//PARM01    DD *                                                        00006200
*FISCAL YEAR $FISFSCLYR
*OLD YEAR DOCUMENT CXRDN9$FISYR./RESTRICT FUND ACTIVITIES OLD YEAR
*NEW YEAR DOCUMENT CZRDN9$FISYR./RESTRICT FUND ACTIVITIES NEW YEAR
*SUBSYSTEM-ID CLOSING
/*                                                                      00006300
//*OLD YEAR DOCUMENT CXRDN608/(MYY FORMAT-ONLY CHANGE YY VALUE)
//*NEW YEAR DOCUMENT CZRDN608/(MYY FORMAT-ONLY CHANGE YY VALUE)
//*====================================================                 02570004
//SORT050  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02580004
//SYSOUT    DD SYSOUT=*                                                 02590004
//SYSPRINT  DD SYSOUT=*                                                 02600004
//SORTIN    DD DSN=FISP.GAA015X.UGAX630B.OLDYEAR.P$FISYYMM,             02610004
//             DISP=OLD                                                 02620004
//SORTOUT   DD DSN=FISP.GAA015X.UGAX630B.OLDYEAR.P$FISYYMM,             02630004
//             DISP=OLD                                                 02640004
//SYSIN     DD *                                                        02680004
           SORT FIELDS=(1,19,CH,A,20,4,ZD,A)                            02690004
/*                                                                      02700004
//SORTWK01  DD SPACE=(TRK,900),                                         02710004
//             UNIT=SYSDA                                               02720004
//SORTWK02  DD SPACE=(TRK,900),                                         02730004
//             UNIT=SYSDA                                               02740004
//SORTWK03  DD SPACE=(TRK,900),                                         02730004
//             UNIT=SYSDA                                               02740004
//*====================================================                 02750004
//PRNT060  EXEC PGM=FGAP020D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02760004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,                                    02770004
//             DISP=SHR                                                 02780004
//SYSOUT    DD SYSOUT=*                                                 02830004
//SYSUDUMP  DD SYSOUT=*                                                 02840004
//UGAUA00I  DD DSN=FISP.GAA015X.UGAX630B.OLDYEAR.P$FISYYMM,             02850004
//             DISP=OLD                                                 02860004
//*GA019R1   DD SYSOUT=(P,,L010)                                        00012500
//GA019R1   DD SYSOUT=(1,,L010)                                         00012500
//*GA019R2   DD SYSOUT=(P,,L010)                                        00012600
//GA019R2   DD SYSOUT=(1,,L010)                                         00012600
//*                                                                     00012800
//*====================================================                 02570004
//SORT070  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02580004
//SYSOUT    DD SYSOUT=*                                                 02590004
//SYSPRINT  DD SYSOUT=*                                                 02600004
//SORTIN    DD DSN=FISP.GAA015X.UGAX630B.NEWYEAR.P$FISYYMM,             02610004
//             DISP=OLD                                                 02620004
//SORTOUT   DD DSN=FISP.GAA015X.UGAX630B.NEWYEAR.P$FISYYMM,             02630004
//             DISP=OLD                                                 02640004
//SYSIN     DD *                                                        02680004
           SORT FIELDS=(1,19,CH,A,20,4,ZD,A)                            02690004
/*                                                                      02700004
//SORTWK01  DD SPACE=(TRK,900),                                         02710004
//             UNIT=SYSDA                                               02720004
//SORTWK02  DD SPACE=(TRK,900),                                         02730004
//             UNIT=SYSDA                                               02740004
//SORTWK03  DD SPACE=(TRK,900),                                         02730004
//             UNIT=SYSDA                                               02740004
//*====================================================                 02750004
//PRNT080  EXEC PGM=FGAP020D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02760004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,                                    02770004
//             DISP=SHR                                                 02780004
//SYSOUT    DD SYSOUT=*                                                 02830004
//SYSUDUMP  DD SYSOUT=*                                                 02840004
//UGAUA00I  DD DSN=FISP.GAA015X.UGAX630B.NEWYEAR.P$FISYYMM,             02850004
//             DISP=OLD                                                 02860004
//*GA019R1   DD SYSOUT=(P,,L010)                                        00012500
//GA019R1   DD SYSOUT=(1,,L010)                                         00012500
//*GA019R2   DD SYSOUT=(P,,L010)                                        00012600
//GA019R2   DD SYSOUT=(1,,L010)                                         00012600
//*                                                                     00016300
//*============================================================         00016300
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00016400
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00016500
//*============================================================         00016600
//STOPZEKE EXEC STOPZEKE                                                00016700
//*                                                                     00016800
//*=========== END  OF JCL GAA015X===================================   00016600
