//GAAP020W JOB (SYS000),'GEN-ACCTG',                                    00000100
//         CLASS=K,MSGCLASS=X,MSGLEVEL=(1,1),                           00000200
//*        RESTART=SORT30,                                              00000300
//         NOTIFY=APCDBM,REGION=20M                                     00000400
//*========================                                             00000300
//*    ZEKE EVENT # 1367   -- LOC REDISTRIBUTION EXTRACT                00000400
//*==== *N.T.E*    -ALL ZEKE VAR.7/2008                                 00000300
//*====================================================                 00000600
//CLEAN10  EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00000700
//DD1       DD DSN=FISP.GAA020W.UGAX625.LOCDIST.P$FISYYMM,              00000800
//             DISP=(MOD,DELETE,DELETE),                                00000900
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00001000
//             SPACE=(TRK,(15,150),RLSE),                               00001100
//             UNIT=SYSDA                                               00001200
//*====================================================                 00001300
//TRANS20  EXEC PGM=UGAX625,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00001400
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX625) PLAN(UGAX625)
/*
//SYSOUT    DD SYSOUT=*                                                 00002300
//SYSLST    DD SYSOUT=*                                                 00002400
//SYSUDUMP  DD SYSOUT=D                                                 00002500
//SYSDBOUT  DD SYSOUT=*                                                 00002600
//REPORT01  DD SYSOUT=*,                                                00002700
//             DCB=BLKSIZE=133                                          00002800
//JVFILE01  DD DSN=FISP.GAA020W.UGAX625.LOCDIST.P$FISYYMM,              00002900
//             DISP=(NEW,CATLG,CATLG),                                  00003000
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00003100
//             SPACE=(TRK,(15,150),RLSE),                               00003200
//             UNIT=SYSDA                                               00003300
//PARM02    DD DUMMY                                                    00003400
//PARM01    DD *                                                        00003500
*UNIVERSITY CODE $FISUNVRSCODE
*COA CODE $FISCOACODE
*FISCAL YEAR $FISFSCLYR
*LOC DOCUMENT FELOC141/LOC REDISTRIBUTION
*SUBSYSTEM-ID CLOSING
*ACCOUNTING PERIOD 14                                                   00030000
/*                                                                      00003600
//*====================================================                 00003700
//* L#44 VALUE = FELOC141 (14=ACCOUNTING PERIOD, 1=FIRST DOC OF YEAR)
//*              ** NOTHING TO EDIT **
//*====================================================                 00003700
//SORT30   EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00003800
//SYSOUT    DD SYSOUT=*                                                 00003900
//SYSPRINT  DD SYSOUT=*                                                 00004000
//SORTIN    DD DSN=FISP.GAA020W.UGAX625.LOCDIST.P$FISYYMM,              00004100
//             DISP=SHR                                                 00004200
//SORTOUT   DD DSN=&&UGAUA00I,                                          00004300
//             DISP=(NEW,PASS,DELETE),                                  00004400
//             DCB=(RECFM=FB,BLKSIZE=23250,LRECL=250),                  00004500
//             SPACE=(TRK,(15,150),RLSE),                               00004600
//             UNIT=VIO                                                 00004700
//SYSIN     DD *                                                        00004800
           SORT FIELDS=(1,19,CH,A,20,4,ZD,A)                            00041100
/*                                                                      00004900
//SORTWK01  DD SPACE=(TRK,300),                                         00005000
//             UNIT=SYSDA                                               00005100
//SORTWK02  DD SPACE=(TRK,300),                                         00005200
//             UNIT=SYSDA                                               00005300
/*                                                                      00005400
//*====================================================                 00005500
//PRINT40  EXEC PGM=FGAP020D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00005600
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 00006300
//UGAUA00I  DD DSN=&&UGAUA00I,                                          00006400
//             DISP=(OLD,DELETE)                                        00006500
//GA019R1   DD SYSOUT=(P,,L010)                                         00006600
//GA019R2   DD SYSOUT=(P,,L010)                                         00006700
/*                                                                      00006800
//*                                                                     00006800
//*============================================================         00006800
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00006800
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00006800
//*============================================================         00006800
//STOPZEKE EXEC STOPZEKE                                                00006800
//*                                                                     00006800
//*================ E N D  O F  J C L  GAA020W  ========                00007500
