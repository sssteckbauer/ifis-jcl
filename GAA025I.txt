//GAAP025I JOB (SYS000),'STE.MARIE-ACCTG',                              00010005
//         CLASS=K,MSGCLASS=X,MSGLEVEL=(1,1),                           00020005
//*        RESTART=PRNT070,                                             00030009
//         NOTIFY=APCDBM,REGION=20M                                     00040005
//*++++++++++++++++++++++++++                                           00050005
//*    ZEKE EVENT # 1392     (NOT RUN SINCE 1995)                       00060005
//*++++++++++++++++++++++++++                                           00070005
//*====================================================                 00080005
//*          INTERIM - BUDGET ROLL/REAPPROPRIATION - REPORTS ONLY       00090005
//*====================================================                 00100005
//*              DELETE CATALOGED DATA SETS                             00110005
//*====================================================                 00120005
//CLEAN000 EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00130005
//DD1       DD DSN=FISP.GAA025I.UGAXC03.RPTDATA.P$FISYYMM,              00140005
//             DISP=(MOD,DELETE,DELETE),                                00150005
//             DCB=(RECFM=FB,LRECL=210,BLKSIZE=23310),                  00160005
//             SPACE=(TRK,(15,15),RLSE),                                00170005
//             UNIT=SYSDA                                               00180005
//DD2       DD DSN=FISP.GAA025I.UGAXC03.BDGTROLL.P$FISYYMM,             00190005
//             DISP=(MOD,DELETE,DELETE),                                00200005
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00210005
//             SPACE=(TRK,(75,75),RLSE),                                00220005
//             UNIT=SYSDA                                               00230005
//DD3       DD DSN=FISP.GAA025I.UGAXC03.JVFILE01.P$FISYYMM,             00190005
//             DISP=(MOD,DELETE,DELETE),                                00200005
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00210005
//             SPACE=(TRK,(75,75),RLSE),                                00220005
//             UNIT=SYSDA                                               00230005
//DD4       DD DSN=FISP.GAA025I.UGAXC03.JVFILE65.P$FISYYMM,             00190005
//             DISP=(MOD,DELETE,DELETE),                                00200005
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00210005
//             SPACE=(TRK,(75,75),RLSE),                                00220005
//             UNIT=SYSDA                                               00230005
//DD5       DD DSN=FISP.GAA025I.UGAXC03.SORTOUT.P$FISYYMM,              00190005
//             DISP=(MOD,DELETE,DELETE),                                00200005
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00210005
//             SPACE=(TRK,(75,75),RLSE),                                00220005
//             UNIT=SYSDA                                               00230005
//*====================================================                 00240005
//*   RUN IDCAMS PROGRAM TO DEFINE AND CATALOG A VSAM FILE              00250005
//*   USING ACCESS METHOD SERVICES COMMANDS.                            00260005
//*====================================================                 00270005
//IDCM010  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00280005
//SYSPRINT  DD SYSOUT=*                                                 00290005
//SYSIN     DD *                                                        00300005
            DELETE -                                                    00310005
                FISP.GAA025I.UGAXC03 -                                  00320005
                PURGE -                                                 00330005
                ERASE                                                   00340005
/*                                                                      00350005
//*====================================================                 00360005
//*       IDCAMS - DEFINE VSAM FILE                                     00370005
//*====================================================                 00380005
//IDCM020  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00390005
//SYSPRINT  DD SYSOUT=*                                                 00400005
//SYSIN     DD *                                                        00410005
            DEFINE CLUSTER -                                            00420005
                (NAME (FISP.GAA025I.ACCTINDX) -                         00430005
                 FREESPACE (20 30) -                                    00440005
                 CISZ (4096) -                                          00450005
                 INDEXED -                                              00460005
                 REUSE -                                                00470005
                 IMBED -                                                00480005
                 NOREPLICATE -                                          00490005
                 KEYS (18,0) -                                          00500005
                 RECORDSIZE (28 28) -                                   00510005
                 VOLUME (APC001)) -                                     00520005
            DATA -                                                      00530005
                (NAME(FISP.GAA025I.ACCTINDX.DATA) -                     00540005
                 CYLINDERS (2,2)) -                                     00550005
            INDEX -                                                     00560005
                (NAME(FISP.GAA025I.ACCTINDX.INDEX))                     00570005
/*                                                                      00580005
//*====================================================                 00590005
//*       SORT INDEX FILE FOR VSAM INPUT                                00600005
//*====================================================                 00610005
//SORT030  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00620005
//SYSLST    DD SYSOUT=*                                                 00630005
//SYSOUT    DD SYSOUT=*                                                 00640005
//SYSUDUMP  DD SYSOUT=D                                                 00650005
//SORTIN    DD DSN=GAOP.BDGTROLL.ACCTINDX,                              00660005
//             DISP=SHR                                                 00670005
//SORTWK01  DD SPACE=(TRK,75),                                          00680005
//             UNIT=SYSDA                                               00690005
//SORTWK02  DD SPACE=(TRK,75),                                          00700005
//             UNIT=SYSDA                                               00710005
//SORTWK03  DD SPACE=(TRK,75),                                          00720005
//             UNIT=SYSDA                                               00730005
//SORTOUT   DD DSN=&&ACCTINDX,                                          00740005
//             DISP=(NEW,PASS,DELETE),                                  00750005
//             DCB=(RECFM=FB,LRECL=28,BLKSIZE=23464),                   00760005
//             SPACE=(TRK,(75,15),RLSE),                                00770005
//             UNIT=VIO                                                 00780005
//SYSIN     DD *                                                        00790005
            OUTREC FIELDS=(1:1,28)                                      00800005
            SORT FIELDS=(1,18,CH,A)                                     00810005
/*                                                                      00820005
//*====================================================                 00830005
//*     COPY INDEX FILE INTO VSAM FILE                                  00840005
//*====================================================                 00850005
//IDCM040  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00860005
//INPUT     DD DSN=&&ACCTINDX,                                          00870005
//             DISP=(OLD,DELETE)                                        00880005
//OUTPUT    DD DSN=FISP.GAA025I.ACCTINDX,                               00890005
//             DISP=SHR                                                 00900005
//SYSPRINT  DD SYSOUT=*                                                 00910005
//SYSIN     DD *                                                        00920005
            REPRO -                                                     00930005
                INFILE(INPUT) -                                         00940005
                OUTFILE(OUTPUT) -                                       00950005
                REUSE                                                   00960005
/*                                                                      00970005
//*====================================================                 00980005
//*       BUDGET ROLL                                                   00990005
//*====================================================                 01000005
//EXTR050  EXEC PGM=UGAXC03,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          01010005
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAXC03) PLAN(UGAXC03)
/*
//SYSOUT    DD SYSOUT=*                                                 01100005
//SYSLST    DD SYSOUT=*                                                 01110005
//SYSUDUMP  DD SYSOUT=D                                                 01120005
//SYSDBOUT  DD SYSOUT=*                                                 01130005
//JVFILE01  DD DSN=FISP.GAA025I.UGAXC03.JVFILE01.P$FISYYMM,             01140005
//             DISP=(NEW,CATLG,DELETE),                                 01150005
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  01160005
//             SPACE=(TRK,(75,75),RLSE),                                01170005
//             UNIT=SYSDA                                               01180005
//EXTRAC01  DD DSN=FISP.GAA025I.UGAXC03.RPTDATA.P$FISYYMM,              01190005
//             DISP=(NEW,CATLG,DELETE),                                 01200005
//             DCB=(RECFM=FB,LRECL=210,BLKSIZE=23310),                  01210005
//             SPACE=(TRK,(225,225),RLSE),                              01220005
//             UNIT=SYSDA                                               01230005
//REPORT01  DD SYSOUT=(H,,L010)                                         01240005
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       01250005
//             DISP=SHR,                                                01260005
//             AMP=('BUFNI=2')                                          01270005
//VSAM01    DD DSN=FISP.GAA025I.ACCTINDX,                               01280005
//             DISP=SHR,                                                01290005
//             AMP=('BUFNI=2')                                          01300005
//PARM01    DD *                                                        01310005
*UNIVERSITY CODE $FISUNVRSCODE                                          01320005
*COA CODE $FISCOACODE                                                   01330005
*FISCAL YEAR $FISFSCLYR                                                 01340005
*SUBSYSTEM-ID BUDROLL                                                   01350005
*REPORTS ONLY                                                           01360005
/*                                                                      01370005
//PARM02    DD *                                                        01380005
*FUND 18000A 18199Z                                                     01390010
*FUND 20500A 20599Z                                                     01400010
*FUND 34100A 39428Z                                                     01410010
/*                                                                      01420005
//*====================================================                 01440005
//*       SORT EXTRACT FROM UGAXC03                                     01450005
//*====================================================                 01460005
//SORT060  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         01470005
//SYSLST    DD SYSOUT=*                                                 01480005
//SYSOUT    DD SYSOUT=*                                                 01490005
//SYSUDUMP  DD SYSOUT=D                                                 01500005
//SORTIN    DD DSN=FISP.GAA025I.UGAXC03.RPTDATA.P$FISYYMM,              01510005
//             DISP=SHR                                                 01520005
//SORTOUT   DD DSN=FISP.GAA025I.UGAXC03.RPTDATA.P$FISYYMM,              01530005
//             DISP=SHR                                                 01540005
//SORTWK01  DD SPACE=(TRK,450),                                         01550005
//             UNIT=SYSDA                                               01560005
//SORTWK02  DD SPACE=(TRK,450),                                         01570005
//             UNIT=SYSDA                                               01580005
//SORTWK03  DD SPACE=(TRK,450),                                         01590005
//             UNIT=SYSDA                                               01600005
//SORTWK04  DD SPACE=(TRK,450),                                         01610005
//             UNIT=SYSDA                                               01620005
//SORTWK05  DD SPACE=(TRK,450),                                         01630005
//             UNIT=SYSDA                                               01640005
//SORTWK06  DD SPACE=(TRK,450),                                         01650005
//             UNIT=SYSDA                                               01660005
//SORTWK07  DD SPACE=(TRK,450),                                         01670005
//             UNIT=SYSDA                                               01680005
//SORTWK08  DD SPACE=(TRK,450),                                         01690005
//             UNIT=SYSDA                                               01700005
//SORTWK09  DD SPACE=(TRK,450),                                         01710005
//             UNIT=SYSDA                                               01720005
//SORTWK10  DD SPACE=(TRK,450),                                         01730005
//             UNIT=SYSDA                                               01740005
//SYSIN     DD DSN=FISP.PARMLIB(UGAPC03A),                              01750005
//             DISP=SHR                                                 01760005
/*                                                                      01770005
//*
//*====================================================
//*       SORT EXTRACT FROM UGAXC03
//*====================================================
//SORT062  EXEC  PGM=SYNCSORT
//SYSOUT   DD  SYSOUT=*
//SYSPRINT DD  SYSOUT=*
//SORTIN    DD DSN=FISP.GAA025I.UGAXC03.JVFILE01.P$FISYYMM,
//             DISP=SHR
//SORTOUT   DD DSN=FISP.GAA025I.UGAXC03.SORTOUT.P$FISYYMM,              01140005
//             DISP=(NEW,CATLG,DELETE),                                 01150005
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  01160005
//             SPACE=(TRK,(75,75),RLSE),                                01170005
//             UNIT=SYSDA                                               01180005
//SORTWK01  DD SPACE=(TRK,300),
//             UNIT=SYSDA
//SORTWK02  DD SPACE=(TRK,300),
//             UNIT=SYSDA
//SORTWK03  DD SPACE=(TRK,300),
//             UNIT=SYSDA
//SORTWK04  DD SPACE=(TRK,300),
//             UNIT=SYSDA
//SYSIN    DD  *
           SORT FIELDS=(11,8,CH,A,19,1,CH,A,77,6,CH,A,83,6,CH,A,
                        95,6,CH,A,89,6,CH,A,113,10,CH,A,75,1,CH,A)
           SUM FIELDS=(28,12,ZD)
/*                                                                      01430005
//*====================================================                 01740004
//*           ROLLUP LIKE ACCOUNTS  ===================                 01740004
//*====================================================                 01740004
//RLUP065  EXEC PGM=UGAXC05,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          01020004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 01110004
//SYSLST    DD SYSOUT=*                                                 01120004
//SYSUDUMP  DD SYSOUT=D                                                 01130004
//SYSDBOUT  DD SYSOUT=*                                                 01140004
//UGAUA00I  DD DSN=FISP.GAA025I.UGAXC03.SORTOUT.P$FISYYMM,DISP=SHR      01150004
//ROLLUP    DD DSN=FISP.GAA025I.UGAXC03.JVFILE65.P$FISYYMM,             01150004
//             DISP=(NEW,CATLG,DELETE),
//             SPACE=(TRK,(1005,150),RLSE),                             00001900
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=27750),
//             UNIT=SYSDA                                               00002000
//*============================================================         00047000
//SORT066  EXEC  PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))        00048000
//SYSOUT   DD  SYSOUT=*                                                 00049000
//SYSPRINT DD  SYSOUT=*                                                 00050000
//SORTIN   DD  DSN=FISP.GAA025I.UGAXC03.JVFILE65.P$FISYYMM,DISP=SHR     00060000
//SORTOUT  DD  DSN=FISP.GAA025I.UGAXC03.BDGTROLL.P$FISYYMM,             00070000
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=27750),
//             SPACE=(TRK,(15,5),RLSE),
//             UNIT=SYSDA                                               00090000
//SORTWK01  DD SPACE=(TRK,300),                                         00092000
//             UNIT=SYSDA                                               00093000
//SORTWK02  DD SPACE=(TRK,300),                                         00094000
//             UNIT=SYSDA                                               00095000
//SORTWK03  DD SPACE=(TRK,300),                                         00096000
//             UNIT=SYSDA                                               00097000
//SORTWK04  DD SPACE=(TRK,300),                                         00098000
//             UNIT=SYSDA                                               00099000
//SYSIN    DD  *                                                        00100000
           SORT FIELDS=(1,29,CH,A)                                      00110000
/*                                                                      00110500
//*====================================================                 01780005
//*  BUDGET ROLL REPORT                                                 01790005
//*====================================================                 01800005
//PRNT070  EXEC PGM=UGAPC03A,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         01810005
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAPC03A) PLAN(UGAPC03A)
/*
//SYSOUT    DD SYSOUT=*                                                 01900005
//SYSLST    DD SYSOUT=*                                                 01910005
//SYSUDUMP  DD SYSOUT=D                                                 01920005
//SYSDBOUT  DD SYSOUT=*                                                 01930005
//REPORT01  DD SYSOUT=(H,,L010)                                         01940005
//REPORT02  DD SYSOUT=(H,,L010)                                         01950005
//EXTRAC01  DD DSN=FISP.GAA025I.UGAXC03.RPTDATA.P$FISYYMM,              01960005
//             DISP=SHR                                                 01970005
//PARM01    DD *                                                        01980005
*UNIVERSITY CODE $FISUNVRSCODE                                          01990005
*COA CODE $FISCOACODE                                                   02000005
*FISCAL YEAR $FISFSCLYR                                                 02010005
/*                                                                      02020005
//*====================================================                 02030005
//*       SORT EXTRACT FROM UGAXC03                                     02040005
//*====================================================                 02050005
//SORT080  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02060005
//SYSLST    DD SYSOUT=*                                                 02070005
//SYSOUT    DD SYSOUT=*                                                 02080005
//SYSUDUMP  DD SYSOUT=D                                                 02090005
//SORTIN    DD DSN=FISP.GAA025I.UGAXC03.RPTDATA.P$FISYYMM,              02100005
//             DISP=SHR                                                 02110005
//SORTWK01  DD SPACE=(TRK,300),                                         02120005
//             UNIT=SYSDA                                               02130005
//SORTWK02  DD SPACE=(TRK,300),                                         02140005
//             UNIT=SYSDA                                               02150005
//SORTWK03  DD SPACE=(TRK,300),                                         02160005
//             UNIT=SYSDA                                               02170005
//SORTWK04  DD SPACE=(TRK,300),                                         02180005
//             UNIT=SYSDA                                               02190005
//SORTWK05  DD SPACE=(TRK,300),                                         02200005
//             UNIT=SYSDA                                               02210005
//SORTWK06  DD SPACE=(TRK,300),                                         02220005
//             UNIT=SYSDA                                               02230005
//SORTWK07  DD SPACE=(TRK,300),                                         02240005
//             UNIT=SYSDA                                               02250005
//SORTWK08  DD SPACE=(TRK,300),                                         02260005
//             UNIT=SYSDA                                               02270005
//SORTWK09  DD SPACE=(TRK,300),                                         02280005
//             UNIT=SYSDA                                               02290005
//SORTWK10  DD SPACE=(TRK,300),                                         02300005
//             UNIT=SYSDA                                               02310005
//SORTOUT   DD DSN=&&UGAP03B,                                           02320005
//             DISP=(NEW,PASS,DELETE),                                  02330005
//             DCB=(RECFM=FB,LRECL=210,BLKSIZE=23310),                  02340005
//             SPACE=(TRK,(225,225),RLSE),                              02350005
//             UNIT=VIO                                                 02360005
//SYSIN     DD DSN=FISP.PARMLIB(UGAPC03B),                              02370005
//             DISP=SHR                                                 02380005
//*====================================================                 02390005
//*  BUDGET ROLL REPORT                                                 02400005
//*====================================================                 02410005
//PRNT090  EXEC PGM=UGAPC03B,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02420005
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAPC03B) PLAN(UGAPC03B)
/*
//SYSOUT    DD SYSOUT=*                                                 02510005
//SYSLST    DD SYSOUT=*                                                 02520005
//SYSUDUMP  DD SYSOUT=D                                                 02530005
//SYSDBOUT  DD SYSOUT=*                                                 02540005
//REPORT01  DD SYSOUT=(H,,L010)                                         02550005
//REPORT02  DD SYSOUT=(H,,L010)                                         02560005
//EXTRAC01  DD DSN=&&UGAP03B,                                           02570005
//             DISP=(OLD,DELETE)                                        02580005
//PARM01    DD *                                                        02590005
*UNIVERSITY CODE $FISUNVRSCODE                                          02600005
*COA CODE $FISCOACODE                                                   02610005
*FISCAL YEAR $FISFSCLYR                                                 02620005
/*                                                                      02630005
//*====================================================                 02640005
//STEP100  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02650005
//SYSOUT    DD SYSOUT=*                                                 02660005
//SYSPRINT  DD SYSOUT=*                                                 02670005
//SORTIN    DD DSN=FISP.GAA025I.UGAXC03.BDGTROLL.P$FISYYMM,             02680005
//             DISP=SHR                                                 02690005
//SORTOUT   DD DSN=&&UGAUA00I,                                          02700005
//             DISP=(NEW,PASS,DELETE),                                  02710005
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  02720005
//             SPACE=(TRK,(15,150),RLSE),                               02730005
//             UNIT=VIO                                                 02740005
//SYSIN     DD *                                                        02750005
           SORT FIELDS=(1,19,CH,A,20,4,ZD,A)                            02760005
/*                                                                      02770005
//SORTWK01  DD SPACE=(TRK,300),                                         02780005
//             UNIT=SYSDA                                               02790005
//SORTWK02  DD SPACE=(TRK,300),                                         02800005
//             UNIT=SYSDA                                               02810005
//*====================================================                 02820005
//PRNT110  EXEC PGM=FGAP020D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         02830005
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 02900005
//SYSUDUMP  DD SYSOUT=D                                                 02910005
//UGAUA00I  DD DSN=&&UGAUA00I,                                          02920005
//             DISP=(OLD,DELETE)                                        02930005
//GA019R1   DD SYSOUT=(H,,L010)                                         02940005
//GA019R2   DD SYSOUT=(H,,L010)                                         02950005
/*                                                                      02960005
//*============================================================
//*      RENAMER OF EXTERNAL FILE
//*============================================================
//RENAMER  EXEC PGM=IDCAMS
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   ALTER      GAOP.BDGTROLL.ACCTINDX -
              NEWNAME(GAOP.BDGTROLL.ACCTINDX.P$FISYYMM)
/*
//*============================================================         02970005
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          02980005
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      02990005
//*============================================================         03000005
//STOPZEKE EXEC STOPZEKE                                                03010005
//*                                                                     03020005
//*================ E N D  O F  J C L  GAA025I  ========                03030005
