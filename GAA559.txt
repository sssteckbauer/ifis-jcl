//GAAP559  JOB (SYS000),'FUND PERM UPDATE',
//            MSGLEVEL=(1,1),
//            MSGCLASS=X,
//            CLASS=S,
//            NOTIFY=ACLPLO,
//            REGION=20M
//***************************************
//* ZEKE EV # 3492
//**********************************************************************
//* JOB SHOULD BE SCHEDULED THE SATURDAY FOLLOWING JULY LEDGER.
//**********************************************************************
//* THIS IS A YEARLY JOB THAT UPDATES FUND PERM (UCF) FUNDS BY UPDATING
//* THE STATUS TO "INACTIVE" IF THE MOST CURRENT FUND (FDE) HAS BEEN
//* INACTIVATED IN THE PAST 24 MONTHS, AND THE UCF FUND HAS NOT BEEN
//* CHANGED.  PLANT FUNDS ARE OMITTED FROM THIS UPDATE.
//*********************************************************************
//* DELETE PRE-EXISTING OUTPUT FILES (IF ANY) CREATED BY THIS JOB
//*********************************************************************
//SDEL010  EXEC PGM=IEFBR14
//DD1       DD DSN=FISP.GAA559.CNTLRPT,
//             DISP=(MOD,DELETE,DELETE),
//             UNIT=SYSDA
//*********************************************************************
//* GAA559 - UPDATE UCOP FUND PERM TABLE
//**********************************************************************
//STEP010  EXEC PGM=GAA559
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(GAA559) PLAN(GAA559)
/*
//CNTLRPT   DD DSN=FISP.GAA559.CNTLRPT,
//             DISP=(NEW,CATLG,CATLG),
//             DCB=(RECFM=FBA,LRECL=133),
//             SPACE=(TRK,(10,05),RLSE),
//             UNIT=SYSDA
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//*
//*----------------------------------------------------------------- */
//*  CONVERT A TEXT FILE TO PDF AND E-MAIL                           */
//*----------------------------------------------------------------- */
//PDFMAIL     EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.EXEC
//RPTFILE     DD   DSN=FISP.GAA559.CNTLRPT,DISP=SHR
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(CYL,(1,1)),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT    DD   SYSOUT=*
//SYSTSPRT    DD   SYSOUT=*
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(GAA559)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(GAA559)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(GAA559)
//*
/*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  GAAP559 ==========
