//GAAP900  JOB (SYS000),'ACCTG-LEDGER',NOTIFY=APCDBM,                   00010004
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M             00020004
//*++++++++++++++++++++++++++                                           00030004
//*        RESTART=STEP03,
//*        RESTART=STEP04,
//*++++++++++++++++++++++++++                                           00030004
//*    ZEKE EVENT # 1291                                                00040004
//*++++++++++++++++++++++++++                                           00050004
// JCLLIB ORDER=APCP.PROCLIB                                            00060004
/*JOBPARM LINES=250                                                     00070004
//OUT1 OUTPUT CLASS=F,FORMS=P011,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL   00080004
//*====================================================                 00090004
//*     G A A 9 0 0  -  "APPROVAL PURGE"                                00100004
//*                                                                     00110004
//*     THIS JCL WILL DELETE SELECTED DOCUMENT APPROVAL RECORDS         00120004
//*     FROM THE IDMS DATABASE. THE '*DOCUMENT SEQUENCE' SYMBOLIC       00130004
//*     PARAMETER SHOULD ALWAYS BE '0007' FOR THIS PROCESS.             00140004
//*                                                                     00150004
//*     ABEND INSTRUCTION:
//*        IF ABEND IN STEP02 (PGM=UUTU900) - PROGRAM IS SET
//*        TO ABEND IF THE PURGE DATE IS NOT JUNE 30TH.
//*      !!JOB IS TO BE RUN DURING FISCAL CLOSING ONLY.
//*        THE ERROR MESSAGE WILL READ:
//*
//*            *PURGE DATE 20100630     (YYYYMMDD)
//*            *DOCUMENT SEQUENCE 0007
//*             ERROR: PURGE DATE INVALID FOR JV DELETE
//*             ERROR: INVALID PARMS
//*--------------------------------------------------------------
//*     RESTART INFORMATION:                                            00150004
//*       IF THE JOB ABENDS AFTER STEP02, RESTART AT STEP03             00150004
//*       (OR STEP04 - IF JOB ABENDED AFTER STEP04) AFTER THE           00150004
//*       PROBLEM IS RESOLVED. OTHERWISE RESTART THE JOB FROM           00150004
//*       THE BEGINNING.                                                00150004
//*                                                                     00150004
//*                                                                     00150004
//*     SYMBOLIC PARAMETERS:                                            00160004
//*         *PURGE DATE $FISYYYYMMDD                                    00170004
//*         *DOCUMENT SEQUENCE 0007                                     00180004
//*         *ZEKE CURRENT DATE VARIABLE $BJDT                           00190004
//*                                                                     00190004
//*     DATA SETS CATALOGED:                                            00200004
//*          FISP.GAA900.UUTU900.EXTRACT.D$BJDT
//*                                                                     00210004
//*==========================================================*
//*   DELETE ANY POTENTIAL CATALOGED (NEW) OUTPUT FILES      *
//*==========================================================*
//STEP01   EXEC PGM=IDCAMS
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
  DELETE    FISP.GAA900.UUTU900.EXTRACT.D$BJDT
/*
//*====================================================*
//*  CREATE THE INITIAL R4212 EXTRACT FILE             *
//*====================================================*
//STEP02   EXEC PGM=UUTU900,COND=EVEN                                   00230004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UUTU900) PLAN(UUTU900)
/*
//EXTRAC01  DD DSN=FISP.GAA900.UUTU900.EXTRACT.D$BJDT,                  00420004
//             DISP=(NEW,CATLG,KEEP),                                   00430004
//             DCB=(LRECL=30,BLKSIZE=27000,RECFM=FB),                   00430004
//             SPACE=(TRK,(2010,45),RLSE),                              00430004
//             UNIT=SYSDA                                               00430004
//SYSOUT    DD SYSOUT=*                                                 00340004
//SYSLST    DD SYSOUT=*                                                 00350004
//SYSUDUMP  DD SYSOUT=*                                                 00360004
//SYSDBOUT  DD SYSOUT=*                                                 00370004
//PARM01    DD *                                                        00380004
*PURGE DATE $FISYEAR.0630                                               00390005
*DOCUMENT SEQUENCE 0007                                                 00400004
//*********************************************************
//* PURGE DATE MUST ALWAYS BE YYYY0630 - MUST BE LAST DAY
//* OF JUNE OF FISCAL YEAR THAT YOU ARE CURRENTLY IN
//*********************************************************
/*                                                                      00410004
//REPORT01  DD SYSOUT=*                                                 00420004
//*                                                                     00430004
//*PURGE DATE YYYYMMDD                                                  00440004
//*PURGE DATE 20100630  (DISPLAY PURGE DATE)                            00390005
//*
//*====================================================                 00450004
//*  SORT THE PURGE EXTRACT FILE BY DBKEY             *                 00004900
//*====================================================                 00450004
//STEP04   EXEC PGM=SYNCSORT                                            00005000
//SYSOUT    DD SYSOUT=*                                                 00005100
//SORTWK01  DD SPACE=(CYL,(20,10)),                                     00005200
//             UNIT=SYSDA                                               00005300
//SORTWK02  DD SPACE=(CYL,(20,10)),                                     00005400
//             UNIT=SYSDA                                               00005500
//SORTWK03  DD SPACE=(CYL,(20,10)),                                     00005600
//             UNIT=SYSDA                                               00005700
//SORTWK04  DD SPACE=(CYL,(20,10)),                                     00005600
//             UNIT=SYSDA                                               00005700
//SORTWK05  DD SPACE=(CYL,(20,10)),                                     00005600
//             UNIT=SYSDA                                               00005700
//SORTIN    DD DSN=FISP.GAA900.UUTU900.EXTRACT.D$BJDT,                  00005800
//             DISP=SHR                                                 00005900
//SORTOUT   DD DSN=&&PURGEIDS,                                          00006000
//             DISP=(NEW,PASS),                                         00006500
//             DCB=(LRECL=30,BLKSIZE=27000,RECFM=FB),                   00006600
//             SPACE=(TRK,(1500,150),RLSE),                             00006700
//             UNIT=SYSDA                                               00006800
//SYSIN     DD *                                                        00006200
      SORT FIELDS=(1,4,CH,A)
/*                                                                      00006300
//*====================================================                 00450004
//*  ERASE THE OBSOLETE R4212 RECORDS                 *                 00004900
//*====================================================                 00450004
//STEP05   EXEC PGM=UUTU901                                             00230004
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UUTU901) PLAN(UUTU901)
/*
//R4212EXT  DD DSN=&&PURGEIDS,                                          00420004
//             DISP=(OLD,DELETE)                                        00430004
//SYSOUT    DD SYSOUT=*                                                 00340004
//SYSLST    DD SYSOUT=*                                                 00350004
//SYSUDUMP  DD SYSOUT=*                                                 00360004
//SYSDBOUT  DD SYSOUT=*                                                 00370004
/*                                                                      00006300
//*====================================================                 00450004
//*SETVAR   EXEC PGM=ZEKESET,                                           00460004
//*             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                      00470004
//*SYSPRINT DD SYSOUT=*                                                 00480004
//*SYSIN    DD *                                                        00490004
//*  SET VAR $CLOSING EQ INACTIVE                                       00500004
//*====================================================                 00450004
/*                                                                      00510004
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
EERMINO@UCSD.EDU
NCOWELL@UCSD.EDU
DCHOCK@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
REMINDER:

THE GAAP900 RAN LAST NIGHT.

MAKE SURE THAT IT COMPLETED SUCCESSFULLY

AND THAT IT PURGED CORRECTLY.

/*
//* CONTROL REPORT FILENAME FOLLOWS:
//SYSTSIN   DD *
 %MAILRPT -
 'FISP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('GAAP900 SCHEDULED LAST NIGHT')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE..
//*---
//*============================================================         00520004
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00530004
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00540004
//*============================================================         00550004
//STOPZEKE EXEC STOPZEKE                                                00560004
//*                                                                     00570004
//*================ E N D  O F  J C L  GAA900   ========                00580004
