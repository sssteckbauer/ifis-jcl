//FISPLDB3 JOB (SYS000),                                                00000100
//        'S.CARTER-ACCTG',                                             00000200
//        CLASS=K,                                                      00000300
//        MSGCLASS=P,                                                   00000400
//        MSGLEVEL=(1,1),                                               00000500
//        NOTIFY=APCDBM,                                                00000600
//        REGION=20M                                                    00000700
//*JOBPARM COPIES=3                                                     00000800
//*+++++++++++++++++++++++++                                            00001100
//*    ZEKE EVENT # 2517 ...AVS.3        (INACTIVE AS OF 11/5/01        00001200
//*                                       WEB IFIS REPLACED THIS)
//*+++++++++++++++++++++++++                                            00001300
// JCLLIB ORDER=APCP.PROCLIB
//*
//CNTL1    OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001500
//CNTL2    OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001600
//CNTL3    OUTPUT COPIES=1,GROUPID=MC0504,CLASS=P,FORMS=A011            00001700
//PRNT021  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001800
//PRNT022  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001900
//PRNT023  OUTPUT COPIES=1,GROUPID=MC0504,CLASS=P,FORMS=A011            00002000
//PRNT031  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00002100
//PRNT032  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00002200
//PRNT033  OUTPUT COPIES=1,GROUPID=MC0504,CLASS=P,FORMS=A011            00002300
//PRNT041  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00002400
//PRNT042  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00002500
//PRNT043  OUTPUT COPIES=1,GROUPID=MC0504,CLASS=P,FORMS=A011            00002600
//OUT3 OUTPUT CLASS=F,FORMS=A011,GROUPID=MC0504,JESDS=ALL,DEFAULT=YES   00002700
//*                                                                     00002800
//*====================================================                 00002300
//*                     FISPLD                                          00002400
//*====================================================                 00002836
//GAD001P  EXEC GAD001P,                                                00002925
//  CNTLOUT='(,),OUTPUT=(*.PRNT021,*.PRNT022,*.PRNT023)',  USER SYOUT   00003125
//  DCMNTOUT='(,),OUTPUT=(*.PRNT031,*.PRNT032,*.PRNT033)', USER SYOUT   00003125
//  TRANSOUT='(,),OUTPUT=(*.PRNT041,*.PRNT042,*.PRNT043)', USER SYOUT   00003125
//  OUTX='(,)',                                           SYSTEM SYSOUT 00003025
//  COPIES=1,                                             # OF COPIES   00003200
//  GROUPID='FISP',                                       TEST OR PROD  00003300
//  JOBID='FISPLDB3',                                     JOB NAME
//*  JVOUCHER='FISP.JVDATA.D$FISPLD.04.AVS',              INPUT DS
//*  JVOUCHER='FISP.JVDATA.D$FISPLD.05.AVS',              INPUT DS
//*  JVOUCHER='FISP.JVDATA.D$FISPLD.06.AVS',              INPUT DS
//*  JVOUCHER='FISP.JVDATA.D$FISPLD.07.AVS',              INPUT DS
//  JVOUCHER='FISP.JVDATA.D$FISPLD.03.AVS',               INPUT DS
//  DB2REGN='DB2F'                                        DB2 DATABASE
//*
//*================ E N D  O F  J C L  GAD001B3 ========                00250007
