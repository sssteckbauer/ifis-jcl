//FISPLD03 JOB (SYS000),                                                00000100
//        'S.CARTER-ACCTG',                                             00000200
//        CLASS=K,                                                      00000300
//*       RESTART=TEST1,
//        MSGCLASS=F,                                                   00000400
//        MSGLEVEL=(1,1),                                               00000500
//        NOTIFY=APCDBM,                                                00000600
//        REGION=20M                                                    00000700
//*++++++++++++++++++++++++++                                           00000800
//*    ZEKE EVENT # 2577     ...CAMPMAIL - FILE #3                      00000900
//*++++++++++++++++++++++++++                                           00001000
// JCLLIB ORDER=APCP.PROCLIB
//*
//CNTL1    OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001200
//CNTL2    OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001300
//CNTL3    OUTPUT COPIES=1,GROUPID=MC0047,CLASS=Z,FORMS=A011            00001400
//PRNT021  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001500
//PRNT022  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001600
//PRNT023  OUTPUT COPIES=1,GROUPID=MC0047,CLASS=Z,FORMS=A011            00001700
//PRNT031  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001800
//PRNT032  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001900
//PRNT033  OUTPUT COPIES=1,GROUPID=MC0047,CLASS=Z,FORMS=A011            00002000
//PRNT041  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00002100
//PRNT042  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00002200
//PRNT043  OUTPUT COPIES=1,GROUPID=MC0047,CLASS=Z,FORMS=A011            00002300
//OUT3 OUTPUT CLASS=F,FORMS=A011,GROUPID=MC0047,JESDS=ALL,DEFAULT=YES   00002400
//*                                                                     00002500
//*==========================================================
//*  TEST TO SEE IF FILE IS EMPTY - ADDED 10/17/2013 FOR AUTOMATION
//*  IF EMPTY RC=4 IS OK
//*==========================================================
//TEST1    EXEC PGM=FILEMGR,REGION=6M
//SYSPRINT DD  SYSOUT=*
//SYLIST   DD  SYSOUT=(,),
//             OUTPUT=(*.OUT3),
//             DCB=(LRECL=250,BLKSIZE=23250)
//DD01      DD DISP=SHR,
//             DSN=FISP.PARMLIB(NULLFILE)
//DD01C     DD DISP=SHR,
//             DSN=FISP.JVDATA.CAMPML3.D$BJDT
//DD01CP   DD  *
//SYSIN    DD  *
$$FILEM SET HEADERPG=NO,PAGESIZE=60,PRTTRANS=ON
$$FILEM DSM TYPE=RECORD,
$$FILEM PACK=UNPACK,
$$FILEM SYNCH=READAHEAD,
$$FILEM LIMIT=100,
$$FILEM LENGTH=1,
$$FILEM LIST=DELTA,
$$FILEM WIDE=YES,
$$FILEM HILIGHT=YES,
$$FILEM CHNGDFLD=YES,
$$FILEM SLCTDFLD=YES,
$$FILEM IGNLEN=YES,
$$FILEM NUMDIFF=ALL,
$$FILEM HEXLEN=YES,
$$FILEM DDOLD=DD01,
$$FILEM SKIPOLD=0,
$$FILEM CMPOLD=ALL,
$$FILEM SKIPNEW=0,
$$FILEM CMPNEW=ALL,
$$FILEM DDNEW=DD01C
/*
//*====================================================                 00002300
//*                     FISPLD                                          00002400
//*====================================================                 00002836
//GAD001P  EXEC GAD001P,COND=(4,EQ,TEST1),                              00002925
//  CNTLOUT='(,),OUTPUT=(*.PRNT021,*.PRNT022,*.PRNT023)',  USER SYOUT   00003125
//  DCMNTOUT='(,),OUTPUT=(*.PRNT031,*.PRNT032,*.PRNT033)', USER SYOUT   00003125
//  TRANSOUT='(,),OUTPUT=(*.PRNT041,*.PRNT042,*.PRNT043)', USER SYOUT   00003125
//  OUTX='(,)',                                           SYSTEM SYSOUT 00003025
//  COPIES=1,                                             # OF COPIES   00003200
//  GROUPID='FISP',                                       TEST OR PROD  00003300
//  JOBID='FISPLD3',                                      JOB NAME
//  JVOUCHER='FISP.JVDATA.CAMPML3.D$BJDT',                INPUT DS
//  DB2REGN='DB2F'                                        DB2 DATABASE
//*
//*=====================================================
//*************************************************************
//*  RENAME 'FISP.JVDATA.CAMPML3.D$BJDT' TO DONE
//*************************************************************
//RENAMER  EXEC PGM=IDCAMS,COND=(4,EQ,TEST1)
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   ALTER      FISP.JVDATA.CAMPML3.D$BJDT -
              NEWNAME(FISP.JVDATA.DONE.CAMPML3.D$BJDT)
/*
//**************************************************************
//*   THIS WILL PRODUCE THE PDFMAIL
//**************************************************************
//BOOGIE      EXEC PGM=IKJEFT1B,COND=(4,EQ,TEST1),
//            REGION=0M,
//            DYNAMNBR=50
//STEPLIB     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.LOAD
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.LOAD
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//TXT1        DD   DISP=SHR,DSN=FISP.FISPLD3.UGAX010.CONTROLR.RPT
//TXT2        DD   DISP=SHR,DSN=FISP.FISPLD3.UGAUA00.PRNTR02.RPT
//TXT3        DD   DISP=SHR,DSN=FISP.FISPLD3.UGAUA00.PRNTR03.RPT
//TXT4        DD   DISP=SHR,DSN=FISP.FISPLD3.UGAUA00.PRNTR04.RPT
//PDF1        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//PDF2        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//PDF3        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//PDF4        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//SYSPRINT    DD   SYSOUT=X
//SYSTSPRT    DD   SYSOUT=X
//SYSTSIN     DD   *
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT1                                                      +
     OUT DD:PDF1                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT2                                                      +
     OUT DD:PDF2                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT3                                                      +
     OUT DD:PDF3                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT4                                                      +
     OUT DD:PDF4                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%XMITIP * +
  SUBJECT 'REPORT# - FISPLD3-FISP.JVDATA.CAMPMAIL  '                  +
  FROM ACT-PRODCONTROL@UCSD.EDU                                       -
  ADDRESSFILEDD ADDRLIST                                              -
  FILEDD (  PDF1      PDF2      PDF3      PDF4         )              -
  FILENAME (FILE1.PDF FILE2.PDF FILE3.PDF FILE4.PDF)                  +
  MSGT 'REPORT RUN ON: &DAY &DATE &TIME, BY JOB: &JOB                 +
       \\\                                                            +
        \THIS REPORT IS VIEWABLE WITH ACROBAT READER.                 +
        \THE LATEST VERSION CAN BE DOWNLOADED FROM:                   +
        HTTP://WWW.ADOBE.COM/PRODINDEX/ACROBAT/READSTEP.HTML#READER + +
        \\'
/*
//ADDRLIST    DD   *
TO  MAILSERVICES@UCSD.EDU
CC  DMESERVE@UCSD.EDU
CC  IFISHELPDESK@AD.UCSD.EDU
CC  DMESERVE@UCSD.EDU
CC  VHER@UCSD.EDU
CC  DCHOCK@UCSD.EDU
CC  EERMINO@UCSD.EDU
CC  NCOWELL@UCSD.EDU
CC  JSC023@UCSD.EDU
//*
//*===============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*===============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  GAD01D03  ========               00250007
