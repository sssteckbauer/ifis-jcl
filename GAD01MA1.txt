//FISPLMA1 JOB (SYS000),                                                00000100
//             'ACCTG',                                                 00000200
//             CLASS=K,                                                 00000300
//             MSGCLASS=F,                                              00000400
//             MSGLEVEL=(1,1),                                          00000500
//             NOTIFY=APCDBM,                                           00000600
//             REGION=20M                                               00000700
/*JOBPARM COPIES=1                                                      00000800
//*++++++++++++++++++++++++++                                           00000900
//*    ZEKE EVENT # 3796  ..MONDAY HOSPINV2 INTERFACE FILES             00001000
//*++++++++++++++++++++++++++++++++++++++++                             00001100
// JCLLIB ORDER=APCP.PROCLIB
//*
//CNTL1    OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001300
//CNTL2    OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001400
//*CNTL3   OUTPUT COPIES=1,GROUPID=MC8914B,CLASS=Z,FORMS=A011           00001500
//PRNT021  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001600
//PRNT022  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001700
//PRNT023  OUTPUT COPIES=1,GROUPID=MC8914B,CLASS=Z,FORMS=A011           00001800
//PRNT031  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00001900
//PRNT032  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00002000
//PRNT033  OUTPUT COPIES=1,GROUPID=MC8914B,CLASS=Z,FORMS=A011           00002100
//PRNT041  OUTPUT COPIES=1,GROUPID=BACKUP,CLASS=B,FORMS=A011            00002200
//PRNT042  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00002300
//PRNT043  OUTPUT COPIES=1,GROUPID=MC8914B,CLASS=Z,FORMS=A011           00002400
//OUT3 OUTPUT CLASS=F,FORMS=A011,GROUPID=MC8914B,JESDS=ALL,DEFAULT=YES  00002500
//*                                                                     00002600
//*====================================================                 00002300
//*                     FISPLD                                          00002400
//*====================================================                 00002836
//GAD001P  EXEC GAD001P,                                                00002925
//  CNTLOUT='(,),OUTPUT=(*.PRNT021,*.PRNT022,*.PRNT023)',  USER SYOUT   00003125
//  DCMNTOUT='(,),OUTPUT=(*.PRNT031,*.PRNT032,*.PRNT033)', USER SYOUT   00003125
//  TRANSOUT='(,),OUTPUT=(*.PRNT041,*.PRNT042,*.PRNT043)', USER SYOUT   00003125
//  OUTX='(,)',                                           SYSTEM SYSOUT 00003025
//  COPIES=1,                                             # OF COPIES   00003200
//  GROUPID='FISP',                                       TEST OR PROD  00003300
//  JOBID='FISPLMA1',                                     JOB NAME
//  JVOUCHER='FISP.JVDATA.HOSPINV2.D$HOSPJVDT',          INPUT DS
//  DB2REGN='DB2F'                                        DB2 DATABASE
//*
//**********************************************************************
//RENAMER  EXEC PGM=IDCAMS,COND=(0,NE)
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   ALTER      FISP.JVDATA.HOSPINV2.D$HOSPJVDT -
              NEWNAME(FISP.JVDATA.DONE.HOSPINV2.D$HOSPJVDT)
/*
//**********************************************************************
//RENAMER  EXEC PGM=IDCAMS,COND=(0,NE)
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   ALTER      FISP.JVDATA.HOSPINV2.D$HOSPJVDT..MSG -
              NEWNAME(FISP.JVDATA.DONE.HOSPINV2.D$HOSPJVDT..MSG)
/*
//**********************************************************************
//* EXECUTE REXX UTILITY FISP.PARMLIB(MAILER) TO PRODUCE THE PDFMAIL,
//* BUT BYPASS ATTACHING THE REPORT(S) THAT ARE CONSIDERED "EMPTY".
//* -----------------------------------------------------------------
//* SYSTSIN parmcard format (beginning in column 1):
//* %MAILER d, s n1 n2 n3 n4 n5 n6 n7 n8 n9
//* Where d = Job/report description (for email subject line)
//*       , = Description delimiter  (must be a comma)
//*       s = "Send email even if no attachments" indicator (Y or N)
//*       n1 n2 = Minimum number of report lines (considered "empty"),
//*               sequentially listed for each TXTxxxxx report file
//*               (max = 10) and separated by a space.
//*             Report is attached if actual line count is > this value.
//*             For value 0, report is attached unless completely empty.
//*
//* Example: %MAILER REPORT# FISPMA - FISP.JVDATA.HOSPINV, Y 8 5
//**********************************************************************
//BOOGIE      EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//STEPLIB     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.LOAD
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.LOAD
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//            DD   DISP=SHR,DSN=FISP.PARMLIB
//TXT3        DD   DISP=SHR,DSN=FISP.FISPLMA1.UGAUA00.PRNTR03.RPT
//TXT4        DD   DISP=SHR,DSN=FISP.FISPLMA1.UGAUA00.PRNTR04.RPT
//SYSPRINT    DD   SYSOUT=X
//SYSTSPRT    DD   SYSOUT=X
//SYSTSIN     DD   *
%MAILER REPORT# - FISPLMA1-FISP.JVDATA.HOSPINV2, Y 8 5
/*
//ADDRLIST    DD   *
TO  MYORK@HEALTH.UCSD.EDU
CC  CWAMSLEY@HEALTH.UCSD.EDU
CC  JCTHOMAS@HEALTH.UCSD.EDU
CC  IFISHELPDESK@AD.UCSD.EDU
CC  ACT-PRODCONTROL@UCSD.EDU
/*
//*
//*===============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*===============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*========================== END OF JCL GAD01MA1 ===============
