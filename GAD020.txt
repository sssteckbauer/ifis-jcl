//POSTJALL JOB (SYS000),'DISBURSEMENTS',MSGCLASS=F,MSGLEVEL=(1,1),      00000100
//        REGION=20M,                                                   00000200
//        NOTIFY=APCDBM,CLASS=K                                         00000300
//*++++++++++++++++++++++++++                                           00000400
//*    ZEKE EVENT # 830                                                 00000500
//*++++++++++++++++++++++++++                                           00000600
//*POST1   OUTPUT CLASS=P,DEST=LOCAL,GROUPID=ACCTG2,FORMS=A001          00000700
//*POST2   OUTPUT CLASS=P,DEST=LOCAL,GROUPID=PURCHSNG,FORMS=A001        00000800
//POST3   OUTPUT CLASS=G,DEST=LOCAL,GROUPID=BACKUP,FORMS=A001           00000900
//POST4   OUTPUT CLASS=G,DEST=LOCAL,GROUPID=DISB,FORMS=A001             00000800
//POST5   OUTPUT CLASS=X,DEST=LOCAL,GROUPID=PURCHSNG,FORMS=A001         00000800
//OUT1   OUTPUT CLASS=F,GROUPID=ACCTG1,FORMS=A011,JESDS=ALL,DEFAULT=YES 00001000
//* (POST3 CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION PROJECT ONLY)
//*====================================================                 00001100
//*   OVERNIGHT PROCESSING **  ONLY  ** !                               00001200
//*====================================================                 00001100
//SETTIME  EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
  SET VAR $POSTTIME EQ $FTIME
/*
//*====================================================                 00001300
//*              REMOVE POTENTIAL OUTPUT FILES                          00001400
//*====================================================                 00001500
//CLEAN01  EXEC PGM=IEFBR14                                             00001600
//SYSPRINT  DD SYSOUT=F                                                 00001700
//DD1       DD DSN=FISP.GAD020.FGAUB00.PRNTR02,                         00002300
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002700
//DD2       DD DSN=FISP.GAD020.FGAUB00.JVS.SORTED,                      00001600
//             DISP=(MOD,DELETE,DELETE),                                00001700
//             UNIT=SYSDA                                               00250007
//DD3       DD DSN=FISP.GAD020.FGAUB00.REPORT1,                         00005200
//             DISP=(MOD,DELETE,DELETE),                                00001700
//             UNIT=SYSDA                                               00250007
/*                                                                      00005800
//*======================================================               00110000
//*   PUT PARM OUT TO FILE FOR DYNAMIC RESTARTS                         00001000
//*    (ORIGINAL)*DOCUMENT SEQUENCE 0004
//*    (GAD010 JOURNAL ONLY)*DOCUMENT SEQUENCE 0012 ONLY
//*======================================================               00110000
/*                                                                      00005800
//SORT     EXEC  PGM=SYNCSORT                                           00050000
//SYSOUT   DD  SYSOUT=*                                                 00060000
//SYSPRINT DD  SYSOUT=*                                                 00070000
//SORTIN   DD  *                                                        00081002
*DOCUMENT SEQUENCE 0004
*EXCLUDE DOC TYPE 0008
/*
//SORTOUT  DD  DSN=&&PARMIN,                                            00090002
//             DISP=(NEW,PASS),
//             DCB=(LRECL=80,BLKSIZE=80),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA
//SYSIN    DD  *                                                        00100000
           SORT FIELDS=COPY                                             00110000
/*                                                                      00120000
//*                                                                     00001700
//*====================================================                 00110000
//*       POSTING DRIVER                                                00001900
//*====================================================                 00110000
//*                                                                     00001700
//STEP01   EXEC PGM=FGARD02,                                            00002100
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(FGARD02) PLAN(FGARD02)
/*
//SYSIN     DD *
 000001 ISISDICT
/*
//SYSOUT    DD SYSOUT=*                                                 00003100
//SYSLST    DD SYSOUT=*                                                 00003200
//SYSUDUMP  DD SYSOUT=D,                                                00003300
//             DEST=LOCAL                                               00003400
//SYSDBOUT  DD SYSOUT=*                                                 00003500
//SORTFILE  DD SPACE=(TRK,300),                                         00003600
//             UNIT=SYSDA                                               00003700
//PRNTR02   DD DSN=FISP.GAD020.FGAUB00.PRNTR02,                         00005200
//             DISP=(NEW,CATLG,CATLG),                                  00005300
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00005400
//             SPACE=(TRK,(150,45),RLSE),                               00005500
//             UNIT=SYSDA                                               00005600
//JVOUCHER  DD DSN=GUEST.GAD020.FGAUB00.JVS,
//             DISP=(MOD,KEEP,KEEP)
//PARMIN    DD DSN=FISP.PARMLIB(GAPOST),
//             DISP=SHR
//PARMFILE  DD DSN=&&PARMIN,DISP=(OLD,DELETE)                           00004200
//*                                                                     00004400
//*====================================================                 00001300
//*      SORT PRINT JOURNAL VOUCHER REPORT                              00001400
//*      NOTE:   SEQUENCE = DOCUMENT NUMBER                             00001400
//*====================================================                 00001500
/*                                                                      00005800
//SORT01   EXEC  PGM=SYNCSORT                                           00050000
//SYSOUT   DD  SYSOUT=F                                                 00060000
//SYSPRINT DD  SYSOUT=F                                                 00070000
//SORTIN   DD  DSN=GUEST.GAD020.FGAUB00.JVS,DISP=(OLD,DELETE,KEEP)      00081051
//SORTOUT  DD  DSN=FISP.GAD020.FGAUB00.JVS.SORTED,                      00090053
//             DCB=(RECFM=FB,LRECL=132,BLKSIZE=27984),
//             DISP=(NEW,CATLG,DELETE),                                 00240008
//             SPACE=(TRK,(150,45),RLSE),                               00240008
//             UNIT=SYSDA                                               00094035
//SYSIN    DD  *                                                        00100000
           SORT FIELDS=(6,9,CH,A)                                       00129153
/*                                                                      00129353
//*******************************************************************
//*      CREATE JOURNAL VOUCHER REPORT FILE TO BE MERGED WITH           00001400
//*      OTHER REPORT                                                   00001400
//*******************************************************************
//FGARD10  EXEC PGM=FGARD10
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT   DD SYSOUT=*
//SYSLST   DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,                                                00004200
//             DEST=LOCAL                                               00004300
//REPORT1   DD DSN=FISP.GAD020.FGAUB00.REPORT1,                         00005200
//             DISP=(NEW,CATLG,DELETE),                                 00005300
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00005400
//             SPACE=(TRK,(150,45),RLSE),                               00005500
//             UNIT=SYSDA                                               00005600
//JVOUCHER DD DSN=FISP.GAD020.FGAUB00.JVS.SORTED,DISP=SHR               00003007
//*====================================================                 00005900
//*      MERGE PRNTR02 AND JVS INTO A SINGLE FILE.                      00006000
//*====================================================                 00006100
//MERGE01  EXEC PGM=SORT                                                00006200
//SYSLST    DD SYSOUT=F                                                 00006400
//SYSOUT    DD SYSOUT=F                                                 00006500
//SYSUDUMP  DD SYSOUT=D                                                 00006600
//SORTIN    DD DSN=FISP.GAD020.FGAUB00.PRNTR02,                         00006700
//             DISP=SHR                                                 00006800
//          DD DSN=FISP.GAD020.FGAUB00.REPORT1,                         00005200
//             DISP=SHR                                                 00006800
//SORTOUT   DD DSN=FISP.GAD020.FGAUB00.CONTROLR.D$DSDATE..T$FTIME,      00007100
//             DISP=(NEW,CATLG,DELETE),                                 00007200
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00007300
//             SPACE=(TRK,(150,45),RLSE),                               00007400
//             UNIT=SYSDA                                               00007500
//SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),                              00007600
//             DISP=SHR                                                 00007700
//*====================================================                 00007800
//*      PRINT MERGED REPORTS                                           00007900
//*====================================================                 00008000
//PRINT03  EXEC PGM=SORT                                                00008100
//SYSLST    DD SYSOUT=F                                                 00008300
//SYSUDUMP  DD SYSOUT=D                                                 00008400
//SORTIN    DD DSN=FISP.GAD020.FGAUB00.CONTROLR.D$DSDATE..T$FTIME,      00008500
//             DISP=OLD                                                 00008600
//SORTOUT   DD SYSOUT=(,),                                              00008700
//             OUTPUT=(*.POST3,*.POST4)                                 00008800
//*            OUTPUT=(*.POST1,*.POST3,*.POST4)                         00008800
//SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),                              00008900
//             DISP=SHR                                                 00009000
//SYSOUT    DD DUMMY
//*
//*************************************************************
//*  CREATE PDF CONTROL FILE2 & EMAIL TO CLIENT
//*************************************************************
//PDFMAIL1    EXEC PDFMAIL,
//            JOB=GAD020,
//            IN=FISP.GAD020.FGAUB00.CONTROLR.D$DSDATE..T$FTIME
//*
//*====================================================                 00009100
//CLEAN02  EXEC PGM=IEFBR14                                             00001400
//SYSPRINT  DD SYSOUT=F                                                 00001500
//DD1       DD DSN=GUEST.GAD020.FGAUB00.JVS,                            00001600
//             DISP=(NEW,CATLG,CATLG),                                  00001700
//             DCB=(RECFM=FB,LRECL=132,BLKSIZE=27984),                  00230009
//             SPACE=(TRK,(150,45),RLSE),                               00240008
//             UNIT=SYSDA                                               00250007
//*====================================================                 00009100
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00009200
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00009300
//*====================================================                 00009400
//STOPZEKE EXEC STOPZEKE                                                00009500
//*================ E N D  O F  J C L  GAD020   ========                00009600
