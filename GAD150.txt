//GADP150  JOB (SYS000),'PRDCTL',REGION=20M,
//            MSGLEVEL=(1,1),
//            MSGCLASS=F,
//            CLASS=K,
//            NOTIFY=APCDBM
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 3495
//*++++++++++++++++++++++++++
//*====================================================================
//*                           G A D 1 5 0
//* DAILY JOB USES DB2 QUERY SELJLH TO PRODUCE THE PDF-MAILED EXCEPTION
//* REPORT "JOURNALS POSTED W/O FINAL APPROVAL".  IT REPORTS JOURNALS
//* THAT POSTED (JLH.PSTNG_IND = 'P') IN THE LAST 2 DAYS EVEN THOUGH
//* THEY DIDN'T RECEIVE FINAL APPROVAL (JLH.APRVL_IND = 'N').
//*
//*      NOTE: THE REPORT IS PDF-MAILED ONLY IF IT'S NOT EMPTY!
//*
//* IF ANY STEP ABENDS, PLEASE NOTIFY ACT-IFISSUPPORTTEAM@AD.UCSD.EDU,
//* AFTER WHICH YOU CAN RESTART THIS JOB FROM THE BEGINNING.
//*====================================================================
//*
//*********************************************************************
//* DELETE OUTPUT DATASET FROM PREVIOUS RUN
//*********************************************************************
//STEP01   EXEC PGM=IEFBR14
//SYSPRINT  DD SYSOUT=*
//DD1       DD DSN=FISP.GAD150.SELJLH.RESULTS.PDF,
//             UNIT=SYSDA,DISP=(MOD,DELETE,DELETE)
//*
//*********************************************************************
//* SUBMIT DB2 QUERY IN BATCH AND WRITE RESULTS TO A SEQUENTIAL FILE
//*********************************************************************
//STEP02   EXEC PGM=IKJEFT01,DYNAMNBR=20,REGION=1M
//STEPLIB  DD DSN=DB2F.DSNEXIT,DISP=SHR
//         DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSPRT DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSTSIN  DD *
 DSN SYSTEM (DB2F)
 RUN PROGRAM(DSNTIAUL) PLAN(DSNTIAUL) LIB('DB2F.RUNLIB.LOAD') -
  PARMS('SQL')
/*
//SYSIN    DD DSN=FISP.SQLLIB(SELJLH),DISP=SHR
//         DD DSN=FISP.SQLLIB(SEMICOLN),DISP=SHR
//*
//SYSREC00 DD DSN=FISP.GAD150.SELJLH.RESULTS,DISP=(NEW,CATLG,DELETE),
//            UNIT=3390,SPACE=(TRK,(1,1),RLSE)
//SYSPUNCH DD DUMMY
//*
//*********************************************************************
//* STRIP OFF DB2 RECLENGTH AND COPY QUERY RESULTS TO A PDF FILE
//*********************************************************************
//STEP03   EXEC PGM=SYNCSORT
//SYSOUT    DD SYSOUT=*
//SORTWK01  DD SPACE=(CYL,(10,10)),UNIT=SYSDA
//SORTWK02  DD SPACE=(CYL,(10,10)),UNIT=SYSDA
//SORTWK03  DD SPACE=(CYL,(10,10)),UNIT=SYSDA
//SORTIN    DD DSN=FISP.GAD150.SELJLH.RESULTS,DISP=(OLD,DELETE,DELETE)
//SORTOUT   DD DSN=FISP.GAD150.SELJLH.RESULTS.PDF,
//             UNIT=SYSDA,DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=080),
//             SPACE=(TRK,(5,10),RLSE)
//SYSIN     DD *
 SORT FIELDS=COPY
 OUTREC FIELDS=(1X,       1 BLANK (NEEDED FOR PDFMAIL CARRIAGE CONTROL)
                03,78,    SELJLH DATA RESULTS
                1X)       1 BLANK (TO COMPLETE LRECL=80)
/*
//*********************************************************************
//* EXECUTE REXX UTILITY FISP.PARMLIB(MAILER) TO PRODUCE THE PDFMAIL.
//* BYPASS SENDING THE REPORT IF "EMPTY" (THERE'S ONLY A HEADING LINE).
//* -------------------------------------------------------------------
//* SYSTSIN parmcard format (beginning in column 1):
//* %MAILER DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD, S N1 N2 N3 N4 N5 N6 N7 N8
//* Where d = Job/report description (for email subject line)
//*       , = Description delimiter  (must be a comma)
//*       s = "Send email even if no attachments" indicator (Y or N)
//*       n1 n2 = Minimum number of report lines (considered "empty"),
//*               sequentially listed for each TXTxxxxx report file
//*               (max = 10) and separated by a space.
//*             Report is attached if actual line count is > this value.
//*             For value 0, report is attached unless completely empty.
//*
//* Example: %MAILER REPORT# - FISPLDC-FISP.JVDATA.BKS, N 0 0 8 5
//*********************************************************************
//BOOGIE      EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//STEPLIB     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.LOAD
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.LOAD
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//            DD   DISP=SHR,DSN=FISP.PARMLIB
//TXT1        DD   DISP=SHR,DSN=FISP.GAD150.SELJLH.RESULTS.PDF
//SYSPRINT    DD   SYSOUT=X
//SYSTSPRT    DD   SYSOUT=X
//SYSTSIN     DD   *
%MAILER GAD150 - JOURNALS POSTED W/O FINAL APPROVAL, N 1
/*
//ADDRLIST    DD   *
TO  IFISHELPDESK@AD.UCSD.EDU
CC  ACT-IFISSUPPORTTEAM@AD.UCSD.EDU
/*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  GAD150  =========
