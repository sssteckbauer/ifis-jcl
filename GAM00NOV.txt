//SETLDGRV  JOB (SYS000),'DOUG - S003',MSGLEVEL=(1,1),                  00010039
//*    RESTART=STEP1,                                                   00011000
//     MSGCLASS=F,                                                      00020007
//     CLASS=K,                                                         00020107
//     NOTIFY=APCDBM                                                    00020207
//*******************************************************               00021207
//*             ** NOVEMBER'S LEDGER **                                 00021331
//*******************************************************               00021407
//*                                                                     00021507
//*++++++++++++++++++++++++                                             00021607
//*    ZEKE EVENT # 892    !!** MUST NEVER RUN SETLEDGR EXCEPT          00021731
//*++++++++++++++++++++++++     LEDGER FRIDAYS &                        00021800
//*                1) AFTER GENLIAB has loaded to IFIS (PPSPL320 -Z#23) 00021900
//*                   &                                                 00022000
//*                2) AFTER BUDGET/STAFFING DAILY LOAD                  00022100
//*                    ( WILL UPSET LOGIC OF BUDGET/STAFFING            00022200
//*                      DAILY LOADS, & GENLIAB creation process)       00022300
//*                    (Budget/Staffing now running Monday AM)          00022400
//*                                                                     00022500
//*++++++++++++++++++++++++++                                           00023400
//*  $DWFYN HAS VALUE OF "NEW FISCAL-YEAR-MONTH" 2006 FOR               00023843
//*  NOVEMBER LEDGER WITH DECEMBER "NEW" VALUE (YYMM)                   00023931
//*                                                                     00024000
//*  $DWYEAR HAS VALUE OF NEW FISCAL YEAR (MATCHES FIRST TWO            00024100
//*  NUMBERS IN $DWFYN'S VALUE). REMEMBER TO CHANGE TO "NEW             00024200
//*  FISCAL-YEAR" WHEN PROCESSING JUNE'S FINAL LEDGER (CHANGED          00024300
//*  ONCE A YEAR IN AUGUST).                                            00024429
//*                                                                     00024500
//*  VARIABLE ($DWFYN) IS BEING USED IN THIS JOB TO PICK UP             00024600
//*  THE ** NEXT ** FISCAL-YEAR-MONTH VALUE.                            00024700
//* EX:                                                                 00024800
//*  FOR MAY'S ledGER:                                                  00024943
//*  (EXAMPLE: 2012 = FISCAL YEAR OF 2020, FISCAL MONTH OF JUNE   (12)) 00025043
//*____________________________________________________________________ 00025100
//*  FOR JUNE'S PRELIMINARY LEDGER:                                     00025200
//*  (EXAMPLE: 2014 = FISCAL YEAR OF 2020, FISCAL MONTH OF JUNE   (14)) 00025343
//*____________________________________________________________________ 00025400
//*  FOR JUNE'S FINAL LEDGER:                                           00025500
//*  (EXAMPLE: 2101 = FISCAL YEAR OF 2021, FISCAL MONTH OF JULY   (01)) 00025643
//*____________________________________________________________________ 00025700
//*  FOR JULY'S LEDGER:                                                 00025800
//*  (EXAMPLE: 2102 = FISCAL YEAR OF 2021, FISCAL MONTH OF AUGUST (02)) 00025943
//*____________________________________________________________________ 00026000
//*____________________________________________________________________ 00026100
//*                                                                     00026200
//*  VARIABLE ($DWYEAR) IS BEING USED IN THIS JOB TO PICK UP            00026300
//*  THE FISCAL-YEAR THAT IS BEING CLOSED (FOR ALL CYCLES EXCEPT FINAL) 00026400
//*  (EXAMPLE:  2020 = FOR MAY'S         LEDGER CLOSING)                00026543
//*  (EXAMPLE:  2020 = FOR JUNE'S PRELIM LEDGER CLOSING)                00026643
//*  (EXAMPLE:  2021 = FOR JUNE'S FINAL  LEDGER CLOSING) NEW YEAR       00026743
//*  (EXAMPLE:  2021 = FOR JULY'S        LEDGER CLOSING)                00026843
//*================================================================     00026900
//*   $FISACTGPRDBSLN    -value of new fiscal month that will begin as  00027000
//*          (N=NEW)     BFS processing completes week after ledger     00027100
//*                                                                     00027200
//* $FISACTGEOMSL1       -value of previous fiscal monthend             00027300
//* $FISACTGEOMSL1N(NEW) -value of fiscal monthend that is being closed 00027400
//* $FISACTGEOMSL2       -value of EDB from UCOP (EOM value)previous    00027500
//* $FISACTGEOMSL2N(NEW) -value of EDB from UCOP (EOM value)being closed00027600
//* $FISACTGEOMSL3       -value of report date (EOM value) previous     00027700
//* $FISACTGEOMSL3N(NEW) -value of report date (EOM value) being closed 00027800
//*                                                                     00027900
//*                                                                     00028000
//*  (this value is used in Budget/Staffing daily/weekly                00028100
//*                    downloads to Darwin-GAWP125...ETC)               00028200
//*                                                                     00028700
//*  (SETBSL2 (#1806) is run in SLJOB3BO is run to pick up new          00028800
//*   EOM EDB extract file from UCOP. This will set the Staffing        00028900
//*   jobs (SL*) to run with with the most current EDB).                00029000
//* ** EXCEPT DURING FISCAL CLOSING-MUST CHANGE B/S VARIABLES BY HAND   00029100
//*================================================================     00029200
//* $FISGENLIAB      = YYMM   (NEW YEAR/MONTH DESIGNATOR)               00029300
//* $FISGENLIABEOM   = YYMMDD (LAST DAY OF NEW YEAR/MONTH)              00029400
//* $FISGENLIABFACTR = nnnn (value supplied by Robert(B&S) & Pearl-     00029500
//*                          June Monthend Payroll processing/          00029600
//*                          Leave Processing -  must complete before   00029700
//*                          make change to variable)                   00029800
//* $FISGENLIABFACTR=0081 (CURRENT VALUE AS OF JULY 2019) MUST BE       00029943
//*                        SET BY HAND UPON NOTICE FROM BUDGET/STAFF.   00030000
//*                                                                     00030100
//* (if running December ledger-then date must be end of January        00030200
//* so that when GENLIAB creation process is run it has correct date    00030300
//* for January's ledger)                                               00030400
//*                                                                     00030500
//* This value is used to set GENLIAB files with correct EOM date.      00030600
//* The GENLIAB creation process has the possibility of being           00030700
//* run the afternoon of Friday-Ledger (SO - SETLEDGER must not         00030800
//* run until after GENLIAB file has been released to load to IFIS)     00030900
//* (FISPLDL1 -> L3)                                                    00031000
//*================================================================     00031100
//* LEAVE PROCESSING USES THE VARIABLE:                                 00031200
//* $PPSLVPEOM (MMDDYY) (LAST DAY OF MONTH)                             00031300
//* THIS MUST BE SET WITH THE SAME LOGIC AS GENLIAB.                    00031400
//*================================================================     00031500
//* ARMP220M/ARMP225M PROCESSING USES THE VARIABLE:                     00031600
//* $ARM2DATE  (MMDDYY) (LAST DAY OF MONTH)                             00031700
//* $ARMJDATE  (YYDDD)  (LAST DAY OF MONTH-JULIAN FORMAT)               00031800
//* THIS MUST BE SET WITH THE LOGIC OF ISIS CLOSING MONTHEND SO         00031900
//* THAT THE PROCESS CAN RUN AFTER MIDNIGHT-BUT CAPTURE CLOSING         00032000
//* MONTH'S DATA AS LEDGER INPUT.                                       00032100
//*================================================================     00032600
//* PPSPM917 USES THE NGN ZEKE VARIABLE TO PURGE LAST MONTH'S           00032700
//* NGN FILES FROM DISK:                                                00032800
//* $FISYYMMNGN                                                         00032900
//*================================================================     00033000
//* $FISYR = YY EQUALS CALENDAR YEAR VALUE.NOT FISCAL YEAR VALUE.       00033100
//*================================================================     00033300
//* $FISYYMMPDF - IS YYMM WITH SUFFIX OF 'P'(PRELIM) OR 'F'(FINAL)      00033400
//*               (OR 'W'(WORKSHEET) OR 'PCL (POSTCLOSING))             00033500
//* 7/11/09 - change to use 'P' value per Bruce.                        00033600
//*================================================================     00033700
//* $GACAPMONTH = M MONTH VALUE ONLY-NO PRECEEDING '0' (JAN=1;JUNE=6)   00033800
//*================================================================     00033900
//Z1     EXEC PGM=ZEKESET,                                              00034000
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00034100
//SYSPRINT DD SYSOUT=*                                                  00034200
//SYSIN    DD *                                                         00034300
  SET VAR $FISUNVRSCODE    EQ   '01'                                    00034400
  SET VAR $FISCOACODE      EQ    A                                      00034500
  SET VAR $FISFSCLYR       EQ   '$NEXTFY'                               00034625
  SET VAR $FISYR           EQ   '$CYR'                                  00034700
  SET VAR $FISYYMM         EQ   '$CYR.11'                               00034831
  SET VAR $FISYYMMPDF      EQ   '$CYR.11'                               00034931
  SET VAR $GACAPMONTH      EQ    11                                     00035031
  SET VAR $FISACTGPRD      EQ   '05'                                    00035131
  SET VAR $FISDESC         EQ    NOVEMBER                               00035231
  SET VAR $LDGRJCL         EQ    FISP.GAM050.LDGRJCL.P$CYR.11           00035331
  SET VAR $FISMONYR        EQ    NOV$CYR                                00035431
  SET VAR $FISMONYRNXT     EQ    DEC$CYR                                00035531
  SET VAR $FISACTGEOMSL1N  EQ   '$DEC2.$CYR'                            00035631
  SET VAR $FISACTGEOMSL2N  EQ   '$M12BSE.-$CYR'                         00035731
  SET VAR $FISACTGEOMSL3N  EQ   '$M12BSE.-$CYR'                         00035831
  SET VAR $FISACTGPRDBSLN  EQ   '06'                                    00035931
  SET VAR $FISYYMMNGN      EQ   '$CYR.11'                               00036031
  SET VAR $FISGENLIAB      EQ   '20$CYR.12'                             00036131
  SET VAR $FISGENLIABEOM   EQ   '20$CYR.$DEC2'                          00036231
  SET VAR $PPSLVPEOM       EQ   '$DEC2.$CYR'                            00036331
  SET VAR $ARM2DATE        EQ   '$DEC2.$CYR'                            00036431
  SET VAR $ARMJDATE        EQ   '$M12BJDE'                              00036731
  SET VAR $DWFYN           EQ   '$NEXTFY.06'                            00036831
  SET VAR $DWYEAR          EQ   '$DSNEXTYEAR'                           00036926
  SET VAR $DSGA400         EQ    INCOMPLETE                             00037000
  SET VAR $GAM400          EQ    INCOMPLETE                             00037100
  SET VAR $FISMYYMM        EQ   'M$CYR.11'                              00037240
  SET VAR $FISPENDDATE     EQ   '$NOV2.$CYR'                            00037338
  SET VAR $FISCLYR         EQ   '$NEXTFY'                               00037441
  SET VAR $FISCURYYMM      EQ   '$CYR.11'                               00037542
/*                                                                      00037637
//* !!! SEE NOTE BELOW  !!!                                             00037737
//*=============================================================        00037837
//*    SET ZENA SYSTEM VARIABLES FOR FTP OF CFS/CAF FILES               00037937
//*    (PRE / FNL - ** NOT WSH ** -THIS IS IN GAM001W; NOT PCL=HAVEN'T  00038037
//*     FTP'D FNL FILES TO UCOP...SO, MUST CHANG PCL BY HAND WHEN TIME. 00038137
//*    VALID VALUES: MAY10 / PRE10 / JUL10 - year value is YY           00038237
//*    VALUES: PCL10 - year value is YY - MUST SET BY HAND AFTER        00038337
//*  FTP FNL FILES TO UCOP...THEN CHANGE VALUE TO PCLyy.                00038437
//*                                                                     00038537
//* ---------------------                                               00038637
//* THE 'ZADD' PART TWO OF ZENA VARIABLE SETTING IS IN                  00038737
//* JOBS: GAM100 / GAM100P / GAM100W / GAM100F                          00038837
//*=============================================================        00038937
//Z2     EXEC PGM=ZEKESET                                               00039037
//SYSPRINT DD SYSOUT=*                                                  00039137
//SYSIN    DD *                                                         00039237
      SET VAR $GETCFS EQ NOV$CYR                                        00039337
      SET VAR $PUTCFS EQ NOV$CYR                                        00039437
      SET VAR $GETCAF EQ NOV$CYR                                        00039537
      SET VAR $PUTCAF EQ NOV$CYR                                        00039637
/*                                                                      00039737
//*=============================================================        00039837
//Z3     EXEC PGM=ZEKESET                                               00039937
//SYSPRINT DD SYSOUT=*                                                  00040037
//SYSIN    DD *                                                         00040137
      SET VAR $COADIM EQ STOP                                           00040237
/*                                                                      00040337
//*=============================================================        00040437
//*  SINCE I NORMALLY RUN SETLEDGR TWICE, THIS VALUE                    00040537
//*  WILL BE SET IN ZEKE ON THE 1ST RUNNING OF THIS JOB,                00040637
//*  IT WILL BE SENT TO ZENA ON THE SECOND RUNNING OF THIS JOB.         00040737
//*=============================================================        00040837
//*  <FISCAL CLOSING VARIABLES FOLLOW:>                                 00042400
//*                                                                     00042500
//*  ** REMEMBER ** $CURRFY = CURRENT FISCAL YEAR THAT                  00042600
//*  IS BEING PROCESSED. WHEN FINAL HAS                                 00042700
//*  COMPLETED AND PCL IS READY TO PROCESS - MUST                       00042800
//*  CHANGE THE VALUE TO THE "NEW" FISCAL YEAR.                         00042900
//*                                                                     00043000
//*  AND/OR (DUE TO TIMING OF LOAD/POSTING OF REAPPROPRIATIONS)         00043100
//*                                                                     00043200
//*  THIS VALUE CONTROL'S THE *GAD7 SERIES. WHEN                        00043300
//*  REAPPROPRIATIONS HAS COMPLETELY LOADED AND POSTED                  00043400
//*  THEN MUST CHANGE THE VALUE TO "NEW" FISCAL YEAR SO                 00043500
//*  THAT IT WILL PICK UP ALL DATA FROM REAPPROPRIATIONS                00043600
//*  (BUDGET "ROLL" INTO NEW YEAR).                                     00043700
//*=====================================================                00044300
//Z5     EXEC PGM=ZEKESET,                                              00044400
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00044500
//SYSPRINT DD SYSOUT=*                                                  00044600
//SYSIN    DD *                                                         00044700
  SET VAR $CURRFY EQ $NEXTFY                                            00044933
  SET VAR $DLD    EQ $FISYYMM                                           00045000
  SET VAR $GAM555 EQ PENDING                                            00045200
/*                                                                      00045300
//*============================================================         00045400
//Z6     EXEC ZEKEUTLP,                                                 00045500
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00045600
//SYSIN    DD *                                                         00045700
   LIST VARIABLES                                                       00045800
/*                                                                      00045900
//*============================================================         00046000
//STEP1 EXEC PGM=IDCAMS                                                 00046100
//SYSPRINT DD SYSOUT=*                                                  00046200
//SYSIN  DD *                                                           00046300
  DELETE (DSET.VACLACS.P$DPD..TAB) PURGE                                00046400
  DELETE (DSET.EMPPED.M$DPD..TAB) PURGE                                 00046500
  DELETE (DSET.BWACUL.M$DPD..TAB) PURGE                                 00046600
  DELETE (DSET.PPSACT.ME$PMCYM..TAB) PURGE                              00046700
  DELETE (DSET.PPSAPP.ME$PMCYM..TAB) PURGE                              00046800
  DELETE (DSET.PPSAWR.ME$PMCYM..TAB) PURGE                              00046900
  DELETE (DSET.PPSBEL.ME$PMCYM..TAB) PURGE                              00047000
  DELETE (DSET.PPSBEN.ME$PMCYM..TAB) PURGE                              00047100
  DELETE (DSET.PPSBKC.ME$PMCYM..TAB) PURGE                              00047200
  DELETE (DSET.PPSBND.ME$PMCYM..TAB) PURGE                              00047300
  DELETE (DSET.PPSBRS.ME$PMCYM..TAB) PURGE                              00047400
  DELETE (DSET.PPSCMP.ME$PMCYM..TAB) PURGE                              00047500
  DELETE (DSET.PPSDBL.ME$PMCYM..TAB) PURGE                              00047600
  DELETE (DSET.PPSDDG.ME$PMCYM..TAB) PURGE                              00047700
  DELETE (DSET.PPSDEP.ME$PMCYM..TAB) PURGE                              00047800
  DELETE (DSET.PPSDIS.ME$PMCYM..TAB) PURGE                              00047900
  DELETE (DSET.PPSDUE.ME$PMCYM..TAB) PURGE                              00048000
  DELETE (DSET.PPSEAD.ME$PMCYM..TAB) PURGE                              00048100
  DELETE (DSET.PPSEAR.ME$PMCYM..TAB) PURGE                              00048200
  DELETE (DSET.PPSEBT.ME$PMCYM..TAB) PURGE                              00048300
  DELETE (DSET.PPSFAD.ME$PMCYM..TAB) PURGE                              00048400
  DELETE (DSET.PPSFBA.ME$PMCYM..TAB) PURGE                              00048500
  DELETE (DSET.PPSFCB.ME$PMCYM..TAB) PURGE                              00048600
  DELETE (DSET.PPSFNA.ME$PMCYM..TAB) PURGE                              00048700
  DELETE (DSET.PPSFNW.ME$PMCYM..TAB) PURGE                              00048800
  DELETE (DSET.PPSHON.ME$PMCYM..TAB) PURGE                              00048900
  DELETE (DSET.PPSLAH.ME$PMCYM..TAB) PURGE                              00049000
  DELETE (DSET.PPSLAP.ME$PMCYM..TAB) PURGE                              00049100
  DELETE (DSET.PPSLCN.ME$PMCYM..TAB) PURGE                              00049200
  DELETE (DSET.PPSLOF.ME$PMCYM..TAB) PURGE                              00049300
  DELETE (DSET.PPSLPH.ME$PMCYM..TAB) PURGE                              00049400
  DELETE (DSET.PPSLSC.ME$PMCYM..TAB) PURGE                              00049500
  DELETE (DSET.PPSNDC.ME$PMCYM..TAB) PURGE                              00049600
  DELETE (DSET.PPSNER.ME$PMCYM..TAB) PURGE                              00049700
  DELETE (DSET.PPSPAY.ME$PMCYM..TAB) PURGE                              00049800
  DELETE (DSET.PPSPCC.ME$PMCYM..TAB) PURGE                              00049900
  DELETE (DSET.PPSPCM.ME$PMCYM..TAB) PURGE                              00050000
  DELETE (DSET.PPSPER.ME$PMCYM..TAB) PURGE                              00050100
  DELETE (DSET.PPSPPA.ME$PMCYM..TAB) PURGE                              00050200
  DELETE (DSET.PPSSCR.ME$PMCYM..TAB) PURGE                              00050300
  DELETE (DSET.PPSSPP.ME$PMCYM..TAB) PURGE                              00050400
/*                                                                      00050500
//*                                                                     00050600
//*=============================================================        00050700
//Z10    EXEC PGM=ZEKESET                                               00050800
//SYSPRINT DD SYSOUT=*                                                  00050900
//SYSIN    DD *                                                         00051000
 SET SCOM 'ZHOLD EV (2127)'                                             00051100
/*                                                                      00051200
//*=============================================================        00055812
//Z11    EXEC PGM=ZEKESET                                               00055915
//SYSPRINT DD SYSOUT=*                                                  00056012
//SYSIN    DD *                                                         00056112
 SET SCOM 'ZADD EV (403) REBUILD'                                       00056212
/*                                                                      00056312
//*=============================================================        00056412
//Z12    EXEC PGM=ZEKESET                                               00056515
//SYSPRINT DD SYSOUT=*                                                  00056612
//SYSIN    DD *                                                         00056712
 SET SCOM 'ZALTER EV (403) RUN'                                         00056812
/*                                                                      00056912
//*============================================================         00057012
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00057100
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00057200
//*============================================================         00057300
//STOPZEKE EXEC STOPZEKE                                                00057400
//*                                                                     00058000
//*================ E N D  O F  J C L  GAM001   ========                00060000
