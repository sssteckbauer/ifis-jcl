//GAMP022 JOB (SYS000),'ACCTG-LEDGER',MSGCLASS=F,MSGLEVEL=(1,1),        00000100
//        REGION=20M,                                                   00000200
//        NOTIFY=APCDBM,CLASS=K                                         00000300
//*++++++++++++++++++++++++++                                           00000400
//*    ZEKE EVENT # 108                                                 00000500
//*++++++++++++++++++++++++++                                           00000600
//* //POST1   OUTPUT CLASS=P,DEST=LOCAL,GROUPID=ACCTG2,FORMS=A001       00000700
//* //POST2   OUTPUT CLASS=X,DEST=LOCAL,GROUPID=PURCH,FORMS=A001        00000800
//* //POST3   OUTPUT CLASS=G,DEST=LOCAL,GROUPID=BACKUP,FORMS=A001       00000900
//OUT1   OUTPUT CLASS=F,GROUPID=ACCTG1,FORMS=A011,JESDS=ALL,DEFAULT=YES 00001000
//*====================================================                 00001100
//*   OVERNIGHT PROCESSING **  ONLY  ** !                               00001200
//*====================================================                 00001300
//SETTIME  EXEC PGM=ZEKESET,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00001400
//SYSPRINT  DD SYSOUT=*                                                 00001500
//SYSIN     DD *                                                        00001600
  SET VAR $POSTTIME EQ $FTIME
/*                                                                      00001700
//*====================================================                 00001800
//*              REMOVE POTENTIAL OUTPUT FILES                          00001900
//*====================================================                 00002000
//CLEAN01  EXEC PGM=IEFBR14                                             00002100
//SYSPRINT  DD SYSOUT=F                                                 00002200
//DD1       DD DSN=FISP.GAD020.FGAUB00.PRNTR02,                         00002300
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//DD2       DD DSN=FISP.GAD020.FGAUB00.JVS.SORTED,                      00002600
//             DISP=(MOD,DELETE,DELETE),                                00002700
//             UNIT=SYSDA                                               00002800
//DD3       DD DSN=FISP.GAD020.FGAUB00.REPORT1,                         00002900
//             DISP=(MOD,DELETE,DELETE),                                00003000
//             UNIT=SYSDA                                               00003100
//*====================================================                 00003200
//*       POSTING DRIVER                                                00003300
//*====================================================                 00003400
//POST001  EXEC PGM=FGARD02,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00003500
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(FGARD02) PLAN(FGARD02)
/*
//SYSLST    DD SYSOUT=*                                                 00004400
//SYSUDUMP  DD SYSOUT=D,                                                00004500
//             DEST=LOCAL                                               00004600
//SYSDBOUT  DD SYSOUT=*                                                 00004700
//SORTFILE  DD SPACE=(TRK,900),                                         00004800
//             UNIT=SYSDA                                               00004900
//SYSOUT    DD SYSOUT=*                                                 00005000
//PRNTR02   DD DSN=FISP.GAD020.FGAUB00.PRNTR02,                         00005100
//             DISP=(NEW,CATLG,DELETE),                                 00005200
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00005300
//             SPACE=(TRK,(1,1),RLSE),                                  00005400
//             UNIT=SYSDA                                               00005500
//PARMIN    DD DSN=FISP.PARMLIB(GAPOST),                                00005600
//             DISP=SHR                                                 00005700
//JVOUCHER  DD DSN=GUEST.GAD020.FGAUB00.JVS,                            00005800
//             DISP=(MOD,KEEP,KEEP)                                     00005900
//PARMFILE  DD *                                                        00006000
*DOCUMENT SEQUENCE 0007
/*                                                                      00006100
//SORT01   EXEC PGM=SYNCSORT                                            00006200
//SYSOUT    DD SYSOUT=F                                                 00006300
//SYSPRINT  DD SYSOUT=F                                                 00006400
//SORTIN    DD DSN=GUEST.GAD020.FGAUB00.JVS,                            00006500
//             DISP=(OLD,DELETE,KEEP)                                   00006600
//SORTOUT   DD DSN=FISP.GAD020.FGAUB00.JVS.SORTED,                      00006700
//             DISP=(NEW,CATLG,DELETE),                                 00006800
//             DCB=(RECFM=FB,LRECL=132,BLKSIZE=27984),                  00006900
//             SPACE=(TRK,(150,10),RLSE),                               00007000
//             UNIT=SYSDA                                               00007100
//SYSIN     DD *                                                        00007200
           SORT FIELDS=(6,9,CH,A)                                       00129153
/*                                                                      00007300
//*******************************************************************   00007400
//FGARD10  EXEC PGM=FGARD10                                             00007500
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 00008000
//SYSLST    DD SYSOUT=*                                                 00008100
//SYSDBOUT  DD SYSOUT=*                                                 00008200
//SYSUDUMP  DD SYSOUT=D,                                                00008300
//             DEST=LOCAL                                               00008400
//REPORT1   DD DSN=FISP.GAD020.FGAUB00.REPORT1,                         00008500
//             DISP=(NEW,CATLG,DELETE),                                 00008600
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00008700
//             SPACE=(TRK,(15,1),RLSE),                                 00008800
//             UNIT=SYSDA                                               00008900
//JVOUCHER  DD DSN=FISP.GAD020.FGAUB00.JVS.SORTED,                      00009000
//             DISP=SHR                                                 00009100
//*====================================================                 00009200
//*      MERGE PRNTR02 AND JVS INTO A SINGLE FILE.                      00009300
//*====================================================                 00009400
//MERGE01  EXEC PGM=SORT                                                00009500
//SYSLST    DD SYSOUT=F                                                 00009600
//SYSOUT    DD SYSOUT=F                                                 00009700
//SYSUDUMP  DD SYSOUT=D                                                 00009800
//SORTIN    DD DSN=FISP.GAD020.FGAUB00.PRNTR02,                         00009900
//             DISP=SHR                                                 00010000
//          DD DSN=FISP.GAD020.FGAUB00.REPORT1,                         00010100
//             DISP=SHR                                                 00010200
//SORTOUT   DD DSN=FISP.GAD020.FGAUB00.CONTROLR.D$DSDATE..T$FTIME,      00010300
//             DISP=(NEW,CATLG,DELETE),                                 00010400
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00010500
//             SPACE=(TRK,(1,1),RLSE),                                  00010600
//             UNIT=SYSDA                                               00010700
//SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),                              00010800
//             DISP=SHR                                                 00010900
//*====================================================                 00011000
//*      PRINT MERGED REPORTS                                           00011100
//*====================================================                 00011200
//PRINT03  EXEC PGM=SORT                                                00011300
//SYSLST    DD SYSOUT=F                                                 00011400
//SYSUDUMP  DD SYSOUT=D                                                 00011500
//SORTIN    DD DSN=FISP.GAD020.FGAUB00.CONTROLR.D$DSDATE..T$FTIME,      00011600
//             DISP=OLD                                                 00011700
//SORTOUT   DD SYSOUT=*                                                 00011800
//* //SORTOUT   DD SYSOUT=(,),                                          00011800
//* //             OUTPUT=(*.POST1,*.POST2,*.POST3)                     00011900
//SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),                              00012000
//             DISP=SHR                                                 00012100
//SYSOUT    DD DUMMY                                                    00012200
//*====================================================                 00012300
//CLEAN02  EXEC PGM=IEFBR14                                             00012400
//SYSPRINT  DD SYSOUT=F                                                 00012500
//DD1       DD DSN=GUEST.GAD020.FGAUB00.JVS,                            00012600
//             DISP=(NEW,CATLG,CATLG),                                  00012700
//             DCB=(RECFM=FB,LRECL=132,BLKSIZE=27984),                  00012800
//             SPACE=(TRK,(45,15),RLSE),                                00012900
//             UNIT=SYSDA                                               00013000
//*================================================================
//*  SUBMIT DATA WAREHOUSE INCREMENTAL EXTRACT JOBS/MONTHLY PROCESS
//*               (FOR CORRECT SEQUENCING)
//*================================================================
//SUBGAD01 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (3109 3032 3041 3046) REBUILD'
/*
//*================================================================
//*  SUBMIT DATA WAREHOUSE SUPPLEMENTAL TABLE JOBS/MONTHLY PROCESS
//*       AND DWZ4F (FOR CORRECT SEQUENCING)
//*================================================================
//SUBGAD02 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2453 2456 2029) REBUILD'
/*
//*================================================================
//*  ALTER DATA WAREHOUSE INCREMENTAL EXTRACT JOBS/MONTHLY PROCESS
//*    (SO THAT THE TIME WILL BE SATISFIED AND RUN DURING DAY)
//*================================================================
//SUBGAD04 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZALTER EV (3109 3032 3041 3046) TIMEOK'
/*
//*================================================================
//*  ALTER DATA WAREHOUSE SUPPLEMENTAL TABLE JOBS/MONTHLY PROCESS
//*    (SO THAT THE TIME WILL BE SATISFIED AND RUN DURING DAY)
//*================================================================
//SUBGAD05 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZALTER EV (2453 2456 2029) TIMEOK'
/*
//*============================================================
//*  SUBMIT DSQDED01 - PAYROLL EXTRACT FOR MONTHLY PROCESSING
//*  (NOW ZADDED IN JOB GAMP100*)
//*============================================================
//*//SUBPSSQ1 EXEC PGM=ZEKESET,
//*//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*//SYSPRINT  DD SYSOUT=*
//*//SYSIN     DD *
//* SET SCOM 'ZADD EV (1466) REBUILD'
//*
//*============================================================
//*  ZALETER DSQDED01 - PAYROLL EXTRACT FOR MONTHLY PROCESSING
//*============================================================
//*//SUBPSSQ2 EXEC PGM=ZEKESET,
//*//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*//SYSPRINT  DD SYSOUT=*
//*//SYSIN     DD *
//* SET SCOM 'ZALTER EV (1466) TIMEOK'
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  GAM022   ========
