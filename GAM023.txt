//GAMP023   JOB (SYS000),'STE.MARIE-ACCTG',CLASS=K,MSGCLASS=F,          00000100
//*           RESTART=*                                                 00000400
//            MSGLEVEL=(1,1),REGION=20M,NOTIFY=APCDBM                   00000200
//*++++++++++++++++++++++++++                                           00000500
//*    ZEKE EVENT # 834                                                 00000600
//*++++++++++++++++++++++++++                                           00000700
//*====================================================                 00000800
//*     G A M 0 2 3                                                     00000900
//*                                                                     00001000
//*     THIS JCL WILL UPDATE THE GRANT ACTIVITY AMOUNT FIELD FOR        00001100
//*     INDIRECT COST PROCESSING.  THE PROGRAM DOING THE UPDATING       00001200
//*     IS UCHU010. THIS JCL WILL BE STRUCTURED TO REPEAT UCHU010       00001300
//*     TEN TIMES IN CASE OF ABNORNAL PROGRAM TERMINATION.              00001400
//*                                                                     00001500
//*     SYMBOLIC PARAMETERS:                                            00001600
//*                                                                     00001700
//*         *UNIVERSITY CODE $FISUNVRSCODE                              00001800
//*         *COA CODE $FISCOACODE                                       00001900
//*         *FISCAL YEAR $FISFSCLYR                                     00002000
//*         *COMMIT COUNTER NNNN                                        00002100
//*                                                                     00002200
//*     DATA SETS CATALOGED:                                            00002300
//*                                                                     00002400
//*         FISP.GAM020.UCHUB00.GRNTFILE.PYYMM                          00002500
//*             WHERE YYMM ARE THE YEAR AND MONTH THAT ARE BEING        00002600
//*             CLOSED. THIS VALUE IS SUPPLIED VIA A ZEKE VARIABLE      00002700
//*             NAMED $FISYYMM.                                         00002800
//*====================================================                 00002900
//CLEAN010 EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00003000
//SYSPRINT  DD SYSOUT=Z                                                 00003100
//DD1       DD DSN=FISP.GAM023.GRNTFILE.SORTED.P$FISYYMM,               00003200
//             DISP=(MOD,DELETE,DELETE),                                00003300
//             DCB=(RECFM=FB,LRECL=80,BLKSIZE=23440),                   00003400
//             SPACE=(TRK,(1,1),RLSE),                                  00003500
//             UNIT=SYSDA                                               00003600
//*====================================================                 00003700
//*       SORT THE GRANT FILE                                           00003800
//*====================================================                 00003900
//SORT020  EXEC PGM=SYNCSORT                                            00004000
//SYSOUT    DD SYSOUT=*                                                 00004100
//SYSPRINT  DD SYSOUT=*                                                 00004200
//SORTIN    DD DSN=FISP.GAM020.UCHUB00.GRNTFILE.P$FISYYMM,              00004300
//             DISP=SHR                                                 00004400
//SORTOUT   DD DSN=FISP.GAM023.GRNTFILE.SORTED.P$FISYYMM,               00004500
//             DISP=(NEW,CATLG,DELETE),                                 00004600
//             DCB=(RECFM=FB,BLKSIZE=23440,LRECL=80),                   00004700
//             SPACE=(TRK,(475,175),RLSE),                              00004800
//*ORIG        SPACE=(TRK,(75,75),RLSE),                                00004800
//             UNIT=SYSDA                                               00004900
//SYSIN     DD *                                                        00005000
        SORT FIELDS=(1,11,CH,A)                                         00041100
/*                                                                      00005100
//SORTWK01  DD SPACE=(TRK,300),                                         00005200
//             UNIT=SYSDA                                               00005300
//SORTWK02  DD SPACE=(TRK,300),                                         00005400
//             UNIT=SYSDA                                               00005500
//SORTWK03  DD SPACE=(TRK,300),                                         00005600
//             UNIT=SYSDA                                               00005700
//SORTWK04  DD SPACE=(TRK,300),                                         00005800
//             UNIT=SYSDA                                               00005900
//SORTWK05  DD SPACE=(TRK,300),                                         00006000
//             UNIT=SYSDA                                               00006100
//SORTWK06  DD SPACE=(TRK,300),                                         00006200
//             UNIT=SYSDA                                               00006300
//SORTWK07  DD SPACE=(TRK,300),                                         00006400
//             UNIT=SYSDA                                               00006500
//SORTWK08  DD SPACE=(TRK,300),                                         00006600
//             UNIT=SYSDA                                               00006700
//*====================================================                 00006800
//UPDTE030 EXEC PGM=UCHU010,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00006900
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UCHU010) PLAN(UCHU010)
/*
//SYSOUT    DD SYSOUT=*                                                 00007800
//SYSDBOUT  DD SYSOUT=*                                                 00007900
//SYSUDUMP  DD SYSOUT=D,                                                00008000
//             DEST=LOCAL                                               00008100
//GRNTFILE  DD DSN=FISP.GAM023.GRNTFILE.SORTED.P$FISYYMM,               00008200
//             DISP=SHR                                                 00008300
//CONTROLR  DD SYSOUT=*                                                 00008400
//CONTROLF  DD *                                                        00008500
*UNIVERSITY CODE $FISUNVRSCODE
*COA CODE $FISCOACODE
*FISCAL YEAR $FISFSCLYR
*COMMIT COUNTER 0100
/*                                                                      00008600
//*====================================================                 00030700
//CLEAN040 EXEC PGM=IEFBR14                                             00030800
//SYSPRINT  DD SYSOUT=Z                                                 00030900
//DD1       DD DSN=FISP.GAM023.GRNTFILE.SORTED.P$FISYYMM,               00031000
//             DISP=(MOD,DELETE,DELETE),                                00031100
//             DCB=(RECFM=FB,LRECL=80,BLKSIZE=23440),                   00031200
//             SPACE=(TRK,(1,1),RLSE),                                  00031300
//             UNIT=SYSDA                                               00031400
//*                                                                     00031800
//*============================================================         00031800
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00031800
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00031800
//*============================================================         00031800
//STOPZEKE EXEC STOPZEKE                                                00031800
//*                                                                     00031800
//*================ E N D  O F  J C L  GAM023  =========                00031800
