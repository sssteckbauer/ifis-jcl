//GAMP090M JOB (SYS000),'ACCTG-LEDGER',REGION=20M,                      00000100
//            MSGLEVEL=(1,1),                                           00000200
//            MSGCLASS=X,                                               00000300
//            CLASS=S,                                                  00000400
//            RESTART=SSORT040,                                         00000500
//*           RESTART=SURPT050,                                         00000500
//            NOTIFY=APCDBM                                             00000600
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # SUBMIT, SPECIAL RUN.                                00000800
//*++++++++++++++++++++++++++                                           00000900
/*JOBPARM LINES=1500                                                    00001000
//FUNDRPT OUTPUT CLASS=X,FORMS=L010,GROUPID=ACCTG                       00001100
//FUNDBKP OUTPUT CLASS=G,FORMS=L010,GROUPID=BACKUP                      00001200
//FUNDRPT2 OUTPUT CLASS=X,FORMS=L010,GROUPID=ACCTG                      00001100
//FUNDBKP2 OUTPUT CLASS=G,FORMS=L010,GROUPID=BACKUP
//*-------------------------------------------------------------------
//*  ALL PRINTING HAS BEEN TURNED OFF PER BOB COLIO - 6/24/2010.
//*  BOTH ALL YEAR AND FISCAL!
//*-------------------------------------------------------------------- 00001300
//*  NOTE:   THIS JOB MUST BE STARTED BY ** ZEKE **                   * 00001400
//*                                                                   * 00001500
//*  ZEKE DATA-NAME $FISGAM090STEP IS USED TO SAVE THE NAME OF THE    * 00001600
//*  LAST JOB STEP PROCESSED IN THE JOB STREAM, SO THAT WHEN ANY STEP * 00001700
//*  ABENDS, THE JOB WILL BE RESTARTED AT THE SAME STEP.              * 00001800
//*-------------------------------------------------------------------- 00001900
//*  PURPOSE:                                                           00002000
//*          EXTRACT DATA FROM THE IFIS OPERATING LEDGER AND            00002100
//*          IFIS GENERAL LEDGER TO CREATE THE FILE FOR PRODUCING       00002200
//*          THE IFIS FUND SUMMARY REPORTS.                             00002300
//*-------------------------------------------------------------------- 00002400
//*     IFIS FUND SUMMARY       - CREATE EXTRACT FILE FOR PRODUCING   * 00002500
//*  REPORTING EXTRACT FILE       THE IFIS FUND SUMMARY REPORTS.      * 00002600
//*  INPUT FILE                 - CNTLCARD : CONTROL FILE             * 00002700
//*                             - UUTC20V2 : FOAPAL-RULE-FILE         * 00002800
//*  OUTPUT FILES               - CNTLRPT  : CONTROL REPORT           * 00002900
//*                             - FUNDEXTR : FUND SUMM EXTRACT FILE   * 00003000
//*-------------------------------------------------------------------- 00003100
//*---------------------------------------------------------------------00014300
//*  RUN SYNCSORT PROGRAM - SORT EXTRACT FILE TO PRODUCE THE            00014400
//*                         SUMMARY BY ACCOUNT REPORT AND THE           00014500
//*                         SUMMARY BY PROGRAM REPORT.                  00014600
//*---------------------------------------------------------------------00014700
//SSORT040 EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00014800
//SYSOUT    DD SYSOUT=*                                                 00014900
//SYSPRINT  DD SYSOUT=*                                                 00015000
//SORTIN    DD DSN=FISP.GAM090.UGAX120.FUNDEXTR.P2001,                  00015100
//             DISP=SHR                                                 00015200
//SORTOUT   DD DSN=&&UGAS120,                                           00015300
//             DISP=(NEW,PASS,DELETE),                                  00015400
//             DCB=(RECFM=FB,BLKSIZE=23380,LRECL=140),                  00015500
//             SPACE=(TRK,(150,360),RLSE),                              00015600
//             UNIT=VIO                                                 00015700
//SORTWK01  DD SPACE=(TRK,(150,150)),                                   00015800
//             UNIT=SYSDA                                               00015900
//SORTWK02  DD SPACE=(TRK,(150,150)),                                   00016000
//             UNIT=SYSDA                                               00016100
//SORTWK03  DD SPACE=(TRK,(150,150)),                                   00016200
//             UNIT=SYSDA                                               00016300
//SORTWK04  DD SPACE=(TRK,(150,150)),                                   00016400
//             UNIT=SYSDA                                               00016500
//SORTWK05  DD SPACE=(TRK,(150,150)),                                   00016600
//             UNIT=SYSDA                                               00016700
//SORTWK06  DD SPACE=(TRK,(150,150)),                                   00016800
//             UNIT=SYSDA                                               00016900
//SORTWK07  DD SPACE=(TRK,(150,150)),                                   00017000
//             UNIT=SYSDA                                               00017100
//SORTWK08  DD SPACE=(TRK,(150,150)),                                   00017200
//             UNIT=SYSDA                                               00017300
//SORTWK09  DD SPACE=(TRK,(150,150)),                                   00017400
//             UNIT=SYSDA                                               00017500
//SORTWK10  DD SPACE=(TRK,(150,150)),                                   00017600
//             UNIT=SYSDA                                               00017700
//SORTWK11  DD SPACE=(TRK,(150,150)),                                   00017800
//             UNIT=SYSDA                                               00017900
//SORTWK12  DD SPACE=(TRK,(150,150)),                                   00018000
//             UNIT=SYSDA                                               00018100
//SORTWK13  DD SPACE=(TRK,(150,150)),                                   00018200
//             UNIT=SYSDA                                               00018300
//SYSIN     DD *                                                        00018400
           SORT FIELDS=(1,54,CH,A)                                      00110000
/*                                                                      00018500
//*---------------------------------------------------------------------00018600
//*  RUN IFIS FUND SUMMARY REPORTING  - PRODUCE THE SUMMARY BY ACCOUNT  00018700
//*    REPORT GENERATION PROCESS      - AND SUMMARY BY PROGRAM REPORTS  00018800
//*  INPUT FILES                      - CNTLCARD : CONTROL FILE         00018900
//*                                   - FUNDEXTR : FUND EXTRACT FILE    00019000
//*  OUTPUT FILES                     - CNTLRPT1 : CONTROL REPORT       00019100
//*                                   - ACSUMRPT : ACCOUNT SUMMARY RPT  00019200
//*                                   - PRSUMRPT : PROGRAM SUMMARY RPT  00019300
//*---------------------------------------------------------------------00019400
//SURPT050 EXEC PGM=UGAP120,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),         00019500
//    PARM='ISISDICT'
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAP120) PLAN(UGAP120)
/*
//SYSOUT    DD SYSOUT=*                                                 00020500
//SYSLST    DD SYSOUT=*                                                 00020600
//SYSUDUMP  DD SYSOUT=D                                                 00020700
//SYSIN     DD DUMMY                                                    00020800
//FUNDEXTR  DD DSN=&&UGAS120,                                           00020900
//             DISP=(OLD,DELETE)                                        00021000
//ACDTLRPT  DD DUMMY                                                    00021100
//ACSUMRPT  DD DSN=FISP.GAM090.UGAP120.ACSUMRPT.P2001,                  00021200
//             DISP=(MOD,CATLG,DELETE),                                 00021300
//             DCB=(RECFM=FB,LRECL=181,BLKSIZE=23349),                  00021400
//             SPACE=(TRK,(150,255),RLSE),                              00021500
//             UNIT=SYSDA                                               00021600
//PRSUMRPT  DD DSN=FISP.GAM090.UGAP120.PRSUMRPT.P2001,                  00021700
//             DISP=(MOD,CATLG,DELETE),                                 00021800
//             DCB=(RECFM=FB,LRECL=181,BLKSIZE=23349),                  00021900
//             SPACE=(TRK,(150,255),RLSE),                              00022000
//             UNIT=SYSDA                                               00022100
//CNTLRPT1  DD SYSOUT=(,),                                              00022200
//             OUTPUT=*.FUNDRPT                                         00022300
//*            OUTPUT=(*.FUNDRPT,*.FUNDBKP)                             00022400
//CNTLCARD  DD *
*UNIVERSITY CODE 01
*COA CODE A
*FISCAL YEAR 20
*ACCOUNTING PERIOD 07
*DESCRIPTION REPORTS
/*
//*CNTLCARD  DD DSN=FISP.PARMLIB(GAM090P1),DISP=SHR                     00022500
//*---------------------------------------------------------------------00022700
//PRINT060 EXEC PGM=SORT                                                00022800
//SYSLST    DD SYSOUT=*                                                 00022900
//SYSOUT    DD SYSOUT=*                                                 00023000
//SYSUDUMP  DD SYSOUT=D                                                 00023100
//SORTIN    DD DSN=FISP.GAM090.UGAP120.ACDTLRPT.P2001,                  00023200
//             DISP=OLD                                                 00023300
//SORTOUT   DD SYSOUT=(,),                                              00023400
//             OUTPUT=*.FUNDRPT                                         00023500
//*            OUTPUT=(*.FUNDRPT,*.FUNDBKP)                             00023600
//SYSIN     DD *                                                        00023700
           SORT FIELDS=COPY
//*                                                                     00023800
//*---------------------------------------------------------------------00023900
//PRINT090 EXEC PGM=SORT                                                00027400
//SYSLST    DD SYSOUT=*                                                 00027500
//SYSOUT    DD SYSOUT=*                                                 00027600
//SYSUDUMP  DD SYSOUT=D                                                 00027700
//SORTIN    DD DSN=FISP.GAM090.UGAP120.ACSUMRPT.P2001,                  00027800
//             DISP=OLD                                                 00027900
//SORTOUT   DD SYSOUT=(,),                                              00028000
//             OUTPUT=(*.FUNDRPT2,*.FUNDBKP2)                           00028200
//SYSIN     DD *                                                        00028300
           SORT FIELDS=COPY
//*                                                                     00028400
//*---------------------------------------------------------------------00031900
//PRINT120 EXEC PGM=SORT                                                00032000
//SYSLST    DD SYSOUT=*                                                 00032100
//SYSOUT    DD SYSOUT=*                                                 00032200
//SYSUDUMP  DD SYSOUT=D                                                 00032300
//SORTIN    DD DSN=FISP.GAM090.UGAP120.PRSUMRPT.P2001,                  00032400
//             DISP=OLD                                                 00032500
//SORTOUT   DD SYSOUT=(,),                                              00032600
//             OUTPUT=(*.FUNDRPT2,*.FUNDBKP2)                           00032800
//SYSIN     DD *                                                        00032900
           SORT FIELDS=COPY
//*                                                                     00033000
//*============================================================
//*  FTP TO BFS
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2076 2089 2092) REBUILD'
/*
//*===============================================================      00036500
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00037900
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00038000
//*===============================================================      00036500
//STOPZEKE EXEC STOPZEKE                                                00038200
//*                                                                     00038300
//*=================== E N D  O F  J C L  GAM090 =================      00038400
