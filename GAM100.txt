//GAMP100 JOB (SYS000),'S.STE.MARIE',MSGLEVEL=(1,1),
//*       RESTART=SUBDWGA1,
//        MSGCLASS=F,CLASS=K,NOTIFY=APCDBM,REGION=20M
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 837
//*++++++++++++++++++++++++++
//*====================================================
//*  PURPOSE:
//*        SUBMIT ALL EXISTING EVENTS OF BALTAPE PROCESS FOR EXECUTION
//*
//*        GAMP540 -Z# 625 DISCONTINUED AS OF 6/19/09-PER BOB COLIO.
//*====================================================
//SUBBF010 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (617 618 619 620 621 622 623 624 626) REBUILD'
/*
//*====================================================
//*   event 1822 - new jcl -gamp600 (monthly)       \same jcl, diff
//*   event 1367 - old jcl -gaa020w (fiscal closing)/times
//*      (gaa020w issued at 'pre-final' times)
//*====================================================
//SUBBF020 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (1822 869 870 2931 930 1460 672 931 1930) REBUILD'
/*
//*============================================================
//SUBBF030 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (583 693 1781 1782 444) REBUILD'
/*
//*============================================================
//SUBBF040 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2058 2191 1292 3581) REBUILD'
/*
//*============================================================
//SUBDWJB2 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2653 2622 2968 2844 2642) REBUILD'
/*
//*============================================================
//SUBDWJB3 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZA EV (2642) RUN'
/*
//*============================================================
//*  SUBMIT DSQDED01 - PAYROLL EXTRACT FOR MONTHLY PROCESSING
//*============================================================
//SUBPSSQ1 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (1466) REBUILD'
//*
//*============================================================
//*  ZALETER DSQDED01 - PAYROLL EXTRACT FOR MONTHLY PROCESSING
//*============================================================
//SUBPSSQ2 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZALTER EV (1466) TIMEOK'
//*
//*============================================================
//*  SUBMIT DATA WAREHOUSE INCREMENTAL/MONTHEND TRIGGER JOB
//*============================================================
//SUBDWTR1 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2459) REBUILD'
/*
//*============================================================
//*  SUBMIT DW JOB TO CLOSE OLD ACCOUNTING PERIOD.
//*============================================================
//SUBOPEN  EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2676) REBUILD'
/*
//*============================================================
//*  SUBMIT SETBSL2 JOB TO PICK UP NEW EDB OF NEW END OF MONTH
//*  10/5/2009 - TURNED OFF TEMPORARILY UNTIL I CAN DETERMINE
//*  CORRECT TIMING.(DOUG)
//*============================================================
//*//SUBSLBV  EXEC PGM=ZEKESET,
//*//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*//SYSPRINT  DD SYSOUT=*
//*//SYSIN     DD *
//* SET SCOM 'ZADD EV (739) REBUILD'
//*//*
//*============================================================
//*  SUBMIT GAMPPDF1/GAMPPDF2 JOBS TO CREATE
//*  PDF TAR FILE FOR BFS OF ALL MC LEDGER REPORTS
//*============================================================
//SUBPDFS  EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2945 2946 1330) REBUILD'
/*
//*==============================================================
//* THIS STEP SUBMITS THE JOB (SETDATLP) THAT SETS THE
//*  **ZENA GLOBAL VARIABLES**
//* (PASSES THE VALUE TO ZENA)
//*==============================================================
//SUBSETQ  EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2979) REBUILD'
/*
//*
//*============================================================
//*  SUBMIT GAMP930 - IFIS ENPET JOURNAL PURGE TO FOLLOW AFTER
//*                   EOJ OF ifis_enpet_purge_jb_step1 ON DARWIN
//* (!!MUST MAKE ACTIVE WHEN LYLE HAS FINISHED TESTING - 10/03/02)
//*============================================================
//*SUBEPRG  EXEC PGM=ZEKESET,
//*             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*SYSPRINT  DD SYSOUT=*
//*SYSIN     DD *
//* SET SCOM 'ZADD EV (1724) REBUILD'
//*/*
//*============================================================
//*  DELETE MEDICAL CENTER DATASETS IN PREPARATION TO THE
//*  RUNNING OF THE ZENA PROCESSES AND FTP FROM HOPPER
//*============================================================
//CLEAN010 EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=Z
//DD1       DD DSN=FISP.HOSP.ALB.PAYROLL,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA
//DD2       DD DSN=FISP.HOSP.PAYROLL,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA
//DD3       DD DSN=FISP.HOSP.BALANCE,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA
//DD4       DD DSN=FISP.HOSP.DLTRAN,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA
//*============================================================
//ALOCH1   EXEC PGM=IEFBR14
//ALC01     DD DSN=FISP.HOSP.ALB.PAYROLL,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(10,2)),
//             UNIT=SYSALLDA,MGMTCLAS=MY
//ALC02     DD DSN=FISP.HOSP.PAYROLL,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(500,50)),
//             UNIT=SYSALLDA,MGMTCLAS=MY
//ALC03     DD DSN=FISP.HOSP.BALANCE,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(200,10)),
//             UNIT=SYSALLDA,MGMTCLAS=MY
//ALC04     DD DSN=FISP.HOSP.DLTRAN,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=VB,LRECL=256,BLKSIZE=6233),
//             SPACE=(TRK,(250,50)),
//             UNIT=SYSALLDA,MGMTCLAS=MY
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  GAM100   ========
