//GAMP100W JOB (SYS000),'S.STE.MARIE',MSGLEVEL=(1,1),
//*       RESTART=SUBDWGA1,
//        MSGCLASS=F,CLASS=K,NOTIFY=APCDBM,REGION=20M
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 1980
//*++++++++++++++++++++++++++
//*====================================================
//*  PURPOSE:
//*        SUBMIT ALL EXISTING EVENTS OF BALTAPE PROCESS FOR EXECUTION
//*====================================================
//SUBBF010 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (942 1292 1460 3581) REBUILD'
/*
//*====================================================
//SUBBF015 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZALTER EV (942) WHENOK EOJ GAMP050'
/*
//*====================================================
//SUBBF020 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (617 618 619 620 621 622 623 2983 930 1930) REBUILD'
/*
//*
//*====================================================
//SUBBF025 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZALTER EV (617) WHENAND (EOJ GAMP060)'
/*
//*
//*====================================================
//SUBBF027 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZALTER EV (618) WHENAND (EOJ GAMP060)'
/*
//*
//*====================================================
//SUBBF030 EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (870 2931 1641 444) REBUILD'
/*
//*
//*==============================================================
//* THIS STEP SUBMITS THE JOB (SETDATLP) THAT SETS THE
//*  **ZENA GLOBAL VARIABLES**
//* (PASSES THE VALUE TO ZENA)
//*==============================================================
//SUBSETQ  EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2979) REBUILD'
/*
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  GAM100W  ========
