//GAMP101 JOB (SYS000),'BUDGET -355TPCS',MSGCLASS=F,MSGLEVEL=(1,1),     00000100
//*       RESTART=STEP04,
//        NOTIFY=APCDBM,CLASS=K,REGION=20M                              00000200
//*++++++++++++++++++++++++++                                           00000300
//*    ZEKE EVENT # 2058                                                00000400
//*++++++++++++++++++++++++++                                           00000500
//OUT1 OUTPUT CLASS=X,FORMS=BS11,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET   00000600
//OUT2 OUTPUT CLASS=X,FORMS=BS02,GROUPID=BUDGET1,COPIES=2               00000700
//OUT3 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BACKUP,COPIES=2                00000800
//*====================================================                 00000900
//*               TRANSFER OF FUNDS REPORTS                             00001000
//*                     ( FGA..21D )                                    00001100
//*====================================================                 00001200
//CLEAN00  EXEC PGM=IEFBR14                                             00001300
//SYSPRINT  DD SYSOUT=*                                                 00001400
//DD1       DD DSN=FISP.GAM101.FGAP021D.CONTROLR.P$FISYYMM,             00001500
//             DISP=(MOD,DELETE,DELETE),MGMTCLAS=MY,                    00001600
//             DCB=(RECFM=FB,BLKSIZE=13200,LRECL=132),                  00001700
//             SPACE=(TRK,(20,20),RLSE),                                00001800
//             UNIT=SYSDA                                               00001900
//DD2       DD DSN=FISP.GAM101.FGAP021D.REPORT02.P$FISYYMM,             00002000
//             DISP=(MOD,DELETE,DELETE),MGMTCLAS=MY,                    00002100
//             DCB=(RECFM=FB,BLKSIZE=13300,LRECL=133),                  00002200
//             SPACE=(TRK,(20,20),RLSE),                                00002300
//             UNIT=SYSDA                                               00002400
//*====================================================                 00002500
//*       TRANSFER OF FUNDS REPORTS                                     00002600
//*====================================================                 00002700
//STEP01   EXEC PGM=FGAP021D                                            00002800
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(FGAP021D) PLAN(FGAP021D)
/*
//SYSOUT    DD SYSOUT=*                                                 00003700
//SYSLST    DD SYSOUT=*                                                 00003800
//SYSUDUMP  DD SYSOUT=D                                                 00003900
//SYSDBOUT  DD SYSOUT=*                                                 00004000
//PARM01    DD *                                                        00004100
*UNIVERSITY CODE $FISUNVRSCODE
*COA CODE $FISCOACODE
*FISCAL YEAR $TOFFSCLYR
*ACCOUNTING PERIOD $TOFACCTGPRD
/*                                                                      00004200
//CONTROLR  DD DSN=FISP.GAM101.FGAP021D.CONTROLR.P$FISYYMM,             00004300
//             DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                       00004400
//             DCB=(RECFM=FB,BLKSIZE=13200,LRECL=132),                  00004500
//             SPACE=(TRK,(920,120),RLSE),                              00004600
//             UNIT=(SYSALLDA,2)                                        00004700
//REPORT02  DD DSN=FISP.GAM101.FGAP021D.REPORT02.P$FISYYMM,             00004800
//             DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                       00004900
//             DCB=(RECFM=FB,BLKSIZE=13300,LRECL=133),                  00005000
//             SPACE=(TRK,(80,60),RLSE),                                00005100
//             UNIT=(SYSALLDA,2)                                        00005200
//*                                                                     00005300
//*====================================================                 00005400
//*       PRINT CONTROL REPORT                                          00005500
//*====================================================                 00005600
//STEP02   EXEC PGM=IEBGENER                                            00005700
//SYSPRINT  DD SYSOUT=*                                                 00005800
//SYSUT2    DD SYSOUT=*                                                 00005900
//SYSUT1    DD DSN=FISP.GAM101.FGAP021D.CONTROLR.P$FISYYMM,             00006000
//             DISP=SHR                                                 00006100
//SYSIN     DD DUMMY                                                    00006200
//*                                                                     00006300
//*====================================================                 00006400
//* PRINT EXTRACT REPORT (INACTIVE AS OF 10-06-94)                      00006500
//*====================================================                 00006600
//*STEP03   EXEC PGM=IEBGENER                                           00006700
//*SYSPRINT  DD SYSOUT=*                                                00006800
//*SYSUT2    DD SYSOUT=(,),                                             00006900
//*             OUTPUT=(*.OUT2,*.OUT3)                                  00007000
//*SYSUT1    DD DSN=FISP.GAM101.FGAP021D.REPORT02.P$FISYYMM,            00007100
//*             DISP=SHR                                                00007200
//*SYSIN     DD DUMMY                                                   00007300
//*                                                                     00007400
//*====================================================                 00007500
//* THIS STEP WILL PRODUCE THE TRANSFER-OF-FUNDS FICHE TAPE             00007600
//*
//* 02/02/01 - FICHE CREATION TURNED OFF PER BUDGET OFFICE-ROBERT H.
//*====================================================                 00007700
//*STEP04   EXEC PGM=SYNCSORT                                           00007800
//*SORTIN    DD DSN=FISP.GAM101.FGAP021D.REPORT02.P$FISYYMM,            00007900
//*             DISP=OLD                                                00008000
//*SORTOUT   DD DSN=FISP.GAM101.FUND.TRANSFER.FICHE,                    00008100
//*             DISP=(NEW,KEEP),                                        00008200
//*             DCB=(DEN=4,RECFM=FB,LRECL=133,BLKSIZE=13300),           00008300
//*             LABEL=(1,SL,RETPD=30),                                  00008400
//*             UNIT=CTAPE                                              00008500
//*SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),                             00008600
//*             DISP=SHR                                                00008700
//*SYSOUT    DD DSN=&&R90CNT,                                           00008800
//*             DISP=(NEW,PASS),                                        00008900
//*             DCB=(RECFM=F,BLKSIZE=132,DSORG=DA),                     00009000
//*             SPACE=(80,100),                                         00009100
//*             UNIT=VIO                                                00009200
//*SYSPRINT  DD SYSOUT=*                                                00009300
//*====================================================                 00009400
//*STEP05   EXEC PGM=XMITTAL                                            00009500
//*TAPECARD  DD DSN=*.STEP04.SORTOUT,                                   00009600
//*             DISP=OLD,                                               00009700
//*             DCB=*.STEP04.SORTOUT,                                   00009800
//*             VOL=REF=*.STEP04.SORTOUT                                00009900
//*RECCOUNT  DD DSN=&&R90CNT,                                           00010000
//*             DISP=(OLD,DELETE)                                       00010100
//*SYSOUT    DD SYSOUT=*                                                00010200
//*TRANSMIT  DD SYSOUT=C,                                               00010300
//*             DEST=LOCAL                                              00010400
//*MAILTO    DD DSN=FISP.PARMLIB(APADDRS4),                             00010500
//*             DISP=SHR                                                00010600
//*COMMENTS  DD DSN=FISP.PARMLIB(APCMNT6),                              00010700
//*             DISP=SHR                                                00010800
//*                                                                     00010900
//*============================================================
//*  FTP TO BFS
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2094) REBUILD'
/*
//*============================================================         00011000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00011100
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00011200
//*============================================================         00011300
//STOPZEKE EXEC STOPZEKE                                                00011400
//*                                                                     00011500
//*================ E N D  O F  J C L  GAM101   ========                00250007
