//GAMP400 JOB (SYS000),'STE.MARIE-ACCTG',                               00000100
//        MSGLEVEL=(1,1),                                               00000200
//        MSGCLASS=F,                                                   00000300
//        CLASS=K,                                                      00000400
//        NOTIFY=APCDBM,                                                00000500
//        REGION=0M                                                     00000600
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # 672                                                 00000800
//*++++++++++++++++++++++++++                                           00000900
//OUT2 OUTPUT CLASS=G,DEST=U321,GROUPID=STEVE                           00110005
//OUTBAK OUTPUT CLASS=4,DEST=U321,GROUPID=STEVE                         00120005
//* (OUTBAK CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION PROJECT ONLY)
//*
/*ROUTE PRINT RMT10                                                     00001000
//*====================================================                 00001100
//*  NOTE: THIS JOB MUST BE STARTED BY ** ZEKE **                     * 00001200
//*====================================================                 00001300
//*  PURPOSE:                                                         * 00001400
//*          EXTRACT RECORDS FROM THE IFIS TRANSACTION AUDIT DATA BY  * 00001500
//*          ACCOUNTING PERIOD AND CREATE TWO FILES - IFIS LEDGER     * 00001600
//*          DETAIL TRANSACTIONS AND THE UCOP GL031 FILE FOR SUBMITTAL *00001700
//*          TO THE OFFICE OF THE PRESIDENT'S EXTERNAL AUDITORS.      * 00001800
//*====================================================                 00001900
//DSDEL010 EXEC DSDEL,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),               00002000
//             PARM.D='NONVSAM,FISP.GAM400.UGAX400S.DETLTRNS.P$FISYYMM' 00002100
/*                                                                      00002200
//*====================================================                 00002300
//DSDEL020 EXEC DSDEL,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),               00002400
//             PARM.D='NONVSAM,FISP.GAM400.UGAX400S.UCOPGL31.P$FISYYMM' 00002500
/*                                                                      00002600
//*====================================================                 00002700
//*  IFIS LEDGER DTL TRANSACTIONS     - CREATE IFIS LEDGER DETAIL       00002800
//*       AND UCOP GL031 FILE           TRANSACTIONS FILE AND UCOP GL03100002900
//*                                     FILE .                          00003000
//*  INPUT FILE                       - CNTLCARD : CONTROL FILE         00003100
//*  OUTPUT FILES                     - CNTLRPT1 : CONTROL REPORT       00003200
//*                                   - DETLTRNS : LEDGER DTL TRANS FILE00003300
//*                                   - UCOPGL31 : UCOP GL031 FILE      00003400
//*====================================================                 00003500
//GL031030 EXEC PGM=UGAX400S,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),        00003600
//     PARM='ISISDICT'
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX400S) PLAN(UGAX400S)
/*
//SYSOUT    DD SYSOUT=*                                                 00004600
//SYSLST    DD SYSOUT=*                                                 00004700
//SYSUDUMP  DD SYSOUT=D,                                                00004800
//             DEST=LOCAL                                               00004900
//SYSIN     DD DUMMY                                                    00005000
//ERRRPT    DD DUMMY                                                    00450002
//* VSAM FILE OF IFIS EXTERNAL REPORT CODES                             00005100
//UUTC20V1  DD DSN=FISP.UUTC20V1,                                       00005200
//             DISP=SHR,                                                00005300
//             AMP=('BUFNI=2')                                          00005400
//* VSAM FILE OF IFIS FOAPAL/RULE DATA                                  00005500
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00005600
//             DISP=SHR,                                                00005700
//             AMP=('BUFNI=2')                                          00005800
//DETLTRNS  DD DSN=FISP.GAM400.UGAX400S.DETLTRNS.P$FISYYMM,             00005900
//             DISP=(NEW,CATLG,KEEP),                                   00006000
//             DCB=(LRECL=300,BLKSIZE=27900,BUFNO=8,RECFM=FB),          00006100
//             SPACE=(TRK,(9500,9500),RLSE),                            00006200
//*            SPACE=(TRK,(9000,500),RLSE),                             00006200
//*            STORCLAS=APCBIG,                                         00006400
//             UNIT=(SYSALLDA,3)                                        00006400
//*            VOL=SER=APC009                                           00006400
//*            VOL=SER=SYSDA4                                           00006400
//UCOPGL31  DD DSN=&&GL031,                                             00006500
//             DISP=(NEW,PASS),                                         00006600
//             DCB=(LRECL=127,BLKSIZE=27940,BUFNO=8,RECFM=FB),          00006700
//             SPACE=(TRK,(7500,1500),RLSE),                            00006800
//             UNIT=VIO                                                 00006900
//CNTLRPT1  DD SYSOUT=*,                                                00007000
//             DCB=(LRECL=133,BLKSIZE=23408,BUFNO=8,RECFM=FBA)          00007100
//PARMCARD  DD DUMMY                                                    00007200
//CNTLCARD  DD *                                                        00007300
*UNIVERSITY CODE $FISUNVRSCODE                                          00780006
*COA CODE $FISCOACODE                                                   00790006
*FISCAL YEAR $FISFSCLYR                                                 00800006
*ACCOUNTING PERIOD $FISACTGPRD                                          00810006
*DESCRIPTION $FISDESC
/*                                                                      00007400
//*====================================================                 00007500
//SGL31040 EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00007600
//SYSOUT    DD SYSOUT=*                                                 00007700
//SORTIN    DD DSN=&&GL031,                                             00007800
//             DISP=(OLD,DELETE)                                        00007900
//SORTOUT   DD DSN=FISP.GAM400.UGAX400S.UCOPGL31.P$FISYYMM,             00008000
//             DISP=(NEW,CATLG,DELETE),                                 00008100
//             DCB=(LRECL=127,BLKSIZE=27940,BUFNO=8,RECFM=FB),          00008200
//             SPACE=(TRK,(1500,750),RLSE),                             00008300
//             UNIT=SYSALLDA,                                           00008400
//             VOL=SER=SYSDA4                                           00008500
//SORTWK01  DD SPACE=(TRK,300),                                         00008600
//             UNIT=SYSDA                                               00008700
//SORTWK02  DD SPACE=(TRK,300),                                         00008800
//             UNIT=SYSDA                                               00008900
//SORTWK03  DD SPACE=(TRK,300),                                         00009000
//             UNIT=SYSDA                                               00009100
//SORTWK04  DD SPACE=(TRK,300),                                         00009200
//             UNIT=SYSDA                                               00009300
//SORTWK05  DD SPACE=(TRK,300),                                         00009400
//             UNIT=SYSDA                                               00009500
//SORTWK06  DD SPACE=(TRK,300),                                         00009600
//             UNIT=SYSDA                                               00009700
//SORTWK07  DD SPACE=(TRK,300),                                         00009800
//             UNIT=SYSDA                                               00009900
//SORTWK08  DD SPACE=(TRK,300),                                         00010000
//             UNIT=SYSDA                                               00010100
//SORTWK09  DD SPACE=(TRK,300),                                         00010200
//             UNIT=SYSDA                                               00010300
//SORTWK10  DD SPACE=(TRK,300),                                         00010400
//             UNIT=SYSDA                                               00010500
//SORTWK11  DD SPACE=(TRK,300),                                         00010600
//             UNIT=SYSDA                                               00010700
//SORTWK12  DD SPACE=(TRK,300),                                         00010800
//             UNIT=SYSDA                                               00010900
//SYSIN     DD *                                                        00011000
              SORT FIELDS=(6,6,CH,A,
                           27,5,CH,A,
                           32,1,CH,A,
                           33,4,CH,A,
                           61,2,CH,A),EQUALS
/*                                                                      00011100
//*================================================================
//SETVAR   EXEC PGM=ZEKESET
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
  SET VAR $GAM400 EQ 'DONE'
/*
//*============================================================         00011200
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00011300
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00011400
//*============================================================         00011500
//STOPZEKE EXEC STOPZEKE                                                00011600
//*                                                                     00011700
//*================ E N D  O F  J C L  GAM400   ========                00011800
