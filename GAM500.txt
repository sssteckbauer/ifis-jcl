//GAMP500 JOB (SYS000),'ACCTG-LEDGER',                                  00010002
//        MSGLEVEL=(1,1),                                               00020002
//        MSGCLASS=F,                                                   00030002
//        CLASS=K,                                                      00040003
//        NOTIFY=APCDBM,                                                00050002
//        REGION=20M                                                    00060002
//*++++++++++++++++++++++++++                                           00070002
//*    ZEKE EVENT # 617                                                 00080002
//*++++++++++++++++++++++++++                                           00090002
//OUT2 OUTPUT CLASS=P,DEST=U321,GROUPID=STEVE                           00110005
//OUTBAK OUTPUT CLASS=4,DEST=U321,GROUPID=STEVE                         00120005
//* (OUTBAK CHANGED FROM 'G' TO '4' FOR PRINT REDUCTION ONLY).
//*====================================================                 00110002
/*ROUTE PRINT RMT10                                                     00100002
//*====================================================                 00110002
//*  NOTE:  THIS JOB MUST BE STARTED BY ** ZEKE **                      00120002
//*====================================================                 00130002
//*  PURPOSE:                                                           00140002
//*          EXTRACT RECORDS FROM THE GENERAL LEDGER AND CREATE TWO     00150002
//*          FILES - CFS ASSET & LIABLITY BALANCESHEET ACCOUNTS(CFS     00160002
//*          RECORD TYPE 10) AND CFS FUNDS BALANCE ACCOUNTS(CFS RECORD  00170002
//*          TYPE 10,11).                                               00180002
//*====================================================                 00190002
//*  RUN DSDEL PROGRAM                - DELETE PRIOR CATALOGED SETS     00200002
//*====================================================                 00210002
//DSDEL010 EXEC DSDEL,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),               00220002
//             PARM.D='NONVSAM,FISP.GAM500.UGAX500.ASLBLTEX.P$FISYYMM'  00230002
/*                                                                      00240002
//*====================================================                 00250002
//DSDEL020 EXEC DSDEL,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),               00260002
//             PARM.D='NONVSAM,FISP.GAM500.UGAX500.FNDBLNEX.P$FISYYMM'  00270002
/*                                                                      00280002
//*====================================================                 00290002
//*  RUN IFIS GENERAL LEDGER EXTRACT  - CREATE CFS ASSET & LIABILITY    00300002
//*  FOR THE CORPORATE FINANCIAL        BALANCESHEET ACCOUNTS(CFS REC   00310002
//*      SYSTEM (CFS).                  TYPE 10) AND CFS FUNDS BALANCE  00320002
//*                                     ACCOUNTS(CFS RECORD TYPES 10,11)00330002
//*  INPUT FILE                       - CNTLCARD : CONTROL FILE         00340002
//*  OUTPUT FILES                     - CNTLRPT1 : CONTROL REPORT       00350002
//*                                   - ASLBLTEX : ASSET & LIABIL ACCTS 00360002
//*                                   - FNDBLNEX : FUNDS BALANCE ACCTS  00370002
//*====================================================                 00380002
//CFSBF030 EXEC PGM=UGAX500,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),         00390002
//     PARM='ISISDICT'
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX500) PLAN(UGAX500)
/*
//SYSOUT    DD SYSOUT=*                                                 00490002
//SYSLST    DD SYSOUT=*                                                 00500002
//SYSUDUMP  DD SYSOUT=D,                                                00510002
//             DEST=LOCAL                                               00520002
//ERRRPT    DD SYSOUT=(,),                                              00450002
//             OUTPUT=(*.OUT2,*.OUTBAK)                                 00480002
//SYSIN     DD DUMMY                                                    00530002
//* VSAM FILE OF IFIS EXTERNAL REPORT CODES                             00540002
//UUTC20V1  DD DSN=FISP.UUTC20V1,                                       00550002
//             DISP=SHR,                                                00560002
//             AMP=('BUFNI=2')                                          00570002
//* VSAM FILE OF IFIS FOAPAL/RULE DATA                                  00580002
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00590002
//             DISP=SHR,                                                00600002
//             AMP=('BUFNI=2')                                          00610002
//ASLBLTEX  DD DSN=FISP.GAM500.UGAX500.ASLBLTEX.P$FISYYMM,              00620002
//             DISP=(NEW,CATLG,DELETE),                                 00630002
//             DCB=(LRECL=169,BLKSIZE=23322,RECFM=FB),                  00640002
//             SPACE=(TRK,(15,75),RLSE),                                00650002
//             UNIT=SYSALLDA,                                           00660002
//             VOL=SER=SYSDA4                                           00670002
//FNDBLNEX  DD DSN=FISP.GAM500.UGAX500.FNDBLNEX.P$FISYYMM,              00680002
//             DISP=(NEW,CATLG,DELETE),                                 00690002
//             DCB=(LRECL=169,BLKSIZE=23322,RECFM=FB),                  00700002
//             SPACE=(TRK,(90,90),RLSE),                                00710002
//             UNIT=SYSALLDA,                                           00720002
//             VOL=SER=SYSDA4                                           00730002
//CNTLRPT1  DD SYSOUT=*,                                                00740002
//             DCB=(LRECL=133,BLKSIZE=23408,RECFM=FBA)                  00750002
//CNTLCARD  DD *                                                        00760002
*UNIVERSITY CODE $FISUNVRSCODE                                          00770002
*COA CODE $FISCOACODE                                                   00780002
*FISCAL YEAR $FISFSCLYR                                                 00790002
*ACCOUNTING PERIOD $FISACTGPRD                                          00800002
*DESCRIPTION $FISDESC                                                   00810002
/*                                                                      00820002
//*============================================================         00830002
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00840002
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00850002
//*============================================================         00860002
//STOPZEKE EXEC STOPZEKE                                                00870002
//*                                                                     00880002
//*================ E N D  O F  J C L  GAM500   ========                00890002
