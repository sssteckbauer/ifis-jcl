//GAMP515 JOB (SYS000),'ACCTG-LEDGER',                                  00000100
//        MSGLEVEL=(1,1),                                               00000200
//        MSGCLASS=F,                                                   00000300
//        CLASS=K,                                                      00000400
//        NOTIFY=APCDBM,                                                00000500
//        REGION=20M                                                    00000600
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # 620                                                 00000800
//*++++++++++++++++++++++++++                                           00000900
/*ROUTE PRINT RMT10                                                     00001000
//*====================================================                 00001100
//*  NOTE:  THIS JOB MUST BE STARTED BY ** ZEKE **                      00001200
//*         (ESTABLISHED NOT TO DISPATCHED UNTIL NORMAL END OF JOBS     00001300
//*         GAM500 & GAM505).                                           00001400
//*====================================================                 00001500
//*  PURPOSE:                                                           00001600
//*          SUMMARIZE THE CFS FUNDS BALANCE ACCOUNTS (CFS RECORD TYPE  00001700
//*          10 & 11) EXTRACT FILE INTO A SINGLE RECORD FOR EACH        00001800
//*          CFS ACCOUNT/FUND/SUB-ACCOUNT/RECORD-TYPE COMBINATION.      00001900
//*====================================================                 00002000
//*  RUN DSDEL PROGRAM                - DELETE PRIOR CATALOGED SETS     00002100
//*====================================================                 00002200
//DSDEL000 EXEC DSDEL,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),               00002300
//             PARM.D='NONVSAM,FISP.GAM515.UGAX515.FNDBLNEX.P$FISYYMM'  00002400
/*                                                                      00002500
//*====================================================                 00002600
//DSDEL010 EXEC DSDEL,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),               00002700
//             PARM.D='NONVSAM,FISP.GAM515.UGAX515.FNDBLNSU.P$FISYYMM'  00002800
/*                                                                      00002900
//*====================================================                 00003000
//*  RUN SYNCSORT PROGRAM             - MERGE TWO CFS FUNDS BALANCE     00003100
//*                                     ACCOUNTS FILES EXTRACTED FROM   00003200
//*                                     THE GENERAL LEDGER AND THE      00003300
//*                                     OPERATIING LEDGER INTO ONE CFS  00003400
//*                                     FUNDS BALANCE ACCOUNTS (CFS     00003500
//*                                     RECORD TYPE 10 & 11).           00003600
//*====================================================                 00003700
//SORT020  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00003800
//SYSOUT    DD SYSOUT=*                                                 00003900
//SYSPRINT  DD SYSOUT=*                                                 00004000
//SORTIN    DD DSN=FISP.GAM500.UGAX500.FNDBLNEX.P$FISYYMM,              00004100
//             DISP=SHR                                                 00004200
//          DD DSN=FISP.GAM505.UGAX505.TRANSFEX.P$FISYYMM,              00004300
//             DISP=SHR                                                 00004400
//*         DD DSN=$FISGAM515                                           00004500
//SORTOUT   DD DSN=&&SORT020,                                           00004600
//             DISP=(NEW,PASS,DELETE),                                  00004700
//             DCB=(LRECL=169,BLKSIZE=23322,RECFM=FB),                  00004800
//             SPACE=(TRK,(15,75),RLSE),                                00004900
//             UNIT=VIO                                                 00005000
//SORTWK01  DD SPACE=(TRK,300),                                         00005100
//             UNIT=SYSDA                                               00005200
//SORTWK02  DD SPACE=(TRK,300),                                         00005300
//             UNIT=SYSDA                                               00005400
//SORTWK03  DD SPACE=(TRK,300),                                         00005500
//             UNIT=SYSDA                                               00005600
//SORTWK04  DD SPACE=(TRK,300),                                         00005700
//             UNIT=SYSDA                                               00005800
//SYSIN     DD *                                                        00005900
               SORT FIELDS=(50,38,CH,A)
/*                                                                      00006000
//*====================================================                 00006100
//*  RUN IFIS FUNDS BALANCE ACCOUNTS  - CREATE THE SUMMARIZED CFS FUNDS 00006200
//*  SUMMARIZATION FOR THE CORPORATE    BALANCE ACCOUNTS (CFS RECORD    00006300
//*      FINANCIAL SYSTEM (CFS)         TYPE 10 & 11)                   00006400
//*  INPUT FILES                      - CNTLCARD : CONTROL FILE         00006500
//*                                   - FNDBLNEX : FUNDS BALANCE ACCTS  00006600
//*                                   - TRANSFEX : FUNDS BALANCE ACCTS  00006700
//*  OUTPUT FILES                     - FNDBLNSU : SUMMAR FUNDS BALANCE 00006800
//*                                   - CNTLRPT1 : INPUT CONTROL REPORT 00006900
//*                                   - CNTLRPT2 : OUTPUT CONTROL REPORT00007000
//*====================================================                 00007100
//CFSFN030 EXEC PGM=UGAX515,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),         00007200
//     PARM='ISISDICT'
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX515) PLAN(UGAX515)
/*
//SYSOUT    DD SYSOUT=*                                                 00008200
//SYSLST    DD SYSOUT=*                                                 00008300
//SYSUDUMP  DD SYSOUT=D                                                 00008400
//SYSIN     DD DUMMY                                                    00008500
//FNDBLNEX  DD DSN=&&SORT020,                                           00008600
//             DISP=(OLD,DELETE)                                        00008700
//FNDBLNSU  DD DSN=FISP.GAM515.UGAX515.FNDBLNSU.P$FISYYMM,              00008800
//             DISP=(NEW,CATLG,DELETE),                                 00008900
//             DCB=(LRECL=120,BLKSIZE=23400,RECFM=FB),                  00009000
//             SPACE=(TRK,(15,75),RLSE),                                00009100
//             UNIT=SYSALLDA,                                           00009200
//             VOL=SER=SYSDA4                                           00009300
//CNTLRPT1  DD SYSOUT=*,                                                00009400
//             DCB=(LRECL=133,BLKSIZE=23408,RECFM=FBA)                  00009500
//CNTLRPT2  DD SYSOUT=*,                                                00009600
//             DCB=(LRECL=133,BLKSIZE=23408,RECFM=FBA)                  00009700
//CNTLCARD  DD *                                                        00009800
*UNIVERSITY CODE $FISUNVRSCODE                                          00762000
*COA CODE $FISCOACODE                                                   00763000
*FISCAL YEAR $FISFSCLYR                                                 00764000
*ACCOUNTING PERIOD $FISACTGPRD                                          00765000
*DESCRIPTION $FISDESC
/*                                                                      00009900
//*============================================================         00010000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00010100
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00010200
//*============================================================         00010300
//STOPZEKE EXEC STOPZEKE                                                00010400
//*                                                                     00010500
//*================ E N D  O F  J C L  GAM515   ========                00010600
