//GAMP520  JOB (SYS000),'ACCTG-LEDGER',                                 00000100
//        MSGLEVEL=(1,1),                                               00000200
//        MSGCLASS=F,                                                   00000300
//        CLASS=K,                                                      00000400
//        NOTIFY=APCDBM,                                                00000500
//        REGION=20M                                                    00000600
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # 621                                                 00000800
//*++++++++++++++++++++++++++                                           00000900
/*ROUTE PRINT RMT10                                                     00001000
//*====================================================                 00001100
//*  NOTE:  THIS JOB MUST BE STARTED BY ** ZEKE **                      00001200
//*         (ESTABLISHED NOT TO DISPATCHED UNTIL NORMAL END OF JOB      00001300
//*         GAM505).                                                    00001400
//*====================================================                 00001500
//*  PURPOSE:                                                           00001600
//*          SUMMARIZE THE CFS REVENUE ACCOUNTS (CFS RECORD TYPE 20)    00001700
//*          EXTRACT FILE INTO A SINGLE RECORD FOR EACH CFS ACCOUNT/FUND00001800
//*          COMBINATION.                                               00001900
//*====================================================                 00002000
//*  RUN DSDEL PROGRAM                - DELETE PRIOR CATALOGED SETS     00002100
//*====================================================                 00002200
//DSDEL010 EXEC DSDEL,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),               00002300
//             PARM.D='NONVSAM,FISP.GAM520.UGAX520.REVENUSU.P$FISYYMM'  00002400
/*                                                                      00002500
//*====================================================                 00002600
//*   RUN IFIS REVENUE ACCOUNTS       - CREATE THE SUMMARIZED CFS       00002700
//*  SUMMARIZATION FOR THE CORPORATE    REVENUE ACCOUNTS (CFS RECORD    00002800
//*      FINANCIAL SYSTEM (CFS)         TYPE 20)                        00002900
//*  INPUT FILES                      - CNTLCARD : CONTROL FILE         00003000
//*                                   - REVENUEX : REVENUE ACCT EXTRACT 00003100
//*  OUTPUT FILES                     - REVENUSU : SUMMAR REVENUE ACCT  00003200
//*                                   - CNTLRPT1 : INPUT CONTROL REPORT 00003300
//*                                   - CNTLRPT2 : OUTPUT CONTROL REPORT00003400
//*====================================================                 00003500
//CFSRE020 EXEC PGM=UGAX520,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),         00003600
//     PARM='ISISDICT'
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX520) PLAN(UGAX520)
/*
//SYSOUT    DD SYSOUT=*                                                 00004600
//SYSLST    DD SYSOUT=*                                                 00004700
//SYSUDUMP  DD SYSOUT=D,                                                00004800
//             DEST=LOCAL                                               00004900
//SYSIN     DD DUMMY                                                    00005000
//REVENUEX  DD DSN=FISP.GAM505.UGAX505.REVENUEX.P$FISYYMM,              00005100
//             DISP=SHR                                                 00005200
//REVENUSU  DD DSN=FISP.GAM520.UGAX520.REVENUSU.P$FISYYMM,              00005300
//             DISP=(NEW,CATLG,DELETE),                                 00005400
//             DCB=(LRECL=120,BLKSIZE=23400,RECFM=FB),                  00005500
//             SPACE=(TRK,(15,75),RLSE),                                00005600
//             UNIT=SYSALLDA,                                           00005700
//             VOL=SER=SYSDA4                                           00005800
//CNTLRPT1  DD SYSOUT=*,                                                00005900
//             DCB=(LRECL=133,BLKSIZE=23408,RECFM=FBA)                  00006000
//CNTLRPT2  DD SYSOUT=*,                                                00006100
//             DCB=(LRECL=133,BLKSIZE=23408,RECFM=FBA)                  00006200
//CNTLCARD  DD *                                                        00006300
*UNIVERSITY CODE $FISUNVRSCODE                                          00762000
*COA CODE $FISCOACODE                                                   00763000
*FISCAL YEAR $FISFSCLYR                                                 00764000
*ACCOUNTING PERIOD $FISACTGPRD                                          00765000
*DESCRIPTION $FISDESC
/*                                                                      00006400
//*============================================================         00006500
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00006600
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00006700
//*============================================================         00006800
//STOPZEKE EXEC STOPZEKE                                                00006900
//*                                                                     00007000
//*================ E N D  O F  J C L  GAM520   ========                00007100
