//GAMP560  JOB (SYS000),'ACCTG-LEDGER',                                 00000100
//            MSGLEVEL=(1,1),                                           00000200
//            MSGCLASS=F,                                               00000300
//            CLASS=K,                                                  00040002
//            NOTIFY=ACLPLO,                                            00050002
//            REGION=20M                                                00060002
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # 2931    ** LEDGER/MONTHEND **                       00000800
//*++++++++++++++++++++++++++                                           00000900
//* THIS JOB IS TO CREATE THREE EDIT REPORTS FOR UCOP FILES.    *       00001200
//* THE MAJOR REPORT - COMPARES THE CROSS CODE TABLES AGAINST   *       00001200
//*                    ACCOUNT PERM AND FUND PERM               *       00001200
//* ACCOUNT PERM ERROR REPORT                                   *       00001200
//* FUND PERM ERROR REPORT                                      *       00001200
//***************************************************************       00001200
//*              DELETE CATALOGED DATA SETS                             00001500
//*==================================================================== 00001600
//*                                                                     00001700
//DELEXT1  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00001800
//SYSPRINT  DD SYSOUT=*                                                 00001900
//SYSIN     DD *                                                        00002000
  DELETE    FISP.UGAP301.ACCT.SORTED
  DELETE    FISP.UGAP301.FUND.SORTED
  DELETE    FISP.UGAP302A.MAJERR
  DELETE    FISP.UGAP303.ACCTERR
  DELETE    FISP.UGAP303.FNDERR
  DELETE    FISP.UGAP303.CNTLRPT
  DELETE    FISP.UGAP302A.CNTLRPT
//********************************************************************  00060002
//* SORT THE MONTHLY FILE FROM CROSS CODE TABLE(XTT)                    00060002
//********************************************************************  00060002
//*                                                                     00060002
//SORT010  EXEC PGM=SORT                                                00014314
//SORTIN    DD DSN=FISP.GAZ100E.UCOPFILE.D$MDATE,DISP=SHR               00003800
//SORTOUT   DD DSN=FISP.UGAP301.ACCT.SORTED,                            00003800
//             DISP=(NEW,CATLG,DELETE),                                 00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=27930),                  00009800
//             AVGREC=U,SPACE=(CYL,(100,50),RLSE),
//             STORCLAS=APCBIG,
//*OLD         SPACE=(CYL,(05,05),RLSE),                                00009900
//             UNIT=SYSDA                                               00010000
//SYSIN     DD *                                                        00015215
 SORT FIELDS=(96,6,CH,A,89,1,CH,A,106,5,CH,A,22,50,CH,A)                00041100
/*
//SORTWK01  DD SPACE=(CYL,(25,2),RLSE),                                 00015414
//             UNIT=SYSDA                                               00015514
//SORTWK02  DD SPACE=(CYL,(25,2),RLSE),                                 00015614
//             UNIT=SYSDA                                               00015714
//SORTWK03  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK04  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK05  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK06  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK07  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK08  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK09  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK10  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SYSOUT    DD SYSOUT=*                                                 00016014
//*
//********************************************************************  00060002
//* SORT THE MONTHLY FILE FROM CROSS CODE FILE(XTT) FOR FUND            00060002
//********************************************************************  00060002
//*
//SORT020  EXEC PGM=SORT                                                00014314
//SORTIN    DD DSN=FISP.GAZ100E.UCOPFILE.D$MDATE,DISP=SHR               00003800
//SORTOUT   DD DSN=FISP.UGAP301.FUND.SORTED,                            00003800
//             DISP=(NEW,CATLG,DELETE),                                 00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=27930),                  00009800
//             AVGREC=U,SPACE=(CYL,(100,50),RLSE),
//             STORCLAS=APCBIG,
//*OLD         SPACE=(CYL,(05,05),RLSE),                                00009900
//             UNIT=SYSDA                                               00010000
//SYSIN     DD *                                                        00015215
 SORT FIELDS=(106,5,CH,A,89,1,CH,A,96,6,CH,A,22,50,CH,A)                00041100
/*
//SORTWK01  DD SPACE=(CYL,(25,2),RLSE),                                 00015414
//             UNIT=SYSDA                                               00015514
//SORTWK02  DD SPACE=(CYL,(25,2),RLSE),                                 00015614
//             UNIT=SYSDA                                               00015714
//SORTWK03  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK04  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK05  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK06  DD SPACE=(CYL,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SYSOUT    DD SYSOUT=*                                                 00016014
//*********************************************************************
//* EDIT TO MAKE SURE ALL NEW ACCOUNT AND FUND CODE ARE IN
//* CROSS CODE TABLE ARE IN UCA AND UCF TABLES.
//*********************************************************************
//*CONTROL CARD: IF PARM CARD MISSING, IT WILL DEFAULT TO           ***
//*THE CURRENT FISCAL YEAR AND PROCESS DAILY                        ***
//*********************************************************************
//UGAP302A EXEC PGM=UGAP302A,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00220002
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAP302A) PLAN(UGAP302A)
/*
//SYSOUT    DD SYSOUT=*                                                 00320002
//SYSLST    DD SYSOUT=*                                                 00330002
//ERRRPT    DD SYSOUT=*                                                 00450002
//SYSUDUMP  DD SYSOUT=D,                                                00340002
//             DEST=LOCAL                                               00350002
//SYSIN     DD DUMMY                                                    00360002
//UGAP301A  DD DSN=FISP.UGAP301.ACCT.SORTED,                            00450002
//             DISP=SHR                                                 00009700
//UGAP301F  DD DSN=FISP.UGAP301.FUND.SORTED,                            00450002
//             DISP=SHR                                                 00009700
//MAJERR    DD DSN=FISP.UGAP302A.MAJERR,                                00470002
//             DISP=(NEW,CATLG,CATLG),                                  00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009800
//             SPACE=(TRK,(175,150),RLSE),                              00009900
//             UNIT=SYSDA                                               00010000
//CNTLRPT   DD DSN=FISP.UGAP302A.CNTLRPT,                               00490002
//             DISP=(NEW,CATLG,CATLG),                                  00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009800
//             SPACE=(TRK,(175,150),RLSE),                              00009900
//             UNIT=SYSDA                                               00010000
//CNTLCARD  DD *                                                        00510002
*FISCAL YEAR $FISFSCLYR
*MONTH END                                                              00810002
/*
//*  THE  CARD VARIATIONS:
//*
//*DAILY
//*YEAR END
//*********************************************************************
//* EDIT TO MAKE SURE ALL NEW ACCOUNT AND FUND CODE ARE IN
//* CROSS CODE TABLE ARE IN UCA AND UCF TABLES.
//*
//* CNTLCARD : FISCAL YEAR WILL NOT DEFAULT
//*            IF BYPASS CARD: DEFAULT IS NO
//*            HOWEVER IF BYPASS FOR CFDA = 'NO', THE FNDERR REPORT
//*            WILL BE BIG
//*********************************************************************
//UGAP303  EXEC PGM=UGAP303,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00220002
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAP303) PLAN(UGAP303)
/*
//SYSOUT    DD SYSOUT=*                                                 00320002
//SYSLST    DD SYSOUT=*                                                 00330002
//ERRRPT    DD SYSOUT=*                                                 00450002
//SYSUDUMP  DD SYSOUT=D,                                                00340002
//             DEST=LOCAL                                               00350002
//SYSIN     DD DUMMY                                                    00360002
//ACTERR    DD DSN=FISP.UGAP303.ACCTERR,                                00470002
//             DISP=(NEW,CATLG,CATLG),                                  00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009800
//             SPACE=(TRK,(175,150),RLSE),                              00009900
//             UNIT=SYSDA                                               00010000
//FNDERR    DD DSN=FISP.UGAP303.FNDERR,                                 00470002
//             DISP=(NEW,CATLG,CATLG),                                  00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009800
//             SPACE=(TRK,(175,150),RLSE),                              00009900
//             UNIT=SYSDA                                               00010000
//CNTLRPT   DD DSN=FISP.UGAP303.CNTLRPT,                                00490002
//             DISP=(NEW,CATLG,CATLG),                                  00009700
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00009800
//             SPACE=(TRK,(175,150),RLSE),                              00009900
//             UNIT=SYSDA                                               00010000
//CNTLCARD  DD *                                                        00510002
*FISCAL YEAR $FISFSCLYR
*BYPASS CFDA = YES                                                      00810002
*BYPASS IDCB = YES                                                      00810002
*MONTH END                                                              00810002
/*
//****************************************************************
//* THIS PRODUCES THE PDFMAIL
//****************************************************************
//PDFMAIL1    EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//STEPLIB     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.LOAD
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.LOAD
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//TXT1        DD   DISP=SHR,DSN=FISP.UGAP302A.CNTLRPT
//            DD   DISP=SHR,DSN=FISP.UGAP302A.MAJERR
//TXT2        DD   DISP=SHR,DSN=FISP.UGAP303.ACCTERR
//TXT3        DD   DISP=SHR,DSN=FISP.UGAP303.FNDERR
//TXT4        DD   DISP=SHR,DSN=FISP.UGAP303.CNTLRPT
//PDF1        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//PDF2        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//PDF3        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//PDF4        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//SYSPRINT    DD   SYSOUT=X
//SYSTSPRT    DD   SYSOUT=X
//SYSTSIN     DD   *
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT1                                                      +
     OUT DD:PDF1                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT2                                                      +
     OUT DD:PDF2                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT3                                                      +
     OUT DD:PDF3                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT4                                                      +
     OUT DD:PDF4                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/

%XMITIP * +
  SUBJECT 'REPORT# - CAFPT.EDIT.ERROR.RPT          '                  +
  FROM ACT-PRODCONTROL@UCSD.EDU                                       -
  ADDRESSFILEDD ADDRLIST                                              -
  FILEDD (  PDF1      PDF2      PDF3      PDF4         )              -
  FILENAME (FILE1.PDF FILE2.PDF FILE3.PDF FILE4.PDF)                  +
  MSGT 'REPORT RUN ON: &DAY &DATE &TIME, BY JOB: &JOB                 +
       \\\                                                            +
       \THIS REPORT IS VIEWABLE WITH ACROBAT READER.                  +
       \THE LATEST VERSION CAN BE DOWNLOADED FROM:                    +
       HTTP://WWW.ADOBE.COM/PRODINDEX/ACROBAT/READSTEP.HTML#READER +  +
       \\'
/*
//ADDRLIST    DD   *
TO  IFISHELPDESK@UCSD.EDU
CC  MMCGILL@UCSD.EDU
CC  JLORANCA@UCSD.EDU
CC  DMESERVE@UCSD.EDU
CC  NCOWELL@UCSD.EDU
CC  DCHOCK@UCSD.EDU
/*
//*
//*================ E N D  O F  J C L  GAM560   ========                00600002
