//GAMP600  JOB (SYS000),'ACCTG-LEDGER',                                 00000100
//         CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),                           00000200
//*        RESTART=SORT30,                                              00000300
//         NOTIFY=APCDBM,REGION=20M                                     00000400
//*========================
//*    ZEKE EVENT # 1822   -- LOC REDISTRIBUTION EXTRACT - MONTHLY
//*========================    (MUST EDIT JCL!!HARDCODED)
// JCLLIB ORDER=APCP.PROCLIB
//PRNT021  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=G,FORMS=A011             00001400
//PRNT022  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=4,FORMS=A011             00001500
//PRNT023  OUTPUT COPIES=1,GROUPID=PRDCTL,CLASS=X,FORMS=A011            00001600
//PRNT031  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=X,FORMS=A011             00001700
//PRNT032  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=X,FORMS=A011             00001800
//PRNT033  OUTPUT COPIES=1,GROUPID=PRDCTL,CLASS=X,FORMS=A011            00001900
//PRNT041  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=X,FORMS=A011             00002000
//PRNT042  OUTPUT COPIES=1,GROUPID=ACCTG,CLASS=X,FORMS=A011             00002100
//PRNT043  OUTPUT COPIES=1,GROUPID=PRDCTL,CLASS=X,FORMS=A011            00002200
//*
//* (PRNT022 CHANGED FROM 'X' TO '4' FOR PRINT REDUCTION PROJECT ONLY)
//*====================================================                 00000600
//CLEAN10  EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00000700
//DD1       DD DSN=FISP.GAM600.UGAX625.LOCDIST.P$FISYYMM,               00000800
//             DISP=(MOD,DELETE,DELETE),                                00000900
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00001000
//             SPACE=(TRK,(15,150),RLSE),                               00001100
//             UNIT=SYSDA                                               00001200
//*====================================================                 00001300
//TRANS20  EXEC PGM=UGAX625,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00001400
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX625) PLAN(UGAX625)
/*
//SYSOUT    DD SYSOUT=*                                                 00002300
//SYSLST    DD SYSOUT=*                                                 00002400
//SYSUDUMP  DD SYSOUT=D                                                 00002500
//SYSDBOUT  DD SYSOUT=*                                                 00002600
//REPORT01  DD SYSOUT=*,                                                00002700
//             DCB=BLKSIZE=133                                          00002800
//JVFILE01  DD DSN=FISP.GAM600.UGAX625.LOCDIST.P$FISYYMM,               00002900
//             DISP=(NEW,CATLG,CATLG),                                  00003000
//             DCB=(RECFM=FB,LRECL=250,BLKSIZE=23250),                  00003100
//             SPACE=(TRK,(15,150),RLSE),                               00003200
//             UNIT=SYSDA                                               00003300
//PARM02    DD DUMMY                                                    00003400
//PARM01    DD *                                                        00003500
*UNIVERSITY CODE $FISUNVRSCODE
*COA CODE $FISCOACODE
*FISCAL YEAR $FISFSCLYR
*LOC DOCUMENT FELC0001/LOC REDISTRIBUTION
*SUBSYSTEM-ID SYSTEM01
*ACCOUNTING PERIOD $FISACTGPRD                                          00030000
/*                                                                      00003600
//*DURING MONTHLY PROCESS PROGRAM WILL RECOGNIZE ACCOUNTING PERIOD      00003700
//*DURING FISCAL PROCESS PROGRAM IGNORES ACCOUNING PERIOD AND           00003700
//*RECOGNIZES SUBSYSTEM ID (MUST KEEP ACCOUNTING PERIOD)                00003700
//*====================================================                 00003700
//* L#55 VALUE = FELC0001 (VALUE REMAINS CONSTANT)
//*====================================================                 00003700
//SORT30   EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00003800
//SYSOUT    DD SYSOUT=*                                                 00003900
//SYSPRINT  DD SYSOUT=*                                                 00004000
//SORTIN    DD DSN=FISP.GAM600.UGAX625.LOCDIST.P$FISYYMM,               00004100
//             DISP=SHR                                                 00004200
//SORTOUT   DD DSN=&&UGAUA00I,                                          00004300
//             DISP=(NEW,PASS,DELETE),                                  00004400
//             DCB=(RECFM=FB,BLKSIZE=23250,LRECL=250),                  00004500
//             SPACE=(TRK,(15,150),RLSE),                               00004600
//             UNIT=VIO                                                 00004700
//SYSIN     DD *                                                        00004800
           SORT FIELDS=(1,19,CH,A,20,4,ZD,A)                            00041100
/*                                                                      00004900
//SORTWK01  DD SPACE=(TRK,300),                                         00005000
//             UNIT=SYSDA                                               00005100
//SORTWK02  DD SPACE=(TRK,300),                                         00005200
//             UNIT=SYSDA                                               00005300
/*                                                                      00005400
//*====================================================                 00005500
//PRINT40  EXEC PGM=FGAP020D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00005600
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SYSOUT    DD SYSOUT=*                                                 00006300
//UGAUA00I  DD DSN=&&UGAUA00I,                                          00006400
//             DISP=(OLD,DELETE)                                        00006500
//GA019R1   DD SYSOUT=(Z,,L010)                                         00006600
//GA019R2   DD SYSOUT=(Z,,L010)                                         00006700
//*GA019R1   DD SYSOUT=(P,,L010)                                        00006600
//*GA019R2   DD SYSOUT=(P,,L010)                                        00006700
//*====================================================                 00002300
//*                     FISPLD                                          00002400
//*  SECOND PHASE OF THIS PROCESS WILL LOAD THE DOCUMENT
//*  TO IFIS (POSTJALL WILL PICK UP POSTING OF DOCUMENT)
//*====================================================                 00002836
//GAD001P  EXEC GAD001P,                                                00002925
//  CNTLOUT='*,OUTPUT=(*.PRNT021,*.PRNT022,*.PRNT023)',    USER SYOUT   00003125
//  DCMNTOUT='*,OUTPUT=(*.PRNT031,*.PRNT032,*.PRNT033)',   USER SYOUT   00003125
//  TRANSOUT='*,OUTPUT=(*.PRNT041,*.PRNT042,*.PRNT043)',   USER SYOUT   00003125
//  OUTX='*',                                             SYSTEM SYSOUT 00003025
//  COPIES=1,                                             # OF COPIES   00003200
//  GROUPID='FISP',                                       TEST OR PROD  00003300
//  JOBID='GAMP600',                                      JOB NAME
//  JVOUCHER='FISP.GAM600.UGAX625.LOCDIST.P$FISYYMM',     INPUT DS
//  DB2REGN='DB2F'                                        DB2 DATABASE
//*
//*============================================================         00006900
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00007000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00007100
//*============================================================         00007200
//STOPZEKE EXEC STOPZEKE                                                00007300
//*                                                                     00007400
//*================ E N D  O F  J C L  GAM600   ========                00007500
