//GAM905C   JOB (SYS000),'BEV-PRDCTL',NOTIFY=APCDBM,                    00001000
//           CLASS=K,MSGLEVEL=(1,1),MSGCLASS=F,REGION=20M               00002003
//*++++++++++++++++++++++++++                                           00003000
//*    ZEKE EVENT # 3428   JANUARY ONLY                                 00004000
//*                        FEB-->DEC -RUN GAM905C
//*                        (DEFINES TAPE CREATION IN FIRST JOB OF
//*                        NEW YEAR     - (NEW,KEEP)
//*                        REST OF YEAR - (MOD,KEEP)
//*++++++++++++++++++++++++++                                           00005000
//*-------------------------------------------                          00006000
//* ARCHIVE ENCUMBRANCE PURGE  EXTRACT FOR 25 YEARS                     00006101
//*-------------------------------------------                          00006200
//**********************************************************************00006300
//ARCHIVE  EXEC PGM=FDRABR                                              00006403
//**********************************************************************00006500
//SYSPRINT  DD  SYSOUT=*                                                00006600
//SYSUDUMP  DD  SYSOUT=A,HOLD=YES                                       00006700
//ARCHIVE   DD  DSN=FDRABR.ARCHIVE,DISP=SHR                             00006800
//SYSPRIN1  DD  SYSOUT=*                                                00006900
//*                                                                     00007000
//TAPE1     DD  DSN=FDRABR.LASTAPE.GAMT1,DISP=(NEW,KEEP),               00008000
//          UNIT=VTAPE,LABEL=RETPD=8784                                 00009001
//*         UNIT=VTAPE,LABEL=RETPD=8784                                 00009001
//*         UNIT=CTLIB,LABEL=RETPD=8784                                 00009001
//*         UNIT=CTAPE,LABEL=RETPD=9125                                 00009001
//DISK1     DD  UNIT=3390,VOL=SER=APC006,DISP=SHR                       00010000
//DISK2     DD  UNIT=3390,VOL=SER=APC007,DISP=SHR                       00020000
//DISK3     DD  UNIT=3390,VOL=SER=APC005,DISP=SHR                       00030000
//ABRMAP    DD  SYSOUT=*                                                00040000
//*                                                                     00050000
//SYSIN     DD  *                                                       00060000
  DUMP      TYPE=ARC,BUFNO=MAX,EXPD=NONE,VEXPD=NONE,RECALL,             00070002
            ARCBACKUP=NO,
            PRINT=ABR,SCRATCH=NO,SELTERR=NO,ONLVOL                      00080000
  SELECT    DSN=FISP.UGAU905.EXTRACT.D$BJDT,VOLG=APC                    00090000
//*                                                                     00110000
//*============================================================         00110000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00110000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00110000
//*============================================================         00110000
//STOPZEKE EXEC STOPZEKE                                                00110000
//*                                                                     00110000
//*================ E N D  O F  J C L  GAM905C  ========                00250007
