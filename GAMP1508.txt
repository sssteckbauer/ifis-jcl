//GAMP1508 JOB (SYS000),'M MCGILL',                                     00000100
//        MSGLEVEL=(1,1),                                               00000200
//        MSGCLASS=F,                                                   00000300
//        CLASS=K,                                                      00000400
//        REGION=0M,                                                    00000500
//        NOTIFY=APCDBM                                                 00000700
/*JOBPARM LINES=500
//*=====================================================================00001800
//*  ZEKE EVENT # 3894 ...TO RUN AFTER LEDGER CLOSES                  * 00001900
//*  PURPOSE:                                                         * 00001900
//*          EXTRACT RECORDS FROM THE IFIS DETAIL TRANSACTIONS AND    * 00002000
//*          CREATE FILE CONTAINING DBKEY AND CALC KEYS OF THE        * 00002100
//*          R4187-G-FOAPAL RECORDS THAT WILL BE PURGED FOR FY1508    * 00002200
//*          FISCAL YEAR 2015 / ACCOUNTING PERIOD 08                  * 00002200
//*=====================================================================00002300
//*=====================================================================00002300
//*  IFIS DETAIL TRANSACTION PURGE    - CREATE EXTRACT FILE CONTAINING  00003100
//*       KEY EXTRACT                   DB AND CALC KEYS OF THE         00003200
//*                                     R4187-G-FOAPAL.                 00003300
//*  INPUT FILE                       - CNTLCARD : CONTROL FILE         00003400
//*  OUTPUT FILES                     - CNTLRPT1 : CONTROL REPORT       00003500
//*                                   - EXTRFILE : EXTRACT FILE         00003600
//*=====================================================================00003700
//EXTRT030 EXEC PGM=UGAX900,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),         00003800
//     PARM='ISISDICT'
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX900) PLAN(UGAX900)
/*
//SYSOUT    DD SYSOUT=*                                                 00004100
//SYSLST    DD SYSOUT=*                                                 00004200
//SYSUDUMP  DD SYSOUT=D                                                 00004300
//SYSIN     DD DUMMY                                                    00004400
//EXTRFILE  DD DSN=FISP.GAMP1508.PURGE.RECORDS.SELECTED.D010618,        00004700
//             DISP=(NEW,CATLG,DELETE),MGMTCLAS=AS,                     00004800
//             DCB=(LRECL=58,BLKSIZE=23432,BUFNO=8,RECFM=FB),           00004900
//             SPACE=(TRK,(15,150),RLSE),                               00005000
//             UNIT=SYSDA                                               00005100
//CNTLRPT1  DD SYSOUT=*,                                                00005300
//             DCB=(LRECL=132,BLKSIZE=23364,BUFNO=8,RECFM=FBA)          00005400
//CNTLCARD  DD *                                                        00005500
*UNIVERSITY CODE 01
*COA CODE A
*FISCAL YEAR 15
*ACCOUNTING PERIOD 08
/*
//*
//*=====================================================================00005700
//*  RUN SYNCSORT PROGRAM - SORT EXTRACT FILE ON THE DB KEY           * 00005800
//*=====================================================================00005900
//SOEXT040 EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00006000
//SYSOUT    DD SYSOUT=*                                                 00006100
//SORTIN    DD DSN=FISP.GAMP1508.PURGE.RECORDS.SELECTED.D010618,        00006200
//             DISP=(OLD,KEEP)                                          00006300
//SORTOUT   DD DSN=FISP.GAMP1508.RECORDS.BEING.PURGED.D010618,          00006400
//             DISP=(NEW,CATLG,DELETE),MGMTCLAS=AS,                     00006500
//             DCB=(LRECL=58,BLKSIZE=23432,BUFNO=8,RECFM=FB),           00006600
//             SPACE=(TRK,(15,150),RLSE),                               00006700
//             UNIT=SYSDA                                               00006800
//*            VOL=SER=APC005                                           00006900
//SORTWK01  DD SPACE=(TRK,150),                                         00007000
//             UNIT=SYSDA                                               00007100
//SORTWK02  DD SPACE=(TRK,150),                                         00007200
//             UNIT=SYSDA                                               00007300
//SORTWK03  DD SPACE=(TRK,150),                                         00007400
//             UNIT=SYSDA                                               00007500
//SORTWK04  DD SPACE=(TRK,150),                                         00007600
//             UNIT=SYSDA                                               00007700
//SORTWK05  DD SPACE=(TRK,150),                                         00007800
//             UNIT=SYSDA                                               00007900
//SORTWK06  DD SPACE=(TRK,150),                                         00008000
//             UNIT=SYSDA                                               00008100
//SORTWK07  DD SPACE=(TRK,150),                                         00008200
//             UNIT=SYSDA                                               00008300
//SORTWK08  DD SPACE=(TRK,150),                                         00008400
//             UNIT=SYSDA                                               00008500
//SYSIN     DD *                                                        00008600
              SORT FIELDS=(1,4,CH,D)
/*                                                                      00008700
//*=====================================================================00008800
//*  IFIS DETAIL TRANSACTION PURGE   - RETRIEVES THE R4187-G-FOAPAL     00008900
//*          PROCESS                   RECORD USING CALC FROM THE       00009000
//*                                    EXTRACT FILE AND ERASE ALL       00009100
//*                                    ASSOCIATED RECORDS -             00009200
//*                                    R4188-TRAN-HIST, R4189-TRAN-ACTV.00009300
//*  INPUT FILE                      - CNTLCARD : CONTROL FILE          00009400
//*                                  - EXTRFILE : EXTRACT FILE          00009500
//*  OUTPUT FILES                    - CNTLRPT1 : CONTROL REPORT        00009600
//*                                  - EXTRPURG : EXTR.PURGE DATA FILE  00009700
//*=====================================================================00009800
//PURGE050 EXEC PGM=UGAU900,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),         00009900
//     PARM='ISISDICT'
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAU900) PLAN(UGAU900)
/*
//SYSOUT    DD SYSOUT=*                                                 00010100
//SYSLST    DD SYSOUT=*                                                 00010200
//SYSUDUMP  DD SYSOUT=D                                                 00010300
//*ABNLIGNR  DD DUMMY                                                   00010400
//* VSAM FILE OF IFIS EXTERNAL REPORT CODES                             00010700
//UUTC20V1  DD DSN=FISP.UUTC20V1,                                       00010800
//             DISP=SHR,                                                00010900
//             AMP=('BUFNI=2')                                          00011000
//* VSAM FILE OF IFIS FOAPAL/RULE DATA                                  00011100
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00011200
//             DISP=SHR,                                                00011300
//             AMP=('BUFNI=2')                                          00011400
//EXTRFILE  DD DSN=FISP.GAMP1508.RECORDS.BEING.PURGED.D010618,          00011500
//             DISP=SHR                                                 00011600
//EXTRPURG  DD DUMMY                                                    00011700
//CNTLRPT1  DD SYSOUT=*                                                 00011800
//CNTLCARD  DD *                                                        00011900
*ELAPSE TIME LIMIT 225
*COMMIT COUNTER 0040
/*                                                                      00012000
//*COMMIT COUNTER 0040
//*ELAPSE TIME LIMIT 240
//*ELAPSE TIME LIMIT 420
//*
//*================ E N D  O F  J C L  GAMP1508 ========                00014400
