//GAWP125 JOB (SYS000),'BUDGET - 355 TPCS',MSGCLASS=F,MSGLEVEL=(1,1),   00000100
//*       RESTART=IDCAM40,
//*       RESTART=STEP04,
//*       RESTART=COPYINKP,
//*       RESTART=BACK9000,
//*       RESTART=BACK3100,
//        NOTIFY=APCDBM,CLASS=K,REGION=20M                              00000200
//*++++++++++++++++++++++++++                                           00000300
//*  NOTE:  THIS JOB COMBINES GAWP125 AND BSL01TOF
//*         job may fail in step format60 if file is empty
//*         see notes in step...if so delete BSL02 ev 1805
//*======================================================
//*  07/13/14 IF A RE-RUN IS REQUESTED FOR THIS JOB ON THE
//*           SAME DAY, THE FISP.BSL*.SD.FILE.xx datasets must
//*           be renamed to prevent job failure
//*+++++++++++++++++++++++++++++++++++++++++++++++++++++                00000300
//*    ZEKE EVENT # 2059                                                00000400
//*++++++++++++++++++++++++++                                           00000500
//OUT1 OUTPUT CLASS=X,FORMS=BS02,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET1  00000600
//OUT2 OUTPUT CLASS=X,FORMS=BS02,GROUPID=BUDGET2,COPIES=1               00000700
//OUT3 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BACKUP,COPIES=1                00000800
//*====================================================                 00000900
//*               TRANSFER OF FUNDS EXTRACT                             00001000
//*
//* VALUES CHANGED IN "APCXXX.JCL.CNTL(SETBSL)"                         00001100
//* CHANGE TARGET: ACCTG PERIOD = 12 (Jun)      $FISACTGPRDBSL          00001200
//* CHANGE TARGET: ACCTG PERIOD = 01 (Jul)      $FISACTGPRDBSL          00001200
//* CHANGE TARGET: FISCAL YEAR  = 20 (YY)       $FISFSCLYR              00001300
//*====================================================                 00001400
//CLEAN00 EXEC PGM=IEFBR14                                              00001500
//SYSPRINT DD SYSOUT=Z                                                  00001600
//DD1      DD DSN=FISP.GAW125.UGAX125A.EXTRACT1.D$MDATE,                00001700
//            DISP=(MOD,DELETE,DELETE),MGMTCLAS=MY,                     00001800
//            DCB=(RECFM=FB,LRECL=115,BLKSIZE=11500),                   00001900
//            SPACE=(TRK,(15,75),RLSE),                                 00002000
//            UNIT=SYSDA                                                00002100
//DD2      DD DSN=FISP.GAW125.UGAX125A.CR.D$MDATE,                      00002200
//            DISP=(MOD,DELETE,DELETE),MGMTCLAS=MY,                     00002300
//            DCB=(RECFM=FB,BLKSIZE=23408,LRECL=133),                   00002400
//            SPACE=(TRK,(15,15),RLSE),                                 00002500
//            UNIT=SYSDA                                                00002600
//DD3      DD DSN=FISP.GAW125.UGAX125B.CR.D$MDATE,                      00002700
//            DISP=(MOD,DELETE,DELETE),MGMTCLAS=MY,                     00002800
//            DCB=(RECFM=FB,BLKSIZE=23408,LRECL=133),                   00002900
//            SPACE=(TRK,(15,15),RLSE),                                 00003000
//            UNIT=SYSDA                                                00003100
//DD4      DD DSN=FISP.GAW125.UGAX125A.UPD.D$MDATE,                     00003200
//            DISP=(MOD,DELETE,DELETE),MGMTCLAS=MY,                     00003300
//            DCB=(RECFM=FB,BLKSIZE=800,LRECL=80),                      00003400
//            SPACE=(TRK,(15,15),RLSE),                                 00003500
//            UNIT=SYSDA                                                00003600
//REPORT01 DD DSN=FISP.GAW125.UGAX125A.REPORT02.D$MDATE,                00003700
//            DISP=(MOD,DELETE,DELETE),MGMTCLAS=MY,                     00003800
//            DCB=(RECFM=FB,BLKSIZE=23408,LRECL=133),                   00003900
//            SPACE=(TRK,(15,15),RLSE),                                 00004000
//            UNIT=SYSDA                                                00004100
//DD5       DD DSN=FISP.ACTI.BUDGET.ACTIVE.EXTRACT,                     00400003
//             DISP=(MOD,DELETE,DELETE)                                 00140003
//DD6       DD DSN=FISP.ACTI.BUDGET.ACTIVE.SORTED,                      00002900
//             DISP=(MOD,DELETE,DELETE)                                 00003000
//*====================================================                 00004200
//*       EXTRACT TRANSFER OF FUNDS DATA                                00004300
//*====================================================                 00004400
//STEP01  EXEC PGM=UGAX125A                                             00004500
//STEPLIB  DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//         DD DSN=DB2P.DCA.LOAD,DISP=SHR
//         DD DSN=DB2F.DSNEXIT,DISP=SHR
//         DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//SYSTSIN  DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX125A) PLAN(UGAX125A)
/*
//SYSOUT   DD SYSOUT=*                                                  00005300
//SYSLST   DD SYSOUT=*                                                  00005400
//SYSUDUMP DD SYSOUT=D                                                  00005500
//SYSDBOUT DD SYSOUT=*                                                  00005600
//PARM01   DD *                                                         00005700
*UNIVERSITY CODE 01
*COA CODE A
*FISCAL YEAR $FISFSCLYR
*ACCOUNTING PERIOD $FISACTGPRDBSL
/*                                                                      0000
//*DISPLAY VALUES:
//*UNIVERSITY CODE 01                                                   00005900
//*COA CODE A                                                           00006000
//*FISCAL YEAR $FISFSCLYR                                               00006100
//*ACCOUNTING PERIOD $FISACTGPRDBSL                                     00006200
//*                                                                     00006300
//EXTRACT1 DD DSN=FISP.GAW125.UGAX125A.EXTRACT1.D$MDATE,                00006400
//            DISP=(NEW,CATLG,KEEP),                                    00006500
//            DCB=(RECFM=FB,LRECL=115,BLKSIZE=11500),                   00006600
//            SPACE=(TRK,(15,75),RLSE),                                 00006700
//            UNIT=SYSDA                                                00006800
//UPDATE01 DD DSN=FISP.GAW125.UGAX125A.UPD.D$MDATE,                     00006900
//            DISP=(NEW,CATLG,KEEP),                                    00007000
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=800),                      00007100
//            SPACE=(TRK,(15,75),RLSE),                                 00007200
//            UNIT=SYSDA                                                00007300
//CONTROLR DD DSN=FISP.GAW125.UGAX125A.CR.D$MDATE,                      00007400
//            DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                        00007500
//            DCB=(RECFM=FB,BLKSIZE=23408,LRECL=133),                   00007600
//            SPACE=(TRK,(15,15),RLSE),                                 00007700
//            UNIT=SYSDA                                                00007800
//REPORT01 DD DSN=FISP.GAW125.UGAX125A.REPORT02.D$MDATE,                00007900
//            DISP=(NEW,CATLG,DELETE),MGMTCLAS=MY,                      00008000
//            DCB=(RECFM=FB,BLKSIZE=23408,LRECL=133),                   00008100
//            SPACE=(TRK,(30,30),RLSE),                                 00008200
//            UNIT=SYSDA                                                00008300
//*CONTROLR DD *                                                        00008400
//*REPORT01 DD *                                                        00008500
//*                                                                     00008600
//*====================================================                 00008700
//*       UPDATE  TRANSFER OF FUNDS DATA                                00011100
//*====================================================                 00011200
//STEP04  EXEC PGM=UGAX125B                                             00011300
//STEPLIB  DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//         DD DSN=DB2P.DCA.LOAD,DISP=SHR
//         DD DSN=DB2F.DSNEXIT,DISP=SHR
//         DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//SYSTSIN  DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAX125B) PLAN(UGAX125B)
/*
//SYSOUT   DD SYSOUT=*                                                  00012100
//SYSLST   DD SYSOUT=*                                                  00012200
//SYSUDUMP DD SYSOUT=D                                                  00012300
//SYSDBOUT DD SYSOUT=*                                                  00012400
//PARM01   DD *                                                         00012500
*COMMIT COUNTER 0025
/*                                                                      0001
//*                                                                     2700
//UPDATE01 DD DSN=FISP.GAW125.UGAX125A.UPD.D$MDATE,                     00012800
//            DISP=SHR                                                  00012900
//CONTROLR DD DSN=FISP.GAW125.UGAX125B.CR.D$MDATE,                      00013000
//            DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                        00013100
//            DCB=(RECFM=FB,BLKSIZE=23408,LRECL=133),                   00013200
//            SPACE=(TRK,(15,15),RLSE),                                 00013300
//            UNIT=SYSDA                                                00013400
//*                                                                     00013500
//*====================================================                 00015400
//*  !!NOTE:  IF ABEND OCCURS - MAY NEED TO RUN BSL02 TO RESOLVE
//*           MISSING FILES FROM BSL01 EDITS.
//*----------------------------------------------------                 00000900
//*  BUDGET TRANSFER-OF-FUNDS EDIT ( RUN AFTER GAWP125 )                00001000
//*----------------------------------------------------                 00001100
//*  CHANGE TARGET: 'TOFDATE' ( DATE ON DATASET CREATED IN GAWP125)     00001200
//*     "      "    'MDATE'   ( CURRENT RUNDATE )                       00001300
//*----------------------------------------------------                 00001400
//*                                                                     00190003
//*==================================================================== 00190003
//*   EXTRACT OF CURRENT INDEXES FOR ORG 660067 AND 660068              00200003
//*   WITH STATUS OF ACTIVE                                             00200003
//*==================================================================== 00210003
//*                                                                     00210003
//*==================================================================== 00001600
//*              DELETE CATALOGED DATA SETS                             00001500
//*==================================================================== 00001600
//*                                                                     00001700
//DELEXT1  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00001800
//SYSPRINT  DD SYSOUT=*                                                 00001900
//SYSIN     DD *                                                        00002000
  DELETE    FISP.ACTI.BUDGET.TRAN.LIST.RPT
//************************************************                      00001700
//XTRACT20 EXEC PGM=BSL010A                                             00220003
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(BSL010A) PLAN(BSL010A)
/*
//SYSOUT    DD SYSOUT=*                                                 00310003
//SYSUDUMP  DD SYSOUT=*                                                 00320003
//*                                                                     00390003
//EXTRACT   DD DSN=FISP.ACTI.BUDGET.ACTIVE.EXTRACT,                     00400003
//             DISP=(NEW,CATLG,DELETE),                                 00410003
//             DCB=(RECFM=FB,LRECL=200,BLKSIZE=27800),                  00420003
//             SPACE=(TRK,(15,300),RLSE),                               00430003
//             UNIT=SYSDA                                               00440003
//*                                                                     00440003
//********************************************************************* 00014214
//* SORT  FUND, ORG, START DATE AND ACTI-CODE                           00013614
//********************************************************************* 00014214
//*
//SORT030  EXEC PGM=SORT                                                00014314
//SORTIN    DD DSN=FISP.ACTI.BUDGET.ACTIVE.EXTRACT,                     00003800
//             DISP=SHR                                                 00014614
//SORTOUT   DD DSN=FISP.ACTI.BUDGET.ACTIVE.SORTED,                      00003800
//             DISP=(NEW,CATLG,DELETE),                                 00003900
//             DCB=(RECFM=FB,LRECL=200,BLKSIZE=20000),
//             SPACE=(CYL,(2,3),RLSE),                                  00440000
//             UNIT=SYSDA                                               00004200
//SYSIN     DD *                                                        00015215
       SORT FIELDS=(12,6,CH,A,18,6,CH,A,24,8,CH,A,32,10,CH,A)           00110000
/*
//SORTWK01  DD SPACE=(TRK,(25,2),RLSE),                                 00015414
//             UNIT=SYSDA                                               00015514
//SORTWK02  DD SPACE=(TRK,(25,2),RLSE),                                 00015614
//             UNIT=SYSDA                                               00015714
//SORTWK03  DD SPACE=(TRK,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SORTWK04  DD SPACE=(TRK,(25,2),RLSE),                                 00015814
//             UNIT=SYSDA                                               00015914
//SYSOUT    DD SYSOUT=*                                                 00016014
//*====================================================                 00001200
//*  RUN IDCAMS PROGRAM         - DELETE OLD VSAM FILE OF IFIS        * 00001300
//*                               EXTERNAL REPORT CODES               * 00001400
//* VSAM KEY - FUND, ORG, START DATE, AND ACTI-CODE                   * 00001400
//*====================================================                 00001500
//IDCAM40  EXEC PGM=IDCAMS                                              00001600
//SYSPRINT  DD SYSOUT=*                                                 00001700
//SYSIN     DD *                                                        00001800
           DELETE -
                  FISP.ACTI.BUDGET.ACTIVE.VSAM
           DEFINE -
           CLUSTER (NAME (FISP.ACTI.BUDGET.ACTIVE.VSAM) -
                  FREESPACE (0 0) -
                  CISZ (4096) -
                  INDEXED -
                  REUSE -
                  KEYS (30,11) -
                  RECORDSIZE (200,200) -
                  VOLUME (APC002)) -
           DATA -
                  (NAME(FISP.ACTI.BUDGET.ACTIVE.VSAM.DATA) -
                  TRACKS (8,2)) -
           INDEX -
                  (NAME(FISP.ACTI.BUDGET.ACTIVE.VSAM.INDEX1))
/*                                                                      00001900
//*=====================================================================00002000
//*   REPROCESS THE SORTED EXTRACT FILE TO VSAM FILE                    00002100
//*=====================================================================00002200
/*                                                                      00002300
//REPRO50  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00002400
//SYSPRINT  DD SYSOUT=*                                                 00002500
//SYSIN     DD *                                                        00002600
     REPRO -
       INDATASET (FISP.ACTI.BUDGET.ACTIVE.SORTED) -
       OUTDATASET (FISP.ACTI.BUDGET.ACTIVE.VSAM) -
       REUSE;
/*                                                                      00002700
//*====================================================
//*
//BACK9000 EXEC PGM=IEBGENER                                            00002000
//SYSUT1    DD DSN=FISP.BSL9000.SD.FILE,                                00002100
//             DISP=SHR                                                 00002200
//SYSUT2    DD DSN=FISP.BSL9000.SD.FILE.PREEDIT.TF$MDATE,               00002300
//             DISP=(NEW,KEEP),                                         00002400
//             DCB=(LRECL=80,BLKSIZE=80,RECFM=FB),                      00002500
//             SPACE=(TRK,(1,1),RLSE),                                  00002600
//             UNIT=SYSDA                                               00002700
//SYSIN     DD DUMMY                                                    00002800
//SYSPRINT  DD SYSOUT=*                                                 00002900
//*====================================================                 00003000
//BACK0100 EXEC PGM=IEBGENER                                            00003100
//SYSUT1    DD DSN=FISP.BSL0100.SD.FILE,                                00003200
//             DISP=SHR                                                 00003300
//SYSUT2    DD DSN=FISP.BSL0100.SD.FILE.PREEDIT.TF$MDATE,               00003400
//             DISP=(NEW,KEEP),                                         00003500
//             DCB=(LRECL=146,BLKSIZE=18980,RECFM=FB),                  00003600
//             SPACE=(TRK,(20,5),RLSE),                                 00003700
//             UNIT=SYSDA                                               00003800
//SYSIN     DD DUMMY                                                    00003900
//SYSPRINT  DD SYSOUT=*                                                 00004000
//*====================================================                 00004100
//BACK0101 EXEC PGM=IEBGENER                                            00004200
//SYSUT1    DD DSN=FISP.BSL0101.SD.FILE,                                00004300
//             DISP=SHR                                                 00004400
//SYSUT2    DD DSN=FISP.BSL0101.SD.FILE.PREEDIT.TF$MDATE,               00004500
//             DISP=(NEW,KEEP),                                         00004600
//             DCB=(LRECL=131,BLKSIZE=23449,RECFM=FB),                  00004700
//             SPACE=(TRK,(20,5),RLSE),                                 00004800
//             UNIT=SYSDA                                               00004900
//SYSIN     DD DUMMY                                                    00005000
//SYSPRINT  DD SYSOUT=*                                                 00005100
//*====================================================                 00005200
//BACK3100 EXEC PGM=IEBGENER                                            00005300
//SYSUT1    DD DSN=FISP.BSL3100.SD.FILE,                                00005400
//             DISP=SHR                                                 00005500
//SYSUT2    DD DSN=FISP.BSL3100.SD.FILE.BACKUP,                         00005600
//             DISP=(OLD,KEEP)                                          00005700
//SYSIN     DD DUMMY                                                    00005800
//SYSPRINT  DD SYSOUT=*                                                 00005900
//*====================================================                 00006000
//COPYINKP EXEC PGM=IEBGENER                                            00006100
//SYSUT1    DD DSN=FISP.GAW125.UGAX125A.EXTRACT1.D$MDATE,               00006300
//             DISP=(OLD,KEEP,KEEP)                                     00006400
//SYSUT2    DD DSN=&&BSL0001,                                           00006500
//             DISP=(MOD,PASS,DELETE)                                   00006600
//SYSIN     DD DUMMY                                                    00006700
//SYSPRINT  DD SYSOUT=*                                                 00006800
//*================================================================     00006000
//*  CREATE BACKUP COPY OF FILE FOR FISCAL CLOSING TROUBLE-SHOOTING
//*================================================================     00006000
//COPYEXTR EXEC PGM=IEBGENER                                            00006100
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSUT1    DD DSN=FISP.GAW125.UGAX125A.EXTRACT1.D$MDATE,               00006300
//             DISP=(OLD,KEEP,KEEP)                                     00006400
//SYSUT2    DD DSN=FISP.GAW125.EXTRACT1.D$MDATE..T$TIME,                00006500
//            DISP=(NEW,CATLG,KEEP),                                    00006500
//            DCB=(RECFM=FB,LRECL=115,BLKSIZE=11500),                   00006600
//            SPACE=(TRK,(15,75),RLSE),                                 00006700
//            UNIT=SYSDA                                                00006800
//SYSIN     DD DUMMY                                                    00006700
//*====================================================                 00006900
//*  IF FORMAT60 ABENDS WITH: "IFIS IFOAPAL TO AFP TRANSLATION ERROR"
//*  CHECK FILE: "FISP.GAW125.UGAX125A.EXTRACT1" TO SEE IF EMPTY.
//* IF EMPTY - STOP AND QUESTION BUDGET/STAFFING TO SEE IF THEY
//* FORGOT TO INPUT CHANGES-OR IF TO PROCEED TO ANOTHER JOB.
//* ok to skip job if confimed empty also delete BSL02 ev 1805
//*====================================================                 00006900
//FORMAT60 EXEC PGM=BSL002B,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00007000
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(BSL002B) PLAN(BSL002B)
/*
//CNTFILEI  DD DSN=FISP.BSL9000.SD.FILE,                                00008100
//             DISP=SHR                                                 00008200
//BDTBLIN   DD DSN=FISP.BSL3100.SD.FILE,                                00008300
//             DISP=SHR                                                 00008400
//BDTRNIN   DD DSN=&&BSL0001,                                           00008500
//             DISP=(OLD,DELETE)                                        00008600
//BDTRNOUT  DD DSN=&&BSL0021,                                           00008700
//             DISP=(NEW,PASS,DELETE),                                  00008800
//             DCB=(LRECL=131,BLKSIZE=23449,RECFM=FB),                  00008900
//             SPACE=(TRK,150),                                         00009000
//             UNIT=VIO                                                 00009100
//UUTC20V1  DD DSN=FISP.UUTC20V1,                                       00009200
//             DISP=SHR,                                                00009300
//             AMP=('BUFNI=2')                                          00009400
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00009500
//             DISP=SHR,                                                00009600
//             AMP=('BUFNI=2')                                          00009700
//SYSOUT    DD SYSOUT=*                                                 00009800
//SYSUDUMP  DD SYSOUT=D,                                                00009900
//             DEST=LOCAL                                               00010000
//SYSDBOUT  DD SYSOUT=*                                                 00010100
//*====================================================                 00010200
//SORT70   EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00010300
//SORTIN    DD DSN=&&BSL0021,                                           00010400
//             DISP=(OLD,DELETE)                                        00010500
//          DD DSN=FISP.BSL0101.SD.FILE,                                00010600
//             DISP=SHR                                                 00010700
//SORTOUT   DD DSN=&&BSL0021S,                                          00010800
//             DISP=(NEW,PASS,DELETE),                                  00010900
//             DCB=(LRECL=131,BLKSIZE=23449,RECFM=FB),                  00011000
//             SPACE=(TRK,150),                                         00011100
//             UNIT=VIO                                                 00011200
//SYSIN     DD *                                                        00011300
   SORT FIELDS=(14,3,CH,A,
                1,7,CH,A,
                17,1,CH,A,
                25,5,CH,A,
                32,3,CH,A,
                19,6,CH,A,
                30,2,CH,A)
//*SORT FIELDS=(                                                        00011400
//SORTWK01  DD SPACE=(TRK,150),                                         00011500
//             UNIT=SYSDA                                               00011600
//SORTWK02  DD SPACE=(TRK,150),                                         00011700
//             UNIT=SYSDA                                               00011800
//SORTWK03  DD SPACE=(TRK,150),                                         00011900
//             UNIT=SYSDA                                               00012000
//SORTWK04  DD SPACE=(TRK,150),                                         00012100
//             UNIT=SYSDA                                               00012200
//SYSOUT    DD SYSOUT=*                                                 00012300
//*===========================================================          00012400
//* EDIT STEP WILL ABEND IF GENERATED INCOME BALANCE IS NOT "0"
//*===========================================================          00012400
//EDIT80   EXEC PGM=BSL010B,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00012500
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(BSL010B) PLAN(BSL010B)
/*
//TRANSI    DD DSN=&&BSL0021S,                                          00013600
//             DISP=(OLD,DELETE)                                        00013700
//SYSCTLI   DD DSN=FISP.BSL9000.SD.FILE,                                00013800
//             DISP=SHR                                                 00013900
//TABLES    DD DSN=FISP.BSL3100.SD.FILE,                                00014000
//             DISP=SHR                                                 00014100
//UUTC20V1  DD DSN=FISP.UUTC20V1,                                       00014200
//             DISP=SHR,                                                00014300
//             AMP=('BUFNI=2')                                          00014400
//UUTC20V2  DD DSN=FISP.UUTC20V2,                                       00014500
//             DISP=SHR,                                                00014600
//             AMP=('BUFNI=2')                                          00014700
//INDXFILE  DD DSN=FISP.ACTI.BUDGET.ACTIVE.VSAM,
//             DISP=SHR,                                                00014700
//             AMP=('BUFNI=2')                                          00014800
//TRANSO    DD DSN=&&BSL0101,                                           00014800
//             DISP=(NEW,PASS,DELETE),                                  00014900
//             DCB=(LRECL=131,BLKSIZE=23449,RECFM=FB),                  00015000
//             SPACE=(TRK,90),                                          00015100
//             UNIT=VIO                                                 00015200
//TRANSU    DD DSN=&&BSL0100,                                           00015300
//             DISP=(NEW,PASS,DELETE),                                  00015400
//             DCB=(LRECL=146,BLKSIZE=18980,RECFM=FB),                  00015500
//             SPACE=(TRK,90),                                          00015600
//             UNIT=VIO                                                 00015700
//SYSCTLO   DD DSN=&&BSL9000,                                           00015800
//             DISP=(NEW,PASS,DELETE),                                  00015900
//             DCB=(LRECL=80,BLKSIZE=80,RECFM=F),                       00016000
//             SPACE=(TRK,1),                                           00016100
//             UNIT=VIO                                                 00016200
//** REPORT    DD SYSOUT=(,),                                           00016300
//**              OUTPUT=(*.OUT2,*.OUT3)                                00016400
//REPORT    DD DSN=FISP.ACTI.BUDGET.TRAN.LIST.RPT,
//             DISP=(NEW,CATLG,KEEP),                                   00007500
//             DCB=(RECFM=FB,BLKSIZE=27872,LRECL=134),                  00007600
//             SPACE=(TRK,(20,20),RLSE),                                00007700
//             UNIT=SYSDA                                               00007800
//SYSOUT    DD SYSOUT=*                                                 00016500
//SYSUDUMP  DD SYSOUT=D,                                                00016600
//             DEST=LOCAL                                               00016700
//SYSDBOUT  DD SYSOUT=*                                                 00016800
//*                                                                     00008800
//********************************************************************* 00014500
//*   PRINT                                                             00014600
//*====================================================                 00016900
//COPYCNTL EXEC PGM=IEBGENER,COND=(0,NE,EDIT80)                         00017000
//SYSUT1    DD DSN=&&BSL9000,                                           00017100
//             DISP=(OLD,DELETE)                                        00017200
//SYSUT2    DD DSN=FISP.BSL9000.SD.FILE,                                00017300
//             DISP=(OLD,KEEP)                                          00017400
//SYSIN     DD DUMMY                                                    00017500
//SYSPRINT  DD SYSOUT=*                                                 00017600
//*====================================================                 00017700
//COPYVTRN EXEC PGM=IEBGENER,COND=(0,NE,EDIT80)                         00017800
//SYSUT1    DD DSN=&&BSL0101,                                           00017900
//             DISP=(OLD,DELETE)                                        00018000
//SYSUT2    DD DSN=FISP.BSL0101.SD.FILE,                                00018100
//             DISP=(OLD,KEEP)                                          00018200
//SYSIN     DD DUMMY                                                    00018300
//SYSPRINT  DD SYSOUT=*                                                 00018400
//*====================================================                 00018500
//COPYUTRN EXEC PGM=IEBGENER,COND=(0,NE,EDIT80)                         00018600
//SYSUT1    DD DSN=&&BSL0100,                                           00018700
//             DISP=(OLD,DELETE)                                        00018800
//SYSUT2    DD DSN=FISP.BSL0100.SD.FILE,                                00018900
//             DISP=(OLD,KEEP)                                          00019000
//SYSIN     DD DUMMY                                                    00019100
//SYSPRINT  DD SYSOUT=*                                                 00019200
/*                                                                      00019300
//*====================================================                 00008700
//*       PRINT CONTROL REPORT                                          00008800
//*====================================================                 00008900
//STEP02  EXEC PGM=IEBGENER                                             00009000
//SYSPRINT DD SYSOUT=(,),                                               00009100
//            OUTPUT=*.OUT1                                             00009200
//SYSUT2   DD SYSOUT=(,),                                               00009300
//            OUTPUT=*.OUT3                                             00009400
//SYSUT1   DD DSN=FISP.GAW125.UGAX125A.CR.D$MDATE,                      00009500
//            DISP=SHR                                                  00009600
//SYSIN    DD DUMMY                                                     00009700
//*                                                                     00009800
//*====================================================                 00009900
//*       PRINT EXTRACT REPORT                                          00010000
//*====================================================                 00010100
//STEP03  EXEC PGM=IEBGENER                                             00010200
//SYSPRINT DD SYSOUT=(,),                                               00010300
//            OUTPUT=*.OUT1                                             00010400
//SYSUT2   DD SYSOUT=(,),                                               00010500
//            OUTPUT=*.OUT3                                             00010600
//*SYSUT1   DD DSN=FISP.GAW125.UGAX125A.REPORT02.D071309,               00010700
//SYSUT1   DD DSN=FISP.GAW125.UGAX125A.REPORT02.D$MDATE,                00010700
//            DISP=SHR                                                  00010800
//SYSIN    DD DUMMY                                                     00010900
//*====================================================                 00011000
//*       PRINT CONTROL REPORT                                          00013700
//*====================================================                 00013800
//STEP05  EXEC PGM=IEBGENER                                             00013900
//SYSPRINT DD SYSOUT=*                                                  00014000
//SYSUT2   DD SYSOUT=*                                                  00014100
//*SYSUT1   DD DSN=FISP.GAW125.UGAX125B.CR.D071309,                     00014200
//SYSUT1   DD DSN=FISP.GAW125.UGAX125B.CR.D$MDATE,                      00014200
//            DISP=SHR                                                  00014300
//SYSIN    DD DUMMY                                                     00014400
//*                                                                     00014500
//********************************************************************* 00015300
//*                                                                     00000800
//PRNT050  EXEC PGM=IEBGENER,COND=(EVEN)                                00015400
//*            REGION=0K                                                00015500
//SYSUT1    DD DSN=FISP.ACTI.BUDGET.TRAN.LIST.RPT,                      00015600
//             DISP=SHR                                                 00015700
//SYSUT2    DD SYSOUT=*                                                 00015800
//SYSIN     DD DUMMY                                                    00016000
//SYSPRINT  DD SYSOUT=*                                                 00016100
//*
//*----------------------------------------------------------------- */
//*  CONVERT A TEXT FILE to PDF and e-mail                           */
//*----------------------------------------------------------------- */
//BOOGIE      EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//RPTFILE     DD   DISP=SHR,DSN=FISP.GAW125.UGAX125A.CR.D$MDATE         00009500
//            DD   DISP=SHR,DSN=FISP.GAW125.UGAX125A.REPORT02.D$MDATE   00010700
//            DD   DISP=SHR,DSN=FISP.GAW125.UGAX125B.CR.D$MDATE         00014200
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(TRK,(15,15),RLSE),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT    DD   SYSOUT=*
//SYSTSPRT    DD   SYSOUT=*
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(GAW125)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(GAW125)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(GAW125)
//*
//*----------------------------------------------------------------- */
//*  CONVERT A TEXT FILE TO PDF AND E-MAIL                           */
//*----------------------------------------------------------------- */
//PDFMAIL     EXEC PGM=IKJEFT1B,COND=(EVEN),REGION=0M,DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.EXEC
//RPTFILE     DD   DSN=FISP.ACTI.BUDGET.TRAN.LIST.RPT,
//            DISP=SHR
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(CYL,(1,1)),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT    DD   SYSOUT=*
//SYSTSPRT    DD   SYSOUT=*
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(BSL01TOF)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(BSL01TOF)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(BSL01TOF)
/*                                                                      0001
//*                                                                     0001
//*============================================================         0001
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          0001
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      0001
//*============================================================         0001
//STOPZEKE EXEC STOPZEKE                                                0001
//*                                                                     00015200
//****************************end of jcl gaw125 **************          00015200
