//GAWP945  JOB (SYS000),'PRDCTL - S003',
//        MSGLEVEL=(1,1),REGION=0M,
//*       RESTART=PURGE010,
//*       RESTART=ARCHIVE,
//        MSGCLASS=F,
//        CLASS=K,
//        NOTIFY=APCDBM
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 473
//*++++++++++++++++++++++++++
//OUT1 OUTPUT CLASS=X,FORMS=A011,JESDS=ALL,DEFAULT=YES,GROUPID=ACCTG
//*====================================================                 00001000
//CLEAN010 EXEC PGM=IEFBR14                                             00003000
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//DD1       DD DSN=FISP.GAW945.UGAU945.CNTLRPT.D$MDATE,                 00003200
//             DISP=(MOD,DELETE,DELETE),                                00003300
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00003400
//             SPACE=(TRK,(4,1,50)),                                    00003500
//             UNIT=SYSDA                                               00003600
/*                                                                      00001300
//*********************************************************************
//* DELETE R4069-JRNL-HDR IF THE HDR IS EMPTY AND 120 DAYS OLD AND
//* NOT AUTO JOURNAL
//*********************************************************************
//PURGE010 EXEC PGM=UGAU945
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSTSPRT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UGAU945) PLAN(UGAU945)
/*
//SYSUDUMP  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSOUT    DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSLST    DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//EXTRPURG  DD DSN=FISP.GAM945.PURGED.FILE.D$MDATE,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=120,BLKSIZE=27960),
//             SPACE=(TRK,(45,45),RLSE),
//             UNIT=SYSDA
//CONTROLR  DD DSN=FISP.GAW945.UGAU945.CNTLRPT.D$MDATE,                 00018100
//             DISP=(NEW,CATLG,DELETE),                                 00018200
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),                  00018300
//             SPACE=(TRK,(1,1),RLSE),                                  00018400
//             UNIT=SYSDA                                               00018500
//PARMS     DD *
*COMMIT COUNTER 0025
*PURGE AGE 0120
/*
//********************************************************
//ARCHIVE  EXEC PGM=FDRABR
//********************************************************
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSUDUMP  DD  SYSOUT=X,HOLD=YES
//ARCHIVE   DD  DSN=FDRABR.ARCHIVE,DISP=SHR
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//*
//TAPE1     DD  DSN=FDRABR.LASTAPE.ARCT1,DISP=(NEW,KEEP),
//          UNIT=VTAPE,LABEL=RETPD=35,VOL=(,RETAIN)
//DISK1     DD  UNIT=3390,VOL=SER=APC002,DISP=SHR
//ABRMAP    DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//*
//SYSIN     DD  *
  DUMP      TYPE=ARC,BUFNO=MAX,EXPD=NONE,VEXPD=NONE,RECALL,
            ARCBACKUP=NO,
            PRINT=ABR,SCRATCH=NO,SELTERR=NO,ONLVOL
  SELECT    DSN=FISP.GAM945.PURGED.FILE.D$MDATE,VOLG=APC
//*
//*==================================================
//RESTORE  EXEC PGM=FDRABR
//*==================================================
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSUDUMP  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//ARCHIVE   DD DSN=FDRABR.ARCHIVE,
//             DISP=SHR
//SYSPRIN#  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//ABRMAP    DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//ABRDUMMY  DD DUMMY
//ABRWORK   DD SPACE=(CYL,(1,1)),
//             UNIT=SYSDA
//SYSUT3    DD SPACE=(CYL,(1,1)),
//             UNIT=SYSDA
//SYSUT4    DD SPACE=(CYL,(1,1)),
//             UNIT=SYSDA
//* TYPE=ARC===>RESTORE ARCHIVE  TYPE=ABR===>RESTORE BACKUP
//SYSIN     DD *
 RESTORE  TYPE=ARC,DYNTAPE,ONLINE,RLSE,SELTERR=NO,MAXCARDS=9999,
          ARCBACKUP=NO,
          ARCDSN=FDRABR.ARCHIVE
 SELECT  DSG=FISP.GAM945.PURGED.FILE.D$MDATE
/*
//*
//*----------------------------------------------------------------- */
//*  CONVERT THE UGAU945 FILE TO PDF AND EMAIL                       */
//*----------------------------------------------------------------- */
//PDFMAIL     EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.EXEC
//RPTFILE     DD   DSN=FISP.GAW945.UGAU945.CNTLRPT.D$MDATE,DISP=SHR
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(CYL,(1,1)),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSTSPRT  DD SYSOUT=(,),
//             OUTPUT=(*.OUT1)
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(UGAU945)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(UGAU945)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(UGAU945)
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  GAW945   ========
