//IDCCHG1  JOB (SYS000),'PRDCTL', ',
//            MSGLEVEL=(1,1),
//            MSGCLASS=F,
//            CLASS=K,
//            REGION=0M,
//            NOTIFY=APCDBM
//**********************************************************************
//* IDCCHG - CHANGE GRANT INDIRECT COST CODES FOR FUNDS ON INPUT FILE
//*     ADHOC EV  #XXXX
//**********************************************************************
//STEP01   EXEC PGM=IEFBR14
//DD1       DD DSN=FISP.IDCCHG.OUT1,
//             DISP=(MOD,DELETE,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(0,0))
//DD2       DD DSN=FISP.IDCCHG.CNTLRPT,
//             DISP=(MOD,DELETE,DELETE),
//             UNIT=SYSDA,SPACE=(CYL,(0,0))
/*
//STEP010  EXEC PGM=IDCCHG
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(IDCCHG) PLAN(IDCCHG)
/*
//INP1      DD DSN=FISP.IDCTEST.TRIAL,DISP=SHR
//OUT1      DD DSN=FISP.IDCCHG.OUT1,
//             DISP=(NEW,CATLG,CATLG),
//             DCB=(RECFM=FB,LRECL=260),
//             SPACE=(TRK,(15,10),RLSE),
//             UNIT=SYSDA
//CNTLRPT   DD DSN=FISP.IDCCHG.CNTLRPT,
//             DISP=(NEW,CATLG,CATLG),
//             DCB=(RECFM=FBA,LRECL=133),
//             SPACE=(TRK,(15,10),RLSE),
//             UNIT=SYSDA
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//
