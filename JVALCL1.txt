//JVALCL1 JOB SYS000,'PC 0903',MSGLEVEL=(1,1),                          00010017
//* RESTART=STEP10,                                                     00021019
//* RESTART=STEP4,                                                      00022021
//* RESTART=STEP3,                                                      00023018
// REGION=20M,NOTIFY=APCDBM,CLASS=K,MSGCLASS=F                          00030000
//*++++++++++++++++++++++++++                                           00040000
//*    ZEKE EVENT # 2132                                                00050000
//*++++++++++++++++++++++++++                                           00060000
//*====================================================                 00070000
//* CREATE NEW DATASETS FOR FINAL FISCAL CLOSING ONLY                   00080000
//*====================================================                 00650000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00660000
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.02.HOSPREV,                     00671000
//             DISP=(,CATLG),                                           00680000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00690000
//             SPACE=(TRK,(1,2),RLSE),                                  00700000
//             UNIT=SYSDA,                                              00710000
//             VOL=SER=APC002                                           00720000
//*================================================================     00731000
//STEP1A   EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00750000
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.02.HOSPREV.MSG,                 00760000
//             DISP=(,CATLG),                                           00770000
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         00780000
//             SPACE=(TRK,(1,2),RLSE),                                  00790000
//             UNIT=SYSDA,                                              00800000
//             VOL=SER=APC002                                           00810000
//*============================================================         03786400
//STEP2    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03786600
//DD1       DD DSN=GAOP.BDGTROLL.ACCTINDX,                              03786703
//             DISP=(,CATLG),                                           03786800
//             DCB=(RECFM=FB,DSORG=PS,LRECL=80,BLKSIZE=23440),          03786900
//             SPACE=(TRK,(150,5),RLSE),                                03787000
//             UNIT=SYSDA,                                              03787100
//             VOL=SER=PRM006                                           03787200
//*============================================================         03787307
//STEP3    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03787407
//DD1       DD DSN=GAOT.BDGTROLL.ACCTINDX,                              03787507
//             DISP=(,CATLG),                                           03787607
//             DCB=(RECFM=FB,DSORG=PS,LRECL=80,BLKSIZE=23440),          03787707
//             SPACE=(TRK,(150,5),RLSE),                                03787807
//             UNIT=SYSDA,                                              03787907
//             VOL=SER=PRM006                                           03788007
//*============================================================         03788100
//STEP4    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03788207
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.02.OAR,                         03788300
//             DISP=(,CATLG),                                           03788400
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         03788500
//             SPACE=(TRK,(1,2),RLSE),                                  03788600
//             UNIT=SYSDA,                                              03788700
//             VOL=SER=APC002                                           03788800
//*============================================================         03788900
//*    'BN' WAS '01'         - CHANGED PER ANH (BFS) 4/16/09.           03789005
//*    'CLOSING' WAS 'ACCTG' - CHANGED PER ANH (BFS) 6/18/09.           03789105
//*============================================================         03789200
//STEP5    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03789307
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.BN.CLOSING,                     03789405
//             DISP=(,CATLG),                                           03789500
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         03789600
//             SPACE=(TRK,(750,250),RLSE),                              03789700
//             UNIT=SYSDA,                                              03789800
//             VOL=SER=APC002                                           03789900
//*============================================================         03790000
//*    'FN' WAS '02'         - CHANGED PER ANH (BFS) 4/16/09.           03790105
//*    'CLOSING' WAS 'ACCTG' - CHANGED PER ANH (BFS) 6/18/09.           03790205
//*============================================================         03790300
//STEP6    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03790407
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.FN.CLOSING,                     03790505
//             DISP=(,CATLG),                                           03790600
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         03790700
//             SPACE=(TRK,(750,250),RLSE),                              03790800
//             UNIT=SYSDA,                                              03790900
//             VOL=SER=APC002                                           03791000
//*============================================================         03791100
//*    'SF' WAS '03'         - CHANGED PER ANH (BFS) 4/16/09.           03791205
//*    'CLOSING' WAS 'ACCTG' - CHANGED PER ANH (BFS) 6/18/09.           03791305
//*============================================================         03791400
//STEP7    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03791507
//DD1       DD DSN=FISP.JVDATA.D$YRS.07SF.CLOSING,                      03791606
//             DISP=(,CATLG),                                           03791706
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         03791806
//             SPACE=(TRK,(750,250),RLSE),                              03791906
//             UNIT=SYSDA,                                              03792006
//             VOL=SER=APC002                                           03793000
//*============================================================         03795700
//*STEP8    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         03795919
//*DD1       DD DSN=FISP.JVDATA.D$FISPLD.03.HOSPONLN,                   03796019
//*             DISP=(,CATLG),                                          03796119
//*             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),        03796219
//*             SPACE=(TRK,(1,2),RLSE),                                 03796319
//*             UNIT=SYSDA,                                             03796419
//*             VOL=SER=APC002                                          03796519
//*============================================================         03796600
//*STEP9    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         03796819
//*DD1       DD DSN=FISP.JVDATA.D$FISPLD.03.HOSPONLN.MSG,               03796919
//*             DISP=(,CATLG),                                          03797019
//*             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),        03797119
//*             SPACE=(TRK,(1,2),RLSE),                                 03797219
//*             UNIT=SYSDA,                                             03797319
//*             VOL=SER=APC002                                          03797419
//*============================================================         03797500
//STEP10   EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03797707
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.04.HOSPONLN,                    03797800
//             DISP=(,CATLG),                                           03797900
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         03798000
//             SPACE=(TRK,(1,2),RLSE),                                  03798100
//             UNIT=SYSDA,                                              03798200
//             VOL=SER=APC002                                           03798300
//*============================================================         03798400
//STEP11   EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          03798607
//DD1       DD DSN=FISP.JVDATA.D$FISPLD.04.HOSPONLN.MSG,                03798700
//             DISP=(,CATLG),                                           03798800
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         03798900
//             SPACE=(TRK,(1,2),RLSE),                                  03799000
//             UNIT=SYSDA,                                              03799100
//             VOL=SER=APC002                                           03799200
//*============================================================         03799300
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          03810000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      03820000
//*============================================================         03830000
//STOPZEKE EXEC STOPZEKE                                                03840000
//*                                                                     03850000
//*================ E N D  O F  J C L  JVALCL1  ========                03860002
