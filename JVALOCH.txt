//JVALOCH JOB SYS000,'BEV - 0903',MSGLEVEL=(1,1),                       00010002
//*  RESTART=STEP5,                                                     00021019
// REGION=20M,NOTIFY=APCDBM,CLASS=K,MSGCLASS=F                          00030000
//*++++++++++++++++++++++++++                                           00040000
//*    ZEKE EVENT # 1439                                                00050012
//*++++++++++++++++++++++++++                                           00060000
//*====================================================                 00070000
//* CREATE NEW DATASETS FOR HOSPFASS, BULK, OARASSR OARASSR2            00080021
//*====================================================                 00090000
//STEP1    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          04463001
//DD1       DD DSN=FISP.JVDATA.D$JVDATE.01.HOSPFASS,                    04463107
//             DISP=(,CATLG),                                           04463207
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         04463307
//             SPACE=(TRK,(1,5),RLSE),                                  04463407
//             UNIT=SYSDA,                                              04463507
//             VOL=SER=APC002                                           04463600
//*============================================================         04466500
//STEP2    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          04466601
//DD1       DD DSN=FISP.JVDATA.D$JVDATE.01.HOSPFASS.MSG,                04466707
//             DISP=(,CATLG),                                           04466807
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         04466907
//             SPACE=(TRK,(1,5),RLSE),                                  04467007
//             UNIT=SYSDA,                                              04467107
//             VOL=SER=APC002                                           04467207
//*============================================================         04467416
//STEP3    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          04467501
//DD1       DD DSN=FISP.JVDATA.D$JVDATE.01.HOSPBULK,                    04467608
//             DISP=(,CATLG),                                           04467707
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         04467807
//             SPACE=(TRK,(1,2),RLSE),                                  04467907
//             UNIT=SYSDA,                                              04468007
//             VOL=SER=APC002                                           04468107
//*===========================================================          04468208
//STEP4    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          04468301
//DD1       DD DSN=FISP.JVDATA.D$JVDATE.01.HOSPBULK.MSG,                04468410
//             DISP=(,CATLG),                                           04468507
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         04468607
//             SPACE=(TRK,(1,2),RLSE),                                  04468707
//             UNIT=SYSDA,                                              04468807
//             VOL=SER=APC002                                           04468907
//*===========================================================          04469115
//STEP5    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          04469215
//DD1       DD DSN=FISP.JVDATA.D$JVDATE.01.OARASSR,                     04469315
//             DISP=(,CATLG),                                           04469415
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         04469515
//             SPACE=(TRK,(1,2),RLSE),                                  04469615
//             UNIT=SYSDA,                                              04469715
//             VOL=SER=APC002                                           04469815
//*===========================================================          04469915
//STEP6    EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          04470015
//DD1       DD DSN=FISP.JVDATA.D$JVDATE.02.OARASSR2,                    04470115
//             DISP=(,CATLG),                                           04470215
//             DCB=(RECFM=FB,DSORG=PS,LRECL=250,BLKSIZE=23250),         04470315
//             SPACE=(TRK,(1,2),RLSE),                                  04470415
//             UNIT=SYSDA,                                              04470515
//             VOL=SER=APC002                                           04470615
//*                                                                     04470715
//*============================================================         04470815
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          04470915
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      04471015
//*============================================================         04471115
//STOPZEKE EXEC STOPZEKE                                                04471215
//*                                                                     04472000
//*================ E N D  O F  J C L  JVALOCH ========                 04480006
