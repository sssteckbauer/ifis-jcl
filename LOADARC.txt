//LOADARC  JOB (SYS000),'LOAD UCOP FILE',                               00000100
//            MSGLEVEL=(1,1),                                           00000200
//            MSGCLASS=F,                                               00000300
//            CLASS=K,                                                  00040002
//            NOTIFY=ACLPLO,                                            00050002
//            REGION=20M                                                00060002
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 3075
//*++++++++++++++++++++++++++
//********************************************************************  00001200
//*
//* THIS JOB WILL BE RUN ON DEMAND
//* LOADARC WILL LOAD ARC, UAS AND SPONSOR CODE TABLES.
//* THE PROGRAM WILL HANDLE EMPTY FILES AS WELL AS THE DUPLICATE FILES
//* THEREFORE EVEN IF THIS JOB RUNS MANY TIMES, IT WILL BE FINE.
//*
//* HOWEVER, THE FILES HAVE TO BE IN THE EXACT FORMAT -- PLEASE SEE
//* RUN BOOK FOR THE FILE FORMATS.
//********************************************************************  00001200
//*
//********************************************************************  00001200
//*              DELETE CATALOGED DATA SETS                             00001500
//*===================================================================  00001600
//*                                                                     00001700
//DELEXT1  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00001800
//SYSPRINT  DD SYSOUT=*                                                 00001900
//SYSIN     DD *                                                        00002000
  DELETE    FISP.LOADARC.CNTLRPT
//*
//*
//STEP010  EXEC PGM=LOADARC
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(LOADARC) PLAN(LOADARC)
/*
//UCOARC    DD DSN=FISP.ARC,DISP=SHR
//UCOSPN    DD DSN=GAOP.SPN.CODE,DISP=SHR
//UCOUAS    DD DSN=FISP.UAS.CODE,DISP=SHR
//CNTLRPT   DD DSN=FISP.LOADARC.CNTLRPT,
//             DISP=(NEW,CATLG,DELETE),                                 000
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 000
//             SPACE=(TRK,(50,10),RLSE),                                000
//             UNIT=SYSDA                                               000
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//*
//****************************************************************
//* THIS PRODUCES THE PDFMAIL
//****************************************************************
//PDFMAIL1    EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//STEPLIB     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.LOAD
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.LOAD
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//TXT1        DD   DISP=SHR,DSN=FISP.LOADARC.CNTLRPT
//PDF1        DD   UNIT=SYSALLDA,
//            SPACE=(CYL,(1,1)),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB)
//SYSPRINT    DD   SYSOUT=X
//SYSTSPRT    DD   SYSOUT=X
//SYSTSIN     DD   *
%TXT2PDF BROWSE Y                                                     +
     IN  DD:TXT1                                                      +
     OUT DD:PDF1                                                      +
     CC YES                                                           +
     COMPRESS 9                                                       +
     CONFIRM YES                                                      +
     LPI 8                                                            +
     TM .0 BM .0 LM .5 RM .0                                          +
     ORIENT LANDSCAPE                                                 +
     PAPER /BLUEBAR/
%XMITIP * +
  SUBJECT 'REPORT# - LOAD UCOP FILES FROM FLAT FILES'                 +
  FROM ACT-PRODCONTROL@UCSD.EDU                                       -
  ADDRESSFILEDD ADDRLIST                                              -
  FILEDD (  PDF1  )                                                   -
  FILENAME (FILE1.PDF)                                                +
  MSGT 'REPORT RUN ON: &DAY &DATE &TIME, BY JOB: &JOB                 +
       \\\                                                            +
       \THIS REPORT IS VIEWABLE WITH ACROBAT READER.                  +
       \THE LATEST VERSION CAN BE DOWNLOADED FROM:                    +
       HTTP://WWW.ADOBE.COM/PRODINDEX/ACROBAT/READSTEP.HTML#READER +  +
       \\'
/*
//ADDRLIST    DD   *
TO  IFISHELPDESK@UCSD.EDU
CC  DMESERVE@UCSD.EDU
CC  RCOLIO@UCSD.EDU
/*
//*
//*************************************************************
//*  RENAME INPUT FILE
//*************************************************************
//RENAME1  EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   ALTER      GAOP.SPN.CODE -
              NEWNAME(GAOP.SPN.CODE.DONE)
/*
//*************************************************************
//*  BACKUP INPUT FILE FOR HISTORY
//*************************************************************
//BKUPGAOP EXEC PGM=IEBGENER,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSUT1    DD DSN=GAOP.SPN.CODE.DONE,
//             DISP=SHR
//SYSUT2    DD DSN=GAOP.SPN.CODE.DONE.BKUP(+1),
//             DISP=(NEW,CATLG,CATLG),
//             DCB=(RECFM=FB,LRECL=200),
//             SPACE=(TRK,(75,60),RLSE),
//             UNIT=SYSALLDA,VOL=SER=PRM002
//SYSIN     DD DUMMY
//SYSPRINT  DD SYSOUT=X
//*
//*************************************************************
//*  DELETE 'DONE' DATASET
//*************************************************************
//REMOVE   EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
           DELETE  GAOP.SPN.CODE.DONE  SCRATCH
/*
//*************************************************************
//* CREATE NEW INPUT DATASET FOR GENERAL ACCOUNTING UPLOAD
//*************************************************************
//ALOCGP   EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//DD1       DD DSN=GAOP.SPN.CODE,
//             DISP=(,CATLG),
//             DCB=(RECFM=FB,DSORG=PS,LRECL=200,BLKSIZE=20000),
//             SPACE=(TRK,(150,5),RLSE),
//             UNIT=SYSDA,
//             VOL=SER=PRM002
//*
//*************************************************************
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*************************************************************
//*STOPZEKE EXEC STOPZEKE
//*
//*==========  E N D  O F  J C L  -  L O A D A R C  ==========
