//MEDCTRRP JOB (SYS000),'MED CTR REPORT',CLASS=K,MSGCLASS=F,            00010003
//            MSGLEVEL=(1,1),REGION=20M,NOTIFY=APCDBM                   00020003
//*           RESTART=DELETE,
//*++++++++++++++++++++++++++                                           00050003
//*    ZEKE EVENT # 735  ...CONTACT: AL CHAN (EX:32697)                 00070003
//*                      ...CONTACT: DANIEL ORDILLAS                    00070003
//*++++++++++++++++++++++++++                                           00080003
/*JOBPARM LINES=5000                                                    00090003
//OUTMDP  OUTPUT DEST=LOCAL,CLASS=P,FORMS=MGRP,GROUPID=MDGRP,           00100003
//        LINECT=0                                                      00100003
//OUTBAK  OUTPUT DEST=LOCAL,CLASS=G,FORMS=MGRP,GROUPID=BACKUP1,         00110003
//        LINECT=0                                                      00160003
//*                                                                     00160003
//*====================================================                 00220003
//*     M E D 0 6 0                                                     00230003
//*   MONTHLY DOCTOR'S BILLING REPORTS                                  00240003
//*         FISP.MEDCTR.D$YYMM                                          00340003
//*==========================================================           04280003
//STEP01   EXEC PGM=IEBGENER                                            04290003
//SYSPRINT  DD SYSOUT=*                                                 04300003
//SYSUT2    DD SYSOUT=(,),                                              04310003
//             OUTPUT=(*.OUTMDP,*.OUTBAK)                               04320003
//SYSUT1    DD DSN=FISP.MEDCTR.D$YYMM,                                  04330003
//             DISP=SHR                                                 04340003
//SYSIN     DD DUMMY                                                    04350003
//*********************************************************
//*  DELETE PRINT FILE 'FISP.MEDCTR.DONE'
//*********************************************************
//DELETE   EXEC PGM=IDCAMS,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   DELETE     FISP.MEDCTR.DONE
/*
//*********************************************************
//*  RENAME PRINT FILE 'FISP.MEDCTR.D$YYMM'
//*********************************************************
//RENAMER  EXEC PGM=IDCAMS
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
   ALTER      FISP.MEDCTR.D$YYMM -
              NEWNAME (FISP.MEDCTR.DONE)
/*
//*===========================================================          04360003
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
**ALERT**

MED060 HAS PROCESSED.
THE OPS SHOULD HAVE BEEN REMINDED VIA EMAIL FROM MEDCENTER TO RUN
THIS JOB TO CREATE THE PRINT FILE.

/*
//* CONTROL REPORT FILENAME FOLLOWS:
//SYSTSIN   DD *
 %MAILRPT -
 'FISP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('MEDCENTER MONTHEND REPORT HAS BEEN CREATED')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE..
//*---
//*===========================================================          04360003
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          04670003
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      04680003
//*============================================================         04690003
//STOPZEKE EXEC STOPZEKE                                                04700003
//*                                                                     04710003
//*================ E N D  O F  J C L  MED060  =========                04720003
