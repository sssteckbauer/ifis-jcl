//MSTRUCSS JOB (SYS000),'PRDCTL',
//            MSGCLASS=F,
//            MSGLEVEL=(1,1),
//            CLASS=K,
//            REGION=0M,
//            NOTIFY=APCEGE
//*
//*=============================
//* ZEKE EVENT # 2543
//* THIS JOB CAN RUN ANYTIME
//*=============================
//*********************************************************************
//* DELETE PRE-EXISTING OUTPUT FILES (IF ANY) CREATED BY THIS JOB
//*********************************************************************
//SDEL010  EXEC PGM=IEFBR14
//DD1       DD DSN=FISP.APVLUPDT.CSS.CNTLRPT,
//             DISP=(MOD,DELETE,DELETE),
//             UNIT=SYSDA
//DD2       DD DSN=FISP.APVLUPDT.CSS.ERRRPT,
//             DISP=(MOD,DELETE,DELETE),
//             UNIT=SYSDA
//*********************************************************************
//* APVLUPDT - IFIS - ONE-TIME MASTER TMPL APPROVER UPDATE (CSS)
//*
//* THIS PROGRAM WILL WALK THE APT_APRVL_TRAN_V D/B AND REMOVE
//* ALL LEVEL 1 PRIMARY AND ALTERNATE USER ID'S AND REPLACE WITH
//* THE LEVEL 1 PRIMARY AND ALTERNATE USER ID'S FROM THE MASTER
//* TEMPLATE (MSTRTMPL).  THE USER ID REPLACED MUST HAVE THE SAME
//* DOC TYPE AND APVL TEMPLATE CODE (IF AVAILABLE).
//* IN ADDITION, ALL UNAPPROVED DOCUMENTS ON THE APPROVAL HEADER
//* TABLE (APH_APRVL_HDR_V) WHOSE PRIMARY USER ID HAS CHANGED
//* TO THE MASTER TEMPLATE'S PRIMARY USER ID.                    .
//*********************************************************************
//STEP010  EXEC PGM=APVLUPDT
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSIN   DD *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(APVLUPDT) PLAN(APVLUPDT)
/*
//PARM      DD *
JV CSS
//CNTLRPT   DD DSN=FISP.APVLUPDT.CSS.CNTLRPT,
//             DISP=(NEW,CATLG,CATLG),
//             DCB=(RECFM=FBA,LRECL=133),
//             SPACE=(TRK,(01,01),RLSE),
//             UNIT=SYSDA
//RPT1      DD DSN=FISP.APVLUPDT.CSS.ERRRPT,
//             DISP=(NEW,CATLG,CATLG),
//             DCB=(RECFM=FBA,LRECL=133),
//             SPACE=(TRK,(120,120),RLSE),
//             UNIT=SYSDA
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//*
//*==========  E N D  O F  J C L  -  A P V L U P D T  ==========
