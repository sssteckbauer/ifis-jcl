//PREGXLI4 JOB (SYS000),'REGISTER-XLI4',MSGCLASS=F,MSGLEVEL=(1,1),      00000100
//*       REGION=0M,                                                    00000500
//*=======================                                              00001000
//*   RESTART=PDFMAIL,                                                  00000300
//*=======================                                              00001000
//*   ZEKE EVENT # 3755    - PRINT XLI4 PRELIM CHECK REGISTER
//*
//*======================
//        NOTIFY=APCDBM,CLASS=K                                         00000600
//*====================================================
//* SORT FOR XLI4
//*====================================================
//STEP01   EXEC PGM=IEFBR14,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//FAPUK07S  DD DSN=FISP.FILE.UK07PL4,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=FB,LRECL=285,BLKSIZE=5985),
//             SPACE=(TRK,(15,15),RLSE),
//             UNIT=SYSDA
//DD2       DD DSN=FISP.PRELIM.XLI4.REGISTER,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=27930),
//             SPACE=(TRK,(45,45),RLSE),
//             UNIT=SYSDA
/*
//*====================================================
//*       SORT XLI4 - CHECK REGISTER DATA
//*====================================================
//STEP02   EXEC PGM=SYNCSORT,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSLST    DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SORTIN    DD DSN=FISP.FILE.UK07O,
//             DISP=SHR
//SORTOUT   DD DSN=FISP.FILE.UK07PL4,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=285,BLKSIZE=5985),
//             SPACE=(TRK,(45,45),RLSE),
//             UNIT=SYSDA
//SORTWK01  DD SPACE=(TRK,30),
//             UNIT=SYSDA
//SORTWK02  DD SPACE=(TRK,30),
//             UNIT=SYSDA
//SORTWK03  DD SPACE=(TRK,30),
//             UNIT=SYSDA
//SYSIN     DD *
  INCLUDE COND=(43,1,CH,EQ,C'A',OR,21,4,CH,EQ,C'XLI4')
  SORT FIELDS=(1,43,CH,A)
/*
//*====================================================
//*          CHECK REGISTER PRINT DRIVER #1
//*====================================================
//STEP03   EXEC PGM=UAPUK07,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UAPUK07) PLAN(UAPUK07)
/*
//CONTROLR  DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SYSDBOUT  DD SYSOUT=*
//UAPUK07O  DD DSN=FISP.FILE.UK07PL4,
//             DISP=SHR
//PARM01    DD *
*COMMIT COUNTER 0100
*PRELIMINARY
/*
//REGISTER  DD DSN=FISP.PRELIM.XLI4.REGISTER,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=27930),
//             SPACE=(TRK,(45,45),RLSE),
//             UNIT=SYSDA
//*
/*
//*----------------------------------------------------------------- */
//*  CONVERT A TEXT FILE TO PDF AND E-MAIL                           */
//*----------------------------------------------------------------- */
//PDFMAIL     EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.EXEC
//RPTFILE     DD   DSN=FISP.PRELIM.XLI4.REGISTER,DISP=SHR
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(CYL,(1,1)),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT    DD   SYSOUT=*
//SYSTSPRT    DD   SYSOUT=*
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(PDFMAIL5)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(PREGXLI4)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(CREGXLI4)
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  PREGXLI4 ========
