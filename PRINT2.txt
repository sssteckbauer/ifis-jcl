//TOBEV    JOB SYS000,'BEV-TPCS',MSGLEVEL=(1,1),                        00001000
//        NOTIFY=APCDBM,CLASS=A,MSGCLASS=F,REGION=4096K                 00002001
//*ROUTE PRINT RMT10                                                    00003000
//*OUT1 OUTPUT CLASS=P,FORMS=STD,DEST=RMT10                             00003101
//*-----------------------------                                        00003300
//STEP1   EXEC PGM=IEBGENER                                             00003400
//SYSUT1   DD DSN=DSEP.JCL.CNTL,DISP=SHR,                               00003501
//            DCB=(RECFM=FB,DSORG=PO,LRECL=80,BLKSIZE=27920),           00003601
//            UNIT=SYSALLDA                                             00003701
//SYSUT2   DD DSN=FISP.PAULS.DSEPLIB,                                   00003801
//            DISP=(MOD,KEEP),                                          00003901
//            DCB=(RECFM=FB,DSORG=PO,LRECL=80,BLKSIZE=27920),           00004001
//            UNIT=SYSALLDA                                             00004101
//SYSOUT   DD SYSOUT=*                                                  00004200
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00004300
//SYSPRINT DD SYSOUT=*                                                  00004400
//SYSIN    DD DUMMY                                                     00004500
//*-----------------------------                                        00005000
//                                                                      00005100
//STEP2   EXEC PGM=IEBGENER                                             00006000
//SYSUT1   DD DSN=FISP.APW300B.UAPU300.REPORT02.D090794,DISP=SHR        00007000
//SYSUT2   DD SYSOUT=(,),OUTPUT=(*.OUT1,*.OUT2)                         00008000
//SYSOUT   DD SYSOUT=*                                                  00009000
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00010000
//SYSPRINT DD SYSOUT=*                                                  00011000
//SYSIN    DD DUMMY                                                     00012000
//*-----------------------------                                        00013000
//STEP3   EXEC PGM=IEBGENER                                             00013100
//SYSUT1   DD DSN=FISP.APW300B.UAPU300.REPORT02.D091294,DISP=SHR        00013200
//SYSUT2   DD SYSOUT=(,),OUTPUT=(*.OUT1,*.OUT2)                         00013300
//SYSOUT   DD SYSOUT=*                                                  00013400
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00013500
//SYSPRINT DD SYSOUT=*                                                  00013600
//SYSIN    DD DUMMY                                                     00013700
//*-----------------------------                                        00013800
//STEP4   EXEC PGM=IEBGENER                                             00013900
//SYSUT1   DD DSN=FISP.APW300B.UAPU300.REPORT02.D091994,DISP=SHR        00014000
//SYSUT2   DD SYSOUT=(,),OUTPUT=(*.OUT1,*.OUT2)                         00015000
//SYSOUT   DD SYSOUT=*                                                  00016000
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00017000
//SYSPRINT DD SYSOUT=*                                                  00018000
//SYSIN    DD DUMMY                                                     00019000
//*-----------------------------                                        00020000
//STEP5   EXEC PGM=IEBGENER                                             00030000
//SYSUT1   DD DSN=FISP.APW300B.UAPU300.REPORT02.D092694,DISP=SHR        00040000
//SYSUT2   DD SYSOUT=(,),OUTPUT=(*.OUT1,*.OUT2)                         00050000
//SYSOUT   DD SYSOUT=*                                                  00060000
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00070000
//SYSPRINT DD SYSOUT=*                                                  00080000
//SYSIN    DD DUMMY                                                     00090000
//*-----------------------------                                        00100000
//STEP6   EXEC PGM=IEBGENER                                             00110000
//SYSUT1   DD DSN=FISP.APW300B.UAPU300.REPORT02.D100394,DISP=SHR        00120000
//SYSUT2   DD SYSOUT=(,),OUTPUT=(*.OUT1,*.OUT2)                         00130000
//SYSOUT   DD SYSOUT=*                                                  00140000
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00150000
//SYSPRINT DD SYSOUT=*                                                  00160000
//SYSIN    DD DUMMY                                                     00170000
//*-----------------------------                                        00180000
//STEP7   EXEC PGM=IEBGENER                                             00190000
//SYSUT1   DD DSN=FISP.APW300B.UAPU300.REPORT02.D100694,DISP=SHR        00200000
//SYSUT2   DD SYSOUT=(,),OUTPUT=(*.OUT1,*.OUT2)                         00210000
//SYSOUT   DD SYSOUT=*                                                  00220000
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00230000
//SYSPRINT DD SYSOUT=*                                                  00240000
//SYSIN    DD DUMMY                                                     00250000
//*-----------------------------                                        00260000
//STEP8   EXEC PGM=IEBGENER                                             00270000
//SYSUT1   DD DSN=FISP.APW300B.UAPU300.REPORT02.D101094,DISP=SHR        00280000
//SYSUT2   DD SYSOUT=(,),OUTPUT=(*.OUT1,*.OUT2)                         00290000
//SYSOUT   DD SYSOUT=*                                                  00300000
//SYSUDUMP DD SYSOUT=D,DEST=LOCAL                                       00310000
//SYSPRINT DD SYSOUT=*                                                  00320000
//SYSIN    DD DUMMY                                                     00330000
