//ADHOCPOS JOB (SYS000),'MAILROOM',CLASS=K,MSGCLASS=F,                  00010001
//            NOTIFY=APCDBM,                                            00020001
//            MSGLEVEL=(1,1),REGION=20M,                                00030001
//            RESTART=*                                                 00040001
//*++++++++++++++++++++++++++                                           00050001
//*    ZEKE EVENT # XXX             799                                 00060001
//*++++++++++++++++++++++++++                                           00070001
/*JOBPARM LINES=1500                                                    00080001
//OUTPO    OUTPUT DEST=LOCAL,CLASS=G,FORMS=L007,GROUPID=MAILROOM        00090001
//OUTBAK   OUTPUT DEST=LOCAL,CLASS=G,FORMS=L007,GROUPID=BACKUP          00100001
//OUT1 OUTPUT CLASS=F,FORMS=PU11,JESDS=ALL,DEFAULT=YES,GROUPID=PURCHSNG 00110001
//*====================================================                 00120001
//*     P U Z 0 1 0    ( DON'T RUN DURING THE DAYTIME)                  00130001
//*                      ( USE ZEKE #799 FOR OVERNITE)                  00140001
//*     THIS JCL WILL PRINT PURCHASE ORDERS FROM A PRE-EXISTING         00150001
//*     EXTRACT FILE.                                                   00160001
//*                                                                     00170001
//*     THE PURCHASE ORDERS NEED TO BE PRINTED USING THE ZEROX FORM     00180001
//*     L007.                                                           00190001
//*                                                                     00200001
//*     SYMBOLIC PARAMETERS:                                            00210001
//*                                                                     00220001
//*         NONE REQUIRED                                               00230001
//*                                                                     00240001
//*     DATA SETS CATALOGED:                                            00250001
//*                                                                     00260001
//*         FISP.PUD010.UPUXD10.D093094.T110003                         00270001
//*             WHERE MMDDYY IS THE DATE THE THE FILE WAS CREATED       00280001
//*             AND HHMMSS WAS THE TIME THAT THE FILE WAS CREATED.      00290001
//*====================================================                 00300001
//SORT010  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ)),        00310001
//             PARM='DYNALLOC=(SYSDA,15)'                               00320001
//SYSOUT    DD SYSOUT=*                                                 00330001
//SORTIN    DD DSN=FISP.PUD010.UPUXD10.D093094.T110003,                 00340001
//             DISP=OLD                                                 00350001
//SORTOUT   DD DSN=FISP.PUD010.UPUXD10.D093094.T110003,                 00360001
//             DISP=SHR                                                 00370001
//SYSIN     DD DSN=FISP.PARMLIB(PUD010S1),                              00380001
//             DISP=SHR                                                 00390001
/*                                                                      00400001
//*====================================================                 00410001
//PRINT020 EXEC PGM=UPUP001D,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00420001
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UPUP001D) PLAN(UPUP001D)
/*
//SYSLST    DD SYSOUT=*                                                 00510001
//SYSOUT    DD SYSOUT=*                                                 00520001
//SYSDBOUT  DD SYSOUT=*                                                 00530001
//SYSUDUMP  DD SYSOUT=D,                                                00540001
//             DEST=LOCAL                                               00550001
//EXTRACT   DD DSN=FISP.PUD010.UPUXD10.D093094.T110003,                 00560001
//             DISP=SHR                                                 00570001
//CONTROLR  DD DSN=FISP.PUD010.UPUP001D.CONTROLR.D$MDATE,               00580001
//             MGMTCLAS=AS,DISP=(NEW,CATLG,DELETE),                     00590001
//             DCB=(RECFM=FBM,LRECL=133,BLKSIZE=23408),                 00600001
//             SPACE=(TRK,(1,1),RLSE),                                  00610001
//             UNIT=SYSDA                                               00620001
//*POREPORT DD SYSOUT=(A,,L007)                                         00630001
//POREPORT  DD SYSOUT=(,),                                              00640001
//             OUTPUT=(*.OUTPO,*.OUTBAK)                                00650001
//*====================================================                 00660001
//*          PRINT CONTROL REPORTS                    =                 00670001
//*====================================================                 00680001
//CNTRL030 EXEC PGM=IEBGENER                                            00690001
//SYSPRINT  DD SYSOUT=*                                                 00700001
//SYSUT2    DD SYSOUT=*                                                 00710001
//SYSUT1    DD DSN=FISP.PUD010.UPUP001D.CONTROLR.D$MDATE,               00720001
//             DISP=SHR                                                 00730001
//SYSIN     DD DUMMY                                                    00740001
//*============================================================         00750001
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00760001
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00770001
//*============================================================         00780001
//STOPZEKE EXEC STOPZEKE                                                00790001
//*                                                                     00800001
//*================ E N D  O F  J C L  PUZ010  =========                00810001
