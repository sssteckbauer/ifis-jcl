//SLJOB1C JOB (ACCT),'BUDGET - 355 TPCS',NOTIFY=APCDBM,
//*            RESTART=(RENAMER),
//             CLASS=A,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M
//OUT1 OUTPUT CLASS=F,FORMS=BS11,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET
//*OUT2 OUTPUT CLASS=P,FORMS=BS01,GROUPID=BUDGET
//OUT3 OUTPUT CLASS=G,FORMS=BS01,GROUPID=BACKUP
//*
//*==================
//* ZEKE EV # 3253     **THIS JOB IS SUBMITTED BY SLJOB1B **
//*==================  ** N.T.E. **
//*
//*====================================================                 00001800
//*              REMOVE POTENTIAL OUTPUT FILES                          00001900
//*====================================================                 00002000
//CLEAN01  EXEC PGM=IEFBR14                                             00002100
//SYSPRINT  DD SYSOUT=*                                                 00002200
//DD1       DD DSN=FISP.SLJOB1C.SLPGB030.SLRPT030,
//             DISP=(MOD,DELETE,DELETE),                                00002400
//             UNIT=SYSDA                                               00002500
//*====================================================
//*  STEP01:  SORT PROVISION MASTER FILE INTO LOCATION/SAU/SUB-
//*           BUDGET/ACCOUNT/FUND/SUB-CAMPUS/TITLE CODE/
//*           PROVISION NUMBER SEQUENCE
//*====================================================
//STEP01   EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SORTIN    DD DSN=FISP.RSL.PROV.MSTR,
//             DISP=SHR
//SORTOUT   DD DSN=&&PROV,
//             DISP=(,PASS),
//             DCB=(RECFM=FB,LRECL=503,BLKSIZE=23138),
//             SPACE=(TRK,(75,15),RLSE),
//             UNIT=VIO
//SYSIN     DD *
   SORT FIELDS=(1,3,A,18,2,A,5,13,A,4,1,A,32,11,A),FORMAT=CH
//*SORT FIELDS=(1,3,A,18,2,A,5,13,A,4,1,A,20,11,A),FORMAT=CH
//SYSOUT    DD SYSOUT=*
//SORTWK01  DD SPACE=(TRK,30),
//             UNIT=WORK
//SORTWK02  DD SPACE=(TRK,30),
//             UNIT=WORK
//SORTWK03  DD SPACE=(TRK,30),
//             UNIT=WORK
//SORTWK04  DD SPACE=(TRK,30),
//             UNIT=WORK
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//*====================================================
//*  STEP02:  PRINT PROVISION MASTER FILE (TURNAROUND DOCUMENT FORMAT)
//*           IN LOCATION/SAU/SUB-BUDGET/ACCOUNT/FUND/SUB-CAMPUS/
//*           TITLE CODE/PROVISION NUMBER SEQUENCE
//*====================================================
//STEP02   EXEC PGM=SLPGB030,COND=(0,NE)
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//PROVMSTR  DD DSN=&&PROV,
//             DISP=(OLD,DELETE)
//SLSYSIN   DD *
$MDATE
/*
//* DISPLAY $MDATE (071309)
//*** AS OF DATE IN MMDDYY FORMAT, COLS. 1-6
//SLRPT030  DD DSN=FISP.SLJOB1C.SLPGB030.SLRPT030,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=27930),
//             SPACE=(TRK,(75,15),RLSE),
//             UNIT=SYSDA
//SYSOUT    DD SYSOUT=*
//SYSDBOUT  DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
/*
//*====================================================
//PRNTRPT  EXEC PGM=IEBGENER,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSUT1    DD DSN=FISP.SLJOB1C.SLPGB030.SLRPT030,
//             DISP=SHR
//SYSUT2    DD SYSOUT=(,),
//             OUTPUT=(*.OUT3)
//SYSIN     DD DUMMY
//SYSPRINT  DD SYSOUT=*
/*
//*
//*==================================================================== 00008500
//*       SEND MAIL NOTICES                                             00008600
//*==================================================================== 00008700
//*                                                                     00008800
//PDFMAIL1 EXEC PDFMAILT,
//         JOB=SLJOB1C,
//         IN=FISP.SLJOB1C.SLPGB030.SLRPT030
//*
//*============================================================
//RENAMER EXEC PGM=IDCAMS
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
  ALTER      FISP.RSL.PROVTRNS.UNCNV -
             NEWNAME(FISP.RSL.PROVTRNS.UNCNV.D$MDATE)
  ALTER      FISP.RSL.PROVTRNS -
             NEWNAME(FISP.RSL.PROVTRNS.D$MDATE)
/*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*================ E N D  O F  J C L  SLJOB1C  ========
