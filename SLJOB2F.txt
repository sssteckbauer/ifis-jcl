//SLJOB2F  JOB (ACCT),'BUDGET - 355 TPCS',NOTIFY=APCDBM,
//*        RESTART=BOOGIEA,
//*        RESTART=COPYFIL1,
//*
//*===================
//* ZEKE EVENT # 2552     ** FISCAL CLOSING JOB **
//*===================
//         CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M
/*JOBPARM LINES=500
//OUT2 OUTPUT CLASS=P,FORMS=BS02,GROUPID=BUDGET2,COPIES=1
//*====================================================                 0000050
//* **FISCAL YEAREND VC REPORTING ** - THIS JOB IS RUN
//*    AS WELL AS SLJOB2Q (TO PRODUCE STF100 & 200 FILES).
//* on a fiscal yearend basis (it will run with this fiscal closing
//* in June 2009 and then with each year on a fiscal yearend basis,
//* this will be the last year we send the 'SLC' fiscal file to UCOP
//* The files STF100 & 200 will replace SLC files)
//*
//* THE DATE IN  CONTROLI(BELOW)  MUST BE AN "ENDING" DATE OF SOME
//* PERIOD... SUCH AS 06-30-20 (MMDDYY) ..( FOR SYSTEMWIDE PURPOSES )
//*
//*  GLOBALLY CHANGE TARGETS:  200630    (YYMMDD)
//*  GLOBALLY CHANGE TARGETS:  JUN20     (MMMYY)
//*  GLOBALLY CHANGE TARGETS:  FNL20     (FNLYY)
//*  GLOBALLY CHANGE TARGETS:  06-30-20  (MM-DD-YY)
//*  GLOBALLY CHANGE TARGETS:  07-01-20  (MM-DD-YY)(CURRENT EDB)
//*  CHANGE TARGET: FISP.RSL.PAYTRNS.OLD.D051220 (CURRENT VERSION)
//*  CHANGE TARGET: PPSP.SLPAYEXT.D042420.FROZEN (CURRENT EDB)
//*  CHANGE TARGET: PPSP.SLPAYEXT (No Frozen snapshot for 2017)
//*====================================================
//CLEAN01  EXEC PGM=IEFBR14
//SYSPRINT DD SYSOUT=*
//DD1      DD DSN=FISP.SLJOB2Y.SLPGM100.SLSYSOUT,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD2      DD DSN=FISP.SLJOB2Y.SLPGM200.SLSYSOUT,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD3      DD DSN=FISP.SLJOB2Y.SLPGB131.SLSYSOUT,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD4      DD DSN=FISP.SLJOB2Y.SLPGB135.SLSYSOUT,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD5      DD DSN=FISP.SLJOB2Y.SLPGB170.SLPROVSC,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD6      DD DSN=FISP.SLJOB2Y.SLPGB180.SLPROVLS,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD7      DD DSN=FISP.STF100,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD8      DD DSN=FISP.STF200,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//DD9      DD DSN=FISP.PAYEXT,
//            DISP=(MOD,DELETE,DELETE),
//            UNIT=SYSDA
//*
//*====================================================
//*
//*        STEP01:  SLPGM100 - SIMPLEX LASER - ALL FILES                0000070
//*                                                                     0000080
//*        DURING CLOSING:                                              0000090
//*                 CHECK LINES 76 & 101  TO                            0000100
//*                 ENSURE THAT THE 'FROZEN' PAYROLL                    0000110
//*                 FILE IS BEING USED AS INPUT.                        0000120
//*                                                                     0000130
//*   FROZEN PAYROLL DATE: D122904 ( USED DURING CLOSING ONLY! )
//*   FROZEN PAYROLL DATE: D042420 ( USED DURING CLOSING ONLY! )
//*    ( THER ARE 2 PLACES IN THIS JOB WHERE THE FILE IS USED)          0000130
//*    ( Remember that closing is run in July and January )
//*                                                                     0000130
//*   REMINDER!..JOB SLJOBAA MUST BE RUN AFTER SLJOB2 IS DONE           0000140
//*     (SLJOBAA submitted as last step of this job-automatically)
//*   REMINDER!..JOB BSL70 MUST BE RUN AFTER SLJOB2 AND SLJOB3 ARE DONE 0000140
//*     ( THE BSL70 WILL BE SUBMITTED AUTOMATICALLY WHEN SLJOB3 RUNS )
//*====================================================                 0000200
//STEP01   EXEC PGM=SLPGM101,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         0000210
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SLSYSIN   DD *,                                                       0000240
//             DCB=BLKSIZE=80                                           0000250
SLJOB2SLPGM10110101 REPORT DATE                   06-30-20              0037000
SLJOB2SLPGM10110102 RUN TYPE                      PROD                  0038000
SLJOB2SLPGM10110103 BUDGET INPUT RUN CYCLE        001                   0039000
SLJOB2SLPGM10110104 PAYROLL INPUT FILE ID         EDB AS OF 07-01-20    0040000
SLJOB2SLPGM10110105 PROVISION INPUT RUN CYCLE     001                   0041000
SLJOB2SLPGM13110101 EDIT ERROR SEVERITY           NON-FATAL             0042000
/*                                                                      0000260
//SLJOBCTL  DD DSN=&&JOBCTL,                                            0000270
//             DISP=(,PASS),                                            0000280
//             DCB=(RECFM=F,LRECL=256),                                 0000290
//             SPACE=(TRK,1),                                           0000300
//             UNIT=VIO                                                 0000310
//SLPGMCTL  DD DSN=&&PGMCTL,                                            0000320
//             DISP=(,PASS),                                            0000330
//             DCB=(RECFM=FB,LRECL=1024,BLKSIZE=2048),                  0000340
//             SPACE=(TRK,2),                                           0000350
//             UNIT=VIO                                                 0000360
//SLSYSOUT  DD DSN=FISP.SLJOB2Y.SLPGM100.SLSYSOUT,                      0000370
//             DISP=(NEW,CATLG,DELETE),                                 0000280
//             DCB=(RECFM=FBA,LRECL=134,BLKSIZE=27872),                 0000290
//             SPACE=(TRK,(15,15),RLSE),                                0000300
//             UNIT=SYSDA                                               0000310
//SYSOUT    DD SYSOUT=*                                                 0000390
//SYSUDUMP  DD SYSOUT=D,                                                0000400
//             DEST=LOCAL                                               0000410
//SYSDBOUT  DD SYSOUT=*                                                 0000420
//*====================================================
//*        STEP01B: SORT PAYROLL UPDATE TRANSACTIONS INTO
//*                 EMPLOYEE NO/DISTR NO/ACTION CODE SEQUENCE
//*====================================================
//STEP01B  EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SORTWK01  DD SPACE=(TRK,10),
//             UNIT=WORK
//SORTWK02  DD SPACE=(TRK,10),
//             UNIT=WORK
//SORTWK03  DD SPACE=(TRK,10),
//             UNIT=WORK
//SORTWK04  DD SPACE=(TRK,10),
//             UNIT=WORK
//SORTIN    DD DSN=FISP.RSL.PAYTRNS.OLD.D051220,
//             DISP=SHR
//SORTOUT   DD DSN=&&PAYTRNS,
//             DISP=(,PASS),
//             DCB=(RECFM=FB,LRECL=143,BLKSIZE=23452),
//             SPACE=(TRK,10),
//             UNIT=VIO
//SYSIN     DD *
     SORT FIELDS=(8,11,CH,A,7,1,CH,A)
//*SORT FIELDS=(8,11,CH,A,7,1,CH,A)
//SYSOUT    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//*====================================================
//*        STEP01C: SLPGB131
//*                 UPDATE PAYROLL EXTRACT FILE
//*====================================================
//STEP01C  EXEC PGM=SLPGB131,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT  DD  SYSOUT=*
//SYSTSPRT  DD  SYSOUT=*
//SYSTSIN   DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(SLPGB131) PLAN(SLPGB131)
/*
//SLJOBCTL  DD DSN=&&JOBCTL,
//             DISP=(OLD,PASS)
//SLPGMCTL  DD DSN=&&PGMCTL,
//             DISP=(OLD,PASS)
//TITLETBL  DD DSN=FISP.SLP.TITLECD.TCI,
//             DISP=SHR
//SLPYTRNS  DD DSN=&&PAYTRNS,
//             DISP=(OLD,DELETE)
//*SLOLDPAY  DD DSN=PPSP.SLPAYEXT.D042420.FROZEN,
//SLOLDPAY  DD DSN=PPSP.SLPAYEXT,
//             DISP=SHR
//SLPAYEXT  DD DSN=FISP.PAYEXT,
//             DISP=(NEW,CATLG,KEEP),
//             DCB=(RECFM=FBA,LRECL=161,BLKSIZE=161),
//             SPACE=(TRK,(15,15),RLSE),
//             UNIT=SYSDA
//SLBSTBLS  DD DSN=FISP.BSL3100.SD.FILE,
//             DISP=SHR
//UUTC20V1  DD DSN=FISP.UUTC20V1,
//             DISP=SHR,
//             AMP=('BUFNI=2')
//UUTC20V2  DD DSN=FISP.UUTC20V2,
//             DISP=SHR,
//             AMP=('BUFNI=2')
//SLSYSOUT  DD DSN=FISP.SLJOB2Y.SLPGB131.SLSYSOUT,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FBA,LRECL=134,BLKSIZE=27872),
//             SPACE=(TRK,(15,15),RLSE),
//             UNIT=SYSDA
//SYSOUT    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SYSDBOUT  DD SYSOUT=*
//*====================================================
//*        STEP02:  SLPGM135
//*                 PRINT PAYROLL EXTRACT CONTROL LIST
//*====================================================
//STEP02   EXEC PGM=SLPGB135,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SLJOBCTL  DD DSN=&&JOBCTL,
//             DISP=(OLD,PASS)
//SLPGMCTL  DD DSN=&&PGMCTL,
//             DISP=(OLD,PASS)
//SLPAYEXT  DD DSN=FISP.PAYEXT,
//             DISP=SHR
//SLSYSOUT  DD DSN=FISP.SLJOB2Y.SLPGB135.SLSYSOUT,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(RECFM=FBA,LRECL=134,BLKSIZE=27872),
//             SPACE=(TRK,(15,15),RLSE),
//             UNIT=SYSDA
//SYSOUT    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//SYSDBOUT  DD SYSOUT=*
//*====================================================
//*        STEP03:  SORT PAYROLL DISTRIBUTION EXTRACT FILE INTO
//*                 EMPLOYEE/TITLE/ACCT/FUND/SUB-BUDGET SEQUENCE
//*                 IN PREPARATION FOR CROSS-REFERENCE RECORD
//*                 GENERATION
//*====================================================
//STEP03   EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SORTWK01  DD SPACE=(TRK,150),
//             UNIT=WORK
//SORTWK02  DD SPACE=(TRK,150),
//             UNIT=WORK
//SORTWK03  DD SPACE=(TRK,150),
//             UNIT=WORK
//SORTWK04  DD SPACE=(TRK,150),
//             UNIT=WORK
//SORTIN    DD DSN=FISP.PAYEXT,
//             DISP=SHR
//SORTOUT   DD DSN=&&PAYSRT1,
//             DISP=(,PASS),
//             DCB=(RECFM=FB,LRECL=161,BLKSIZE=23345),
//             SPACE=(TRK,150),
//             UNIT=VIO
//SYSIN     DD *
   SORT FIELDS=(25,9,CH,A,21,4,CH,A,1,20,CH,A,34,2,CH,A)
   INCLUDE COND=(5,6,CH,NE,C'9<EXC>')
//SYSOUT    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=D,
//             DEST=LOCAL
//*====================================================                 0002840
//*        STEP04:  SLPGM170                                            0002850
//*                 CREATE PROVISION EXTRACT FILE                       0002860
//*                 PRINT PROVISION CONTROL TOTALS                      0002870
//*====================================================                 0002880
//STEP04   EXEC PGM=SLPGB170,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         0002890
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SLJOBCTL  DD DSN=&&JOBCTL,                                            0002940
//             DISP=(OLD,PASS)                                          0002950
//SLPGMCTL  DD DSN=&&PGMCTL,                                            0002960
//             DISP=(OLD,PASS)                                          0002970
//PROVISMS  DD DSN=FISP.RSL.PROV.MSTR,                                  0002980
//             DISP=SHR                                                 0002990
//SLPROVEX  DD DSN=&&PVNEXT,                                            0003000
//             DISP=(,PASS),                                            0003010
//             DCB=(RECFM=FB,LRECL=406,BLKSIZE=23142),                  0003020
//             SPACE=(TRK,150),                                         0003030
//             UNIT=VIO                                                 0003040
//SLPROVSC  DD DSN=FISP.SLJOB2Y.SLPGB170.SLPROVSC,                      0000370
//             DISP=(NEW,CATLG,DELETE),                                 0000280
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 0000290
//             SPACE=(TRK,(15,15),RLSE),                                0000300
//             UNIT=SYSDA                                               0000310
//SYSOUT    DD SYSOUT=*                                                 0003070
//SYSUDUMP  DD SYSOUT=D,                                                0003080
//             DEST=LOCAL                                               0003090
//SYSPRINT  DD SYSOUT=*                                                 0003080
//SYSDBOUT  DD SYSOUT=*                                                 0003110
//*====================================================                 0003120
//*        STEP05:  SORT PROVISION EXTRACT FILE INTO                    0003130
//*                 STAFFING LIST SEQUENCE                              0003140
//*====================================================                 0003150
//STEP05   EXEC PGM=SYNCSORT,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         0003160
//SORTWK01  DD SPACE=(TRK,150),                                         0003170
//             UNIT=WORK                                                0003180
//SORTWK02  DD SPACE=(TRK,150),                                         0003190
//             UNIT=WORK                                                0003200
//SORTWK03  DD SPACE=(TRK,150),                                         0003210
//             UNIT=WORK                                                0003220
//SORTWK04  DD SPACE=(TRK,150),                                         0003230
//             UNIT=WORK                                                0003240
//SORTIN    DD DSN=&&PVNEXT,                                            0003250
//             DISP=(OLD,DELETE)                                        0003260
//SORTOUT   DD DSN=&&PVNSRT,                                            0003270
//             DISP=(,PASS),                                            0003280
//             DCB=(RECFM=FB,LRECL=406,BLKSIZE=23142),                  0003290
//             SPACE=(TRK,150),                                         0003300
//             UNIT=VIO                                                 0003310
//SYSIN     DD *                                                        0003320
   SORT FIELDS=(1,31,CH,A)                                              0364000
//*SORT FIELDS=(1,31,CH,A)                                              0003330
//SYSOUT    DD SYSOUT=*                                                 0003340
//SYSUDUMP  DD SYSOUT=D,                                                0003350
//             DEST=LOCAL                                               0003360
//*====================================================                 0001210
//*        STEP06: SLPGM136                                             0001220
//*          CREATES STF100 AND STF200 FILES                            0001230
//*====================================================                 0001240
//STEP06   EXEC PGM=SLPGB136                                            0001250
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SLPAYEXT  DD DSN=&&PAYSRT1,DISP=(OLD,PASS)                            0001340
//SLPRVEXT  DD DSN=&&PVNSRT,DISP=(OLD,PASS)                             0001340
//STF100    DD DSN=FISP.STF100,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(LRECL=125,BLKSIZE=23375,RECFM=FB),
//             SPACE=(TRK,(100,10),RLSE),
//             UNIT=SYSDA
//STF200    DD DSN=FISP.STF200,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(LRECL=135,BLKSIZE=23355,RECFM=FB),
//             SPACE=(TRK,(100,10),RLSE),
//             UNIT=SYSDA
//CONTROLI  DD *
06-30-20
//SYSOUT    DD SYSOUT=*                                                 0001390
//SYSUDUMP  DD SYSOUT=D,                                                0001400
//             DEST=LOCAL                                               0001410
//SYSDBOUT  DD SYSOUT=*                                                 0001420
//**********************************************************            0001420
//*====================================================                 0003370
//*        STEP07:  SLPGM180                                            0003380
//*                 PRINT PROVISION EXTRACT CONTROL LIST                0003390
//*====================================================                 0003400
//STEP07   EXEC PGM=SLPGB180,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         0003410
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SLJOBCTL  DD DSN=&&JOBCTL,                                            0003460
//             DISP=(OLD,PASS)                                          0003470
//SLPGMCTL  DD DSN=&&PGMCTL,                                            0003480
//             DISP=(OLD,PASS)                                          0003490
//SLTABLES  DD DSN=FISP.BSL3100.SD.FILE,                                0003500
//             DISP=SHR                                                 0003510
//SLPROVEX  DD DSN=&&PVNSRT,                                            0003520
//             DISP=(OLD,PASS)                                          0003530
//SLPROVLS  DD DSN=FISP.SLJOB2Y.SLPGB180.SLPROVLS,                      0000370
//             DISP=(NEW,CATLG,DELETE),                                 0000280
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 0000290
//             SPACE=(TRK,(15,15),RLSE),                                0000300
//             UNIT=SYSDA                                               0000310
//SYSOUT    DD SYSOUT=*                                                 0003560
//SYSUDUMP  DD SYSOUT=D,                                                0003570
//             DEST=LOCAL                                               0003580
//SYSPRINT  DD SYSOUT=*                                                 0003590
//SYSDBOUT  DD SYSOUT=*                                                 0003600
//*====================================================                 0003940
//*        STEP08:  SLPGM200                                            0003950
//*                 PRINT JOB RUN SUMMARY REPORT                        0003960
//*====================================================                 0003970
//STEP08   EXEC PGM=SLPGM200,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         0003980
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//SLJOBCTL  DD DSN=&&JOBCTL,                                            0004010
//             DISP=(OLD,PASS)                                          0004020
//SLPGMCTL  DD DSN=&&PGMCTL,                                            0004030
//             DISP=(OLD,PASS)                                          0004040
//SLSYSOUT  DD DSN=FISP.SLJOB2Y.SLPGM200.SLSYSOUT,                      0000370
//             DISP=(NEW,CATLG,DELETE),                                 0000280
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 0000290
//             SPACE=(TRK,(15,15),RLSE),                                0000300
//             UNIT=SYSDA                                               0000310
//SYSOUT    DD SYSOUT=*                                                 0004070
//SYSUDUMP  DD SYSOUT=D,                                                0004080
//             DEST=LOCAL                                               0004090
//SYSDBOUT  DD SYSOUT=*                                                 0004100
/*                                                                      0004110
//*----------------------------------------------------------------- */
//*  CONVERT A TEXT FILE to PDF and e-mail                           */
//*----------------------------------------------------------------- */
//BOOGIEA     EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//RPTFILE     DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGM100.SLSYSOUT
//            DD   DISP=SHR,DSN=PPSP.SLP1301
//            DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGB135.SLSYSOUT
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(TRK,(15,15),RLSE),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT    DD   SYSOUT=*
//SYSTSPRT    DD   SYSOUT=*
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(SLJOB2A)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(SLJOB2A)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(SLJOB2A)
//*
//*----------------------------------------------------------------- */
//*  CONVERT A TEXT FILE to PDF and e-mail                           */
//*----------------------------------------------------------------- */
//BOOGIEB     EXEC PGM=IKJEFT1B,
//            REGION=0M,
//            DYNAMNBR=50
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//RPTFILE     DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGB170.SLPROVSC
//            DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGB180.SLPROVLS
//            DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGM200.SLSYSOUT
//PDFFILE     DD   DISP=(,PASS),
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),
//            SPACE=(TRK,(15,15),RLSE),
//            UNIT=SYSALLDA,
//            DSN=&&PDF
//SYSPRINT    DD   SYSOUT=*
//SYSTSPRT    DD   SYSOUT=*
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(SLJOB2B)
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(SLJOB2B)
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(SLJOB2B)
//*
//*====================================================
//* HARD COPIES DISABLED 06/2006 PER MERCEDES REQUEST
//*====================================================
//*PRNTRPT1 EXEC PGM=IEBGENER
//*SYSUT1    DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGM100.SLSYSOUT
//*          DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGB135.SLSYSOUT
//*          DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGB170.SLPROVSC
//*          DD   DISP=SHR,DSN=FISP.SLJOB2Y.SLPGB180.SLPROVLS
//*SYSUT2    DD SYSOUT=(,),
//*             OUTPUT=(*.OUT2)                                         0072000
//*SYSIN     DD DUMMY
//*SYSPRINT  DD SYSOUT=*
//*
//*
//*====================================================
//* NEW FILE FOR 'STF' SYSTEM AT UCOP(UNIX)
//*====================================================
//COPYFIL1 EXEC PGM=IEBGENER
//SYSUT1    DD DSN=FISP.STF100,
//             DISP=SHR
//SYSUT2    DD DSN=FISP.STF100SD.R200630,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(LRECL=125,BLKSIZE=23375,RECFM=FB),
//             SPACE=(TRK,(100,10),RLSE),
//             UNIT=SYSALLDA,
//             VOL=SER=APC005
//SYSIN     DD DUMMY
//SYSPRINT  DD SYSOUT=*
//*====================================================
//* NEW FILE FOR 'STF' SYSTEM AT UCOP(UNIX)
//*====================================================
//COPYFIL2 EXEC PGM=IEBGENER
//SYSUT1    DD DSN=FISP.STF200,
//             DISP=SHR
//SYSUT2    DD DSN=FISP.STF200SD.R200630,
//             DISP=(NEW,CATLG,DELETE),
//             DCB=(LRECL=135,BLKSIZE=23355,RECFM=FB),
//             SPACE=(TRK,(100,10),RLSE),
//             UNIT=SYSALLDA,
//             VOL=SER=APC005
//SYSIN     DD DUMMY
//SYSPRINT  DD SYSOUT=*
//*====================================================
//*   ARCHIVER STEP ( JOB SLJOBAA )
//*====================================================
//STEP16   EXEC PGM=IEFBR14
//*STEP16   EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//DEL1      DD DSN=FISP.ACADSVCS.PROV.MSTR.COPY,
//             DISP=(MOD,DELETE,DELETE),
//             DCB=(RECFM=FB,LRECL=503,BLKSIZE=23138),
//             SPACE=(TRK,(1,1),RLSE),
//             UNIT=SYSDA
//*====================================================
//STEP17   EXEC PGM=SYNCSORT
//SORTIN    DD DSN=FISP.RSL.PROV.MSTR,
//             DISP=OLD
//SORTOUT   DD DSN=FISP.ACADSVCS.PROV.MSTR.COPY,
//             DISP=(NEW,CATLG,KEEP),
//             DCB=(RECFM=FB,LRECL=503,BLKSIZE=23138),
//             SPACE=(TRK,(15,1),RLSE),
//             UNIT=SYSALLDA,
//             VOL=SER=APC005
//SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),
//             DISP=SHR
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//*====================================================
//*  SUBMIT BSL70 JOB
//*====================================================
//SUBBSL70 EXEC PGM=ZEKESET
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 SET SCOM 'ZADD EV 2998 REBUILD'
/*
//*====================================================
//*  SUBMIT BSL70 ARCHIVE JOB
//*====================================================
//SUBARCH EXEC PGM=ZEKESET
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 SET SCOM 'ZADD EV 2500 REBUILD'
/*
//*============================================================
//*    SET ZENA GLOBAL VARIABLES FOR FTP OF SFT100/200 FILES
//*============================================================
//Z1     EXEC PGM=ZEKESET
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
      SET VAR $GETSTF100 EQ R200630
      SET VAR $PUTSTF100 EQ FNL20
      SET VAR $GETSTF200 EQ R200630
      SET VAR $PUTSTF200 EQ FNL20
/*
//*===========================================================
//* THIS STEP SUBMITS THE JOB (ZETDATM3) THAT SETS THE
//*  **ZENA GLOBAL VARIABLES**
//* (PASSES THE VALUE TO ZENA)
//*==============================================================
//SUBSETQ  EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2553) REBUILD'
/*
//*
//*============================================================         0004120
//RENAME01 EXEC PGM=IDCAMS
//*------------------------------
//SYSPRINT DD SYSOUT=*
//SYSIN    DD  *
  ALTER     FISP.PAYEXT  -
            NEWNAME(FISP.PAYEXT.OLD.D051220)
//*
//*============================================================         0004120
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          0004130
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      0004140
//*============================================================         0004150
//STOPZEKE EXEC STOPZEKE                                                0004160
//*                                                                     0004170
//*================ E N D  O F  J C L  SLJOB2Y ========                 0004180
