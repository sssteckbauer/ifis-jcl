//SL225BO JOB (ACCT),'BUDGET - 355 TPCS',MSGLEVEL=(1,1),                00010000
//            MSGCLASS=F,NOTIFY=APCDBM,CLASS=A,REGION=4096K             00021000
/*JOBPARM LINES=1000                                                    00030000
//*======================================================               00040051
//* ** OBSOLETE **                                                      00040155
//*  NO LONGER NEEDED UNLESS NEED TO RERUN THIS STEP ONLY               00040251
//*  THIS JOB IS IN THE BOTTOM OF SLJOB3                                00040351
//*======================================================               00041051
//OUT1 OUTPUT CLASS=P,FORMS=BS02,GROUPID=BUDGET2,COPIES=1               00043010
//OUT2 OUTPUT CLASS=G,FORMS=BS02,GROUPID=BACKUP2,COPIES=1               00050000
//OUT3 OUTPUT CLASS=F,FORMS=BS11,GROUPID=BUDGET,JESDS=ALL,DEFAULT=YES   00051006
//OUT4 OUTPUT CLASS=G,FORMS=BS01,GROUPID=BUDGET1,COPIES=1               00051352
//OUT5 OUTPUT CLASS=G,FORMS=BS01,GROUPID=BACKUP1,COPIES=1               00052000
//*====================================================                 00070000
//*  OUTPUT=K UNTIL GIVEN OK BY MERCEDES TO PRINT                       00070100
//*  OUT4 TURNED OFF ON 4/2009 PER HUGO.                                00070252
//*====================================================                 00071000
//*                                                                     00080000
//*  THIS JOB READS THE CTAPE CREATED IN "SLJOB3" AND                   00090000
//*  PRODUCES 1 COPY (DUPLEX) ON THE LASER PRINTER.                     00100000
//*  PRODUCES 1 COPY (SIMPLEX) ON THE LASER PRINTER.                    00101000
//*  ALSO PRODUCES PDF OF REPORT                                        00110001
//*                                                                     00111001
//*====================================================                 00120000
//* (REMEMBER - DO NOT RUN Z# 1806 ON LINE # 1507                       00120100
//*  WHEN FISCAL YEAREND AND JULY 1 -UNTIL CLOSE BUDGET & STAFFING)     00120200
//*====================================================                 00121000
//PRINTSTP EXEC PGM=IEBGENER,                                           00130000
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00131000
//SYSUT1    DD DSN=FISP.SL225RPT,                                       00132000
//             LABEL=(1,SL),                                            00133000
//             UNIT=VTAPE,                                              00134057
//*            UNIT=CTLIB,                                              00134100
//*            UNIT=CTAPE,                                              00134200
//             VOL=SER=020087                                           00135054
//SYSUT2    DD SYSOUT=(,),                                              00136000
//             OUTPUT=(*.OUT1,*.OUT2,*.OUT4,*.OUT5)                     00137000
//SYSIN     DD DUMMY                                                    00138000
//SYSPRINT  DD SYSOUT=*                                                 00139000
/*                                                                      00140000
//*----------------------------------------------------------------- *  00140100
//*  CONVERT A TEXT FILE TO PDF AND E-MAIL                           */ 00140200
//*----------------------------------------------------------------- */ 00140300
//BOOGIEA     EXEC PGM=IKJEFT1B,                                        00141000
//            REGION=0M,                                                00141100
//            DYNAMNBR=50                                               00141200
//SYSEXEC     DD   DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC                 00141300
//            DD   DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC                    00141400
//RPTFILE     DD DISP=SHR,DSN=FISP.SL225RPT                             00141500
//PDFFILE     DD   DISP=(,PASS),                                        00141600
//            DCB=(LRECL=27990,BLKSIZE=27998,RECFM=VB),                 00141700
//            SPACE=(TRK,(15,15),RLSE),                                 00141800
//            UNIT=SYSALLDA,                                            00141900
//            DSN=&&PDF                                                 00142000
//SYSPRINT    DD   SYSOUT=*                                             00142100
//SYSTSPRT    DD   SYSOUT=*                                             00142200
//SYSTSIN     DD   DISP=SHR,DSN=APCP.PDFMAIL.PDFORMAT(SLJOB3A)          00142300
//            DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILMSGS(SLJOB3A)          00142403
//ADDRLIST    DD   DISP=SHR,DSN=APCP.PDFMAIL.MAILISTS(SLJOB3A)          00142500
//*                                                                     00142600
//*================================================================     00150000
//* SUBMIT SETBSL - TO CHANGE VALUES IN $FISACTGPRDBSL TO NEXT MONTH    00150100
//*                                                                     00150200
//* (REMEMBER TO TURN OFF STATEMENT AFTER L# 1506 DURING FISCAL         00150312
//*  CLOSING (JUST BEFORE PRELIM-UNTIL CLOSE BUDGET STAFFING)           00150400
//*================================================================     00150500
//*                                                                     00150643
//*                                                                     00150743
//SUBBF010 EXEC PGM=ZEKESET,                                            00150800
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00150900
//SYSPRINT  DD SYSOUT=*                                                 00151000
//SYSIN     DD *                                                        00151100
 SET SCOM 'ZADD EV (1806) REBUILD'                                      00151200
/*                                                                      00151300
//*============================================================         00152000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00160000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00170000
//*============================================================         00180000
//STOPZEKE EXEC STOPZEKE                                                00190000
//*                                                                     00200000
//*================ E N D  O F  J C L  SLJOB3BO  =======                00210000
