//STHSALOC  JOB (ACCT),'APCDBM - S003',NOTIFY=APCDBM,                   00000100
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=2048K           00000300
//*----------------------------------------------------                 00000400
//*=  ALLOCATE STOREHOUSE FILE FOR FTP UPLOAD  ========                 00000500
//*----------------------------------------------------                 00000600
//STEP2    EXEC PGM=IEBGENER                                            00000700
//SYSUT1    DD DUMMY                                                    00000800
//SYSUT2    DD DSN=FISP.STOREHSE.Y1992,                                 00000900
//             MGMTCLAS=AS,DISP=(NEW,CATLG,DELETE),                     00001000
//             DCB=(RECFM=FB,LRECL=100,BLKSIZE=3000),                   00001100
//             SPACE=(TRK,(50,5),RLSE),                                 00001200
//             UNIT=SYSALLDA,                                           00001300
//             VOL=SER=APC002                                           00001400
//SYSIN     DD DUMMY                                                    00001500
//SYSPRINT  DD SYSOUT=*                                                 00001600
//*================ E N D  O F  J C L  STHSALOC  =======                00001700
