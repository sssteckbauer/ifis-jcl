//TAAP100A  JOB (SYS000),'BEV-PRDCTL',NOTIFY=APCDBM,                    00001000
//           CLASS=K,MSGLEVEL=(1,1),MSGCLASS=F,REGION=20M               00002005
//*++++++++++++++++++++++++++                                           00003000
//*    ZEKE EVENT # 2298                                                00004000
//*++++++++++++++++++++++++++                                           00005000
//*********************                                                 00005102
//* TRAVEL PURGE ARCHIVE                                                00005202
//*********************                                                 00005302
//*-------------------------------------------                          00006000
//* ARCHIVE TRAVEL EXTRACTS FOR 25 YEARS                                00006103
//*-------------------------------------------                          00006200
//**********************************************************************00006300
//*====================================================                 00110000
//*  CHECK FOR INPUT DATASETS BEFORE EXECUTING JOB
//*====================================================                 00110000
//CHKDSN1  EXEC PGM=IKJEFT01,DYNAMNBR=99
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR
//SYSTSPRT DD  TERM=TS,SYSOUT=*
//SYSTSIN  DD  *
  %CHKDSN 'FISP.TAA100E.UTAX100O.D$TAA100ED'
/*
//CHKDSN2  EXEC PGM=IKJEFT01,DYNAMNBR=99,COND=(0,NE)
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR
//SYSTSPRT DD  TERM=TS,SYSOUT=*
//SYSTSIN  DD  *
  %CHKDSN 'FISP.TAA100E.UTAX100S.D$TAA100ED'
/*
//CHKDSN3  EXEC PGM=IKJEFT01,DYNAMNBR=99,COND=(0,NE)
//SYSPROC  DD  DSN=SYS2.PRODUCTS.CLIST,DISP=SHR
//SYSLBC   DD  DSN=SYS1.BRODCAST,DISP=SHR
//SYSTSPRT DD  TERM=TS,SYSOUT=*
//SYSTSIN  DD  *
  %CHKDSN 'FISP.TAA100E.UTAX100.CR.D$TAA100ED'
/*
//**********************************************************************00006300
//ARCHIVE  EXEC PGM=FDRABR,COND=(0,NE)                                  00006405
//SYSPRINT  DD  SYSOUT=*                                                00006600
//SYSUDUMP  DD  SYSOUT=A,HOLD=YES                                       00006700
//ARCHIVE   DD  DSN=FDRABR.ARCHIVE,DISP=SHR                             00006800
//SYSPRIN1  DD  SYSOUT=*                                                00006900
//*                                                                     00007000
//TAPE1     DD  DSN=FDRABR.LASTAPE.ARCT1,DISP=(NEW,KEEP),               00008000
//          UNIT=VTAPE,LABEL=RETPD=8784                                 00009003
//DISK1     DD  UNIT=3390,VOL=SER=APC006,DISP=SHR                       00010000
//DISK2     DD  UNIT=3390,VOL=SER=APC007,DISP=SHR                       00020000
//DISK3     DD  UNIT=3390,VOL=SER=APC005,DISP=SHR                       00030000
//ABRMAP    DD  SYSOUT=*                                                00040000
//*                                                                     00050000
//SYSIN     DD  *                                                       00060000
  DUMP      TYPE=ARC,BUFNO=MAX,EXPD=NONE,VEXPD=NONE,RECALL,             00070004
            ARCBACKUP=NO,
            PRINT=ABR,SCRATCH=NO,SELTERR=NO,ONLVOL                      00080000
  SELECT    DSN=FISP.TAA100E.UTAX100O.D$TAA100ED,VOLG=APC               00090001
  SELECT    DSN=FISP.TAA100E.UTAX100S.D$TAA100ED,VOLG=APC               00090101
  SELECT    DSN=FISP.TAA100E.UTAX100.CR.D$TAA100ED,VOLG=APC             00091000
//*                                                                     00110000
//*============================================================         00110000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00110000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00110000
//*============================================================         00110000
//STOPZEKE EXEC STOPZEKE                                                00110000
//*                                                                     00110000
//*================ E N D  O F  J C L  TAA100A  ========                00250007
