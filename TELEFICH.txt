//TELEFICH JOB (APC000),'PRODUCTION CONTROL',NOTIFY=APCDBM,             00000121
//           CLASS=B,MSGLEVEL=(1,1),                                    00000208
//           MSGCLASS=F,
//           REGION=2048K
/*ROUTE PRINT RMT10
//*============================================================         00000300
//*     THIS ONE IS NOW IN PRODUCTION
//*     TMS BILL PRINT MICROFICHE FILE.
//*     THE FILE IS CREATED AND UPLOADED BY
//*     RICH ROBERTS/LUIS VALDEZ (TELECOM)
//*     (CHANGE JULIAN DATE OF M9910 FOR CORRECT MYYMM)
//*============================================================
//STEP1    EXEC PGM=IEBGENER,REGION=4096K
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=FISP.TELEFICH,DISP=(OLD,KEEP),
//            UNIT=SYSDA
//SYSUT2   DD DSN=FISP.TELEFICH.M9910,DISP=(NEW,KEEP),UNIT=VTAPE,
//            LABEL=(1,NL),
//*           DCB=(RECFM=F,LRECL=100),
//*************************************************************
//*       THE ABOVE DCB WILL PRODUCE A TAPE THAT IS UNBLOCKED.
//*************************************************************
//            DCB=(RECFM=FB,LRECL=100,BLKSIZE=1000),
//*************************************************************
//*       THE ABOVE DCB WILL PRODUCE A TAPE THAT IS BLOCKED(NORMAL)
//*************************************************************
//            DEN=3
//SYSIN    DD DUMMY
/*
//*=================================================
//*       TRANSMITTAL FOR MICROFICHE TAPE
//*=================================================
//XMIT010  EXEC PGM=XMITTAL
//SYSOUT    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//TAPECARD  DD DSN=*.STEP1.SYSUT2,
//             DISP=OLD,
//             DCB=*.STEP1.SYSUT2,
//             VOL=REF=*.STEP1.SYSUT2
//TRANSMIT  DD SYSOUT=C,
//             DEST=LOCAL
//MAILTO    DD DSN=FISP.PARMLIB(TELEMAIL),
//             DISP=SHR
//COMMENTS  DD DSN=FISP.PARMLIB(TELECOM),
//             DISP=SHR
//*
//*============================================================
//*  THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//*  ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE
//*
//*============= E N D   O F  J C L  TELEFICH  ================
