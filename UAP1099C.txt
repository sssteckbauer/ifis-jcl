//TEN99JB3 JOB (SYS000),'DISBRSMT',NOTIFY=APCDBM,                       00000100
//*            RESTART=MAILIT,
//*            RESTART=COPYPPSP,
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=20M             00000200
//*++++++++++++++++++++++++++                                           00000300
//*    ZEKE EVENT # 1203   'LIVE' FORMS active !!                       00000400
//*++++++++++++++++++++++++++                                           00000500
//* **N.T.E.**                                                          00000600
//*
//* NOTE! THIS JOB IS SETUP TO PRODUCE...                               00000700
//*       'LIVE' 1099 FORMS (THE DUMMY FORMS ARE IN JOB UAP1099B!)      00000900
//*                                                                     00001100
//*
//*** IRS 'TAPE' HAS BEEN CHANGED TO IRS 'FILE' - THE FILE
//*    IS NOW BEING ELECTRONICALLY SENT TO IRS THROUGH THE
//*    PC IN THE COMPUTER ROOM.
//*_____________________________________________________________
//*
//*Tax year 2019(processed in Jan 2020)
//*   (AS OF 11/2009 - IRS ACCEPTS ONLY FTP OF ONE FILE.THEY THEN
//*    ASSIGN THE FILE TO BOTH FEDERAL AND STATE).
//*_____________________________________________________________        00002000
//*  BFS FTP PROD STEP ENABLED !!                                       00002100
//*  LAST ARCHIVE STEP ENABLED !!                                       00002100
//*------------------------------------                                 00002200
//OUT1 OUTPUT CLASS=A,FORMS=P105,GROUPID=DSBRSMNT                       00002500
//OUT3 OUTPUT CLASS=G,FORMS=P105,GROUPID=BACKUP                         00002700
//OUT4 OUTPUT CLASS=G,FORMS=A002,GROUPID=DSBRSMNT                       00002800
//OUT5 OUTPUT CLASS=4,FORMS=A002,GROUPID=BACKUP1                        00002900
//*========================================================             00003000
//STEP0    EXEC PGM=IDCAMS,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))           00003100
//SYSPRINT  DD SYSOUT=*                                                 00003200
//SYSIN     DD *                                                        00003300
           DELETE FISP.UAP1099B.FORM1099
           DELETE FISP.UAP1099B.CNTRLRPT
           DELETE FISP.UAP1099B.AP9320
           DELETE FISP.UAP1099B.APX9066
           DELETE FISP.UAP1099B.APX9073
           DELETE FISP.UAP1099B.IRSFILE.D$TEN99DT
           DELETE FISP.UAP1099B.CATAPE.D$TEN99DT
           DELETE GAOP.FORM1099.TY$BSYEARPRIOR
           DELETE GAOT.TEST.FORM1099.TY$BSYEARPRIOR
/*                                                                      00003400
//*========================================================             00003500
//*                                                                     00003600
//STEP1    EXEC PGM=APTXRPTR,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))         00003700
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(APTXRPTR) PLAN(APTXRPTR)
/*
//SYSOUT    DD SYSOUT=(,),                                              00004800
//             OUTPUT=(*.OUT4,*.OUT5)                                   00004900
//SYSLST    DD SYSOUT=*                                                 00005000
//ABENDAID  DD SYSOUT=*                                                 00005100
//SYSUDUMP  DD SYSOUT=D,                                                00005200
//             DEST=LOCAL                                               00005300
//SYSDBOUT  DD SYSOUT=*                                                 00005600
//*================================================                     00005700
//* THE 'PPSP' FILE IS THE "FINAL" UPDATED VERSION FOR INPUT            00005800
//* TO THE 1099 REPORTING/PRINTING PROCESS.                             00005900
//*                                                                     00006000
//*    ** PPSP.A1099.COMB.APYTD.OUTPUT <----since UCOP conversion       00006100
//*----------------
//*  IF ROBIN UPDATES IFIS ADDRESS INFO WITHIN ONLINE AND ASKS TO
//*  HAVE THE CORRECTED INFO MERGED WITH THE ALREADY EXTRACTED PAYROLL
//*  INFO.... THEN ** DO NOT ** DO THIS!
//*
//*  ALREADY EXTRACTED PAYROLL FILE IS THE **FINAL FILE**
//*
//*  SHE MUST DO THE ADDRESS CORRECTIONS MANUALLY AFTER THE
//*  PAYROLL XX COMPUTE HAS RUN.
//*
//* ** THERE IS **NO WAY** TO UPDATE THE 1099'S AUTOMATICALLY
//* AFTER PAYROLL PROCESSES HAVE RUN.!!
//*
//*================================================                     00006300
//APYTDFL   DD DSN=PPSP.A1099.COMB.APYTD.OUTPUT,                        00006600
//             DISP=SHR                                                 00006500
//ADDRFILE  DD DSN=FISP.HOSP1099,                                       00006700
//*********************************************
//*  FISP.HOSP1099 IS OUTPUT FILE FROM UAPLOAD
//*********************************************
//            DISP=SHR,                                                 00006800
//            AMP=('BUFNI=2')                                           00006900
//FORM1099  DD DSN=&&FORM1099,
//             DISP=(NEW,PASS,DELETE),MGMTCLAS=MY,
//             DCB=(RECFM=FB,LRECL=133,BLKSIZE=23408),
//             SPACE=(TRK,(30,30),RLSE),
//             UNIT=SYSALLDA,
//             VOL=SER=APC002                                           00007800
//IRSTAPE   DD DSN=&&PRESORT,                                           00007300
//             DISP=(NEW,PASS,DELETE),MGMTCLAS=MY,                      00007400
//             DCB=(RECFM=FB,LRECL=751,BLKSIZE=23281),                  00007500
//             SPACE=(TRK,(2,10),RLSE),                                 00007600
//             UNIT=SYSALLDA,                                           00007700
//             VOL=SER=APC002                                           00007800
//CATAPE    DD DSN=&&PRESORT1,                                          00007300
//             DISP=(NEW,PASS,DELETE),MGMTCLAS=MY,                      00007400
//             DCB=(RECFM=FB,LRECL=751,BLKSIZE=23281),                  00007500
//             SPACE=(TRK,(2,10),RLSE),                                 00007600
//             UNIT=SYSALLDA,                                           00007700
//             VOL=SER=APC002                                           00007800
//CNTRLRPT  DD SYSOUT=(,),                                              00007900
//             OUTPUT=(*.OUT4,*.OUT5)                                   00008000
//AP9320    DD SYSOUT=(,),                                              00008100
//             OUTPUT=(*.OUT4,*.OUT5)                                   00008200
//APX9066   DD SYSOUT=(,),                                              00008300
//             OUTPUT=(*.OUT4,*.OUT5)                                   00008400
//APX9073   DD SYSOUT=(,),                                              00008500
//             OUTPUT=(*.OUT4,*.OUT5)                                   00008600
//SORTWK01  DD SPACE=(TRK,150),                                         00008700
//             UNIT=SYSDA                                               00008800
//SYSIN     DD *                                                        00008900
ALL*DATE $PAUL12H
/*                                                                      00009000
//* ORIGINAL JCL :  ALL*DATE 123111 (MMDDYY)(LAST DAY/TAX REPORTING YR)
//* OUTPUT REVIEW:  ALL*DATE $PAUL12H
//*==================================================
//* PRINT 1099 FORMS
//*==================================================
//STEP1A   EXEC PGM=IEBGENER
//SYSPRINT  DD SYSOUT=*
//SYSUT1    DD DSN=&&FORM1099,
//             DISP=OLD
//SYSUT2    DD SYSOUT=(,),
//             OUTPUT=(*.OUT1,*.OUT3)
//SYSIN     DD DUMMY
//*===================================================================  00011200
//*THIS STRIPS OFF THE FIRST CHARACTER (CARRIAGE CONTROL)               00011300
//*===================================================================  00011400
//STEP1B   EXEC PGM=SYNCSORT                                            00011500
//SYSOUT    DD SYSOUT=*                                                 00011600
//SORTIN    DD DSN=&&FORM1099,                                          00011700
//             DISP=(OLD,DELETE)                                        00011800
//SORTOUT   DD DSN=&&FILE1099,                                          00011900
//             DISP=(NEW,PASS,DELETE),MGMTCLAS=MY,                      00012000
//             DCB=(RECFM=FBA,BLKSIZE=23364,LRECL=132),                 00012100
//             SPACE=(TRK,(30,30)),                                     00012200
//             UNIT=SYSDA,                                              00012300
//             VOL=SER=APC002                                           00007800
//SYSPRINT  DD SYSOUT=*                                                 00012400
//SORTWK01  DD SPACE=(TRK,20),                                          00012500
//             UNIT=SYSDA                                               00012600
//SYSIN     DD *                                                        00012700
           SORT FIELDS=COPY
           OUTFIL OUTREC=(2,132)                                        00041100
/*                                                                      00009000
//*====================================================                 00009100
//*       SORT IRSFILE                                                  00009200
//*====================================================                 00009300
//STEP02   EXEC PGM=SORT                                                00009400
//SYSOUT    DD SYSOUT=*                                                 00009500
//SORTIN    DD DSN=&&PRESORT,                                           00009600
//             DISP=(OLD,DELETE)                                        00009700
//SORTOUT   DD DSN=&&POSTSORT,                                          00009800
//             DISP=(NEW,PASS),                                         00009900
//             DCB=(RECFM=FB,LRECL=751,BLKSIZE=23281),                  00010000
//             SPACE=(TRK,(450,75),RLSE),                               00010100
//             UNIT=SYSALLDA                                            00010200
//SORTWK01  DD SPACE=(TRK,(300,150)),                                   00010300
//             UNIT=SYSDA                                               00010400
//SORTWK02  DD SPACE=(TRK,(300,150)),                                   00010500
//             UNIT=SYSDA                                               00010600
//SORTWK03  DD SPACE=(TRK,(300,150)),                                   00010700
//             UNIT=SYSDA                                               00010800
//SORTWK04  DD SPACE=(TRK,(300,150)),                                   00010900
//             UNIT=SYSDA                                               00011000
//SYSIN     DD *                                                        00011100
   SORT FIELDS=(1,1,CH,A)
//*===================================================================  00011200
//*THIS STRIPS OFF THE FIRST CHARACTER (CARRIAGE CONTROL)               00011300
//*===================================================================  00011400
//STEP03   EXEC PGM=SYNCSORT                                            00011500
//SYSOUT    DD SYSOUT=*                                                 00011600
//SORTIN    DD DSN=&&POSTSORT,                                          00011700
//             DISP=(OLD,DELETE)                                        00011800
//SORTOUT   DD DSN=FISP.UAP1099B.IRSFILE.D$TEN99DT,                     00011900
//             DISP=(NEW,CATLG,DELETE),                                 00012000
//             DCB=(RECFM=FBA,BLKSIZE=23250,LRECL=750),                 00012100
//             SPACE=(TRK,(30,30)),                                     00012200
//             UNIT=SYSDA                                               00012300
//SYSPRINT  DD SYSOUT=*                                                 00012400
//SORTWK01  DD SPACE=(TRK,20),                                          00012500
//             UNIT=SYSDA                                               00012600
//SYSIN     DD *                                                        00012700
           SORT FIELDS=COPY
           OUTFIL OUTREC=(2,750)                                        00041100
/*                                                                      00009000
//*====================================================                 00009100
//*       SORT CATAPE                                                   00009200
//*====================================================                 00009300
//STEP04   EXEC PGM=SORT                                                00009400
//SYSOUT    DD SYSOUT=*                                                 00009500
//SORTIN    DD DSN=&&PRESORT1,                                          00009600
//             DISP=(OLD,DELETE)                                        00009700
//SORTOUT   DD DSN=&&POSTSRT1,                                          00009800
//             DISP=(NEW,PASS),                                         00009900
//             DCB=(RECFM=FB,LRECL=751,BLKSIZE=23281),                  00010000
//             SPACE=(TRK,(450,75),RLSE),                               00010100
//             UNIT=SYSALLDA                                            00010200
//SORTWK01  DD SPACE=(TRK,(300,150)),                                   00010300
//             UNIT=SYSDA                                               00010400
//SORTWK02  DD SPACE=(TRK,(300,150)),                                   00010500
//             UNIT=SYSDA                                               00010600
//SORTWK03  DD SPACE=(TRK,(300,150)),                                   00010700
//             UNIT=SYSDA                                               00010800
//SORTWK04  DD SPACE=(TRK,(300,150)),                                   00010900
//             UNIT=SYSDA                                               00011000
//SYSIN     DD *                                                        00011100
   SORT FIELDS=(1,1,CH,A)
//*===================================================================  00011200
//*THIS STRIPS OFF THE FIRST CHARACTER (CARRIAGE CONTROL)               00011300
//*===================================================================  00011400
//STEP05   EXEC PGM=SYNCSORT                                            00011500
//SYSOUT    DD SYSOUT=*                                                 00011600
//SORTIN    DD DSN=&&POSTSRT1,                                          00011700
//             DISP=(OLD,DELETE)                                        00011800
//SORTOUT   DD DSN=FISP.UAP1099B.CATAPE.D$TEN99DT,                      00011900
//             DISP=(NEW,CATLG,DELETE),                                 00012000
//             DCB=(RECFM=FBA,BLKSIZE=23250,LRECL=750),                 00012100
//             SPACE=(TRK,(30,30)),                                     00012200
//             UNIT=SYSDA                                               00012300
//SYSPRINT  DD SYSOUT=*                                                 00012400
//SORTWK01  DD SPACE=(TRK,20),                                          00012500
//             UNIT=SYSDA                                               00012600
//SYSIN     DD *                                                        00012700
           SORT FIELDS=COPY
           OUTFIL OUTREC=(2,750)                                        00041100
/*                                                                      00012800
//*============================================================
//* CREATE FILE FORM 1099 IMAGE FILE FOR BFS
//*============================================================
//STEP6    EXEC PGM=IEBGENER
//SYSPRINT  DD SYSOUT=*
//SYSUT1    DD DSN=&&FILE1099,
//             DISP=(OLD,DELETE)
//SYSUT2    DD DSN=GAOP.FORM1099.TY$BSYEARPRIOR,                        00007300
//             DISP=(NEW,CATLG,KEEP),MGMTCLAS=MY,                       00007400
//             DCB=(RECFM=FB,LRECL=132,BLKSIZE=23364),                  00007500
//             SPACE=(TRK,(30,30),RLSE),                                00007600
//             UNIT=SYSALLDA                                            00007700
//SYSIN     DD DUMMY
//*                                                                     00013000
//*====================================================                 00013200
//* MAKE A COPY OF THE PPSP FILE FOR 10 YEARS ARCHIVING                 00013300
//*====================================================                 00013400
//COPYPPSP EXEC PGM=IEBGENER                                            00013500
//SYSPRINT  DD SYSOUT=*                                                 00013600
//SYSUT1    DD DSN=PPSP.A1099.COMB.APYTD.OUTPUT,                        00013700
//             DISP=(OLD,KEEP)                                          00013800
//SYSUT2    DD DSN=FISP.A1099.COMB.APYTD.OUTPUT.D$TEN99DT,              00014000
//             DISP=(NEW,CATLG,DELETE),MGMTCLAS=MY,                     00014100
//             DCB=(LRECL=153,BLKSIZE=23409,RECFM=FB),                  00014200
//             SPACE=(TRK,(30,5),RLSE),                                 00014300
//             UNIT=SYSALLDA,                                           00014400
//             VOL=SER=APC002                                           00014500
//SYSIN     DD DUMMY                                                    00014600
/*                                                                      00014700
//*============================================================         00014800
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
BFSSYSTEMSFTP@AD.UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
BFS REMINDER:

The final '1099' file has been created:

"GAOP.FORM1099.TY$BSYEARPRIOR"

Please download the file to your system.

/*
//* CONTROL REPORT FILENAME FOLLOWS:
//SYSTSIN   DD *
 %MAILRPT -
 'FISP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('ANNUAL 1099 FILE CREATED')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE...
//*---
//*============================================================
//*   FTP TO BFS
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2008) REBUILD'
/*                                                                      00014800
//*============================================================         00014800
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00014900
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00015000
//*============================================================         00015100
//STOPZEKE EXEC STOPZEKE                                                00015200
//*                                                                     00015300
//*================ E N D  O F  J C L  TEN99JB3  =======                00015400
