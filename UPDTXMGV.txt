//UPDTXMGV JOB (SYS000),'PRDCTL',MSGCLASS=F,MSGLEVEL=(1,1),             00000100
//*====================================================                 00000200
//*       RESTART=STEP03,                                               00000300
//*====================================================                 00000400
//        NOTIFY=APCPSB,CLASS=K,REGION=0M                               00000500
//*====================================================                 00000600
//*  YOU ** MUST ** SUBMIT THIS JOB THRU ZEKE !                         00000700
//*++++++++++++++++++++++++++                                           00000800
//*====================================================                 00000900
//*++++++++++++++++++++++++++                                           00001000
//*    ZEKE EVENT # 249                                                 00001100
//*++++++++++++++++++++++++++                                           00001200
//*====================================================                 00001300
//*               CHECK RUN PHASE TWO                                   00001400
//*====================================================                 00001500
//*  CREATE TEMP RUNPARMS FILE                                          00001600
//*====================================================                 00001700
//STEP00   EXEC PGM=SORT                                                00001800
//SYSOUT    DD SYSOUT=*                                                 00001900
//SORTIN    DD *                                                        00002000
01 APCDBM   XMGV 01                     0010
/*                                                                      00002100
//*-------
//* COMMIT COUNTER (ABOVE) DROPPED TO '10' FROM '25' FOR PERIOD
//* WHEN PURCHASING IS CONTENDING WITH CHECK-WRITE.
//*-------
//SORTOUT   DD DSN=FISP.FILE.CHECKS.RUNUPDT,                            00002200
//             DISP=SHR                                                 00002300
//SYSIN     DD *                                                        00002400
   SORT FIELDS=COPY
/*                                                                      00002500
//*====================================================                 00002600
//*              DELETE CATALOGED DATA SETS                             00002700
//*====================================================                 00002800
//STEP01   EXEC PGM=IEFBR14,COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))          00002900
//FAPUK05O  DD DSN=FISP.FILE.UK05O.XMGV,                                00003000
//             DISP=(MOD,DELETE,DELETE),                                00003100
//             DCB=(RECFM=FB,LRECL=59,BLKSIZE=5900),                    00003200
//             SPACE=(TRK,(15,15),RLSE),                                00003300
//             UNIT=SYSDA                                               00003400
//EXTIFILE  DD DSN=FISP.FILE.EXTIFILE.XMGV,                             00003500
//             DISP=(MOD,DELETE,DELETE),                                00003600
//             DCB=(RECFM=FB,LRECL=64,BLKSIZE=6400),                    00003700
//             SPACE=(TRK,(15,15),RLSE),                                00003800
//             UNIT=SYSDA                                               00003900
//FAPUK07O  DD DSN=FISP.FILE.UK07O.XMGV,                                00004000
//             DISP=(MOD,DELETE,DELETE),                                00004100
//             DCB=(RECFM=FB,LRECL=285,BLKSIZE=5985),                   00004200
//             SPACE=(TRK,(15,15),RLSE),                                00004300
//             UNIT=SYSDA                                               00004400
/*                                                                      00004500
//*====================================================                 00004600
//*             CREATE FAPUK05 & EXTERNAL APPLICATION FILES             00004700
//*====================================================                 00004800
//*====================================================                 00005000
//STEP02   EXEC PGM=FAPXK05                                             00005100
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(FAPXK05) PLAN(FAPXK05)
/*
//SYSIN     DD *
 000001 ISISDICT
/*
//CONTROLR  DD DSN=FISP.APDXMGV.FAPXK05.CR.D$APDAT..T$FTIME,            00006100
//             DISP=(NEW,CATLG,KEEP),                                   00006200
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00006300
//             SPACE=(TRK,(2,2),RLSE),                                  00006400
//             UNIT=SYSDA                                               00006500
//SYSOUT    DD SYSOUT=*                                                 00006600
//SYSLST    DD SYSOUT=*                                                 00006700
//SYSUDUMP  DD SYSOUT=D,                                                00006800
//             DEST=LOCAL                                               00006900
//SYSDBOUT  DD SYSOUT=*                                                 00007000
//RUNPARMS  DD DSN=FISP.FILE.CHECKS.RUNUPDT,                            00007100
//             DISP=SHR                                                 00007200
//FAPDK04O  DD DSN=FISP.FILE.DK04O.XMGV,                                00007300
//             DISP=SHR                                                 00007400
//FAPUK05O  DD DSN=FISP.FILE.UK05O.XMGV,                                00007500
//             DISP=(NEW,CATLG,CATLG),                                  00007600
//             DCB=(RECFM=FB,LRECL=59,BLKSIZE=5900),                    00007700
//             SPACE=(TRK,(15,15),RLSE),                                00007800
//             UNIT=SYSDA                                               00007900
//EXTIFILE  DD DSN=FISP.FILE.EXTIFILE.XMGV,                             00008000
//             DISP=(NEW,CATLG,DELETE),                                 00008100
//             DCB=(RECFM=FB,LRECL=64,BLKSIZE=6400),                    00008200
//             SPACE=(TRK,(15,15),RLSE),                                00008300
//             UNIT=SYSDA                                               00008400
//RPTFILE   DD SYSOUT=*                                                 00008500
//*====================================================                 00008800
//*              UPDATE DATABASE                                        00008900
//*====================================================                 00009000
//STEP03   EXEC PGM=FAPUK05                                             00009100
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(FAPUK05) PLAN(FAPUK05)
/*
//SYSIN     DD *
 000001 ISISDICT
/*
//CONTROLR  DD DSN=FISP.APDXMGV.FAPUK05.CR.D$APDAT..T$FTIME,            00010000
//             DISP=(NEW,CATLG,KEEP),                                   00010100
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00010200
//             SPACE=(TRK,(2,2),RLSE),                                  00010300
//             UNIT=SYSDA                                               00010400
//SYSOUT    DD SYSOUT=*                                                 00010500
//SYSLST    DD SYSOUT=*                                                 00010600
//SYSUDUMP  DD SYSOUT=D,                                                00010700
//             DEST=LOCAL                                               00010800
//SYSDBOUT  DD SYSOUT=*                                                 00010900
//RUNPARMS  DD DSN=FISP.FILE.CHECKS.RUNUPDT,                            00011000
//             DISP=SHR                                                 00011100
//FAPDK04O  DD DSN=FISP.FILE.DK04O.XMGV,                                00011200
//             DISP=SHR                                                 00011300
//RPTFILE   DD SYSOUT=*                                                 00011400
//*                                                                     00011700
//*====================================================                 00011800
//*       APPROVED RECORDS GENERATOR                                    00011900
//*====================================================                 00012000
//STEP04   EXEC PGM=FAPUK06,COND=(0,NE)                                 00012100
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSPRINT DD  SYSOUT=*
//SYSTSPRT DD  SYSOUT=*
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(FAPUK06) PLAN(FAPUK06)
/*
//SYSIN     DD *
 000001 ISISDICT
/*
//CONTROLR  DD DSN=FISP.APDXMGV.FAPUK06.CR.D$APDAT..T$FTIME,            00013000
//             DISP=(NEW,CATLG,KEEP),                                   00013100
//             DCB=(RECFM=FBA,LRECL=133,BLKSIZE=23408),                 00013200
//             SPACE=(TRK,(2,2),RLSE),                                  00013300
//             UNIT=SYSDA                                               00013400
//SYSOUT    DD SYSOUT=*                                                 00013500
//SYSLST    DD SYSOUT=*                                                 00013600
//SYSUDUMP  DD SYSOUT=D,                                                00013700
//             DEST=LOCAL                                               00013800
//SYSDBOUT  DD SYSOUT=*                                                 00013900
//RUNPARMS  DD DSN=FISP.FILE.CHECKS.RUNUPDT,                            00014000
//             DISP=SHR                                                 00014100
//FAPUK05O  DD DSN=FISP.FILE.UK05O.XMGV,                                00014200
//             DISP=SHR                                                 00014300
//*============================================================
//* SETS ZEKE VARIABLE TO "GO". WATCHDOG FOR "WRAPPING-UP"
//* THE CHECKWRITE PROCESS AND RUNNING BANKFILE JOB
//*============================================================
//SETVAR   EXEC PGM=ZEKESET,COND=(0,NE)
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
  SET VAR $APDXMGV EQ 'GO'
/*
//*
//*============================================================
//ZEKESET  EXEC PGM=ZEKESET
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
  SET SCOM 'ZADD EV (3710) REBUILD'
/*
//*============================================================         00014600
//*   SUBMITS THE XMGV ALLOCATION JOB FOR NEXT FTP OF FILE
//* (AUTOMATICALLY ALLOCATING AT 11AM ON SUNDAY FOR MONDAY PROCESSING)
//*============================================================         00014600
//*SUBALOC EXEC PGM=ZEKESET,
//*             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*SYSPRINT  DD SYSOUT=*
//*SYSIN     DD *
//* SET SCOM 'ZADD EV (1000) REBUILD'
//*/*
//*============================================================         00014600
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00014700
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00014800
//*============================================================         00014900
//STOPZEKE EXEC STOPZEKE                                                00015000
//*                                                                     00015100
//*================ E N D  O F  J C L  UPDTXMGV ========                00015200
