//UPDVDRYR JOB (SYS000),'PRDCTL',
//            MSGLEVEL=(1,1),
//            MSGCLASS=F,
//            CLASS=K,
//            REGION=0M,
//            NOTIFY=APCDBM
//*=============================
//* ZEKE EVENT # 1190
//* THIS JOB MUST BE RUN ON DEC 31 OR JAN 01 EVERY YEAR
//*=============================
//**********************************************************************
//*  THIS SQL INITIALIZES ALL THRESHOLD AMOUNTS ON
//*  ALL VENDOR RECORDS FOR THE NEW YEAR
//**********************************************************************
//* SUBMIT DB2 UPDATE SQL IN BATCH
//* NOTE:  BATCH-SUBMITTED SQL CANNOT CONTAIN COMMENTS (--)
//*        AND MUST END WITH SEMICOLON (;)
//**********************************************************************
//STEP010  EXEC PGM=IKJEFT01,DYNAMNBR=20,REGION=1M
//STEPLIB  DD DSN=DB2F.DSNEXIT,DISP=SHR
//         DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSPRT DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSTSIN  DD *
 DSN SYSTEM (DB2F)
 RUN PROGRAM(DSNTIAD) PLAN(DSNTIAD) LIB('DB2F.RUNLIB.LOAD')
 END
/*
//SYSIN    DD DSN=FISP.SQLLIB(UPDVDRYR),DISP=SHR
//         DD DSN=FISP.SQLLIB(SEMICOLN),DISP=SHR
//*
//*===========================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN "UNACCEPTABLE" CONDITION CODE.
//*===========================================================
//STOPZEKE EXEC STOPZEKE
//*
//**************************** END OF JCL UPDVDRYR ****************
