//UPEU910O JOB (SYS000),'PRDCTL',
//            MSGCLASS=F,
//            MSGLEVEL=(1,1),
//            CLASS=K,
//            REGION=0M,
//            NOTIFY=APCDBM
//***************************************
//*     ZEKE EV # 46
//***************************************
//********************************************************************* 00003100
//*   SORT THE INCOMING EMAIL FILE                                      00003000
//********************************************************************* 00003100
//SORT01   EXEC PGM=SYNCSORT                                            00003203
//SORTIN    DD DSN=SISP.INCOMING.EMAIL.DATA,DISP=(SHR,PASS)             00003300
//SORTOUT   DD DSN=&&EMAIL,                                             00003400
//             DISP=(NEW,PASS,DELETE),                                  00003500
//             DCB=(RECFM=FB,LRECL=80,BLKSIZE=23440),                   00003600
//             SPACE=(TRK,(15,15),RLSE),                                00003700
//             UNIT=SYSDA                                               00003800
//SYSPRINT  DD SYSOUT=*                                                 00003900
//SORTWK01  DD SPACE=(TRK,(30,30),RLSE),                                00004000
//             UNIT=SYSDA                                               00004100
//SORTWK02  DD SPACE=(TRK,(30,30),RLSE),                                00004200
//             UNIT=SYSDA                                               00004300
//SORTWK03  DD SPACE=(TRK,(30,30),RLSE),                                00004400
//             UNIT=SYSDA                                               00004500
//SYSOUT    DD SYSOUT=*                                                 00004700
//SYSIN     DD *                                                        00004602
 SORT FIELDS=COPY
 INCLUDE COND=(1,1,CH,NE,C' ',AND,                 EMPLOYEE ID
              34,1,CH,NE,C' ',AND,                 NETWORK USERNAME
              45,1,CH,EQ,C' ')                     EMAIL ADDRESS
//*                                                                     00004801
//********************************************************************* 00004900
//*   RUN UGEU910 PROGRAM - UPDATE                                      00005000
//********************************************************************* 00005100
//UPDT010  EXEC PGM=UPEU910,COND=(0,NE)                                 00005200
//STEPLIB   DD DSN=FISP.FIS.LOADLIB,DISP=SHR
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR
//          DD DSN=DB2F.DSNEXIT,DISP=SHR
//          DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSIN  DD  *
 DSN SYSTEM(DB2F)
 RUN PROGRAM(UPEU910) PLAN(UPEU910)
/*
//EMAILS    DD DSN=&&EMAIL,DISP=(OLD,DELETE)
//CNTLRPT   DD SYSOUT=*
//RPT1      DD SYSOUT=*
//SYSOUT    DD SYSOUT=*
//SYSPRINT  DD SYSOUT=*
//SYSTSPRT  DD SYSOUT=*
//SYSLST    DD SYSOUT=*
//SYSUDUMP  DD SYSOUT=*
//*
//**********************************************************************
//* THIS STEP WILL UPDATE EMPLYEE IND TO 'N' IN TRVL IF THE PERSON NEVER
//* BEEN AN EMPLOYEE
//**********************************************************************
//UPDTTVL1 EXEC PGM=IKJEFT01,DYNAMNBR=20,REGION=1M
//STEPLIB  DD DSN=DB2F.DSNEXIT,DISP=SHR
//         DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSPRT DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSTSIN  DD *
 DSN SYSTEM (DB2F)
 RUN PROGRAM(DSNTIAD) PLAN(DSNTIAD) LIB('DB2F.RUNLIB.LOAD')
 END
/*
//SYSIN    DD DSN=FISP.SQLLIB(UPDTVL1),DISP=SHR
//         DD DSN=FISP.SQLLIB(SEMICOLN),DISP=SHR
//*
//*
//**********************************************************************
//* THIS STEP WILL UPDATE EMPLYEE IND TO 'E' IN TRVL IF THE PERSON IS AN
//* ACTIVE EMPLOYEE
//**********************************************************************
//UPDTTVL2 EXEC PGM=IKJEFT01,DYNAMNBR=20,REGION=1M
//STEPLIB  DD DSN=DB2F.DSNEXIT,DISP=SHR
//         DD DSN=DB2F.DSNLOAD,DISP=SHR
//SYSTSPRT DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSTSIN  DD *
 DSN SYSTEM (DB2F)
 RUN PROGRAM(DSNTIAD) PLAN(DSNTIAD) LIB('DB2F.RUNLIB.LOAD')
 END
/*
//SYSIN    DD DSN=FISP.SQLLIB(UPDTVL2),DISP=SHR
//         DD DSN=FISP.SQLLIB(SEMICOLN),DISP=SHR
//*
//*###############  E N D  O F  P R O C  GPZ700P  ###################   00011300
