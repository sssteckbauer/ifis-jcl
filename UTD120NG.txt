//UTD120NG JOB (DEV000),'DEFINE GDG',
//             CLASS=K,
//             MSGLEVEL=(1,1),
//             MSGCLASS=F,
//             NOTIFY=APCDBM
//*
//*
//**********************************************************************
//** DEFINE NEW GDG BASE
//**********************************************************************
//REPRO01  EXEC PGM=IDCAMS
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD *
 DEFINE GDG(NAME(FISP.UTD120N.INPUT.UCPATH.I303.SAVE) -
            LIMIT(18) -
            NOEMPTY -
            SCRATCH)
/*
//*
//*
