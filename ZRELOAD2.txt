//ZRELOAD2 JOB (ACCT),'PRODUCTION CONTROL',NOTIFY=APCDBM,               00010000
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=2048K           00021000
//OUT1 OUTPUT CLASS=F,FORMS=P011,JESDS=ALL,DEFAULT=YES,GROUPID=BUDGET   00030000
//*===============================================================      00040000
//*  THIS JOB RESTORES A FILE FROM TAPE TO DISK                         00050000
//*===============================================================      00060000
//STEP1    EXEC PGM=SYNCSORT                                            00070000
//SORTIN    DD DSN=FISP.APM920A.EXTRACTS.R121196,                       00080000
//             DISP=OLD,                                                00090000
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00100000
//             LABEL=(1,SL),                                            00110000
//             UNIT=VTAPE,                                              00120005
//*            UNIT=CTAPE,                                              00121002
//             VOL=SER=001890                                           00130000
//SORTOUT   DD DSN=FISP.APM920A.UAPX920.EXTRAC01.R121196,               00140000
//             DISP=(NEW,KEEP,KEEP),                                    00150000
//             DCB=(RECFM=FB,LRECL=330,BLKSIZE=23430),                  00160000
//             SPACE=(TRK,(750,750),RLSE),                              00170000
//             STORCLAS=NONSMS,                                         00171000
//             VOL=SER=FAR012,                                          00172000
//             UNIT=SYSALLDA                                            00180000
//SYSIN     DD DSN=FISP.PARMLIB(SORTCOPY),                              00190000
//             DISP=SHR                                                 00200000
//SYSOUT    DD SYSOUT=*                                                 00210000
//SYSPRINT  DD SYSOUT=*                                                 00220000
//*                                                                     00230000
//*================ E N D  O F  J C L  ZRELOAD2 ========                00240000
